var host = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var attribution = '';
var zoomLvl = 13
var startLat = 48
var startLong = 15
var id = 'local5-6fee3af1'
var siteId = 'socialmaps2-b136b545'
var mapViews = {0: 3,1:6,2:9,3:12,4:15};
var startview = 0;
var isTabActive; //Define is current tab active, we dont wanna allow several tabs to feed data


/*
 * Layer groups
 */
var cityMarkers = L.layerGroup();
var avatarMarkers = L.layerGroup();
var mainUserMarker = L.layerGroup();
var otherAvatarsMarkers = L.layerGroup();
var otherAvatarsPostPopupMarkers = L.layerGroup();
var personalMarkers = L.layerGroup();
var avatarActionMarkers = L.layerGroup();
var locationMarkers = L.layerGroup();
var beaconMarkers = L.layerGroup();
var settingsMarkers = L.layerGroup();
var testLayers = L.layerGroup();
var alllocations = L.layerGroup();
var mukilayers = L.layerGroup();
/*
 * 
 */

if(user) {
    var reachRadius = user.reach
    var privacyRadius = user.radius
}

var siteUrl = document.location.origin

if(siteUrl == 'http://socialmaps.live'){
	id = siteId
}

