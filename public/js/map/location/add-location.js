
var map;
var marker;
var host = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var attribution = '';
var zoomLvl = 13



$( document ).ready(function() {
    
    var startLat = 0;
    var startLong = 0;

    lmap = L.map('location-map').setView([0, 0], 2);

    L.tileLayer(host).addTo(lmap);
    lmap.attributionControl.setPrefix(false);
    
    var marker = new L.marker([0,0], {
        draggable: 'true'
    });
    
    marker.on('dragend', function(event) {
        var position = marker.getLatLng();
        
        $('#location-lat').val(position.lat);
        $('#location-long').val(position.lng);
        marker.setLatLng(position, {
          draggable: 'true'
        }).update();
        
        
        
    });
   
    lmap.addLayer(marker);
    
    
});