otherAvatarsEnabled = other_avatars_layer.active;

setTimeout(() => {
    getUsers();
}, 5000) // 5 seconds to prevent users constantly refreshin page in order to get data faster

function getUsers() {
    if (!otherAvatarsEnabled || !friendsAvatarsEnabled) {
        return;
    }

    let max_icons = other_avatars_layer.max_icons;
    let pages = other_avatars_layer.cached_pages;
    let limiter = max_icons * pages;

    axios.get(`api/get-avatars-data/${limiter}`, {
        params: {
            user_id: user.id,
            lat: user.displaylocation.coordinates[1],
            lng: user.displaylocation.coordinates[0],
            radius: user.reach
        }
    }).then(res => {
        let otherAvatars = res.data.users;
        let interval = Math.ceil(otherAvatars.length / pages);

        let start = 0;
        let end = other_avatars_layer.max_icons;

        if (initialLoad) {
            printUsers(otherAvatars.slice(start, end), other_avatars_layer);
            document.getElementById('usersIndicator').innerText = otherAvatars.length;
            start += interval;
            end += interval;

            otherAvatarsMarkers.addTo(map);

            printUsersInterval(otherAvatars, interval, start, end);
        } else {
            printUsersInterval(otherAvatars, interval, start, end);
        }
    })
}

function printUsersInterval(otherAvatars, interval, start, end) {
    let otherAvatarsInterval = setInterval(() => {
        if (!otherAvatarsEnabled || !friendsAvatarsEnabled) {
            clearInterval(otherAvatarsInterval);
            return;
        }

        clearUserLayers();
        printUsers(otherAvatars.slice(start, end), other_avatars_layer);
        document.getElementById('usersIndicator').innerText = otherAvatars.length;
        start += interval;
        end += interval;

        addToMap(otherAvatarsMarkers);

        if (otherAvatars.length <= end) {
            clearInterval(otherAvatarsInterval);
            getUsers();
        }
    }, (other_avatars_layer.display_duration * 1000))
}