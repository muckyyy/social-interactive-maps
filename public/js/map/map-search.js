let searchLocationsLimiter = 0;
let alowSearchLocations = true;

function searchData(e, option) {
    let bounds = map.getBounds();
    let zoom = map.getZoom();

    $('#input-close-button').css("visibility", "visible");
    $("#input-close-button").click(function () {
        document.getElementById("searchCitiesInput").value = "";
        $('#search-navigation-toggle').hide();
        $('#left-menu-section-toggle').fadeIn(500);
    });

    var formText = $.trim($(e.target).val());
    var formTextLength = formText.length;

    var getEnteredText = document.getElementById("searchCitiesInput").value.length;

    if (window.location.pathname == "/") {
        $('#search-other-data-button').fadeIn();
    }

    if (getEnteredText < 1) {
        $('#search-other-data-button').fadeOut();
        $('#location-profile-show').fadeOut();
    } else {
        $('#search-other-data-button').fadeIn();
        $('#location-profile-show').fadeIn();
    }

    if (!alowSearchLocations && getEnteredText > 0) {
        return;
    }

    if (option == 'location-profile') {
        increaseSearchLocationsBuffer();
        axios.get(`/api/search-locations-profile`, {
            params: {
                keyword: e.target.value,
                bounds: bounds
            }
        }).then(res => {
            locations = res.data;

            clearLocationLayers();
           
            // print locations
           
            $('#location-profile-show').html('');
            $('#location-profile-show').append(`<div class="col-md-12" style="font-weight:bold; padding-left: 30px; font-size:1.25em;">Search map</div>`)
            
            if(!locations.length) {
                $('#location-profile-show').append(`<div class="col-md-12 mt-4" style="padding-left: 30px; font-size:1.25em;">No results found</div>`)
                return;
            }

            locations.forEach(location => {
                let locationMarker;
                locationMarker = L.marker([location.lat, location.lng],
                    {
                        locationId: location.id,
                        icon: L.AwesomeMarkers.icon({
                            icon: location.icon_class,
                            prefix: location.icon_prefix || 'fa',
                            markerColor: location.icon_color || 'blue',
                        })
                    }).on('click', function () {
                        showLocationInfo(location);
                    })

                locationMarkers.addLayer(locationMarker);

                locationMarkers.addTo(map).on('click')

                $('#location-profile-show').removeClass('d-none');
                let locationSearchResult;
                if (location.rating >= 4) {
                    locationSearchResult =
                        `<div class="notification-section-top-right col-xs-2 no-padding" style="margin-top: -3px;">
                            <span class="search-thumbnail-rating" style="background-color:#59A205; color: #fff; padding: 5px; border-radius: 2px;">${location.rating}.0
                            </span>
                        </div>`;
                }
                else if (location.rating >= 3 && location.rating <= 3.9) {
                    locationSearchResult =
                        `<div class="notification-section-top-right col-xs-2 no-padding" style="margin-top: -3px;">
                            <span class="search-thumbnail-rating" style="background-color:#74D504; color: #fff; padding: 5px; border-radius: 2px;">${location.rating}.0
                            </span>
                        </div>`;
                }
                else if (location.rating < 3) {
                    locationSearchResult =
                        `<div class="notification-section-top-right col-xs-2 no-padding" style="margin-top: -3px;">
                            <span class="search-thumbnail-rating" style="background-color:orange; color: #fff; padding: 5px; border-radius: 2px;">${location.rating}.0
                            </span>
                        </div>`;
                } else {
                    locationSearchResult = ``;
                }

                let maxLengthForLocationName = 15;
                let locationFullName = location.name;
                if ($.trim(location.name).length >= maxLengthForLocationName) {
                     location.name = location.name.substring(0, maxLengthForLocationName) + "...";
                }

                let maxLengthForAddressName = 18;
                let addressFullName = location.address;
                if ($.trim(location.address).length >= maxLengthForAddressName) {
                    location.address = location.address.substring(0, maxLengthForAddressName) + "...";
                }

                let maxLengthForWebsiteName = 20;
                let websiteFullName = location.website;
                if ($.trim(location.website).length >= maxLengthForWebsiteName) {
                    location.website = location.website.substring(0, maxLengthForLocationName) + "...";
                }
                if (formTextLength > 0) {
                    $('#left-menu-section-toggle').hide();
                    $('#search-navigation-toggle').fadeIn(500);
                    $('#location-profile-show').append(
                        `
                            <div class="search-thumbnail notification-section-li col-xs-12">
                                <div class="notification-section-top-left col-xs-10 no-padding">
                                    <a href="location/${location.id}">
                                        <img class="notification-section-top-img" src="${location.icon}" style="border-radius: 5px; height: 50px;">
                                    </a>
                                    <div class="notification-section-top-left-text" style="line-height: 20px; margin-top: -4px;">
                                        <a onmouseover='showLocationOnMap(${location.id})' href="location/${location.id}" data-toggle="tooltip" title="${locationFullName}" style="text-decoration: none; color: #0080a0;">
                                            ${location.name}
                                        </a> 
                                        <br>
                                        <small style="font-weight: 400; data-toggle="tooltip" title="${addressFullName}">${location.address}</small>
                                        <br>
                                        <small class="add-hover-cursor" style="color:#0080a0; font-weight: 400;" data-toggle="tooltip" title="${websiteFullName}">${location.website}</small>
                                    </div>
                                </div>
                                ${locationSearchResult}
                                <div class="notification-section-bottom col-xs-12 no-padding" style="padding-top: 5px;">
                                    <p style="text-align: justify; word-break: break-all; font-weight: 350; font-size: 0.9em; line-height: 15px;">${location.description}
                                    </p>
                                </div>
                            </div>
                            `
                    )
                } 
                else {
                    $('#search-navigation-toggle').hide();
                    $('#left-menu-section-toggle').fadeIn(500);
                    $('#input-close-button').css("visibility", "hidden");
                }
            });
        })
    }
}

function increaseSearchLocationsBuffer() {
    alowSearchLocations = false;
    let bufferInterval = setInterval(() => {
        if (searchLocationsLimiter >= 1) {
            searchLocationsLimiter = 0;
            alowSearchLocations = true;
            return clearInterval(bufferInterval);
        }
        searchLocationsLimiter += 1;
    }, 500)
}
