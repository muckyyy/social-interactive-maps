var defaultPath = '/css/leaflet/images/'
var iconWidth = 35
var iconHeight = 57

var myAvatar = L.icon({
    iconUrl: defaultPath + 'avatar2.png',
    iconSize:     [iconWidth, iconHeight], // size of the icon
    popupAnchor:  [0, -25] // point from which the popup should open relative to the iconAnchor
});


var friendAvatar = L.icon({
    iconUrl: defaultPath + 'friend.png',
    iconSize:     [iconWidth, iconHeight], // size of the icon
    popupAnchor:  [0, -25] // point from which the popup should open relative to the iconAnchor
});

var nonFriendAvatar = L.icon({
    iconUrl: defaultPath + 'other.png',
    iconSize:     [iconWidth, iconHeight], // size of the icon
    popupAnchor:  [0, -25] // point from which the popup should open relative to the iconAnchor
});

var shopAvatar = L.icon({
    iconUrl: defaultPath + 'shop.png',
    iconSize:     [iconWidth, iconHeight], // size of the icon
    popupAnchor:  [0, -25] // point from which the popup should open relative to the iconAnchor
});