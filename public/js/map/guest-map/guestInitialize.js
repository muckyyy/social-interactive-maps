var map;
var marker;

function initMap() {

  map = L.map('map').setView([startLat, startLong], zoomLvl)

    L.tileLayer(host, {
      id: id,
      attribution: attribution,
	  minZoom: minZoom,
	  maxZoom: maxZoom,
	  zoomDelta: zoomDelta
    }).addTo(map);
  map.attributionControl.setPrefix(false);
}

initMap();

$('.leaflet-top').hide()