/*
map.on('zoomend', function() {
	console.log(zoomLvl)
    zoomLvl += 5
    if(zoomLvl >= 15){
    	zoomLvl = 15
    }
    map.setZoom(zoomLvl);
});
*/
/*
map.on('zoomend', function(e) {

	console.log('zoomLvl', zoomLvl)
	console.log('e zoom', e.target._zoom)

	//Map zoomed in +
	if(zoomLvl <= e.target._zoom) {
		zoomLvl = 11
		e.target._zoom = 11
		map.setView([e.target._lastCenter.lat, e.target._lastCenter.lng], zoomLvl);
	} else {
		//Map zoomed out -
		zoomLvl = 5
		e.target._zoom = 5
		map.setView([e.target._lastCenter.lat, e.target._lastCenter.lng], zoomLvl);
	}
});
*/

zoomLvl = 3

console.log('map views', mapViews)
var currentMapViewLvl = 2

var constUserLat
var constUserLong

map.addEventListener('mousemove', function(ev) {
   constUserLat = ev.latlng.lat;
   constUserLong = ev.latlng.lng;
});


$('#map').on('wheel', function(event){

  // deltaY obviously records vertical scroll, deltaX and deltaZ exist too
  if(event.originalEvent.deltaY < 0){
    // wheeled up
    console.log("up")
    currentMapViewLvl++
    if(currentMapViewLvl > 4){
    	currentMapViewLvl = 4
    }

    console.log('up num', currentMapViewLvl)
    console.log('up zoom', mapViews[currentMapViewLvl])
    map.setView([constUserLat, constUserLong], mapViews[currentMapViewLvl]);
  }
  else {
    // wheeled down
    console.log('down')
    currentMapViewLvl--
    if(currentMapViewLvl < 0){
    	currentMapViewLvl = 0
    }

    console.log('down num', currentMapViewLvl)
    console.log('down zoom', mapViews[currentMapViewLvl])
    map.setView([constUserLat, constUserLong], mapViews[currentMapViewLvl]);
  }
});


//test location
var testShop = {
	name: "Test Shop",
	description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
	open: '09:00 - 17:00',
	street: 'Test street 77',
	phone: '033-123-543',
	website: 'test-shop.com',
	reviews: '5'
}

var testLocation = L.marker([50.1509, 8.6821], {icon: shopAvatar}).addTo(map).on('click', function(e) {
	loadShop(testShop, e)
})

var testLocation2 = L.marker([50.0909, 8.6821], {icon: shopAvatar}).addTo(map).on('click', function(e) {
	loadShop(testShop, e)
})

var testLocation3 = L.marker([50.1109, 8.721], {icon: shopAvatar}).addTo(map).on('click', function(e) {
	loadShop(testShop, e)
})

function loadShop(shop, e) {
	console.log(e)
	map.setView([e.latlng.lat, e.latlng.lng], 13);
	$('.right-menu-section').fadeIn(500)
}