axios.get('/api/main-avatar-data').then(res => {
    let u = res.data.user;

    let sizeMultiply = 1.2;
    let avatarColor = 'light';
    let markerIcon;

    if (u.profile_img && u.map_profile_display) {
        newUserIcon = new L.icon({
            iconUrl: '/uploads/' + u.profile_img,
            iconSize: [settings.icon_size_x * sizeMultiply, settings.icon_size_y * sizeMultiply],
            className: "avatar-map-image",
        });

        markerIcon = newUserIcon;
    } else {
        newUserIcon = new L.icon({
            iconUrl: '/img/icons/avatar_walk_' + avatarColor + '_blue.gif',
            iconSize: [settings.icon_size_x * sizeMultiply, settings.icon_size_y * sizeMultiply],
            className: "avatar_icon",
        });
    }

    let popupAction = `<div class='popupActions'>
                                    <a class='far fa-hand-point-right btn btn-primary'></a>
                                    <a class="far fa-comment btn btn-primary"></a>
                                </div>`;

    let onlineStatus = u.lastSeen;
    let userStatus;

    if (u.post) {
        userStatus = u.post.text;
        newUserIcon.options.className = newUserIcon.options.className + ' customAvatar';
    }
    else {
        userStatus = '';
    }

    let profileImg;

    if (u.profile_img) {
        profileImg = `./uploads/${u.profile_img}`;
    } else {
        profileImg = '/img/no-image.png';
    }

    let popupContent = `<div class='userPopupContent'>
                                    <div class='d-flex align-items-center mb-2'>
                                        <a onclick='showUserDetails(${u.id})'>
                                            <img class='statusPopupImg' src='${profileImg}'>
                                        </a>
                                        <div class='d-flex flex-direction-column'>
                                            <a onclick='showUserDetails(${u.id})' class='ml-2 truncate' style='color:#000'>
                                                ${u.full_name}
                                            </a>
                                            ${onlineStatus}
                                        </div>    
                                        </div>
                                    ${userStatus}  
                                    ${popupAction} 
                                </div>`;

    popup = L.popup({ offset: [0, -35] }).setContent(popupContent);

    avatar = L.marker(
        [
            u.lat, u.lng
        ],
        {
            icon: newUserIcon,
            userId: u.id,
            addedAt: Date.now(),
            customIcon: u.map_profile_display,
            zIndexOffset: 1000
        }).bindPopup(
            popup,
            {
                closeOnClick: true,
                autoClose: false
            }).on('click', openUserPopup);

    avatar.on('mouseover', function (ev) {
        hoverAvatar(ev);
    });
   
    mainUserMarker.addLayer(avatar);
    mainUserMarker.addTo(map);
   
    if (/* initialLoad && */ !markerIcon) {
        walkInterval(u, avatar, markerIcon, other_avatars_layer);
    } 
})