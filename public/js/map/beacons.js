let beacons;
let userCurrentBeacon;
let activeBeaconRange;
let beaconLatLng;
let userLatLng;
let beaconMarker;

function printBeacons(beacons) {
    beacons.forEach((beacon) => {
        let newBeaconIcon = new L.icon({
            iconUrl: beacon.icon,
            iconSize: [beacon.icon_size_x, beacon.icon_size_y],
            iconAnchor: [beacon.icon_ancor_point_x, beacon.icon_ancor_point_y],
            popupAnchor: [0, -60]
        });

        beaconMarker = L.marker([beacon.lat, beacon.lng], { icon: newBeaconIcon, beaconId: beacon.id }).on('click', showBeaconDetails);
        beaconMarkers.addLayer(beaconMarker);
        getUserActiveBeacon(beacon);
    })
    beaconMarkers.addTo(map).on('click')
}

function getUserActiveBeacon(beacon) {
    if (user.city == beacon.city_id) {
        activeBeaconRange = beacon.beacon_range;
        beaconLatLng = new L.LatLng(beacon.lat, beacon.lng);
        userLatLng = new L.LatLng(user.currentlocation.coordinates[1], user.currentlocation.coordinates[0]);
    }
}

function showBeaconDetails(e) {
    removeSelectedAvatar();

    if (beaconClicked && e.target.options.beaconId != activeBeacon.options.beaconId) {
        pulsateRadius('down');
        iconInterval('down', true, e);
        $('.beacon-info-section').fadeIn(500)
        $('.map-beacon').fadeOut();
        beaconClicked = false;
        return;
    }
    activeBeacon = e.target;
    let data = e.target;
    let id = e.target.options.beaconId;
    axios.get('/get-beacon-data/' + id).then(res => {
        let beacon = res.data.beacon;
        let coords = beacon.geolocation.coordinates;

        if (beaconLoading == true) return;
        beaconLoading = true;
        if (beaconClicked == false) {
            let x = beacon.icon_size_x;
            let y = beacon.icon_size_y;

            let currentRadius = 100;
            beaconRadius = L.circle([coords[1], coords[0]], { radius: currentRadius, color: 'rgba(51,136,255, 1)', opacity: 1 }).addTo(map);

            pulsateRadius('up', beacon.administrative_range);
            iconInterval('up');
            $('.beacon-info-section').fadeOut()
            $('.map-beacon').fadeIn(500);
            beaconClicked = true;

        } else {
            setTimeout(() => {
                opacity = 1;
            }, 1000)
            pulsateRadius('down');
            iconInterval('down');
            $('.beacon-info-section').fadeIn(500)
            $('.map-beacon').fadeOut();
            beaconClicked = false;
        }
    })
}

function clearBeaconsLayers() {
    beaconMarkers.clearLayers();
}