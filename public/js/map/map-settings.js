let beaconsEnabled, locationsEnabled, otherAvatarsEnabled, friendsAvatarsEnabled;

function toggleLocations(action) {
    if (action == 'active') {
        locationsEnabled = true;

        let bounds = map.getBounds();
        let zoom = map.getZoom();
        getLocations(bounds, zoom);
    } else {
        locationsEnabled = false;
        clearLocationLayers();
    }
}

function toggleBeacons(action) {
    if (action == 'active') {
        beaconsEnabled = true;

        let bounds = map.getBounds();
        let zoom = map.getZoom();
        getBeacons(bounds, zoom);

    } else {
        beaconsEnabled = false;
        clearBeaconsLayers();

    }
}

function toggleFriends(action) {
    if (action == 'active') {
        friendsAvatarsEnabled = true;

        let bounds = map.getBounds();
        let zoom = map.getZoom();
        getUsers(bounds, zoom);
    } else {
        friendsAvatarsEnabled = false;
        clearUserLayers();
    }
}

function toggleOtherAvatars(action) {
    if (action == 'active') {
        otherAvatarsEnabled = true;

        let bounds = map.getBounds();
        let zoom = map.getZoom();
        getUsers(bounds, zoom);
    } else {
        otherAvatarsEnabled = false;
        clearUserLayers();
    }
}