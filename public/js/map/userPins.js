var userPin = []
var constUserLat
var constUserLong
var allowMovingPin = false
var pins
var friends
var messages = []

//test location
var testShop = {
	name: "Test Shop",
	description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
	open: '09:00 - 17:00',
	street: 'Test street 77',
	phone: '033-123-543',
	website: 'test-shop.com',
	reviews: '5'
}
var testLocation = L.marker([43.83, 18.37], {icon: shopAvatar}).addTo(map).on('click', function(e) {
	loadShop(testShop)
})

function loadShop(shop) {
	console.log(shop)
	$('.right-menu-section').fadeIn(500)
}




//render user
user.marker = L.marker([user.lat, user.long], {icon: myAvatar}).addTo(map);
if(user.last_message != null) {
	user.marker.bindPopup("<div class='popout-messages'>"+ user.last_message +"</div>", {closeOnClick: false, autoClose: false})
}
//render user radius
userReachRadius = L.circle([user.lat, user.long], {color: '#247BA0', fillOpacity: 0.3, stroke: false, radius: reachRadius}).addTo(map);
userReachPrivacy = L.circle([user.lat, user.long], {color: '#F25F5C', fillOpacity: 0.3, stroke: false, radius: privacyRadius}).addTo(map);

//Get friends
$.ajax
({
	type: "GET",
	url: "/get-user-friends",
		success: function(data)
		{
			
			messages = data.friendsMessages
			console.log('data', data)

			let htmlContent = '';

			data.friendsMessages.forEach(function(e) {
				htmlContent += '<div class="left-menu-li">' + e.description
				htmlContent += '<div class="left-menu-li-name"> By ' + e.email + '<div class="left-menu-li-date">' + formatDate(e.updated_at) + '</div></div>';
				htmlContent += '</div>';
			})

			$('.left-menu-ul').append(htmlContent)

			//render friends
			data.friends.forEach(function(e) {

				friends = L.marker([e.lat, e.long], {icon: friendAvatar}).addTo(map);
				let htmlFriendsPopup = ''
				
				if(e.last_message != null) {
					htmlFriendsPopup += "<div class='popout-messages'>"+ e.last_message +"</div>"
				}
				htmlFriendsPopup += "<a href='/global-profile/"+ e.id +"'><div class='popout-profile-img'><i class='fas fa-user-circle'></i></div></a>"
				friends.bindPopup(htmlFriendsPopup, {closeOnClick: false, autoClose: false}).openPopup();
			})


			//render non friends
			data.nonFriends.forEach(function(e) {

				nonFriends = L.marker([e.lat, e.long], {icon: nonFriendAvatar}).addTo(map);

				if(e.last_message != null) {
					let htmlPopup = '';
					htmlPopup = "<div class='popout-messages-non-friends'>"+ e.last_message +"</div>";
					if(user.type != 'basic'){
						htmlPopup += "<div class='popout-img' onclick='sendFriendRequest("+e.id+")'><i class='fas fa-user-plus'></i></div>";
						htmlPopup += "<a href='/global-profile/"+ e.id +"'><div class='popout-profile-img'><i class='fas fa-user-circle'></i></div></a>";
					}
					nonFriends.bindPopup(htmlPopup)
				}
			})

		}
});


function sendFriendRequest(id) {
	console.log('id', id)

	//Send friend requests
	$.ajax
	({
		type: "GET",
		url: "/send-friend-request",
		data: { "id": id },
			success: function(data)
			{
				console.log('friend request', data)
				$('.message-section').fadeIn(1000).delay(3000).fadeOut(1000)
				$('.message-section').append(data)
			}
	});
}




//Get pins
/*
$.ajax
({
	type: "GET",
	url: "/get-pins",
		success: function(data)
		{
			pins = data
			data.forEach
			console.log('pins', data)
		}
});
*/






function generatePin(lat, long, icon) {
	L.marker([lat, long], {icon: icon}).addTo(map);
}

function formatDate(date) {

	date = new Date(date)

	var monthNames = [
	"January", "February", "March",
	"April", "May", "June", "July",
	"August", "September", "October",
	"November", "December"
	];

	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();

	return monthNames[monthIndex] + ' ' + day + ', ' + year;
}

//Activate random pin section
$('.create-random-pin-close-btn').click(function(){
    $('.bottom-pin-location-section').fadeOut(500)
})


/*
map.addEventListener('mousemove', function(ev) {
   constUserLat = ev.latlng.lat;
   constUserLong = ev.latlng.lng;
});

map.on('zoomend', function() {
    user.marker.setLatLng([constUserLat, constUserLong]).update();
});

map.on('click', function(e) {
	if(allowMovingPin) {
		user.marker.setLatLng([e.latlng.lat, e.latlng.lng]).update();
		constUserLat = e.latlng.lat
		constUserLong = e.latlng.lng
		$('#pin-form-lat').val(e.latlng.lat)
		$('#pin-form-long').val(e.latlng.lng)
		$('.add-pin-btn').prop("disabled",false);
	}
});
*/
