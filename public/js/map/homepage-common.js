//Add location map homepage
$('.custom-pin-location-li-e').click(function(){
	marketTempCheck = true;
	$('.frendly-cloud-locations-section').fadeIn(500)
	var baloon = $('.frendly-cloud-locations-section')
	function runIt() {
	   baloon.animate({top:'+=5'}, 650);
	   baloon.animate({top:'-=5'}, 650, runIt);
	}
	runIt();
})

// $('#add-location').click(function(){
//     if ($('#active-locations').length == 0) {
//         return;
//     }
// 	let customPinLocations = $('.custom-pin-list-section-locations');
// 	if (customPinLocations.is(':visible')) {
// 		$('.custom-pin-list-section-locations').fadeOut(300)
// 	} else {
// 		$('.custom-pin-list-section-locations').fadeIn(300)
// 	}
// })

$(document).click(function(e){
    if ($(e.target).closest(".custom-pin-list-section-locations").length === 0 && $(e.target).closest("#add-location").length === 0) {
        $(".custom-pin-list-section-locations").fadeOut(300);
    }
})

$('.frendly-cloud-btn').click(function(){
	$('.frendly-cloud-locations-section').fadeOut(300)
	$('.add-location-section').delay(300).fadeIn(500)
})

$('.add-location-close-btn').click(function(){
  $('.add-location-section').fadeOut(500)
})

//show hide 1 pin options
$('#bottom-navigation-create-random-pin').click(function(){
	$('.custom-pin-list').fadeIn(500)
})

$('.create-random-pin-close-btn').click(function(){
    $('.bottom-pin-location-section').fadeOut(500)
})

function peopleToFollowPopup(user_id, user_fullName, user_profileImg) {

    var popup = document.getElementById("myPopup");
    var link = "profile/" + user_id + "/timeline";

    if (popup.classList.contains("show")) {
        popup.classList.remove("show");
    }
    popup.classList.toggle("show");

    document.getElementById('popup-fullname').innerHTML = user_fullName;
    document.getElementById('popup-fullname-href').setAttribute("href", link);
    document.getElementById("popup-profile-href").setAttribute("href", link);

    var photo = user_profileImg;
    var source;

    if (photo == null) {
        source = "/img/no-image.png";
    } else {
        source = "/uploads/" + user_profileImg;
    }
    document.getElementById("popup-img").setAttribute("src", source);

    var span = document.getElementsByClassName("close-x")[0];
    span.onclick = function () {
        popup.classList.remove("show");
    }
}

var setBottomOption1 = false;

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('toggleMapLayers').addEventListener('click', () => {
        document.getElementById('mapLayersContainer').classList.remove('d-none');
    })

    function fadeAllSettings(time = 100) {
        $('#userSettingsDropdown').fadeOut(time);
        $('#layersSettingsDropdown').fadeOut(time);
        $('#radiusDropdown').fadeOut(time);

    }

    $('#toggleUserSettings').click(function (e) {
        if (e.target.id != 'toggleUserSettings') {
            return;
        }
        fadeAllSettings();
        $('#userSettingsDropdown').toggle();
    });

    $('#toggleRadiusSettings').click(function (e) {
        if (e.target.id != 'toggleRadiusSettings') {
            return;
        }
        fadeAllSettings();
        $('#radiusDropdown').toggle();
    });

    $('#toggleLayersSettings').click(function (e) {
        if (e.target.id != 'toggleLayersSettings') {
            return;
        }
        fadeAllSettings();
        $('#layersSettingsDropdown').toggle();
    });

    $('#bottom-navigation-create-random-pin').click(function () {
        let customPinSelection = $('.custom-pin-list-section');
        if (!customPinSelection.is(':visible')) {
            customPinSelection.fadeIn(300);
        } else {
            customPinSelection.fadeOut(300)
        }
    })
}, false);

$(document).mouseup(function (e) {
    var container = $("#map-settings-menu");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.fadeOut(300);
    }
});

function toggleMapProfilePicture() {
    let formData = document.getElementById('toggleMapProfilePicture');
    let input = document.getElementById('mapProfileDisplayInput');

    axios.post(`api/map_settings/toggle_profile_picture/${user.id}`).then(res => {
        location.reload();
    })

    return false;
}

$('.left-menu-section').bind('contextmenu', function (e) {
    $("div.custom-menu").hide();
    return true;
});
$('.custom-right-public-section').bind('contextmenu', function (e) {
    $("div.custom-menu").hide();
    return true;
});

$('.leaflet-container').bind("contextmenu", function (event) {
    event.preventDefault();
    $("div.custom-menu").hide();

    $(` <div class='custom-menu p-0'> 
            <ul class='list-group mb-0'> 
                <li class='list-group-item homepage-main-list' onclick="openLocationsModal()">
                    <a>
                        Add location
                    </a>
                </li> 
                <li class='list-group-item homepage-main-list'>
                    <a href=''>
                        Get direction
                    </a>
                </li> 
                <li class='list-group-item homepage-main-list'>
                    <a href=''>
                        Search nearby
                    </a>
                </li> 
                <li class='list-group-item homepage-main-list'>
                    <a href=''>
                        Print
                    </a>
                </li> 
                <li class='list-group-item homepage-main-list'>
                    <a href=''>
                        Measure distance
                    </a>
                </li> 
                <li class='list-group-item homepage-main-list'>
                    <a href=''>
                        Change view
                    </a>
                </li> 
            </ul> 
        </div>`)
        .appendTo("#map")
        .css({ top: event.pageY - 75 + "px", left: event.pageX + "px" });
}).bind("click", function (event) {
    $("div.custom-menu").hide();
});

function openLocationsModal(center = false) {
    $('.locationsModal').removeClass('d-none');
    showLocationAddMap(center);
}

function openMapPostModal(center = false) {
    $('.mapPostModal').removeClass('d-none');
}

function showLocationAddMap(center) {
    locationAddMap.invalidateSize();

    let LeafIcon = L.Icon.extend({
        options: {
            iconSize: [80, 81],
            iconAnchor: [22, 94],
            popupAnchor: [-3, -76]
        }
    });

    let pin = new LeafIcon({ iconUrl: '/img/pins/pngfuel.com.png' });
    
    if (addNewLocationMarker) {
        locationAddMap.removeLayer(addNewLocationMarker);
    }

    if (center) {
        let mapCenter = map.getCenter();
        locationAddMap.setView([mapCenter.lat, mapCenter.lng], map.getZoom());
        addNewLocationMarker = L.marker([mapCenter.lat, mapCenter.lng], { icon: pin }).addTo(locationAddMap);
    } else {
        locationAddMap.setView([clickedMapCoordinates.lat, clickedMapCoordinates.lng], map.getZoom());
        addNewLocationMarker = L.marker([clickedMapCoordinates.lat, clickedMapCoordinates.lng], { icon: pin }).addTo(locationAddMap);
    }

    $('.locationMapIdContainer').addClass('disabledMap');
    
    addNewLocationCoordinates = locationAddMap.getCenter();
}

function centerLocationAddMarker() {
    if (addNewLocationMarker) {
        locationAddMap.removeLayer(addNewLocationMarker);
    }

    let lat = locationAddMap.getCenter().lat;
    let lng = locationAddMap.getCenter().lng;

    let LeafIcon = L.Icon.extend({
        options: {
            iconSize: [80, 81],
            iconAnchor: [22, 94],
            popupAnchor: [-3, -76]
        }
    });

    let pin = new LeafIcon({ iconUrl: '/img/pins/pngfuel.com.png' });

    addNewLocationMarker = L.marker([lat, lng], { icon: pin }).addTo(locationAddMap);
    addNewLocationCoordinates = locationAddMap.getCenter();

    $('#locationMapId .leaflet-control-attribution').text(addNewLocationCoordinates);
}

