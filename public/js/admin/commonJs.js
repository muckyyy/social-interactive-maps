$('.category-icon').click(function(e){
	console.log('e', e.currentTarget.firstElementChild.outerHTML)
	$('#admin-category-icon').val(e.currentTarget.firstElementChild.outerHTML)
	$('.category-icon').css('background', 'transparent')
	$('.category-icon').css('color', '#636b6f')
	$(this).css('background', '#3097d1')
	$(this).css('color', '#fff')
});

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();
        $('.pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var url = $(this).attr('href');  
        getCities(url);
        window.history.pushState("", "", url);
    });
    
    function getCities(url) {
        $.ajax({
            url : url + '&query=' + $('#cities-search').val()  
        }).done(function (data) {
            $('#cities').html(data);  
        }).fail(function () {
            alert('Cities could not be loaded.');
        });
    }
    
    
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#cities-search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping () {
      $.ajax({
            url : '/admin-city?query=' + $('#cities-search').val() + '&page=0'  
        }).done(function (data) {
            $('#cities').html(data);  
        }).fail(function () {
            alert('Cities could not be loaded.');
        });
    }
});
