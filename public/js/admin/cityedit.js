
var map;
var marker;
var host = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var attribution = '';
var zoomLvl = 13
var id = 'test-b52c4827'
var siteId = 'test-b52c4827' 
//radius
var reachRadius = 2200
var privacyRadius = 900


$( document ).ready(function() {
    
    var startLat = $('#city-lat').val();
    var startLong = $('#city-long').val();
    
    if ($('#city-action').val() == 'add'){
        map = L.map('city-map-add').setView([0, 0], 0);
    } 
    else{
        map = L.map('city-map-edit').setView([startLat, startLong], 1);
    }
    
    
    

    L.tileLayer(host).addTo(map);
    map.attributionControl.setPrefix(false);
    
    var marker = new L.marker([startLat,startLong], {
        draggable: 'true'
    });
    
    marker.on('dragend', function(event) {
        var position = marker.getLatLng();
        marker.setLatLng(position, {
          draggable: 'true'
        }).bindPopup(position).update();
        $('#city-lat').val(position.lat);
        $('#city-long').val(position.lng);
        $("#Latitude").val(position.lat);
        $("#Longitude").val(position.lng).keyup();
        
        
    });
   
    map.addLayer(marker);
    
    $('#country-select').change(function(){
        $('#region-select').html('<option "">Leading...</option>');  
        $.ajax({
            url : '/admin-region/get-regions/' + $('#country-select').val()   
        }).done(function (data) {
            $('#region-select').html(data);  
        }).fail(function () {
            alert('Cities could not be loaded.');
        }).beforeSend(function(){
            $('#cities').html('Loading...');
        });
       
    });
});