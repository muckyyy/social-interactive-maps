$( document ).ready(function() {
    $( "#findCities" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "/search-city/" + $('#country').val() + '/' +request.term ,
          dataType: "json",
         
          success: function( data ) {
                response($.map(data, function(v,i){
                    return {
                        label: v.name + ', ' + v.country,
                        value: v.name,
                        cityid: v.id,  
                        };
                }));
            }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#findCities').val(ui.item.value);
        $('#cityid').val(ui.item.cityid);
        
        return false;
        
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
    
     if ($('#country').val() == 0){
        
        $('#city').autocomplete('disable');
        
    }
    $('#country').on('change',function(){
        if($(this).val()==0){
           $('#city').autocomplete('disable');
            
        }
        else{
            $('#city').autocomplete('enable');
        }
    });
});

