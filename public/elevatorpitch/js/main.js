





var slideCount = 0;

//slide 5
//next
$('#slide-5-btn-next').click(function(){
    $('#slide5').fadeOut(700)
    $('#slide6').delay(700).fadeIn(700)
    $('#i62').delay(1400).fadeIn(700)
    $('#s61').delay(1400).fadeIn(700)
    slideCount = 0;
})
//back
$('#slide-5-btn-back').click(function(){
    $('#slide5').fadeOut(700)
    $('#slide4').delay(700).fadeIn(700)
})

//slide 6

//next
$('#slide-6-btn-next').click(function(){
    
    slideCount++
    slide6Progress(slideCount)

})
//back
$('#slide-6-btn-back').click(function(){
    

    $('#s62').hide()
    $('#p62').hide()
    $('#s63').hide()
    $('#p63').hide()
    $('#s641').hide()
    $('#s642').hide()
    $('#s643').hide()
    $('#s651').hide()
    $('#s652').hide()
    $('#s653').hide()
    $('#s654').hide()
    $('#s661').hide()
    $('#s662').hide()
    $('#s663').hide()
    $('#s671').hide()
    $('#s672').hide()
    $('#s673').hide()
    $('#s674').hide()
    $('#s675').hide()
    $('#s676').hide()

    $('#i62').hide()
    $('#i63').hide()
    $('#i64').hide()
    $('#i65').hide()

    slideCount = 0

    $('#slide6').fadeOut(700)
    $('#slide5').delay(700).fadeIn(700)





})


function slide6Progress(count) {

    if(count == 1) {
        $('#s62').fadeIn()
        $('#s61').delay(200).fadeOut(1000)
        $('#i62').fadeIn(1000)
        $('#p62').fadeIn(1000)
    }

    if(count == 2) {
        $('#s63').fadeIn()
        $('#s62').delay(200).fadeOut(1000)
        $('#i63').fadeIn(1000)
        $('#p63').fadeIn(1000)
    }

    if(count == 3) {

        $('#s641').fadeIn(1000)
        $('#s642').delay(1000).fadeIn(1000)
        $('#s643').delay(2000).fadeIn(1000)

        $('#s63').delay(800).fadeOut(1000)

        $('#i64').fadeIn(1000)
        $('#p64').fadeIn(1000)
    }

    if(count == 4) {

        $('#s651').fadeIn(1000)
        $('#i65').fadeIn(1000)

        $('#s652').delay(1000).fadeIn(1000)
        $('#s653').delay(2000).fadeIn(1000)
        $('#s654').delay(3000).fadeIn(1000)
    }

    if(count == 5) {

        $('#s661').fadeIn(1000)

        $('#s652').hide(1000)
        $('#s653').hide(1000)
        $('#s654').delay(800).fadeOut(1000)

        $('#s662').delay(1000).fadeIn(1000)
        $('#s663').delay(2000).fadeIn(1000)
    }

    if(count == 6) {

        $('#s671').fadeIn(1000)

        $('#s663').delay(800).fadeOut(1000)

        $('#s672').delay(1000).fadeIn(1000)
        $('#s673').delay(2000).fadeIn(1000)
        $('#s674').delay(3000).fadeIn(1000)
        $('#s675').delay(4000).fadeIn(1000)
        $('#s676').delay(5000).fadeIn(1000)
    }
}