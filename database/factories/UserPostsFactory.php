<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    $user = \App\User::inRandomOrder()->first();

    return [
        'profile_id' => $user->id,
        'user_id' => $user->id,
        'text' => $faker->text(100),
        'public' => rand(0, 1)
    ];
});
