<?php

use App\Location;
use Faker\Generator as Faker;
use Grimzy\LaravelMysqlSpatial\Types\Point;

$factory->define(Location::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['Restaurant', 'Cinema', 'Theater', 'Coffe Shop']),
        'location' => new Point($faker->latitude(43.70, 43.9), $faker->longitude(18.2, 18.45)), // Rand Coordinates for Sarajevo
        'address' => $faker->address,
        'category_id' => \App\Category::inRandomOrder()->first()->id,
        'phone_number' => $faker->phoneNumber,
        'rating' => $faker->numberBetween($min = 1, $max = 5)
    ];
});
