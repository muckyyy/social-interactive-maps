<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Grimzy\LaravelMysqlSpatial\Types\Point;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'user_name' => $faker->userName,
        'full_name' => $faker->firstName . ' ' . $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'currentlocation' => new Point($faker->latitude(43.70, 43.9), $faker->longitude(18.2, 18.45)), // Rand Coordinates for Sarajevo
        'displaylocation' => new Point($faker->latitude(43.70, 43.9), $faker->longitude(18.2, 18.45)), // Rand Coordinates for Sarajevo
        'last_action' => Carbon::now()
    ];
});
