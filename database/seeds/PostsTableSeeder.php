<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return vouser_id
     */
    public function run()
    {
        DB::table('posts')->insert([
            // Landmarks category 
            [
                'user_id' => '1',
                'text' => "Travelled a lot !",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
            // Administration category 
            [
                'user_id' => '2',
                'text' => "Having a goot time :D",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
                
            // Local Business category  
            [
                'user_id' => '3',
                'text' => "Feel good!",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
            // Administration category 
            [
                'user_id' => '4',
                'text' => "Eating a sushi",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],

               [
                'user_id' => '5',
                'text' => "Drinking cola",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
            // Administration category 
            [
                'user_id' => '6',
                'text' => "Travelling to Vienna",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
                
            // Local Business category  
            [
                'user_id' => '7',
                'text' => "Check this restaurant out!",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
            // Administration category 
            [
                'user_id' => '8',
                'text' => "There is good food around ?",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],

                 [
                'user_id' => '9',
                'text' => "Lets drink coffee :D",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
            // Administration category 
            [
                'user_id' => '10',
                'text' => "Looking for friend",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
                
            // Local Business category  
            [
                'user_id' => '11',
                'text' => "Seeding a database",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
            // Administration category 
            [
                'user_id' => '12',
                'text' => "Anyone arround me ?",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],

               [
                'user_id' => '13',
                'text' => "Stop and eat delicious food",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
            // Administration category 
            [
                'user_id' => '14',
                'text' => "Going back home",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
                
            // Local Business category  
            [
                'user_id' => '15',
                'text' => "Having a dinner",
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18'
            ],
        ]);
    }
}
