<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(SubcategoriesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(BeaconsTableSeeder::class);        
        $this->call(RolesTableSeeder::class);
        $this->call(RoleAssignmentsTableSeeder::class);
        $this->call(ContextTableSeeder::class);
        $this->call(RoleCapabilitiesTableSeeder::class);
        $this->call(RoleContextLevelsTableSeeder::class);
        $this->call(ContextsTableSeeder::class);
        $this->call(MapLayersTableSeeder::class);
        $this->call(HeaderCoverImageSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(SarajevoLocationsSeeder::class);
        $this->call(LocationReviewSeeder::class);
    }
}
