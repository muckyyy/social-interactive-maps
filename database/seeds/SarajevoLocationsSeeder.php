<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SarajevoLocationsSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(File::get(base_path('database/seeds/sql/locations.sql')));
    }
}
