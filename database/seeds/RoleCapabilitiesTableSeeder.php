<?php

use Illuminate\Database\Seeder;

class RoleCapabilitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_capabilities')->delete();
        
        \DB::table('role_capabilities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:view',
                'permission' => 1,
                'created_at' => '2019-11-30 17:38:53',
                'updated_at' => '2019-11-30 17:38:55',
            ),
            1 => 
            array (
                'id' => 2,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:config',
                'permission' => 1,
                'created_at' => '2019-11-30 17:38:58',
                'updated_at' => '2019-11-30 17:39:10',
            ),
            2 => 
            array (
                'id' => 3,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:dashboard',
                'permission' => 1,
                'created_at' => '2019-11-30 17:39:04',
                'updated_at' => '2019-11-30 17:39:13',
            ),
            3 => 
            array (
                'id' => 4,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:categorymanagement',
                'permission' => 1,
                'created_at' => '2019-11-30 17:39:17',
                'updated_at' => '2019-11-30 17:39:21',
            ),
            4 => 
            array (
                'id' => 5,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:gemification',
                'permission' => 1,
                'created_at' => '2019-11-30 17:39:24',
                'updated_at' => '2019-11-30 17:39:29',
            ),
            5 => 
            array (
                'id' => 6,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:tileservermanagement',
                'permission' => 1,
                'created_at' => '2019-11-30 17:39:32',
                'updated_at' => '2019-11-30 17:39:37',
            ),
            6 => 
            array (
                'id' => 7,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:mapsettings',
                'permission' => 1,
                'created_at' => '2019-11-30 17:39:40',
                'updated_at' => '2019-11-30 17:39:45',
            ),
            7 => 
            array (
                'id' => 8,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:beaconsetings',
                'permission' => 1,
                'created_at' => '2019-11-30 17:39:48',
                'updated_at' => '2019-11-30 17:39:51',
            ),
            8 => 
            array (
                'id' => 9,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:locationsettings',
                'permission' => 1,
                'created_at' => '2019-11-30 17:39:56',
                'updated_at' => '2019-11-30 17:39:59',
            ),
            9 => 
            array (
                'id' => 10,
                'context_id' => 1,
                'role_id' => 3,
                'capability' => 'admin/settings:usersettings',
                'permission' => 1,
                'created_at' => '2019-11-30 17:40:04',
                'updated_at' => '2019-11-30 17:40:07',
            ),
        ));
        
        
    }
}