<?php

use Illuminate\Database\Seeder;

class ContextTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contexts')->delete();
        
        \DB::table('contexts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'context_level' => 10,
                'instance_id' => 0,
                'path' => '/1',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            1 => 
            array (
                'id' => 2,
                'context_level' => 20,
                'instance_id' => 1,
                'path' => '/1/2',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
        ));
        
        
    }
}