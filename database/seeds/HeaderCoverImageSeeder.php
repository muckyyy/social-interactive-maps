<?php

use Illuminate\Database\Seeder;

class HeaderCoverImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('header_cover_images')->insert([
            // Landmarks category 
            [
                'id' => '1',
                'name' => "/background/embossed-diamond.png"
            ],
            // Administration category 
            [
                'id' => '2',
                'name' => "/background/green_cup.png"
            ],
                
            // Local Business category  
            [
                'id' => '3',
                'name' => "/background/hotel-wallpaper.png"
            ],
            // Administration category 
            [
                'id' => '4',
                'name' => "/background/pixel-heart.png"
            ],

               [
                'id' => '5',
                'name' => "/background/ricepaper_v3.png"
            ],
            // Administration category 
            [
                'id' => '6',
                'name' => "/background/school.png"
            ],
                
            // Local Business category  
            [
                'id' => '7',
                'name' => "/background/spikes.png"
            ],
            // Administration category 
            [
                'id' => '8',
                'name' => "/background/white-waves.png"
            ],
        ]);
    }
}
