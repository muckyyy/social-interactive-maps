<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Guest User',
                'shortname' => 'guest',
                'description' => 'All users who have not logged in',
                'contextlevel' => 10,
                'sortorder' => 0,
                'created_at' => '2019-11-30 13:38:02',
                'updated_at' => '2019-11-30 13:38:05',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Authenticated User',
                'shortname' => 'authenticateduser',
                'description' => 'All users who logged in users and not administrators',
                'contextlevel' => 10,
                'sortorder' => 1,
                'created_at' => '2019-11-30 13:39:53',
                'updated_at' => '2019-11-30 13:39:57',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Administrator',
                'shortname' => 'administrators',
                'description' => 'Administrators users',
                'contextlevel' => 10,
                'sortorder' => 2,
                'created_at' => '2019-11-30 13:40:14',
                'updated_at' => '2019-11-30 13:40:16',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Banned',
                'shortname' => 'banned',
                'description' => 'Users who are banned from using system',
                'contextlevel' => 10,
                'sortorder' => 3,
                'created_at' => '2019-11-30 14:25:01',
                'updated_at' => '2019-11-30 14:25:01',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Moderator',
                'shortname' => 'moderator',
                'description' => 'Moderator users at beacon level',
                'contextlevel' => 30,
                'sortorder' => 4,
                'created_at' => '2019-11-30 13:41:06',
                'updated_at' => '2019-11-30 13:41:06',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Community Member',
                'shortname' => 'communitymember',
                'description' => 'Users on beacons, they will have more permissions than others',
                'contextlevel' => 30,
                'sortorder' => 5,
                'created_at' => '2019-11-30 13:42:12',
                'updated_at' => '2019-11-30 13:42:12',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Manager',
                'shortname' => 'manager',
                'description' => 'Users who are managing private venues',
                'contextlevel' => 40,
                'sortorder' => 6,
                'created_at' => '2019-11-30 13:43:55',
                'updated_at' => '2019-11-30 13:43:55',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Editor',
                'shortname' => 'editor',
                'description' => 'Users who have in general all capabilities on private venu except adding new editors',
                'contextlevel' => 40,
                'sortorder' => 7,
                'created_at' => '2019-11-30 13:48:32',
                'updated_at' => '2019-11-30 13:48:37',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Location moderator',
                'shortname' => 'locationmoderator',
                'description' => 'Users who can moderatorate locations ',
                'contextlevel' => 40,
                'sortorder' => 8,
                'created_at' => '2019-11-30 14:20:18',
                'updated_at' => '2019-11-30 14:20:19',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Fan',
                'shortname' => 'fan',
                'description' => 'Location fan, users who are actively following location',
                'contextlevel' => 40,
                'sortorder' => 9,
                'created_at' => '2019-11-30 14:21:11',
                'updated_at' => '2019-11-30 14:21:11',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Profile owner',
                'shortname' => 'profileowner',
                'description' => 'Users who are owning a profile',
                'contextlevel' => 20,
                'sortorder' => 10,
                'created_at' => '2019-11-30 14:22:11',
                'updated_at' => '2019-11-30 14:22:11',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Friend',
                'shortname' => 'friend',
                'description' => 'Users listed as close friedns',
                'contextlevel' => 20,
                'sortorder' => 11,
                'created_at' => '2019-11-30 14:22:36',
                'updated_at' => '2019-11-30 14:22:37',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Follower',
                'shortname' => 'follower',
                'description' => 'Users who are following profile',
                'contextlevel' => 20,
                'sortorder' => 12,
                'created_at' => '2019-11-30 14:22:56',
                'updated_at' => '2019-11-30 14:22:56',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Blocked',
                'shortname' => 'blocked',
                'description' => 'User who blocked a profile',
                'contextlevel' => 20,
                'sortorder' => 13,
                'created_at' => '2019-11-30 14:23:30',
                'updated_at' => '2019-11-30 14:23:31',
            ),
        ));
        
        
    }
}