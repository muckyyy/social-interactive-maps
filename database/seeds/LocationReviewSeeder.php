<?php

use Illuminate\Database\Seeder;

class LocationReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_reviews')->insert([
            [
                'id' => 1,
                'location_id' => 1,
                'user_id' => 1,
                'rating' => 5,
                'review' => 'This is location 1 with rating 5',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 2,
                'location_id' => 2,
                'user_id' => 1,
                'rating' => 4,
                'review' => 'This is location 2 with rating 4, from user 1',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 3,
                'location_id' => 3,
                'user_id' => 1,
                'rating' => 3,
                'review' => 'This is location 3 with rating 3',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 4,
                'location_id' => 4,
                'user_id' => 1,
                'rating' => 2,
                'review' => 'This is location 4 with rating 2',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 5,
                'location_id' => 5,
                'user_id' => 1,
                'rating' => 1,
                'review' => 'This is location 5 with rating 1',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 6,
                'location_id' => 6,
                'user_id' => 1,
                'rating' => 5,
                'review' => 'This is location 6 with rating 5',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 7,
                'location_id' => 7,
                'user_id' => 1,
                'rating' => 4,
                'review' => 'This is location 7 with rating 4',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 8,
                'location_id' => 8,
                'user_id' => 1,
                'rating' => 3,
                'review' => 'This is location 8 with rating 3',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 9,
                'location_id' => 9,
                'user_id' => 1,
                'rating' => 2,
                'review' => 'This is location 9 with rating 2',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 10,
                'location_id' => 10,
                'user_id' => 1,
                'rating' => 1,
                'review' => 'This is location 10 with rating 1',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 11,
                'location_id' => 11,
                'user_id' => 1,
                'rating' => 5,
                'review' => 'This is location 11 with rating 5',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 12,
                'location_id' => 12,
                'user_id' => 1,
                'rating' => 4,
                'review' => 'This is location 12 with rating 4',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 13,
                'location_id' => 13,
                'user_id' => 1,
                'rating' => 3,
                'review' => 'This is location 13 with rating 3',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 14,
                'location_id' => 14,
                'user_id' => 1,
                'rating' => 2,
                'review' => 'This is location 14 with rating 2',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
            [
                'id' => 15,
                'location_id' => 15,
                'user_id' => 1,
                'rating' => 1,
                'review' => 'This is location 15 with rating 1',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
             [
                'id' => 16,
                'location_id' => 2,
                'user_id' => 5,
                'rating' => 4,
                'review' => 'This is location 2 with rating 4, from user 5',
                'created_at' => '2020-02-26 16:09:57',
                'updated_at' => '2020-02-26 17:09:18',
            ],
        ]);
    }
}
