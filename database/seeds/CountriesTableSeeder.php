<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Bosnia and Herzegovina',
                'country_code' => 'ba',
                'population' => NULL,
                'area' => NULL,
                'callcode' => NULL,
                'website' => NULL,
                'founded' => NULL,
                'phone' => NULL,
                'capitol' => 0,
                'geometry' => '',
                'created_at' => '2019-11-30 11:20:08',
                'updated_at' => '2019-11-30 11:20:08',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Germany',
                'country_code' => 'de',
                'population' => NULL,
                'area' => NULL,
                'callcode' => NULL,
                'website' => NULL,
                'founded' => NULL,
                'phone' => NULL,
                'capitol' => 0,
                'geometry' => '',
                'created_at' => '2019-11-30 11:20:08',
                'updated_at' => '2019-11-30 11:20:08',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Switzerland',
                'country_code' => 'ch',
                'population' => NULL,
                'area' => NULL,
                'callcode' => NULL,
                'website' => NULL,
                'founded' => NULL,
                'phone' => NULL,
                'capitol' => 0,
                'geometry' => '',
                'created_at' => '2019-11-30 11:20:08',
                'updated_at' => '2019-11-30 11:20:08',
            ),
        ));
        
        
    }
}