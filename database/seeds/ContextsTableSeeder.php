<?php

use Illuminate\Database\Seeder;

class ContextsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contexts')->delete();
        
        \DB::table('contexts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'context_level' => 10,
                'instance_id' => 0,
                'path' => '/1',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            1 => 
            array (
                'id' => 2,
                'context_level' => 30,
                'instance_id' => 1,
                'path' => '/1/2',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            2 => 
            array (
                'id' => 3,
                'context_level' => 30,
                'instance_id' => 2,
                'path' => '/1/3',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            3 => 
            array (
                'id' => 4,
                'context_level' => 30,
                'instance_id' => 3,
                'path' => '/1/4',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            4 => 
            array (
                'id' => 5,
                'context_level' => 30,
                'instance_id' => 4,
                'path' => '/1/5',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            5 => 
            array (
                'id' => 6,
                'context_level' => 30,
                'instance_id' => 5,
                'path' => '/1/6',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            6 => 
            array (
                'id' => 7,
                'context_level' => 30,
                'instance_id' => 6,
                'path' => '/1/7',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            7 => 
            array (
                'id' => 8,
                'context_level' => 30,
                'instance_id' => 7,
                'path' => '/1/8',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            8 => 
            array (
                'id' => 9,
                'context_level' => 30,
                'instance_id' => 8,
                'path' => '/1/9',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            9 => 
            array (
                'id' => 10,
                'context_level' => 30,
                'instance_id' => 9,
                'path' => '/1/10',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            10 => 
            array (
                'id' => 11,
                'context_level' => 30,
                'instance_id' => 10,
                'path' => '/1/11',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            11 => 
            array (
                'id' => 12,
                'context_level' => 30,
                'instance_id' => 11,
                'path' => '/1/12',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            12 => 
            array (
                'id' => 13,
                'context_level' => 30,
                'instance_id' => 12,
                'path' => '/1/13',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            13 => 
            array (
                'id' => 14,
                'context_level' => 30,
                'instance_id' => 13,
                'path' => '/1/14',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            14 => 
            array (
                'id' => 15,
                'context_level' => 30,
                'instance_id' => 14,
                'path' => '/1/15',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            15 => 
            array (
                'id' => 16,
                'context_level' => 30,
                'instance_id' => 15,
                'path' => '/1/16',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            16 => 
            array (
                'id' => 17,
                'context_level' => 30,
                'instance_id' => 16,
                'path' => '/1/17',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            17 => 
            array (
                'id' => 18,
                'context_level' => 30,
                'instance_id' => 17,
                'path' => '/1/18',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            18 => 
            array (
                'id' => 19,
                'context_level' => 30,
                'instance_id' => 18,
                'path' => '/1/19',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            19 => 
            array (
                'id' => 20,
                'context_level' => 30,
                'instance_id' => 19,
                'path' => '/1/20',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            20 => 
            array (
                'id' => 21,
                'context_level' => 30,
                'instance_id' => 20,
                'path' => '/1/21',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            21 => 
            array (
                'id' => 22,
                'context_level' => 30,
                'instance_id' => 21,
                'path' => '/1/22',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            22 => 
            array (
                'id' => 23,
                'context_level' => 30,
                'instance_id' => 22,
                'path' => '/1/23',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            23 => 
            array (
                'id' => 24,
                'context_level' => 30,
                'instance_id' => 23,
                'path' => '/1/24',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            24 => 
            array (
                'id' => 25,
                'context_level' => 30,
                'instance_id' => 24,
                'path' => '/1/25',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            25 => 
            array (
                'id' => 26,
                'context_level' => 30,
                'instance_id' => 25,
                'path' => '/1/26',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            26 => 
            array (
                'id' => 27,
                'context_level' => 30,
                'instance_id' => 26,
                'path' => '/1/27',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            27 => 
            array (
                'id' => 28,
                'context_level' => 30,
                'instance_id' => 27,
                'path' => '/1/28',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            28 => 
            array (
                'id' => 29,
                'context_level' => 30,
                'instance_id' => 28,
                'path' => '/1/29',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            29 => 
            array (
                'id' => 30,
                'context_level' => 30,
                'instance_id' => 29,
                'path' => '/1/30',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            30 => 
            array (
                'id' => 31,
                'context_level' => 30,
                'instance_id' => 30,
                'path' => '/1/31',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            31 => 
            array (
                'id' => 32,
                'context_level' => 30,
                'instance_id' => 31,
                'path' => '/1/32',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            32 => 
            array (
                'id' => 33,
                'context_level' => 30,
                'instance_id' => 32,
                'path' => '/1/33',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            33 => 
            array (
                'id' => 34,
                'context_level' => 30,
                'instance_id' => 33,
                'path' => '/1/34',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            34 => 
            array (
                'id' => 35,
                'context_level' => 30,
                'instance_id' => 34,
                'path' => '/1/35',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            35 => 
            array (
                'id' => 36,
                'context_level' => 30,
                'instance_id' => 35,
                'path' => '/1/36',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            36 => 
            array (
                'id' => 37,
                'context_level' => 30,
                'instance_id' => 36,
                'path' => '/1/37',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            37 => 
            array (
                'id' => 38,
                'context_level' => 30,
                'instance_id' => 37,
                'path' => '/1/38',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            38 => 
            array (
                'id' => 39,
                'context_level' => 30,
                'instance_id' => 38,
                'path' => '/1/39',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            39 => 
            array (
                'id' => 40,
                'context_level' => 30,
                'instance_id' => 39,
                'path' => '/1/40',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            40 => 
            array (
                'id' => 41,
                'context_level' => 30,
                'instance_id' => 40,
                'path' => '/1/41',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            41 => 
            array (
                'id' => 42,
                'context_level' => 30,
                'instance_id' => 41,
                'path' => '/1/42',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            42 => 
            array (
                'id' => 43,
                'context_level' => 30,
                'instance_id' => 42,
                'path' => '/1/43',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            43 => 
            array (
                'id' => 44,
                'context_level' => 30,
                'instance_id' => 43,
                'path' => '/1/44',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            44 => 
            array (
                'id' => 45,
                'context_level' => 30,
                'instance_id' => 44,
                'path' => '/1/45',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            45 => 
            array (
                'id' => 46,
                'context_level' => 30,
                'instance_id' => 45,
                'path' => '/1/46',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            46 => 
            array (
                'id' => 47,
                'context_level' => 30,
                'instance_id' => 46,
                'path' => '/1/47',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            47 => 
            array (
                'id' => 48,
                'context_level' => 30,
                'instance_id' => 47,
                'path' => '/1/48',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            48 => 
            array (
                'id' => 49,
                'context_level' => 30,
                'instance_id' => 48,
                'path' => '/1/49',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            49 => 
            array (
                'id' => 50,
                'context_level' => 30,
                'instance_id' => 49,
                'path' => '/1/50',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            50 => 
            array (
                'id' => 51,
                'context_level' => 30,
                'instance_id' => 50,
                'path' => '/1/51',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            51 => 
            array (
                'id' => 52,
                'context_level' => 30,
                'instance_id' => 51,
                'path' => '/1/52',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            52 => 
            array (
                'id' => 53,
                'context_level' => 30,
                'instance_id' => 52,
                'path' => '/1/53',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            53 => 
            array (
                'id' => 54,
                'context_level' => 30,
                'instance_id' => 53,
                'path' => '/1/54',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            54 => 
            array (
                'id' => 55,
                'context_level' => 30,
                'instance_id' => 54,
                'path' => '/1/55',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            55 => 
            array (
                'id' => 56,
                'context_level' => 30,
                'instance_id' => 55,
                'path' => '/1/56',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            56 => 
            array (
                'id' => 57,
                'context_level' => 30,
                'instance_id' => 56,
                'path' => '/1/57',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            57 => 
            array (
                'id' => 58,
                'context_level' => 30,
                'instance_id' => 57,
                'path' => '/1/58',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            58 => 
            array (
                'id' => 59,
                'context_level' => 30,
                'instance_id' => 58,
                'path' => '/1/59',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            59 => 
            array (
                'id' => 60,
                'context_level' => 30,
                'instance_id' => 59,
                'path' => '/1/60',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            60 => 
            array (
                'id' => 61,
                'context_level' => 30,
                'instance_id' => 60,
                'path' => '/1/61',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            61 => 
            array (
                'id' => 62,
                'context_level' => 30,
                'instance_id' => 61,
                'path' => '/1/62',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            62 => 
            array (
                'id' => 63,
                'context_level' => 30,
                'instance_id' => 62,
                'path' => '/1/63',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            63 => 
            array (
                'id' => 64,
                'context_level' => 30,
                'instance_id' => 63,
                'path' => '/1/64',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            64 => 
            array (
                'id' => 65,
                'context_level' => 30,
                'instance_id' => 64,
                'path' => '/1/65',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            65 => 
            array (
                'id' => 66,
                'context_level' => 30,
                'instance_id' => 65,
                'path' => '/1/66',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            66 => 
            array (
                'id' => 67,
                'context_level' => 30,
                'instance_id' => 66,
                'path' => '/1/67',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            67 => 
            array (
                'id' => 68,
                'context_level' => 30,
                'instance_id' => 67,
                'path' => '/1/68',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            68 => 
            array (
                'id' => 69,
                'context_level' => 30,
                'instance_id' => 68,
                'path' => '/1/69',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            69 => 
            array (
                'id' => 70,
                'context_level' => 30,
                'instance_id' => 69,
                'path' => '/1/70',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            70 => 
            array (
                'id' => 71,
                'context_level' => 30,
                'instance_id' => 70,
                'path' => '/1/71',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            71 => 
            array (
                'id' => 72,
                'context_level' => 30,
                'instance_id' => 71,
                'path' => '/1/72',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            72 => 
            array (
                'id' => 73,
                'context_level' => 30,
                'instance_id' => 72,
                'path' => '/1/73',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            73 => 
            array (
                'id' => 74,
                'context_level' => 30,
                'instance_id' => 73,
                'path' => '/1/74',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            74 => 
            array (
                'id' => 75,
                'context_level' => 30,
                'instance_id' => 74,
                'path' => '/1/75',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            75 => 
            array (
                'id' => 76,
                'context_level' => 30,
                'instance_id' => 75,
                'path' => '/1/76',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            76 => 
            array (
                'id' => 77,
                'context_level' => 30,
                'instance_id' => 76,
                'path' => '/1/77',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            77 => 
            array (
                'id' => 78,
                'context_level' => 30,
                'instance_id' => 77,
                'path' => '/1/78',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            78 => 
            array (
                'id' => 79,
                'context_level' => 30,
                'instance_id' => 78,
                'path' => '/1/79',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            79 => 
            array (
                'id' => 80,
                'context_level' => 30,
                'instance_id' => 79,
                'path' => '/1/80',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            80 => 
            array (
                'id' => 81,
                'context_level' => 30,
                'instance_id' => 80,
                'path' => '/1/81',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            81 => 
            array (
                'id' => 82,
                'context_level' => 30,
                'instance_id' => 81,
                'path' => '/1/82',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            82 => 
            array (
                'id' => 83,
                'context_level' => 30,
                'instance_id' => 82,
                'path' => '/1/83',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            83 => 
            array (
                'id' => 84,
                'context_level' => 30,
                'instance_id' => 83,
                'path' => '/1/84',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            84 => 
            array (
                'id' => 85,
                'context_level' => 30,
                'instance_id' => 84,
                'path' => '/1/85',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            85 => 
            array (
                'id' => 86,
                'context_level' => 30,
                'instance_id' => 85,
                'path' => '/1/86',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            86 => 
            array (
                'id' => 87,
                'context_level' => 30,
                'instance_id' => 86,
                'path' => '/1/87',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            87 => 
            array (
                'id' => 88,
                'context_level' => 30,
                'instance_id' => 87,
                'path' => '/1/88',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            88 => 
            array (
                'id' => 89,
                'context_level' => 30,
                'instance_id' => 88,
                'path' => '/1/89',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            89 => 
            array (
                'id' => 90,
                'context_level' => 30,
                'instance_id' => 89,
                'path' => '/1/90',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            90 => 
            array (
                'id' => 91,
                'context_level' => 30,
                'instance_id' => 90,
                'path' => '/1/91',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            91 => 
            array (
                'id' => 92,
                'context_level' => 30,
                'instance_id' => 91,
                'path' => '/1/92',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            92 => 
            array (
                'id' => 93,
                'context_level' => 30,
                'instance_id' => 92,
                'path' => '/1/93',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            93 => 
            array (
                'id' => 94,
                'context_level' => 30,
                'instance_id' => 93,
                'path' => '/1/94',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            94 => 
            array (
                'id' => 95,
                'context_level' => 30,
                'instance_id' => 94,
                'path' => '/1/95',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            95 => 
            array (
                'id' => 96,
                'context_level' => 30,
                'instance_id' => 95,
                'path' => '/1/96',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            96 => 
            array (
                'id' => 97,
                'context_level' => 30,
                'instance_id' => 96,
                'path' => '/1/97',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            97 => 
            array (
                'id' => 98,
                'context_level' => 30,
                'instance_id' => 97,
                'path' => '/1/98',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            98 => 
            array (
                'id' => 99,
                'context_level' => 30,
                'instance_id' => 98,
                'path' => '/1/99',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            99 => 
            array (
                'id' => 100,
                'context_level' => 30,
                'instance_id' => 99,
                'path' => '/1/100',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
            100 => 
            array (
                'id' => 101,
                'context_level' => 30,
                'instance_id' => 100,
                'path' => '/1/101',
                'created_at' => '2019-11-30 14:59:43',
                'updated_at' => '2019-11-30 14:59:43',
            ),
            101 => 
            array (
                'id' => 102,
                'context_level' => 30,
                'instance_id' => 101,
                'path' => '/1/102',
                'created_at' => '2020-01-26 13:10:51',
                'updated_at' => '2020-01-26 13:10:58',
            ),
            102 => 
            array (
                'id' => 103,
                'context_level' => 30,
                'instance_id' => 102,
                'path' => '/1/103',
                'created_at' => '2019-11-30 14:55:42',
                'updated_at' => '2019-11-30 14:55:42',
            ),
        ));
        
        
    }
}