<?php

use Illuminate\Database\Seeder;

class RoleAssignmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_assignments')->delete();
        
        \DB::table('role_assignments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 3,
                'context_id' => 1,
                'user_id' => 1,
                'modifier_id' => 1,
                'component' => 'core',
                'item_id' => 0,
                'created_at' => '2019-11-30 15:06:39',
                'updated_at' => '2019-11-30 15:06:42',
            ),
            1 => 
            array (
                'id' => 2,
                'role_id' => 11,
                'context_id' => 2,
                'user_id' => 1,
                'modifier_id' => 1,
                'component' => 'core',
                'item_id' => 0,
                'created_at' => '2019-11-30 15:09:54',
                'updated_at' => '2019-11-30 15:09:54',
            ),
        ));
        
        
    }
}