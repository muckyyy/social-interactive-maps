<?php

use Illuminate\Database\Seeder;

class RoleContextLevelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_context_levels')->delete();
        
        \DB::table('role_context_levels')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 3,
                'context_id' => 1,
                'created_at' => '2019-11-30 17:41:14',
                'updated_at' => '2019-11-30 17:41:20',
            ),
            1 => 
            array (
                'id' => 2,
                'role_id' => 1,
                'context_id' => 1,
                'created_at' => '2019-11-30 17:41:48',
                'updated_at' => '2019-11-30 17:41:51',
            ),
            2 => 
            array (
                'id' => 3,
                'role_id' => 2,
                'context_id' => 1,
                'created_at' => '2019-11-30 17:42:22',
                'updated_at' => '2019-11-30 17:42:26',
            ),
        ));
        
        
    }
}