<?php

use Illuminate\Database\Seeder;

class SubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
                [
                    'id' => '5',
                    'name' => "Museums",
                    'parent_id' => "1",
                    'icon_prefix' => "fa",
                    'icon_class' => 'landmark',
                    'icon_color' => 'darkred'
                ],
                [
                    'id' => '6',
                    'name' => "Monuments",
                    'parent_id' => "1",
                    'icon_prefix' => "fa",
                    'icon_class' => 'monument',
                    'icon_color' => 'black'
                ],
                [
                    'id' => '7',
                    'name' => "Parks",
                    'parent_id' => "1",
                    'icon_prefix' => "fa",
                    'icon_class' => 'tree',
                    'icon_color' => 'green'
                ],
                [
                    'id' => '8',
                    'name' => "Galleries",
                    'parent_id' => "1",
                    'icon_prefix' => "fa",
                    'icon_class' => 'camera-retro',
                    'icon_color' => 'gray'
                ],
                [
                    'id' => '9',
                    'name' => "Theaters",
                    'parent_id' => "1",
                    'icon_prefix' => "fa",
                    'icon_class' => 'theater-masks',
                    'icon_color' => 'gray'
                ],
                [
                    'id' => '10',
                    'name' => "Sport Halls",
                    'parent_id' => "1",
                    'icon_prefix' => "fa",
                    'icon_class' => 'baseball-ball',
                    'icon_color' => 'lightblue'
                ],
                // administration
                [
                    'id' => '11',
                    'name' => "Government Institutions",
                    'parent_id' => "2",
                    'icon_prefix' => "fa",
                    'icon_class' => 'city',
                    'icon_color' => 'darkblue'
                ],
                [
                    'id' => '12',
                    'name' => "Banks",
                    'parent_id' => "2",
                    'icon_prefix' => "fa",
                    'icon_class' => 'money-check',
                    'icon_color' => 'gray'
                ],
                [
                    'id' => '13',
                    'name' => "Health care",
                    'parent_id' => "2",
                    'icon_prefix' => "fa",
                    'icon_class' => 'heartbeat',
                    'icon_color' => 'lightred'
                ],
                [
                    'id' => '14',
                    'name' => "Education",
                    'parent_id' => "2",
                    'icon_prefix' => "fa",
                    'icon_class' => 'university',
                    'icon_color' => 'white'
                ],
                [
                    'id' => '15',
                    'name' => "Transport",
                    'parent_id' => "2",
                    'icon_prefix' => "fa",
                    'icon_class' => 'bus-alt',
                    'icon_color' => 'darkred'
                ],
                [
                    'id' => '16',
                    'name' => "Emergency services",
                    'parent_id' => "2",
                    'icon_prefix' => "fa",
                    'icon_class' => 'first-aid',
                    'icon_color' => 'lightred'
                ],
                [
                    'id' => '17',
                    'name' => "Courts",
                    'parent_id' => "2",
                    'icon_prefix' => "fa",
                    'icon_class' => 'flag',
                    'icon_color' => 'lightgray'
                ],
                [
                    'id' => '18',
                    'name' => "Posts",
                    'parent_id' => "2",
                    'icon_prefix' => "fa",
                    'icon_class' => 'envelope',
                    'icon_color' => 'gray'
                ],
                // local business
                [
                    'id' => '19',
                    'name' => "Food and Drinks",
                    'parent_id' => "3",
                    'icon_prefix' => "fa",
                    'icon_class' => 'hamburger',
                    'icon_color' => 'blue'
                ],
                [
                    'id' => '20',
                    'name' => "Accomodation",
                    'parent_id' => "3",
                    'icon_prefix' => "fa",
                    'icon_class' => 'hotel',
                    'icon_color' => 'lightgray'
                ],
                [
                    'id' => '21',
                    'name' => "Shopping",
                    'parent_id' => "3",
                    'icon_prefix' => "fa",
                    'icon_class' => 'shopping-cart',
                    'icon_color' => 'purple'
                ],
                [
                    'id' => '22',
                    'name' => "Services",
                    'parent_id' => "3",
                    'icon_prefix' => "fa",
                    'icon_class' => 'concierge-bell',
                    'icon_color' => 'orange'
                ],
                // fun relaxation
                [
                    'id' => '23',
                    'name' => "Theme Parks",
                    'parent_id' => "4",
                    'icon_prefix' => "fa",
                    'icon_class' => 'tree',
                    'icon_color' => 'green'
                ],
                [
                    'id' => '24',
                    'name' => "Welness and Spa",
                    'parent_id' => "4",
                    'icon_prefix' => "fa",
                    'icon_class' => 'spa',
                    'icon_color' => 'lightblue'
                ],
                [
                    'id' => '25',
                    'name' => "Outdoor activities",
                    'parent_id' => "4",
                    'icon_prefix' => "fa",
                    'icon_class' => 'running',
                    'icon_color' => 'lightgreen'
                ],
                [
                    'id' => '26',
                    'name' => "Nature",
                    'parent_id' => "4",
                    'icon_prefix' => "fa",
                    'icon_class' => 'leaf',
                    'icon_color' => 'green'
                ]
        ]);
    }
}
