INSERT INTO beacons (city_id, name, geolocation, display_location) VALUES (1, 'Sarajevo Main Beacon', GeomFromText('POINT(18.413029 43.856430)'), GeomFromText('POINT(18.413029 43.856430)')), 
                                                                          (2, 'Mostar Main Beacon', GeomFromText('POINT(17.807894 43.343033 )'), GeomFromText('POINT(17.807894 43.343033 )')),
                                                                          (3, 'Frankfurt Main Beacon', GeomFromText('POINT(8.682127 50.11092 )'), GeomFromText('POINT(8.682127 50.11092 )')),
                                                                          (4, 'Munich Main Beacon', GeomFromText('POINT(11.576124 48.137154)'), GeomFromText('POINT(11.576124 48.137154)')),
                                                                          (5, 'Ljubljana Main Beacon', GeomFromText('POINT(14.505751 46.05694)'), GeomFromText('POINT(14.505751 46.05694)')),
                                                                          (6, 'Zagreb Main Beacon', GeomFromText('POINT(15.978783 45.814632)'), GeomFromText('POINT(15.978783 45.814632)')),
                                                                          (7, 'New York Main Beacon', GeomFromText('POINT(-73.935242 40.730610)'), GeomFromText('POINT(-73.935242 40.730610)')),
                                                                          (8, 'London Main Beacon', GeomFromText('POINT(-0.118092 51.509865)'), GeomFromText('POINT(-0.118092 51.509865)')),
                                                                          (9, 'Rome Main Beacon', GeomFromText('POINT(12.496366 41.902782)'), GeomFromText('POINT(12.496366 41.902782)')),
                                                                          (10, 'Paris Main Beacon', GeomFromText('POINT(2.547778 49.009724)'), GeomFromText('POINT(2.547778 49.009724)')),
                                                                          (11, 'Vienna Main Beacon', GeomFromText('POINT(16.363449 48.210033)'), GeomFromText('POINT(16.363449 48.210033)')),
                                                                          (12, 'Madrid Main Beacon', GeomFromText('POINT(-3.703790 40.416775)'), GeomFromText('POINT(-3.703790 40.416775)')),
                                                                          (13, 'Istanbul Main Beacon', GeomFromText('POINT(	28.979530 41.015137)'), GeomFromText('POINT(	28.979530 41.015137)'));





