INSERT INTO cities (country, name, geopoint) VALUES ('1', 'Sarajevo', GeomFromText('POINT(18.413029 43.856430)')), 
                                                    ('1', 'Mostar', GeomFromText('POINT(17.807894 43.343033 )')),
                                                    ('2', 'Frankfurt', GeomFromText('POINT(8.682127 50.11092 )')),
                                                    ('2', 'Munich', GeomFromText('POINT(11.576124 48.137154)')),
                                                    ('1', 'Ljubljana', GeomFromText('POINT(14.505751 46.05694)')),
                                                    ('1', 'Zagreb', GeomFromText('POINT(15.978783 45.814632)')),
                                                    ('3', 'New York', GeomFromText('POINT(-73.935242 40.730610)')),
                                                    ('4', 'London', GeomFromText('POINT(-0.118092 51.509865)')),
                                                    ('5', 'Rome', GeomFromText('POINT(12.496366 41.902782)')),
                                                    ('6', 'Paris', GeomFromText('POINT(2.547778 49.009724)')),
                                                    ('7', 'Vienna', GeomFromText('POINT(16.363449 48.210033)')),
                                                    ('8', 'Madrid', GeomFromText('POINT(-3.703790 40.416775)')),
                                                    ('9', 'Istanbul', GeomFromText('POINT(	28.979530 41.015137)'));





