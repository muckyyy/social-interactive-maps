<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            // Landmarks category
            [
                'id' => '1',
                'name' => "Landmarks and iconic buildings",
                'icon_prefix' => "fa",
                'icon_class' => 'landmark',
                'icon_color' => 'darkblue',
                'parent_id' => "0"
            ],
            // Administration category
            [
                'id' => '2',
                'name' => "Administration and Public Services",
                'icon_prefix' => "fa",
                'icon_class' => 'city',
                'icon_color' => 'darkpurple',
                'parent_id' => "0"
            ],
                
            // Local Business category
            [
                'id' => '3',
                'name' => 'Local Business and Companies',
                'icon_prefix' => "fa",
                'icon_class' => 'building',
                'icon_color' => 'cadetblue',
                'parent_id' => "0"
            ],
                
            // Fun Relaxation category
            [
                'id' => '4',
                'name' => 'Fun, Relaxation and Outdoors',
                'icon_prefix' => "fa",
                'icon_class' => 'walking',
                'icon_color' => 'green',
                'parent_id' => "0"
            ],
        ]);
    }
}
