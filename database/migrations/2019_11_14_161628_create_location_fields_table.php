<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_name');
            $table->string('name');
            $table->text('description');
            $table->boolean('is_admin');
            $table->boolean('show_on_creation');
            $table->integer('group_id')->unsigned();
            $table->string('data_type')->default('text');
            $table->enum('sort_order', ['ASC', 'DESC']);
            $table->boolean('required')->default(0);
            $table->boolean('unique')->default(0);
            $table->boolean('locked')->default(0);
            $table->string('visible')->default('everyone');
            $table->boolean('map_form')->default(0);
            $table->string('default_data')->nullable();
            $table->longText('param_1')->nullable();
            $table->longText('param_2')->nullable();
            $table->longText('param_3')->nullable();
            $table->longText('param_4')->nullable();
            $table->longText('param_5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_fields');
    }
}
