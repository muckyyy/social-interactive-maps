<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('parent_id')->default(0);
            $table->string('icon_prefix')->nullable();
            $table->string('icon_class')->nullable();
            $table->string('icon_color')->nullable();
            $table->string('custom_icon')->nullable();
            $table->integer('category_lvl')->default(0);
            $table->integer('active')->default(1);
            $table->integer('force_child')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
