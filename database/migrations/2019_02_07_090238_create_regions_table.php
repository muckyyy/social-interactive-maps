<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country')->unsigned();
            $table->string('name')->nullable();
            $table->text('about')->nullable();
            $table->string('icon')->default('<i class="fas fa-city"></i>');
            $table->string('website')->nullable();
            $table->string('population')->nullable();
            $table->string('area')->nullable();
            $table->string('phone')->nullable();
            $table->multiPolygon('geometry');
            $table->timestamps();

           $table->index('country');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('regions', function (Blueprint $table)
        {
            $table->dropIndex(['country']);
           
        });
        Schema::dropIfExists('regions');
    }
}
