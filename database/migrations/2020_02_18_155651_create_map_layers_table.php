<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapLayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_layers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_name');
            $table->string('display_name');
            $table->text('description');
            $table->integer('sort_order');
            $table->boolean('active');
            $table->integer('param1')->nullable();
            $table->integer('param2')->nullable();
            $table->integer('param3')->nullable();
            $table->integer('param4')->nullable();
            $table->integer('param5')->nullable();
            $table->integer('param6')->default(20);
            $table->integer('param7')->default(5);
            $table->string('param8')->nullable();
            $table->integer('param9')->nullable();
            $table->integer('param10')->nullable();
            $table->integer('param11')->nullable();
            $table->integer('param12')->nullable();
            $table->integer('param13')->nullable();
            $table->integer('param14')->nullable();
            $table->integer('param15')->nullable();
            $table->integer('param16')->nullable();
            $table->integer('param17')->nullable();
            $table->integer('param18')->default(10);
            $table->integer('param19')->default(30);
            $table->integer('param20')->nullable();
            $table->integer('param21')->nullable();
            $table->integer('param22')->nullable();
            $table->integer('param23')->nullable();
            $table->string('param24')->nullable();
            $table->string('param25')->nullable();
            $table->text('param26')->nullable();
            $table->text('param27')->nullable();
            $table->text('param28')->nullable();
            $table->text('param29')->nullable();
            $table->text('param30')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_layers');
    }
}
