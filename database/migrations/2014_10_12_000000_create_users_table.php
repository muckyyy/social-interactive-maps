<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name');
            $table->string('full_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->text('about')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('profile_img')->nullable();
            $table->boolean('map_profile_display')->default(false);
            $table->integer('cover_img')->nullable();
            $table->string('type')->default('basic');
            $table->string('mode')->default('anonymous');
            $table->string('gender')->nullable();
            $table->integer('verified')->default(0);
            $table->point('currentlocation')->nullable();
            $table->point('displaylocation')->nullable();
            $table->string('last_message')->nullable();
            $table->integer('admin')->default(0);
            $table->integer('banned')->default(0);
            $table->text('session_id')->nullable()->default(null);
            $table->integer('city')->default(0);
            $table->integer('active_beacon')->nullable();
            $table->integer('home_beacon')->nullable();
            $table->integer('avatar_reset')->nullable();
            $table->integer('privacy')->default(1000);//meters
            $table->integer('reach')->default(5000); //meters
            $table->enum('auth', ['manual', 'email', 'oAuth'])->default('manual');
            $table->timestamp('last_action')->nullable();
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
