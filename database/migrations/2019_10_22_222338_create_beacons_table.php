<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeaconsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beacons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('city_id')->nullable();
            $table->point('geolocation')->nullable();
            $table->point('display_location')->nullable();
            $table->bigInteger('administrative_range')->default(7000);
            $table->bigInteger('beacon_range')->default(8000);
            $table->string('icon')->default('/img/beacons/default_beacon.png');
            $table->integer('icon_size_x')->default(80);
            $table->integer('icon_size_y')->default(110);
            $table->integer('icon_ancor_point_x')->default(31);
            $table->integer('icon_ancor_point_y')->default(64);
            $table->integer('allow_image_upload')->default(0);
            $table->integer('enable_places')->default(0);
            $table->integer('enable_events')->default(0);
            $table->integer('enable_groups')->default(0);
            $table->string('popup')->nullable();
            $table->integer('public')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beacons');
    }
}
