<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country')->unsigned();
            $table->integer('region')->nullable()->unsigned();
            $table->string('name')->nullable();
            $table->text('about')->nullable();
            $table->point('geopoint');
            
            $table->string('icon')->default('<i class="fas fa-city"></i>');
            $table->string('website')->nullable();
            $table->string('population')->nullable();
            $table->string('area')->nullable();
            $table->string('founded')->nullable();
            $table->string('phone')->nullable();
            $table->integer('active')->unsigned()->default(0);
            $table->multiPolygon('geometry')->nullable();
            $table->timestamps();

            $table->index('country');
            $table->index('region');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table)
        {
            $table->dropIndex(['country']);
            $table->dropIndex(['region']);
        });
        Schema::dropIfExists('cities');
    }
}
