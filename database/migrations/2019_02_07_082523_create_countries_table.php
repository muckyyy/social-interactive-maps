<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            
            //$table->foreign('currency')->references('id')->on('currencies');
            $table->string('name')->nullable();
            $table->string('country_code')->nullable();
            $table->string('population')->nullable();
            $table->string('area')->nullable();
            $table->string('callcode')->nullable();
            $table->string('website')->nullable();
            $table->string('founded')->nullable();
            $table->string('phone')->nullable();
            $table->integer('capitol')->default(0);
            $table->multiPolygon('geometry');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
