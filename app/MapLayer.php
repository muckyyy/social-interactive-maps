<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MapLayer extends Model
{
    

    // param1: icon_size_x
    // param2: icon_size_y
    // param3: initial_zoom
    // param4: min_zoom
    // param5: max_zoom
    // param6: max_icons
    // param7: cached_pages
    // param8: layer_type
    // param9: walk_duration
    // param10: post_show_duration
    // param11: post_age
    // param12: walk_length
    // param13: privacy_radius
    // param14: reach_radius
    // param15: icon_ancor_x
    // param16: icon_ancor_y
    // param17: avatar_refresh_position
    // param18: display_duration
    // param19: post_popup_duration
    // param20: number_of_quadrants
    // param21: icons_per_quadrant
    // param22: data_request_delay
    // param23: inactivity_time
    
    protected $guarded = [];
}
