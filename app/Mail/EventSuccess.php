<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class EventSuccess extends Mailable
{
    use Queueable, SerializesModels;
     
    /**
     * The email object instance.
     *
     * @var Email
     */
    public $email;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@avatarsmap.com')
                    ->view('mails.event_success')
                    ->text('mails.event_success_plain');
    }
}