<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MapPost extends Model
{
	
    protected $fillable = [
    	'profile_id',
    	'user_id',
    	'text',
    	'media',
    	'type',
    	'status',
    ];
}
