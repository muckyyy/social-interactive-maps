<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    
    protected $fillable = [
        'profile_id',
        'user_id',
        'text',
        'media',
        'status',
        'og_title',
        'og_description',
        'og_image',
        'og_url'
    ];

    public function comments()
    {
        return $this->hasMany('App\PostComment');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }
}
