<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Region extends Model
{
    

    protected $table = 'regions';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'name',
    	'country',
        'about',
        'website',
        'population',
        'area',
        'phone'
    ];
    /**
     * The attributes that are spatial fields.
     *
     * @var array
     */
    protected $spatialFields = [
        'geometry'
    ];
}
