<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class LocationImage extends Model
{
	

    protected $fillable = [
    	'location_id',
    	'user_id',
    	'image',
    	'status'
    ];
}
