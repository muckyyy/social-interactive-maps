<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminIcon extends Model
{
    protected $fillable = [
        'icon',
        'tag'
    ];
}
