<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Country extends Model
{
    
    protected $table = 'countries';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'name',
       
    	'country_code',
        'population',
        'area',
        'callcode',
        'website',
        'founded',
        'phone'
    ];
     /**
     * The attributes that are spatial fields.
     *
     * @var array
     */
    protected $spatialFields = [
        'geometry'
    ];
}
