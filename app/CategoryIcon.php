<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CategoryIcon extends Model
{

	 
	
    protected $fillable = [
    	'category_id',
    	'icon',
    	'status'
    ];
}
