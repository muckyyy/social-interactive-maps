<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;


class City extends Model
{
    use SpatialTrait;
    
    protected $table = 'cities';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'name',
    	'country',
    	'region',
        'about',
        'geopoint',
        'website',
        'population',
        'area',
        'founded',
        'phone'
    ];
    
     /**
     * The attributes that are spatial fields.
     *
     * @var array
     */
    protected $spatialFields = [
        'geometry',
        'geopoint'
    ];
   
}
