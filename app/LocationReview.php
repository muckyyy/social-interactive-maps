<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class LocationReview extends Model
{
	
    protected $fillable = [
        'location_id',
        'user_id',
        'rating',
        'review'
    ];
}
