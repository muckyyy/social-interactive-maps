<?php

namespace App\Console\Commands;

use App\City;
use App\User;
use App\Beacon;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class FakeAvatarsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:avatarData {amount=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fake Avatar locations and login time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('id', '!=', 1)->get()->random($this->argument('amount'));

        $user = User::find(1);

        foreach ($users as $u) {
            $city = City::find($user->city);
            $beacon = Beacon::where('city_id', $city->id)->first();
            //First lets generate random points for user in beacons radius, which will serve as actual location
            $ual = $this->generate_random_point(array($beacon->geolocation->getLat(),$beacon->geolocation->getLng()), 0, $beacon->range, true);
            //Now generate "display" location depending on newly generated actual location
            $udl = $this->generate_random_point(array($ual[0],$ual[1]), 0, $beacon->range, true);
            $loc = array($user->displaylocation->getLat(),$user->displaylocation->getLng());
            $coords = $this->generate_random_point($loc, 1000, 10000);

            $u->session_id = Session::getId();
            $u->currentlocation = new Point($ual[0], $ual[1]);
            $u->displaylocation = new Point($udl[0], $udl[1]);
            $u->active_beacon = $beacon->id;
            $u->last_action = Carbon::now();

            $u->save();
        }
    }

    public function rand_float($st_num=0, $end_num=1, $mul=1000000)
    {
        if ($st_num>$end_num) {
            return false;
        }
        return mt_rand($st_num*$mul, $end_num*$mul)/$mul;
    }

    public function generate_random_point($centre, $minradius, $maxradius, $randomradius = false)
    {
        $radius_earth = 6371; //km
        //Pick random distance within $distance;
        if ($randomradius && ($maxradius>$minradius)) {
            $distance = rand($minradius, $maxradius);
        } else {
            $distance = $minradius;
        }
        $distance = $distance/1000; //Transfering to kilometers
        //Convert degrees to radians.
        $centre_rads = array_map('deg2rad', $centre);
        //First suppose our point is the north pole.
        //Find a random point $distance miles away
        $lat_rads = (pi() / 2) - $distance / $radius_earth;
        $lng_rads = lcg_value() * 2 * pi();
        //($lat_rads,$lng_rads) is a point on the circle which is
        //$distance miles from the north pole. Convert to Cartesian
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);
        //Rotate that sphere so that the north pole is now at $centre.
        //Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];
        $rot = (pi() / 2) - $centre_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);
        //Rotate in z axis by $rot = $centre_rads[1]
        $rot = $centre_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;
        //Finally convert this point to polar co-ords
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);
        return array_map('rad2deg', array($lat_rads, $lng_rads));
    }
}
