<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class UpdateUserLocations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateuserlocations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This function updates user locations depending on their preferences. This is only false locations, actual locations are handled by user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereNotNull('currentlocation')->get();
        foreach($users as $u){
            $loc = array($u->currentlocation->getLat(),$u->currentlocation->getLng());
            $saveLoc = $this->generate_random_point($loc, $u->privacy,$u->reach,true);
            //dd($saveLoc);
            $u->displaylocation = new Point($saveLoc[0],$saveLoc[1]);
            $u->save();
        }
        echo 'Locations have been updated\n';
    }

    /**
    * Given a $centre (latitude, longitude) co-ordinates and a
    * distance $radius (miles), returns a random point (latitude,longtitude)
    * which is within $radius miles of $centre.
    *
    * @param  array $centre Numeric array of floats. First element is
    *                       latitude, second is longitude.
    * @param  float $radius The radius (in meters).
    * @return array         Numeric array of floats (lat/lng). First
    *                       element is latitude, second is longitude.
    */

    function generate_random_point($centre, $minradius,$maxradius,$randomradius = false)
    {
        $radius_earth = 6371; //km
        //Pick random distance within $distance;
        if ($randomradius && ($maxradius>$minradius)){
            $distance = rand($minradius,$maxradius);
        }
        else{
            $distance = $minradius;
        }
        $distance = $distance/1000; //Transfering to kilometers
        //Convert degrees to radians.
        $centre_rads = array_map('deg2rad', $centre);
        //First suppose our point is the north pole.
        //Find a random point $distance miles away
        $lat_rads = (pi() / 2) - $distance / $radius_earth;
        $lng_rads = lcg_value() * 2 * pi();
        //($lat_rads,$lng_rads) is a point on the circle which is
        //$distance miles from the north pole. Convert to Cartesian
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);
        //Rotate that sphere so that the north pole is now at $centre.
        //Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];
        $rot = (pi() / 2) - $centre_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);
        //Rotate in z axis by $rot = $centre_rads[1]
        $rot = $centre_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;
        //Finally convert this point to polar co-ords
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);
        return array_map('rad2deg', array($lat_rads, $lng_rads));
    }
}
