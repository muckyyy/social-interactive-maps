<?php

namespace App\Console\Commands;

use App\Post;
use App\User;
use App\Location;
use Illuminate\Console\Command;

class GenerateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:data {amount=200}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the data from available factories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        factory(User::class)->times($this->argument('amount'))->create();
        factory(Location::class)->times($this->argument('amount'))->create();
        factory(Post::class)->times(floor($this->argument('amount') / 3))->create();
        $this->info('Success! ' . $this->argument('amount') . ' records of data has been generated!');
    }
}
