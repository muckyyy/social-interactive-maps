<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Location extends Model
{
    use SpatialTrait;
    
    protected $table = 'locations';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'category_id',
        'parent_location_id',
        'city_id',
        'name',
        'icon',
        'description',
        'address',
        'postal_code',
        'phone_number',
        'rating',
        'website',
        'type',
        'location',
        'open',
        'closed'
    ];

    /**
    * The attributes that are spatial fields.
    *
    * @var array
    */
    public $spatialFields = [
        'location'
    ];

    public function category()
    {
        return $this->belongsTo('Category', 'id');
    }
}
