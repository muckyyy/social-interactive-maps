<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{

    protected $fillable = [
        'user_id',
        'image',
        'text',
        'type',
        'alert_location',
        'active',
        'duration_in_s'
    ];
}
