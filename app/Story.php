<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;


class Story extends Model
{
    use SpatialTrait;
    

    protected $fillable = [
        'user_id',
        'image',
        'text',
        'background',
        'type',
        'story_location',
        'duration_in_s',
        'active'
    ];

    protected $spatialFields = [
        'story_location'
    ];
}
