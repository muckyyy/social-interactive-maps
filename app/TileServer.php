<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TileServer extends Model
{
    
    protected $table = 'tile_servers';
    protected $primaryKey = 'id';
    protected $fillable = [
    	'name',
    	'domain',
        'ip',
        'token',
        'username',
        'pw',
        'tilesurl',
        'active',
        'maxusers'
    ];
}
