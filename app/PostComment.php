<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PostComment extends Model
{
	
    protected $fillable = [
    	'post_id',
    	'commentor_id',
    	'text',
    	'media',
    	'status',
    	'parent_id'
    ];
}
