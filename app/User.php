<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class User extends Authenticatable
{
    use Notifiable;
    use SpatialTrait;

    protected $table = 'users';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'user_name',
        'full_name',
        'date_of_birth',
        'profile_img',
        'cover_img',
        'type',
        'mode',
        'verified',
        'session_id',
        'gender',
        'currentlocation',
        'displaylocation',
        'privacy',
        'reach',
        'last_action'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $spatialFields = ['currentlocation', 'displaylocation'];

    public function posts()
    {
        return $this->hasMany('App\Post', 'user_id');
    }

    // TODO : get followers and other avatars
    public function followers()
    {
        return $this->hasMany('App\Followers', 'user_id');
    }
}
