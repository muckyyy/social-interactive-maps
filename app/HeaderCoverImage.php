<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HeaderCoverImage extends Model
{
    
    protected $table = 'header_cover_images';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
}