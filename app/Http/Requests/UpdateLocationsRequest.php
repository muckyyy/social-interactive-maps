<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLocationsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|max:100",
            "description" => "required",
            "category" => "required|integer",
            "parent_location" => "integer",
            "city" => "required|integer",
            "locationLat" => "required",
            "locationLng" => "required",
            "address" => "string",
            "post_code" => "string",
            "phone" => "required|regex:/^([0-9\s\-\+\(\)]*)$/|min:6",
            "website" => "string",
        ];
    }
}
