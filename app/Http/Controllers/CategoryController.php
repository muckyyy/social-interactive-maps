<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Input;
use App\Image;
use App\Category;
use App\Tag;
use App\CategoryCustomField;
use App\CategoryIcon;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function deleteCategory($id)
    {
        Category::where('id', $id)->delete();
        Tag::where('category_id', $id)->delete();
        // CategoryCustomField::where('category_id', $id)->delete();

        return redirect()->back();
    }

    public function addCustomField($id, Request $request)
    {
        $input = $request->all();

        //text
        if ($input['type'] == 'text') {
            $input['content'] = $input['text'];
        }

        //largeText
        if ($input['type'] == 'largeText') {
            $input['content'] = $input['largeText'];
        }

        //number
        if ($input['type'] == 'number') {
            $input['content'] = $input['number'];
        }

        //image
        if ($input['type'] == 'image') {
            $file = Input::file('image');
            $destinationPath = public_path().'/img/categories/';

            if ($file) {
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName1 = rand(11111, 99999).'.'.$extension;
                Input::file('image')->move($destinationPath, $fileName1);
                Image::make($destinationPath.$fileName1)->save($destinationPath.$fileName1);
                
                $input['content'] = $fileName1;
            }
        }

        $output = [];
        $output['category_id'] = $id;
        $output['name'] = $input['name'];
        $output['type'] = $input['type'];
        $output['content'] = $input['content'];

        CategoryCustomField::create($output);

        // return redirect('/admin-category/'.$id);
        return redirect('admin/category_management/category_add_continue/'.$id);
    }

    public function editCategory($id)
    {
        $user = Auth::user();
        $category = Category::find($id);
        $subCategories = Category::where('parent_id', $id)->get();
        // $customFields = CategoryCustomField::where('category_id', $id)->get();
        $parentCategory = Category::where('id', $category->parent_id)->first();
        $tags = Tag::where('category_id', $id)->get();

        $tagString = '';

        foreach ($tags as $tag) {
            $tagString .= '#'.$tag->name.' ';
        }

        // return view('admin.edit-category', compact('user','category','subCategories','customFields','parentCategory', 'tagString'));
        return view('admin-v2/category_management/category_edit/edit', compact('user', 'category', 'subCategories', /* 'customFields', */ 'parentCategory', 'tagString'));
    }

    public function deleteCustomField($id, $categoryId)
    {
        CategoryCustomField::where('id', $id)->delete();
        
        return redirect()->back();

        // return redirect('/admin-edit-category/'.$categoryId);
    }

    public function activeCategory($id, $status)
    {
        Category::where('id', $id)->update([
            'active' => $status
        ]);

        return redirect()->back();
    }

    public function changeCustomField($id, $categoryId)
    {
        $customField = CategoryCustomField::find($id);
        $user = Auth::user();
        // return view('admin.change-custom-field', compact('user','customField','categoryId'));
        return view('admin-v2/category_management/category_edit/custom_field', compact('user', 'customField', 'categoryId'));
    }

    public function changeCustomFieldPost($id, $category_id, Request $request)
    {
        $input = $request->all();

        //text
        if ($input['type'] == 'text') {
            $input['content'] = $input['text'];
        }

        //largeText
        if ($input['type'] == 'largeText') {
            $input['content'] = $input['largeText'];
        }

        //number
        if ($input['type'] == 'number') {
            $input['content'] = $input['number'];
        }

        //image
        if ($input['type'] == 'image') {
            $file = Input::file('image');
            $destinationPath = public_path().'/img/categories/';

            if ($file) {
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName1 = rand(11111, 99999).'.'.$extension;
                Input::file('image')->move($destinationPath, $fileName1);
                Image::make($destinationPath.$fileName1)->save($destinationPath.$fileName1);
                
                $input['content'] = $fileName1;
            }
        }

        $output = [];
        $output['category_id'] = $category_id;
        $output['name'] = $input['name'];
        $output['type'] = $input['type'];
        $output['content'] = $input['content'];

        CategoryCustomField::where('id', $id)->update($output);

        return redirect('/admin/category_management/admin-edit-category/'.$category_id);
    }

    public function changeCategoryBasics($id)
    {
        $user = Auth::user();
        $categories = Category::get();
        $category = Category::find($id);
        $tags = Tag::where('category_id', $id)->get();
        $tagString = '';

        foreach ($tags as $tag) {
            $tagString .= '#'.$tag->name.' ';
        }

        // return view('admin.change-category-basics', compact('user','category','categories','tagString','id'));
        return view('admin-v2.category_management/category_edit.edit_category_basics', compact('user', 'category', 'categories', 'tagString', 'id'));
    }

    public function changeCategoryBasicsPost($id, Request $request)
    {
        $category = Category::find($id);
        $file = Input::file('icon_file');
        
        $input = $request->all();

        if ($input['custom_icon'] || $file) {
            if ($file) {
                $destinationPath = public_path().'/img/icons/';
                $extension = Input::file('icon_file')->getClientOriginalExtension();
                $fileName1 = rand(11111, 99999).'.'.$extension;
                Input::file('icon_file')->move($destinationPath, $fileName1);
                Image::make($destinationPath.$fileName1)->fit(100, 100)->save($destinationPath.$fileName1);
                $imgIcon = "<img src='/img/icons/". $fileName1 ."'>";
                $input['custom_icon'] = $imgIcon;
                unset($input['icon_file']);
            }
        } else {
            $input['custom_icon'] = $category->custom_icon ?: '';
        }

        $tags = $input['tags'];
        $tags = str_replace(' ', '', $tags);
        $filteredTags = explode('#', $tags);
        array_shift($filteredTags);

        unset($input['_token']);
        unset($input['tags']);

        Tag::where('category_id', $id)->delete();

        Category::where('id', $id)->update($input);

        for ($i=0;$i<count($filteredTags);$i++) {
            Tag::create([
                'category_id' => $category->id,
                'name' => $filteredTags[$i]
            ]);
        }
        return redirect("/admin/category_management/admin-edit-category/$id");
    }

    public function adminIconsView($id)
    {
        $user = Auth::user();
        $category = Category::find($id);

        // return view('admin.categories.category-icons', compact('user','category','id'));
        return view('admin-v2.category_management.category_add.category_icons', compact('user', 'category', 'id'));
    }

    public function createAdminIcons(Request $request)
    {
        $input = $request->all();

        foreach ($input['iconSet'] as $key => $icon) {
            CategoryIcon::create([
                'category_id' => $input['id'],
                'icon' => $icon
            ]);
        }

        return 200;
    }

    public function getChildCategories(Request $request)
    {
        $id = $request->input('id');

        return $categories = Category::where('parent_id', $id)->get();
    }

    /*
    public function recursiveCategories($id){

        $categories = Category::where('parent_id', $id)->get();
        $categoriesId = [];

        if($categories){

            foreach($categories as $category){
                array_push($categoriesId, $category->id);
                array_push($categoriesId,$this->recursiveCategories($category->id));
            }

        }
        return $categoriesId;
    }
    */
}
