<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\City;
use App\Beacon;
use App\Location;
use App\LocationImage;
use App\Context;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Session;

class BeaconController extends Controller
{
    use UploadTrait;

    use SpatialTrait;
    protected $spatialFields = ['geolocation', 'display_location'];
    // Beacon settings
    public function index()
    {
        $user = Auth::user();
        $city = City::find($user->city);
        $beacons = Beacon::paginate(5);

        //locations
        $locations = Location::join('categories', 'locations.category_id', '=', 'categories.id')
            ->select('locations.*', 'categories.name as categoryName')
            ->get();


        foreach ($locations as $l) {
            $l['lat'] = $l->location->getLat();
            $l['lng'] = $l->location->getLng();
            $l['images'] = LocationImage::where('location_id', $l->id)->select('image')->get();
        }

        $user->displaylat = $user->displaylocation->getLat();
        $user->displaylng = $user->displaylocation->getLng();

        return view('admin-v2.beacon_administration.index', compact('user', 'beacons', 'city', 'locations'));
    }

    public function create()
    {
        $user = Auth::user();
        $cities = [];

        return view('admin-v2.beacon_administration.create', compact('user', 'cities'));
    }

    public function store(Request $request)
    {
        // TODO: check why session is not working on this page
        $validation = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'city_id' => 'required|integer',
            'icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'locationLat' => 'required',
            'locationLng' => 'required',
            'displayLocationLat' => 'required',
            'displayLocationLng' => 'required',
            'administrative_range' => 'required|integer',
            'beacon_range' => 'required|integer',
            'icon_size_x' => 'required|integer',
            'icon_size_x' => 'required|integer',
            'icon_ancor_point_x' => 'required|integer',
            'icon_ancor_point_y' => 'required|integer',
        ]);

        if ($request->has('icon')) {
            // Get image file
            $image = $request->file('icon');
            // Make a image name based on user name and current timestamp
            $name = Str::slug(Auth::user()->full_name).'_'.time();
            // Define folder path
            $folder = '/images/beacons/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
        }

        Beacon::create([
            'name' => $request->name,
            'description' => $request->description,
            'city_id' => $request->city_id,
            'geolocation' =>  new Point($request->locationLat, $request->locationLng),
            'display_location' =>  new Point($request->displayLocationLat, $request->displayLocationLng),
            'administrative_range' => $request->administrative_range,
            'beacon_range' => $request->beacon_range,
            'icon' => $filePath,
            'icon_size_x' => $request->icon_size_x ?: '0',
            'icon_size_y' => $request->icon_size_y ?: '0',
            'icon_ancor_point_x' => $request->icon_ancor_point_x ?: '0',
            'icon_ancor_point_y' => $request->icon_ancor_point_y ?: '0',
            'allow_image_upload' => $request->allow_image_upload == 'on' ? '1 ': '0',
            'popup' => $request->popup,
            'enable_places' => $request->enable_places == 'on' ? '1 ': '0',
            'enable_events' => $request->enable_events == 'on' ? '1 ': '0',
            'enable_groups' => $request->enable_groups == 'on' ? '1 ': '0',
            'public' => $request->public == 'on' ? '1 ': '0',
        ]);

        $beacon = Beacon::latest()->first();

        Context::create([
            // which context level ?
            'context_level' => 10,
            'instance_id' => $beacon->id,
            // what is path ?
            'path' => '/1/2'
        ]);

        return redirect()->route('beacon_administration.index')->withSuccess("Beacon <strong>$beacon->name</strong> created!");
    }

    // Update beacon
    public function update(Request $request, $id)
    {
        $validation = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'city_id' => 'required|integer',
        ]);
        
        Beacon::find($id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'city_id' => $request->city_id,
            'beacon_range' => $request->beacon_range,
            'administrative_range' => $request->administrative_range,
            'icon_size_x' => $request->icon_size_x ?: '0',
            'icon_size_y' => $request->icon_size_y ?: '0',
            'popup' => $request->popup,
            'icon_ancor_point_x' => $request->icon_ancor_point_x ?: '0',
            'icon_ancor_point_y' => $request->icon_ancor_point_y ?: '0',
            'allow_image_upload' => $request->allow_image_upload == 'on' ? '1 ' : '0',
            'enable_places' => $request->enable_places == 'on' ? '1 ' : '0',
            'enable_events' => $request->enable_events == 'on' ? '1 ' : '0',
            'enable_groups' => $request->enable_groups == 'on' ? '1 ' : '0',
            'public' => $request->public == 'on' ? '1 ' : '0',
        ]);

        if ($request->locationLat && $request->locationLng) {
            Beacon::find($id)->update([
                'geolocation' => new Point($request->locationLat, $request->locationLng),
            ]);
        }

        if ($request->displocationLat && $request->displocationLng) {
            Beacon::find($id)->update([
                'display_location' => new Point($request->displocationLat, $request->displocationLng),
            ]);
        }

        if ($request->icon && $request->has('icon')) {
            // Get image file
            $image = $request->file('icon');
            // Make a image name based on user name and current timestamp
            $name = Str::slug(Auth::user()->full_name).'_'.time();
            // Define folder path
            $folder = '/images/beacons/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);

            Beacon::find($id)->update([
                'icon' => $filePath
            ]);
        }

        $updatedBeacon = Beacon::latest('updated_at')->first();

        return redirect()->route('beacon_administration.index')->withSuccess("Beacon <strong>$updatedBeacon->name</strong> updated!");
    }

    // EDIT
    public function edit($id)
    {
        $user = Auth::user();
        $beacon = Beacon::find($id);

        return view('admin-v2.beacon_administration.edit', compact('user', 'beacon'));
    }

    // EDIT
    public function destroy($id)
    {
        $beacon = Beacon::find($id);
        $beaconName = $beacon->name;
        $context = Context::where('instance_id', $id)->first();

        $beacon->delete();
        $context->delete();

        return redirect()->back()->withSuccess("Beacon <strong>$beaconName</strong> deleted!");
    }

    public function local_beacons()
    {
        $user = Auth::user();
        $city = City::find($user->city);
        $beacons = Beacon::where('public', 0)->paginate(10);

        //locations
        $locations = Location::join('categories', 'locations.category_id', '=', 'categories.id')
            ->select('locations.*', 'categories.name as categoryName')
            ->get();


        foreach ($locations as $l) {
            $l['lat'] = $l->location->getLat();
            $l['lng'] = $l->location->getLng();
            $l['images'] = LocationImage::where('location_id', $l->id)->select('image')->get();
        }

        $user->displaylat = $user->displaylocation->getLat();
        $user->displaylng = $user->displaylocation->getLng();

        return view('admin-v2.beacon_administration.local_beacons', compact('user', 'beacons', 'city', 'locations'));
    }

    public function default_settings()
    {
        $beacons = Beacon::all();
        $user = Auth::user();
        return view('admin-v2.beacon_administration.default_settings', compact('user', 'beacons'));
    }
}
