<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Category;
use App\Location;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Http\Requests\StoreLocationsRequest;
use App\Http\Requests\UpdateLocationsRequest;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class LocationsController extends Controller
{
    use UploadTrait;

    public function index()
    {
        $user = Auth::user();
        $locations = Location::paginate(5);

        return view('admin-v2.locations.index', compact('user', 'locations'));
    }

    public function create()
    {
        return view('admin-v2.locations.create', ['user' => Auth::user()]);
    }

    public function store(StoreLocationsRequest $request)
    {
        if ($request->has('icon')) {
            // Get image file
            $image = $request->file('icon');
            // Make a image name based on user name and current timestamp
            $name = Str::slug(Auth::user()->full_name).'_'.time();
            // Define folder path
            $folder = '/images/locations/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
        }

        // TODO: MULTIPLE FILE UPLOAD

        Location::create([
            'category_id' => $request->category,
            'parent_location_id' => $request->parent_location,
            'city_id' => $request->city,
            'icon' => $filePath,
            'location' =>  new Point($request->locationLat, $request->locationLng),
            'name' => $request->name,
            'description' => $request->description,
            'address' => $request->address,
            'postal_code' => $request->post_code,
            'phone_number' => $request->phone,
            // 'images' => $request->images, //TODO
            'website' => $request->website,
            'type' => $request->type,
        ]);

        return redirect()->route('locations_management.index')->withSuccess("Location <strong>$request->name</strong> created!");
    }

    public function edit($id)
    {
        $location = Location::find($id);

        return view('admin-v2.locations.edit', ['user' => Auth::user(), 'location' => $location]);
    }

    public function update(UpdateLocationsRequest $request, $id)
    {
        $location = Location::find($id);

        $location->update([
            'category_id' => $request->category,
            'parent_location_id' => $request->parent_location,
            'city_id' => $request->city,
            'name' => $request->name,
            'description' => $request->description,
            'address' => $request->address,
            'postal_code' => $request->post_code,
            'phone_number' => $request->phone,
            // 'images' => $request->images, //TODO
            'website' => $request->website,
            'type' => $request->type,
        ]);

        if ($request->locationLat && $request->locationLng) {
            Location::find($id)->update([
                'geolocation' => new Point($request->locationLat, $request->locationLng),
            ]);
        }

        if ($request->icon && $request->has('icon')) {
            // Get image file
            $image = $request->file('icon');
            // Make a image name based on user name and current timestamp
            $name = Str::slug(Auth::user()->full_name).'_'.time();
            // Define folder path
            $folder = '/images/beacons/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);

            Location::find($id)->update([
                'icon' => $filePath
            ]);
        }

        return redirect()->route('locations_management.index')->withSuccess("Location <strong>$request->name</strong> updated!");
    }

    public function destroy($id)
    {
        Location::find($id)->delete();

        return redirect()->back()->withSuccess("Location  deleted!");
    }

    public function searchCities(Request $request)
    {
        $cities = City::where('name', 'like', '%' . $request->keyword . '%')->take(20)->get();

        return response()->json($cities);
    }

    public function checkDuplicates(Request $request)
    {
        $locations = Location::where('name', $request->keyword)->take(20)->get();

        return response()->json($locations);
    }

    public function searchLocations(Request $request)
    {
        $keyword = $request->keyword;

        $locations = DB::table('locations')
            ->where('locations.name', 'like', '%' . $keyword . '%')
            ->leftJoin('cities', 'locations.city_id', '=', 'cities.id')
            ->select('locations.id', 'locations.icon', 'locations.name', 'cities.name as city')
            ->take(20)
            ->get();

        return response()->json($locations);
    }

    public function searchLocationsProfile(Request $request)
    {
        $bounds = json_decode($request->bounds);
        $southWestBounds = $bounds->_southWest;
        $northEastBounds = $bounds->_northEast;
        $keyword = $request->keyword;

        $locationQuery = 'SELECT locations.id, locations.category_id, locations.icon, locations.name, locations.description, locations.rating, locations.address, locations.website, locations.phone_number, Y(locations.location) as lat, X(locations.location) as lng, categories.icon_prefix, categories.icon_class, categories.icon_color
                        FROM locations
                        LEFT JOIN categories
                        ON locations.category_id = categories.id
                        WHERE locations.name LIKE "%'.$keyword.'%"
                        AND Y(locations.location) > ' . $southWestBounds->lat .
                        ' AND Y(locations.location) < '. $northEastBounds->lat .
                        ' AND X(locations.location) > '. $southWestBounds->lng .
                        ' AND X(locations.location) < '. $northEastBounds->lng . ' limit 0,50 ';

        $locations = collect(DB::select($locationQuery));

        return response()->json($locations);
    }

    public function deleteIcon(Location $location)
    {
        $location->icon = null;
        $location->save();

        return response()->json('Location Icon Deleted!');
    }
}
