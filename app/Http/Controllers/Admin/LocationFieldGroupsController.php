<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\LocationFieldGroup;

class LocationFieldGroupsController extends Controller
{
    // show location field groups views
    public function index()
    {
        $user = Auth::user();
        $locationFieldGroups = LocationFieldGroup::orderBy('ordering', 'asc')->paginate(10);
        $maxOrder = LocationFieldGroup::max('ordering');
        $minOrder = LocationFieldGroup::min('ordering');

        return view('admin-v2.category_management.location_fields_groups.index', compact('user', 'locationFieldGroups', 'minOrder', 'maxOrder'));
    }

    // show create a location field group view
    public function create_location_fields_groups()
    {
        $user = Auth::user();

        return view('admin-v2.category_management.location_fields_groups.create', compact('user'));
    }

    // create a new location field group
    public function create_new_location_field_group(Request $request)
    {
        $user = Auth::user();

        $request->validate([
            'name' => 'required|string|unique:location_field_groups'
        ]);

        if ($request->visible || $request->visible == 'on') {
            $visible = 1;
        } else {
            $visible = 0;
        }

        $locationFieldGroup = new LocationFieldGroup;
        $locationFieldGroup->name = $request->name;
        $locationFieldGroup->visible = $visible;

        if (!LocationFieldGroup::first()) {
            $locationFieldGroup->ordering = 0;
        } else {
            $locationFieldGroup->ordering = LocationFieldGroup::max('ordering') + 1;
        }

        $locationFieldGroup->save();

        $message = 'Location field group <strong>' . $locationFieldGroup->name . '</strong> created!';

        return redirect()->back()->withSuccess($message);
    }

    // show edit view of a location field group
    public function edit($id)
    {
        $user = Auth::user();
        $locationFieldGroup = LocationFieldGroup::find($id);

        return view('admin-v2.category_management.location_fields_groups.edit', compact('user', 'locationFieldGroup'));
    }

    // update a location field group
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|unique:location_field_groups'
        ]);

        if ($request->visible || $request->visible == 'on') {
            $visible = 1;
        } else {
            $visible = 0;
        }

        $locationFieldGroup = LocationFieldGroup::find($id);
        $locationFieldGroup->name = $request->name;
        $locationFieldGroup->visible = $visible;

        $locationFieldGroup->save();

        $message = "Location field group <strong>$locationFieldGroup->name</strong> edited successfully!";

        return redirect('admin/category_management/location_fields_groups')->withSuccess($message);
    }

    // delete a location field group
    public function delete_location_field_group($id)
    {

        $locationFieldGroup = LocationFieldGroup::find($id);
        $locationFieldGroup->delete();

        $message = "Location field group <strong>$locationFieldGroup->name</strong> deleted!";

        return redirect()->back()->withSuccess($message);
    }

    // change ordering of location field groups
    public function change_ordering(Request $request, $id) {
        $location = LocationFieldGroup::find($id);

        if( $request['reorder'] == 'up') {
            $reorderLocation = LocationFieldGroup::where('ordering', $location->ordering - 1)->first();
            if($reorderLocation) {
                $newReorder = $reorderLocation->ordering;
                $reorderLocation->ordering = $location->ordering;
                $reorderLocation->save();
                $location->ordering = $newReorder;
                $location->save();
            }
            else {
                $location->ordering = $location->ordering - 1;
                $location->save();
            }
        } else {
            $reorderLocation = LocationFieldGroup::where('ordering', $location->ordering + 1)->first();
            if ($reorderLocation) {
                $newReorder = $reorderLocation->ordering;
                $reorderLocation->ordering = $location->ordering;
                $reorderLocation->save();
                $location->ordering = $newReorder;
                $location->save();
            } else {
                $location->ordering = $location->ordering + 1;
                $location->save();
            }
        }

        return redirect()->back()->withSuccess("Ordering of <strong>$location->name</strong> changed!");
    }
    // toggle visibility of location field group
    public function change_visibility(Request $request ,$id)
    {
        $locationFieldGroup = LocationFieldGroup::find($id);

        $locationFieldGroup->visible = $request->visible;
        $locationFieldGroup->save();

        $locationFieldGroup->visible ? $status = 'on' : $status = 'off';

        return redirect()->back()->withSuccess("Location field group <strong>$locationFieldGroup->name</strong> visibility turned <strong>$status</strong>");
    }
}
