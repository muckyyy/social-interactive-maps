<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LocationField;
use Auth;
use Illuminate\Validation\Rule;

class LocationFieldsController extends Controller
{
    public function index() {
        $locations = LocationField::paginate(10);
        $user = Auth::user();

        return view('admin-v2.category_management.location_fields.index', compact('user', 'locations'));
    }

    // change visibility of location fields
    public function change_visibility($id)
    {
        $locationField = LocationField::where('id', $id)->first();

        if ($locationField->visible) {
            $locationField->visible = 0;
        } else {
            $locationField->visible = 1;
        }

        $locationField->save();

        return redirect()->back()->withSuccess('Changed visibility of location field <strong>' . $locationField->name . '</strong>');
    }

    // delete location fields
    public function delete($id)
    {
        $locationField = LocationField::where('id', $id)->first();
        $locationField->delete();

        return redirect()->back()->withSuccess('Deleted location field <strong>' . $locationField->name . '</strong>');
    }

    // create view
    public function create()
    {
        $user = Auth::user();
        $field_types = [
            'text', 'text_area', 'radio', 'checkbox', 'select', 'date', 'map', 'file'
        ];
        return view('admin-v2.category_management.location_fields.create', compact('user', 'field_types'));
    }

    // create continue view
    public function create_continue(Request $request)
    {
        $type = $request->type;
        $user = Auth::user();

        return view('admin-v2.category_management.location_fields.create_continue', compact('user', 'type'));
    }
    

    // store
    public function store(Request $request)
    {
        LocationField::create($request->validate([
            "short_name" => "required|string|unique:location_fields",
            "name" => "required|string",
            "description" => "required|min:3|max:1000",
            "is_admin" => "required|integer",
            "show_on_creation" => "required|integer",
            "group_id" => "required|integer",
            "sort_order" => "required|in:ASC,DESC",
            "required" => "required|integer",
            "unique" => "required|integer",
            "locked" => "required|integer",
            "visible" => "required|string",
            "data_type" => "required|string",
            "map_form" => "required|integer"
        ]));

        return redirect()->route('location_fields.index')->withSuccess("New location field created!");
    }

    // edit view
    public function edit($id) {
        return view('admin-v2.category_management.location_fields.edit', ['locationField' => LocationField::find($id), 'user' => Auth::user()]);
    }

    // update
    public function update(Request $request, $id) {
        LocationField::find($id)->update($request->validate([
            'short_name' => [
                'required',
                Rule::unique('location_fields')->ignore($id),
            ],
            "name" => "required|string",
            "description" => "required|min:3|max:1000",
            "is_admin" => "required|integer",
            "show_on_creation" => "required|integer",
            "group_id" => "required|integer",
            "sort_order" => "required|in:ASC,DESC",
            "required" => "required|integer",
            "unique" => "required|integer",
            "locked" => "required|integer",
            "visible" => "required|string",
            "data_type" => "required|string",
            "map_form" => "required|integer" 
        ]));

        return redirect()->route('location_fields.index')->withSuccess("Location field <strong>" . LocationField::find($id)->name . " </strong>updated!");
    }
}
