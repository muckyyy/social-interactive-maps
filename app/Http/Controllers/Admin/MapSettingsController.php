<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\MapLayer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MapSettingsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $map_layers = DB::table('map_layers')->select('id', 'short_name', 'display_name', 'description', 'sort_order', 'active', 'param1 as icon_size_x', 'param2 as icon_size_y', 'param3 as initial_zoom', 'param4 as min_zoom', 'param5 as max_zoom', 'param6 as max_icons', 'param7 as cached_pages', 'param8 as layer_type', 'param9 as walk_duration', 'param10 as post_show_duration', 'param11 as post_age', 'param12 as walk_length', 'param13 as privacy_radius', 'param14 as reach_radius', 'param15 as icon_ancor_x', 'param16 as icon_ancor_y', 'param17 as avatar_refresh_position', 'param18 as display_duration', 'param19 as post_popup_duration', 'param20 as number_of_quadrants', 'param21 as icons_per_quadrant', 'param22 as data_request_delay', 'param23 as inactivity_time', 'param24', 'param25', 'param26', 'param27', 'param28', 'param29', 'param30')->get();

        return view('admin-v2.map_settings.index', compact('user', 'map_layers'));
    }

    public function toggle_layer($id)
    {
        $layer = MapLayer::find($id);

        if ($layer->active == 1) {
            $active = 0;
            $status = 'inactive';
        } else {
            $active = 1;
            $status = 'active';
        }

        $layer->update(['active' => $active]);

        return redirect()->back()->withSuccess("$layer->display_name toggled to $status");
    }

    public function toggle_active($id)
    {
        $layer = MapLayer::find($id);

        if ($layer->active == 1) {
            $active = 0;
            $status = 'inactive';
        } else {
            $active = 1;
            $status = 'active';
        }

        $layer->update(['active' => $active]);

        return response()->json([
            'success' => 200,
            'message' => "$layer->display_name toggled to $status",
            'layer' => $layer->display_name,
            'action' => $status
        ]);
    }

    public function settings_layer()
    {
        $user = Auth::user();
        $layer = MapLayer::where('short_name', 'settings_layer')->first();
        return view('admin-v2.map_settings.settings_layer', compact('user', 'layer'));
    }

    public function store_settings_layer(Request $request)
    {
        $layer = MapLayer::where('short_name', $request->short_name)->first();
        $layer->update([
            "short_name" => $request->short_name,
            "display_name" => $request->display_name,
            "description" => $request->description,
            "param23" => $request->inactivity_time,
            'param13' => $request->privacy_radius,
            'param14' => $request->reach_radius,
            'param3' => $request->initial_zoom,
            'param1' =>  $request->icon_size_x,
            'param2' => $request->icon_size_y,
            'param15' => $request->icon_ancor_x,
            'param16' => $request->icon_ancor_y,
            'param17' => $request->avatar_refresh_position,
        ]);

        return redirect()->back()->withSuccess('Settings Map Layer Updated!');
    }

    public function friends_avatars_layer()
    {
        $user = Auth::user();
        $layer = MapLayer::where('short_name', 'friends_avatars_layer')->first();
        return view('admin-v2.map_settings.friends_avatars_layer', compact('user', 'layer'));
    }

    public function store_friends_avatars_layer(Request $request)
    {
        $layer = MapLayer::where('short_name', $request->short_name)->first();
        $layer->update([
            "short_name" => $request->short_name,
            "display_name" => $request->display_name,
            "description" => $request->description,
            "param4" => $request->active_zoom_range_min,
            "param5" => $request->active_zoom_range_max,
            "param1" => $request->icon_size_x,
            "param2" => $request->icon_size_y,
            "param15" => $request->icon_ancor_x,
            "param16" => $request->icon_ancor_y,
            "param18" => $request->display_duration,
            "param6" => $request->max_icons,
            "param7" => $request->cached_pages,
        ]);

        return redirect()->back()->withSuccess('Friends Avatars Layer Updated!');
    }

    public function other_avatars_layer()
    {
        $user = Auth::user();
        $layer = MapLayer::where('short_name', 'other_avatars_layer')->first();
        return view('admin-v2.map_settings.other_avatars_layer', compact('user', 'layer'));
    }

    public function store_other_avatars_layer(Request $request)
    {
        $layer = MapLayer::where('short_name', $request->short_name)->first();
        $layer->update([
            "short_name" => $request->short_name,
            "display_name" => $request->display_name,
            "description" => $request->description,
            "param4" => $request->active_zoom_x,
            "param5" => $request->active_zoom_y,
            "param1" => $request->icon_size_x,
            "param2" => $request->icon_size_y,
            "param15" => $request->anchorPositionX,
            "param16" => $request->anchorPositionY,
            "param9" => $request->walk_duration,
            "param12" => $request->walk_length,
            "param10" => $request->post_show_duration,
            "param11" => $request->post_age,
            "param18" => $request->displayDuration,
            "param6" => $request->max_icons,
            "param7" => $request->cached_pages,
        ]);

        return redirect()->back()->withSuccess('Other Avatars Layer Updated!');
    }

    public function locations_layer()
    {
        $layer = MapLayer::where('short_name', 'locations_layer')->first();

        $user = Auth::user();
        return view('admin-v2.map_settings.locations_layer', compact('user', 'layer'));
    }

    public function store_locations_layer(Request $request)
    {
        $layer = MapLayer::where('short_name', $request->short_name)->first();
        $layer->update([
            "short_name" => $request->short_name,
            "display_name" => $request->display_name,
            "description" => $request->description,
            "param4" => $request->active_zoom_min,
            "param5" => $request->active_zoom_max,
            "param14" => $request->reach_radius,
            "param20" => $request->number_of_quadrants,
            "param21" => $request->icons_per_quadrant,
            "param1" => $request->icon_size_x,
            "param2" => $request->icon_size_y,
            "param15" => $request->icon_ancor_x,
            "param16" => $request->icon_ancor_y,
            "param22" => $request->data_request_delay,
            "param7" => $request->cached_pages,
        ]);

        return redirect()->back()->withSuccess('Locations Layer Updated!');
    }

    public function beacon_layer()
    {
        $layer = MapLayer::where('short_name', 'beacons_layer')->first();

        $user = Auth::user();
        return view('admin-v2.map_settings.beacons_layer', compact('user', 'layer'));
    }

    public function store_beacon_layer(Request $request)
    {
        $layer = MapLayer::where('short_name', 'beacons_layer')->first();

        $layer->update([
            "short_name" => $request->short_name,
            "display_name" => $request->display_name,
            "description" => $request->description,
            "param4" => $request->zoom_range_min,
            "param5" => $request->zoom_range_max,
            "param6" => $request->max_icons,
        ]);

        return redirect()->back()->withSuccess('Beacons Layer Updated!');
    }

    //Guest User Map Layers
    public function guest_map_layers()
    {
        $user = Auth::user();
        $map_layers = MapLayer::where('param8', 'guest_map')->get();
        return view('admin-v2.map_settings.guest_layer.index', compact('user', 'map_layers'));
    }

    public function avatars_layer()
    {
        $user = Auth::user();
        return view('admin-v2.map_settings.guest_layer.avatars_layer', compact('user'));
    }

    public function guest_locations_layer()
    {
        $user = Auth::user();
        return view('admin-v2.map_settings.guest_layer.locations_layer', compact('user'));
    }

    // Other Settings
    public function other_settings()
    {
        $user = Auth::user();
        return view('admin-v2.map_settings.other_settings.index', compact('user'));
    }
}
