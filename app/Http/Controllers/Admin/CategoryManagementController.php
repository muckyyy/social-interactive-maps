<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Input;
use App\Tag;
use App\Image;
use App\Category;
use App\Location;
use App\CategoryIcon;
use App\CategoryFieldGroup;
use App\LocationFieldGroup;
use App\CategoryCustomField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryManagementController extends Controller
{
    public function category_management()
    {
        $user = Auth::user();
        $categories = Category::paginate(10);

        return view('admin-v2.category_management.category_management', compact('user', 'categories'));
    }

    public function addCategoryToGroups(Request $request)
    {
        $request->validate([
            'group_id' => 'required|integer',
            'category_id' => 'required|integer'
        ]);

        $group = LocationFieldGroup::find($request->group_id)->name;
        $category = Category::find($request->category_id);
       
        CategoryFieldGroup::create([
            'title' =>  $category->name . '/' . LocationFieldGroup::find($request->group_id)->name,
            'group_id' => $request->group_id,
            'category_id' => $request->category_id
        ]);

        return redirect()->back()->withSuccess("Location field group <strong>$group</strong> added to <strong>$category->name</strong> category!");
    }

    // add new category view
    public function category_add()
    {
        $user = Auth::user();
        $categories = Category::where('parent_id', 0)->paginate(20);
        return view('admin-v2.category_management.category_add.category_add', compact('user', 'categories'));
    }

    // Add Category
    public function addCategory(Request $request)
    {
        $request->validate([
            'name' => 'string|unique:categories',
            'parent_id' => 'required|integer'
        ]);

        $input = $request->all();

        if (!isset($input['force_child'])) {
            $input['force_child'] = 0;
        }

        $destinationPath = public_path() . '/img/icons/';
        $file = Input::file('icon_file');

        if ($file) {
            $extension = Input::file('icon_file')->getClientOriginalExtension();
            $fileName1 = rand(11111, 99999) . '.' . $extension;
            Input::file('icon_file')->move($destinationPath, $fileName1);
            Image::make($destinationPath . $fileName1)->fit(100, 100)->save($destinationPath . $fileName1);
            $imgIcon = "<img src='/img/icons/" . $fileName1 . "'>";
            $input['icon'] = $imgIcon;
        }

        $tags = $input['tags'];
        $tags = str_replace(' ', '', $tags);
        $filteredTags = explode('#', $tags);
        array_shift($filteredTags);

        
        $category = Category::create($input);
        
       

        for ($i = 0; $i < count($filteredTags); $i++) {
            Tag::create([
                'category_id' => $category->id,
                'name' => $filteredTags[$i]
            ]);
        }

        return redirect('/admin/category_management/category_add_continue/' . $category->id);
    }

    // category add continuation
    public function category_add_continue($id)
    {
        $user = Auth::user();
        $category = Category::find($id);
        $categoryParent = Category::find($category->parent_id);
        $parentGroups = [];
        $parentGroupsIds = [];

        if (!$category->parent_id == 0) {
            $groups = CategoryFieldGroup::where('category_id', $categoryParent->id)->get();
            foreach ($groups as $group) {
                array_push($parentGroups, LocationFieldGroup::where('id', $group->group_id)->first());
                array_push($parentGroupsIds, LocationFieldGroup::where('id', $group->group_id)->first()->id);
            }

            // inherit parent category's groups
            foreach ($parentGroups as $group) {
                $categoryFieldGroup = CategoryFieldGroup::where('category_id', $category->id)->where('group_id', $group->id)->first();
                if (!$categoryFieldGroup) {
                    CategoryFieldGroup::create([
                        'title' => $category->name . '/' . $group->name,
                        'group_id' => $group->id,
                        'category_id' => $category->id
                    ]);
                }
            }

            // remove parent's location fields from dropdown(make them disabled)
            $categoryLocationFields =  CategoryFieldGroup::where('category_id', $id)->get();
            foreach ($categoryLocationFields as $field) {
                array_push($parentGroups, LocationFieldGroup::where('id', $field->group_id)->first());
                array_push($parentGroupsIds, LocationFieldGroup::where('id', $field->group_id)->first()->id);
            }

            $parentGroups = array_unique($parentGroups);

            $locationFieldGroups = LocationFieldGroup::whereNotIn('id', $parentGroupsIds)->get();
        } else {
            $existingLocationField = CategoryFieldGroup::where('category_id', $category->id)->get()->pluck('group_id');
            $locationFieldGroups = LocationFieldGroup::whereNotIn('id', $existingLocationField)->get();
        }
        if (!count($locationFieldGroups)) {
            return redirect()->route('category_management')->withSuccess("No more location field groups to include for category <strong>$category->name</strong>.");
        } else {
            return view('admin-v2.category_management.category_add.category_add_continue', compact('user', 'locationFieldGroups', 'parentGroups', 'category'));
        }
    }
   
    // add location field type
    public function add_location_field_type(Request $request, $id)
    {
        $user = Auth::user();

        $request->validate([
            'type' => 'required|string'
        ]);

        $type = $request->type;

        return view('admin-v2.category_management.category_add.location_field.location_field_details', compact('user', 'id', 'type'));
    }

    // location field details
    public function location_field_details(Request $request, $id)
    {
        // $request->validate([
        //     'type' => 'required|string'
        // ]);

        dd($request->all(), $id);

        $user = Auth::user();

        return view('admin-v2.category_management.category_add.location_field.location_field_details', compact('user', 'id', 'type'));
    }

    public function searchCategories(Request $request)
    {
        $categories = Category::where('name', 'like', '%' . $request->keyword . '%')->take(20)->get();

        return response()->json($categories);
    }
}
