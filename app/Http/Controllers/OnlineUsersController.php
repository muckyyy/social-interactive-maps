<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OnlineUsersController extends Controller
{
    public function __invoke()
    {
        return response()->json(['users' => User::where('last_action', '>', Carbon::now()->subMinutes(30))->select('id', 'user_name', 'profile_img')->get()]);
    }
}
