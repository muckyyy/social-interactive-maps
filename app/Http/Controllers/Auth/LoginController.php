<?php

namespace App\Http\Controllers\Auth;

use App\Beacon;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\City;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';
   
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
  
    protected function sendLoginResponse($request)
    {
        $request->session()->regenerate();
        $previous_session = Auth::User()->session_id;
       
        if ($previous_session) {
            Session::getHandler()->destroy($previous_session);
        }

        $city = City::find(Auth::user()->city);
        $beacon = Beacon::where('city_id', $city->id)->first();
        $coords = new Point($beacon->geolocation->getLat(), $beacon->geolocation->getLng());

        $user = Auth::user();
        $user->session_id = Session::getId();
        $user->currentlocation = $coords;
        $user->active_beacon = $beacon->id;

        $user->save();

        $this->clearLoginAttempts($request);

        //    $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return $this->authenticated($request, $this->guard()->user())
           ?: redirect()->intended($this->redirectPath());
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        return $user->token;
    }
}
