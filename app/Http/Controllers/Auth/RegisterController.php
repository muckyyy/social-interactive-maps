<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Country;
use App\City;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_name' => 'required|string|unique:users',
            'full_name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'country' => 'required|integer',
            'city_id' => 'required|integer',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $city = City::find($data['city_id']);
        $coords = array($city->geopoint->getLat(),$city->geopoint->getLng());
        $displayloc = $this->generate_random_point($coords, 1);
        $displaylocsave = new Point($displayloc[0], $displayloc[1]);
        $savecoords = new Point($city->geopoint->getLat(), $city->geopoint->getLng());

        $user = new User();
        $user->user_name = $data['user_name'];
        $user->full_name = $data['full_name'];
        $user->currentlocation = $savecoords;
        $user->displaylocation = $displaylocsave;
        $user->email = $data['email'];
        $user->city = $data['city_id'];
        $user->password = bcrypt($data['password']);
        $user->session_id = Session::getId();
        $user->save();
        // $accessToken = $user->createToken('authToken')->accessToken;
        
        return $user;
    }

    public function showRegistrationForm(Request $request)
    {
        // TODO: enable landign on register route once we are ready for registration
        if (config('app.env') !== 'production' || app()->environment() !== 'production') {
            $countries = Country::get();
            $cities = City::where('name', 'like', '%' . $request->keyword . '%')->get();
            return view("auth.login", compact("countries", "cities"));
        } else {
            return redirect()->back();
        }
    }

    /**
    * Given a $centre (latitude, longitude) co-ordinates and a
    * distance $radius (miles), returns a random point (latitude,longtitude)
    * which is within $radius miles of $centre.
    *
    * @param  array $centre Numeric array of floats. First element is
    *                       latitude, second is longitude.
    * @param  float $radius The radius (in kilometers).
    * @param  float $maxradius The max radius (in kilometers).
    * @return array         Numeric array of floats (lat/lng). First
    *                       element is latitude, second is longitude.
    */

    public function generate_random_point($centre, $minradius, $maxradius = 0, $randomradius = false)
    {
        $radius_earth = 6371; //km
        //Pick random distance within $distance;
        if ($randomradius && ($maxradius>$minradius)) {
            $distance = rand($minradius, $maxradius);
        } else {
            $distance = $minradius;
        }

        //Convert degrees to radians.
        $centre_rads = array_map('deg2rad', $centre);
        //First suppose our point is the north pole.
        //Find a random point $distance miles away
        $lat_rads = (pi() / 2) - $distance / $radius_earth;
        $lng_rads = lcg_value() * 2 * pi();
        //($lat_rads,$lng_rads) is a point on the circle which is
        //$distance miles from the north pole. Convert to Cartesian
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);
        //Rotate that sphere so that the north pole is now at $centre.
        //Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];
        $rot = (pi() / 2) - $centre_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);
        //Rotate in z axis by $rot = $centre_rads[1]
        $rot = $centre_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;
        //Finally convert this point to polar co-ords
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);
        return array_map('rad2deg', array($lat_rads, $lng_rads));
    }
}
