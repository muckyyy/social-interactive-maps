<?php

namespace App\Http\Controllers;

use App\Pin;
use App\City;
use App\Post;
use App\User;
use App\Beacon;
use App\Friend;
use App\Country;
use App\Category;
use App\Follower;
use App\Location;
use App\MapLayer;
use App\LocationImage;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['guestIndex','getcitiesregister']]);
    }

    public function getjs()
    {
        $user = Auth::user();
        $script = 'var host = \'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png\'';
    }

    public function getcities($lat, $long, $zoomlvl, Request $request)
    {
        $rad = 50;
        $sql = "SELECT *,X(geopoint) as Lng, Y(geopoint) as Lat FROM `cities` WHERE
            SQRT(POW(Y(`geopoint`) - $lat , 2) + POW(X(`geopoint`) - $long, 2)) * 100 < 500 and active = 1"; //Active cities in given radius
        $data = DB::select($sql);
        $cities = array();
        foreach ($data as $d) {
            $cities[] = array('id'=>$d->id,'name'=>$d->name,'icon'=>url('/img/cities/8200/sarajevopop.png'),'lng'=>$d->Lng,'lat'=>$d->Lat);
        }
        echo json_encode($cities);
    }
    /*
     * Function that loads other users avatars
     * This function is empty if user is watching too far away from his current location
     * Current location can be his actual location or city location (we are checking again location saved in database)
     * @param $lat - current view latitude
     * @param $long - Current view longitude
     * $param $zoomlvl  - Current view zoom
     * $paramt Request $request - Current route request
     *
     * $return array() of avatars with all data
     */

    

    /*
     * Function that returns current settings for logged in user
     * retrun json
     */
    public function getsettings()
    {
        $user = Auth::user();

        $return['currentloc'] = array($user->currentlocation->getLat(),$user->currentlocation->getLng());
        $return['reachradius'] = $user->reach;
        $return['privacyradius'] = $user->privacy;

        return response()->json($return);
    }

    /*
     * Function that updates current location for logged in user
     */
    public function updatelocation(Request $request)
    {
        $input = $request->all();
        $user = Auth::user();

        $user->currentlocation = new Point($input['latitude'], $input['longitude']);
        $saveLoc = $this->generate_random_point(array($input['latitude'],$input['longitude']), $user->privacy, $user->reach, true);
        $user->displaylocation = new Point($saveLoc[0], $saveLoc[1]);
        $user->active_beacon = null;
        $user->save();

        $return['currentloc'] = array($user->currentlocation->getLat(),$user->currentlocation->getLng());
        $return['displayloc'] = array($user->displaylocation->getLat(), $user->displaylocation->getLng());
        $return['reachradius'] = $user->reach;
        $return['privacyradius'] = $user->privacy;

        echo json_encode($return);
    }

    /*
     * Function that updates current location for logged in user
     */
    public function simulatedots(Request $request)
    {
        $input = $request->all();
        $user = Auth::user();
       
        //Lets generate another display location
        $displayloc = $this->generate_random_point(array($user->currentlocation->getLat(),$user->currentlocation->getLng()), $user->privacy, $user->reach, false);
        //Now lets generate random walk start location, at 200 m distance
        $walkloc = $this->generate_random_point(array($displayloc[0],$displayloc[1]), 200, 0, false);
        

        $return['actuallocation'] = array($user->currentlocation->getLat(),$user->currentlocation->getLng());
        $return['displayloc'] = array($displayloc[0], $displayloc[1]);

        $return['walktstart'] = array($walkloc[0], $walkloc[1]);
        

        echo json_encode($return);
    }

    /*
     * Function that updates privacy and reach radius
     */
    public function saveSettings(Request $request)
    {
        $user = Auth::user();
        //Validation to be done here
        // ... code it in
        //And finally save data
        $user->privacy = $request->privacy_radius;
        $user->reach = $request->reach_radius;
        $user->save();

        return response()->json(array('message'=>'success'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // TODO: login of which beacons to show to the user
        $beacons = Beacon::take(10)->get();

        $users = User::whereNotNull('currentlocation')->with(['posts' => function ($query) {
            $query->where('updated_at', '>', Carbon::now()->subMinutes(60));
        } ])->take(20)->get();

        $user = Auth::user();

        $loc = array($user->currentlocation->getLat(),$user->currentlocation->getLng());
        $saveLoc = $this->generate_random_point($loc, $user->privacy, $user->reach);
        $user->displaylocation = new Point($saveLoc[0], $saveLoc[1]);
        $user->save();

        $userPost = $user->posts()->first();
        
        if ($userPost && $userPost->updated_at > Carbon::now()->subMinutes(60) == false) {
            $userPost = null;
        }
        
        $city = City::find($user->city);

        $map_layers = DB::table('map_layers')->select('id', 'short_name', 'display_name', 'description', 'sort_order', 'active', 'param1 as icon_size_x', 'param2 as icon_size_y', 'param3 as initial_zoom', 'param4 as min_zoom', 'param5 as max_zoom', 'param6 as max_icons', 'param7 as cached_pages', 'param8 as layer_type', 'param9 as walk_duration', 'param10 as post_show_duration', 'param11 as post_age', 'param12 as walk_length', 'param13 as privacy_radius', 'param14 as reach_radius', 'param15 as icon_ancor_x', 'param16 as icon_ancor_y', 'param17 as avatar_refresh_position', 'param18 as display_duration', 'param19 as post_popup_duration', 'param20 as number_of_quadrants', 'param21 as icons_per_quadrant', 'param22 as data_request_delay', 'param23 as inactivity_time', 'param24', 'param25', 'param26', 'param27', 'param28', 'param29', 'param30')->get();
        
        $locations = Location::leftJoin('categories', 'locations.category_id', '=', 'categories.id')
                               ->select('locations.*', 'categories.name as categoryName')
                               ->get();

        foreach ($locations as $l) {
            $l['lat'] = $l->location->getLat();
            $l['lng'] = $l->location->getLng();
            $l['images'] = LocationImage::where('location_id', $l->id)->select('image')->get();
        }

        // todo: make a condition for approved
        $followedUsers = Follower::where('follower_id', $user->id)->pluck('user_id')->toArray();

        $followedUsers = implode(',', $followedUsers);

        $sql = "SELECT posts.id, posts.text,  posts.user_id, posts.public, posts.created_at, posts.text, users.id AS user_id, users.profile_img AS user_profile_image, users.full_name AS user_full_name
                FROM posts
                LEFT JOIN users
                ON users.id = posts.user_id
                WHERE posts.user_id IN (" . $followedUsers . ")
                ORDER BY posts.created_at desc 
                LIMIT 20
            ";
        
        if ($followedUsers) {
            $posts = DB::select(DB::raw($sql));
        } else {
            $posts = [];
        }

        $lastOnline = Auth::user()->last_online;

        $lastSeen = Carbon::now()->diffInMinutes($lastOnline);

        return view('homepage.main', compact('user', 'lastSeen', 'userPost', 'users', 'city', 'locations', 'beacons', 'map_layers', 'posts'));
    }

    public function getUsers()
    {
        $users = User::whereNotNull('currentlocation')->take(50)->get();

        return response()->json(['users'=> $users]);
    }

    public function getPeopleToFollow()
    {
        $followers = Follower::where('user_id', '!=', Auth::user()->id)->pluck('id')->toArray();

        $users = User::whereNotIn('id', $followers)->inRandomOrder()->limit(50)->get();

        return response()->json(['users' => $users]);
    }

    public function guestIndex()
    {
        return view('homepage.guest');
    }

    public function profile()
    {
        $user = Auth::user();
        return view('profile.user', compact('user'));
    }

    public function update_profile(Request $request)
    {
        $input = $request->all();

        $date = $input['day'].'.'.$input['month'].'.'.$input['year'];

        User::where('id', Auth::user()->id)->update([
            'name' => $input['name'],
            'last_name' => $input['last_name'],
            'type' => 'standard',
            'date_of_birth' => $date
        ]);

        return redirect('/profile');
    }

    public function privacy()
    {
        $user = Auth::user();
        return view('profile.privacy', compact('user'));
    }

    public function getPins()
    {
        $pins = Pin::join('users', 'pins.user_id', '=', 'users.id')
                    ->select('pins.*', 'users.name')
                    ->get();

        return $pins;
    }

    public function getUserFriends()
    {
        $receivedRequests = Friend::join('users', 'friends.sender_id', '=', 'users.id')
                ->where('friends.receiver_id', Auth::user()->id)
                ->where('friends.status', 'friends')
                ->select('users.id', 'users.email', 'users.name', 'users.last_name', 'users.last_message', 'users.lat', 'users.long', 'users.profile_img')
                ->get();

        $sentRequests = Friend::join('users', 'friends.receiver_id', '=', 'users.id')
                ->where('friends.sender_id', Auth::user()->id)
                ->where('friends.status', 'friends')
                ->select('users.id', 'users.email', 'users.name', 'users.last_name', 'users.last_message', 'users.lat', 'users.long', 'users.profile_img')
                ->get();

        $friends = $receivedRequests->merge($sentRequests);

        $friendsIdArray = [];

        foreach ($friends as $f) {
            array_push($friendsIdArray, $f->id);
        }

        //get messages before we insert our id in
        $messages = Pin::join('users', 'users.id', '=', 'pins.user_id')
                        ->whereIn('user_id', $friendsIdArray)
                        ->orderBy('updated_at', 'DESC')
                        ->select('pins.*', 'users.email', 'users.name', 'users.last_name')
                        ->get();

        //we dont want to have ourself as friend
        array_push($friendsIdArray, Auth::user()->id);

        $nonFriends = User::whereNotIn('id', $friendsIdArray)->get();

        $people = [];
        $people['friends'] = $friends;
        $people['nonFriends'] = $nonFriends;
        $people['friendsMessages'] = $messages;

        return $people;
    }

    public function sendFriendRequest(Request $request)
    {
        $input = $request->all();

        $checkSendRequest = Friend::where('sender_id', Auth::user()->id)->where('receiver_id', $input['id'])->get();
        $checkReceiveRequest = Friend::where('sender_id', $input['id'])->where('receiver_id', Auth::user()->id)->get();

        $checkRequests = $checkSendRequest->merge($checkReceiveRequest);

        $message = '';

        if ($checkRequests->isEmpty()) {
            $friendRequest['sender_id'] = Auth::user()->id;
            $friendRequest['receiver_id'] = $input['id'];
            $friendRequest['status'] = 'pending';

            $friendRequest = Friend::create($friendRequest);

            return $message = 'Your friend request has been sent!';
        } else {
            return $message = 'Your friend request has been sent!';
        }
    }

    public function getFriendRequest()
    {
        return $friendRequest = Friend::join('users', 'users.id', '=', 'friends.sender_id')
                                    ->where('friends.receiver_id', Auth::user()->id)
                                    ->where('friends.status', 'pending')
                                    ->select('users.email', 'users.id')
                                    ->get();
    }

    public function friendRequestResponse(Request $request)
    {
        $input = $request->all();

        Friend::where('sender_id', $input['id'])->where('receiver_id', Auth::user()->id)->update([
            'status' => $input['response']
        ]);

        return 'response updated';
    }

    public function globalProfile($id)
    {
        $user = Auth::user();
        $selectedUser = User::find($id);
        $selectedUserMessages = Pin::where('user_id', $id)->get();
        return view('profile.global', compact('user', 'selectedUser', 'selectedUserMessages'));
    }

    public function userLocations()
    {
        $user = Auth::user();
        $locations = Location::where('user_id', $user->id)->get();

        return view('profile.locations', compact('user', 'locations'));
    }

    public function getcitiesregister($country, $query)
    {
        $data = DB::table('cities')
                    ->where('country', $country)
                    ->where('cities.name', 'like', '%'.$query.'%')
                    ->join('countries', 'countries.id', '=', 'cities.country')
                    ->select('cities.id', 'cities.name', 'cities.region', 'countries.name AS country')
                    ->take(20)
                    ->get();

        return response()->json($data);
    }
    
    /**
    * Given a $centre (latitude, longitude) co-ordinates and a
    * distance $radius (miles), returns a random point (latitude,longtitude)
    * which is within $radius miles of $centre.
    *
    * @param  array $centre Numeric array of floats. First element is
    *                       latitude, second is longitude.
    * @param  float $radius The radius (in kilometers).
    * @return array         Numeric array of floats (lat/lng). First
    *                       element is latitude, second is longitude.
    */

    public function generate_random_point($centre, $minradius, $maxradius = 0, $randomradius = false)
    {
        $radius_earth = 6371; //km
        //Pick random distance within $distance;
        if ($randomradius && ($maxradius>$minradius)) {
            $distance = rand($minradius, $maxradius);
        } else {
            $distance = $minradius;
        }
        $distance = $distance/1000;
        //Convert degrees to radians.
        $centre_rads = array_map('deg2rad', $centre);
        //First suppose our point is the north pole.
        //Find a random point $distance miles away
        $lat_rads = (pi() / 2) - $distance / $radius_earth;
        $lng_rads = lcg_value() * 2 * pi();
        //($lat_rads,$lng_rads) is a point on the circle which is
        //$distance miles from the north pole. Convert to Cartesian
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);
        //Rotate that sphere so that the north pole is now at $centre.
        //Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];
        $rot = (pi() / 2) - $centre_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);
        //Rotate in z axis by $rot = $centre_rads[1]
        $rot = $centre_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;
        //Finally convert this point to polar co-ords
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);
        return array_map('rad2deg', array($lat_rads, $lng_rads));
    }

    /**
    * Calculates the great-circle distance between two points, with
    * the Haversine formula.
    * @param float $latitudeFrom Latitude of start point in [deg decimal]
    * @param float $longitudeFrom Longitude of start point in [deg decimal]
    * @param float $latitudeTo Latitude of target point in [deg decimal]
    * @param float $longitudeTo Longitude of target point in [deg decimal]
    * @param float $earthRadius Mean earth radius in [m]
    * @return float Distance between points in [m] (same as earthRadius)
    */
    public function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
          cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    /*
     * Function that returns map locations
     */
    public function getMapLocations(Request $request)
    {
        $user = Auth::user();

        if ($request->ajax()) {
            $data = $request->input();
            $bounds = json_decode($data['bounds']);

           
            if (!$bounds->_southWest || !$bounds->_southWest) {
                return array('status'=>0, 'message'=>'Invalid request');
            }

            if (($bounds->_southWest->lat < -90 || $bounds->_southWest->lat > 90) || ($bounds->_northEast->lat < -90 || $bounds->_northEast->lat > 90)) {
                return array('status'=>0, 'message'=>'Latitude is out found bounds');
            }

            if (($bounds->_southWest->lng < -180 || $bounds->_southWest->lng > 180) || ($bounds->_northEast->lng < -180 || $bounds->_northEast->lng > 180)) {
                return array('status'=>0, 'message'=>'Longitude is out of bounds');
            }

           
            if ($data['search-depth'] == 0) {//Just simple search, usually for guests only, 9 key quadrants

                $locationsarray = array();
               
                $quadrants = ($this->getMapQuadrants($bounds, 4));
                $alllocations = array();
                foreach ($quadrants as $key=>$q) {
                    //echo 'Starting quadrant '.$key.' at: '.date("H:i:s").'... ';
                    $sql = 'SELECT *,Y(location) as lat,X(location) as lng from locations WHERE Y(location) < '.$q['top_left']['lat'].' AND Y(location) > '.$q['bottom_right']['lat'].' AND X(location) < '.$q['bottom_right']['lng'].' AND X(location) > '.$q['top_left']['lng'] .' limit 0,20 ';
                   
                    $locations = DB::select(DB::raw($sql));
                    //echo 'Ending '.$key.' at: '.date("H:i:s").'... <br>';
                    if (count($locations)) {
                        $alllocations[$key] = $locations;
                    }
                }
                //exit;

                foreach ($alllocations as $quadrant => $locations) {
                    foreach ($locations as $l) {
                        $ldata = array();
                        $category = Category::find($l->category_id);
                        $ldata['name'] = $l->name;
                        $ldata['address'] = $l->address;
                        $ldata['lat'] = $l->lat;
                        $ldata['lng'] = $l->lng;
                        preg_match_all('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $category->icon, $matches);
                        $ldata['quadrant'] = $quadrant;

                        $ldata['icon'] = $matches['src'][0];
                        $locationsarray[] = $ldata;
                    }
                }

                $result['locations'] = $locationsarray;
                $result['quadrants'] = $this->getMapQuadrants($bounds, 4);
              
                return json_encode($result);
            }
        }

        return array('status'=>0, 'message'=>'Invalid request');
    }
    /*
     * Function that splits area xy number of quadrants
     * @bound expect 2 coordinates (lat,lng) for north east (upper right) and soth west (bottom left).
     * It returns coordinates for quadrants as array()
     *
     * @param  object $bound - must contain _southWest and _northEast properties
     * @return array         Numeric array of floats (lat/lng). First
     *                       element is latitude, second is longitude.
     */


    public function getMapQuadrants($bounds, $divisionnum = 3)
    {
        $sw = $bounds->_southWest;
        $ne = $bounds->_northEast;
      
        if (!isset($sw->lat) || !isset($sw->lng) || !isset($ne->lat) || !isset($sw->lng)) {
            return array('error' => 'Must pass valid parametars');
        }

        $partsnum = $divisionnum;
        $tileWidth = ($ne->lng - $sw->lng) / $partsnum;
        $tileHeight = ($ne->lat - $sw->lat) / $partsnum;
        $areas = array();
        $areacoordinates = array();
        $quadrants = array();
        $beginlat = $ne->lat;
        $beginlng = $sw->lng;
       
        for ($x=0;$x < $partsnum;$x++) {
            for ($y=0;$y<$partsnum;$y++) {
                $quadrants[] = array(
                'top_left' => array(
                    'lat'=> $beginlat,
                    'lng'=>$beginlng
                ),
                'top_right'=> array(
                    'lat'=> $beginlat ,
                    'lng'=>$beginlng + $tileWidth
                ),
                'bottom_left' => array(
                    'lat' => $beginlat - $tileHeight,
                    'lng' => $beginlng
                ),
                'bottom_right' => array(
                    'lat' => $beginlat - $tileHeight,
                    'lng' => $beginlng + $tileWidth
                )
            );

                $beginlng = $beginlng+$tileWidth;
            }
            //Update pointers
            $beginlng = $sw->lng;
            $beginlat = $beginlat - $tileHeight;
        }

        return $quadrants;
    }
    /**
    * Given a $centre (latitude, longitude) gets coordinates
    * distance $distance (kilometers), returns a  point (latitude,longtitude)
    * which is within $radius miles where angle is 0 (over latitude distance)
    *
    * @param  array $startcoords Numeric array of floats. First element is
    *                       latitude, second is longitude.
    * @param  float $distance The radius (in meters).
    * @return array         Numeric array of floats (lat/lng). First
    *                       element is latitude, second is longitude.
    */
    public function getPointByDistanceOnLatitude($startcoords, $distance)
    {
        //Transfer distance into kilometers
        $distance = $distance/1000;
        $radius_earth = 6371; //kilometers
        //Pick random distance within $distance;

        //Convert degrees to radians.
        $centre_rads = array_map('deg2rad', $startcoords);
        //First suppose our point is the north pole.
        //Find a random point $distance miles away
        $lat_rads = (pi() / 2) - $distance / $radius_earth;
        $lng_rads = lcg_value() * 2 * pi();
        //($lat_rads,$lng_rads) is a point on the circle which is
        //$distance miles from the north pole. Convert to Cartesian
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);
        //Rotate that sphere so that the north pole is now at $centre.
        //Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];
        $rot = (pi() / 2) - $centre_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);
        //Rotate in z axis by $rot = $centre_rads[1]
        $rot = $centre_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;
        //Finally convert this point to polar co-ords
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);
        return array_map('rad2deg', array($lat_rads, $lng_rads));
    }

    public function getBeaconData($id)
    {
        return response()->json(['beacon' => Beacon::find($id)]);
    }
}
