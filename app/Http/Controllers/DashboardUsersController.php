<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Session;
use App\Country;
use App\City;
use App\RoleAssignment;
use App\Role;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Validation\Rule;
use App\Context;

class DashboardUsersController extends Controller
{
    public function index(Request $request) 
    {
        $name = $request->name;

        $user = Auth::user();
        
        $users = User::where('full_name', 'like', '%' . $name . '%')
                       ->orWhere('user_name', 'like', '%' . $name . '%')
                       ->orWhere('email', 'like', '%' . $name . '%')
                       ->paginate(5);

        return view('admin-v2.users.index', compact('user', 'users', 'name'));
    }

    public function create() {
        $cities = City::all();
        $user = Auth::user();

        return view('admin-v2.users.create', compact('user', 'cities'));
    }

    protected function create_user(Request $data)
    {  

        $data->validate([
            'user_name' => 'required|string|unique:users',
            'full_name' => 'required|string',
            'about' => 'required|string|min:10',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $city = City::where('id', $data['city'])->first();
        $coords = array($city->geopoint->getLat(),$city->geopoint->getLng());
        $displayloc = $this->generate_random_point($coords, 1);
        $displaylocsave = new Point($displayloc[0], $displayloc[1]);
        $savecoords = new Point($city->geopoint->getLat(), $city->geopoint->getLng());

        $user = new User;
        $id = User::max('id');
        $user->id = User::max('id') + 1;
        $user->full_name = $data['full_name'];
        $user->user_name = $data['user_name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->about = $data['about'];
        $user->date_of_birth = $data['birth_date'];
        $user->profile_img = $data['profile_img'];
        $user->currentlocation = $savecoords;
        $user->displaylocation = $displaylocsave;
        $user->cover_img = $data['cover_img'];
        $user->city = $data['city'];
        $user->privacy = $data['privacy'];
        $user->reach = $data['reach'];
        $user->gender = $data['gender'];
        $user->session_id = Session::getId();
        if($data['is_admin']) {
            $user->is_admin = 1;
        }
        $user->auth = 'oAuth';
        $user->save();
        
        return redirect()->back()->withSuccess('User ' . $user->user_name . ' created!');
    }
    

    public function registered_invitations(Request $request) 
    {
        $name = $request->name;

        $user = Auth::user();

        // Temporaty
        $invitations = [];
        
        $users = User::where('full_name', 'like', '%' . $name . '%')
                       ->orWhere('user_name', 'like', '%' . $name . '%')
                       ->orWhere('email', 'like', '%' . $name . '%')
                       ->paginate(5);

        return view('admin-v2.users.reg_invitations', compact('user', 'users', 'name', 'invitations'));
    }

    public function roles(Request $request) 
    {
        $user = Auth::user();

        $roles = Role::orderBy('contextlevel', 'asc')->paginate(10);

        return view('admin-v2.users.roles.index', compact('user', 'roles'));
    }

    public function roles_edit(Request $request, $id)
    {
        $user = Auth::user();

        return view('admin-v2.users.roles.edit', ['user' => Auth::user(), 'role' => Role::find($id) ]);
    }

    public function roles_update(Request $request, $id)
    {
        Role::find($id)->update($request->validate([
            "name" => "required|string",
            'shortname' => [
                'required',
                'alpha_dash',
                Rule::unique('roles')->ignore($id),
            ],
            "description" => "required|min:3|max:1000",
            "contextlevel" => "required|integer|in:10,20,30,40,50"
        ]));

        return redirect()->route('dashboard.users.roles.index')->withSuccess("Successfully updated role " . Role::find($id)->name);
    }

    public function roles_delete(Request $request, $id)
    {
        $role = Role::find($id);
        $roleName = $role->name;
        $role->delete();

        return redirect()->route('dashboard.users.roles.index')->withSuccess("Successfully deleted role <strong>$roleName</strong>");
    }

    public function roles_add(Request $request)
    {
        $user = Auth::user();

        $roles = Role::paginate(10);

        return view('admin-v2.users.roles.create', compact('user', 'roles'));
    }


    public function roles_create(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'shortname' => 'required|alpha_dash|unique:roles',
            'description' => 'required|min:3|max:1000',
            "contextlevel" => "required|integer|in:10,20,30,40,50",
        ]);

        $role = new Role;
        $role->name = $request->name;
        $role->shortname = $request->shortname;
        $role->description = $request->description;
        $role->contextlevel = $request->contextlevel;
        $role->sortorder = Role::max('sortorder') + 1;

        $role->save();

        return redirect()->route('dashboard.users.roles.index')->withSuccess("Success! New Role <strong>$role->name</strong> created.");
    }

    public function role_assignments(Request $request)
    {
        $user = Auth::user();
        $roleAssignments = RoleAssignment::paginate(10);
        

        return view('admin-v2.users.role_assignments.index', compact('user', 'roleAssignments'));
    }

    public function role_assignments_edit($id)
    {
        $user = Auth::user();
        $roleAssignment = RoleAssignment::find($id);
        $users = User::paginate(20);
        $roles = Role::paginate(20);
        $contexts = Context::paginate(20);

        return view('admin-v2.users.role_assignments.edit', compact('user', 'roleAssignment', 'users', 'roles', 'contexts'));
    }

    public function role_assignments_update(Request $request, $id)
    {
        $user = Auth::user();
        $roleAssignments = RoleAssignment::paginate(10);
       
       RoleAssignment::find($id)->update($request->validate([
            "context_id" => "required|integer",
            "user_id" => "required|integer",
            "modifier_id" => "required|integer",
            "component" => "required|string",
            "item_id" => "required|integer"
       ]));

        return redirect()->route('dashboard.users.role_assignments.index', compact('user', 'roleAssignments'))->withSuccess('Role assignment updated!');
    }

    public function role_assignments_delete($id)
    {
        RoleAssignment::find($id)->delete();

        return redirect()->back()->withSuccess('Role assignment deleted!');
    }

    public function roles_add_users(Request $request)
    {  
        $role_id = $request->role_id;
        
        $filter = $request->filter;
        $potentialUserFilter = $request->potentialUserFilter;
       
        $user = Auth::user();
        
        $u = [];
        $roleAssignments = RoleAssignment::where('role_id', $role_id)->get();
        foreach($roleAssignments as $role) {
            array_push($u, User::findOrFail($role->user_id)); 
        }

        if($potentialUserFilter) {
            $contextUsers = array_filter($u, function ($user) use($potentialUserFilter) {
                if (stripos($user->user_name, $potentialUserFilter) !== FALSE) {
                    return true;
                } 
                elseif (stripos($user->full_name, $potentialUserFilter) !== FALSE) {
                    return true;
                }
                elseif (stripos($user->email, $potentialUserFilter) !== FALSE) {
                    return true;
                } 
                return false;
            });
            $contextUsers = array_unique($contextUsers);
        } 
        else 
        {
            $contextUsers = collect($u);
            $contextUsers = $contextUsers->unique();
        }

        $users = User::where('full_name', 'like', '%' . $filter . '%')
            ->orWhere('user_name', 'like', '%' . $filter . '%')
            ->orWhere('email', 'like', '%' . $filter . '%')
            ->paginate(20);

        $diff = $users->diff($u);

        $users = $diff->all(); 

        return view('admin-v2.users.roles.add_users', compact('user', 'users', 'contextUsers', 'filter', 'potentialUserFilter', 'role_id'));
    }

    public function roles_post_add_users(Request $request)
    {
        $request->validate([
            'role_id' => 'required|integer',
            'user' => 'required|integer'
        ]);

        $user = User::find($request->user);

        RoleAssignment::create([
            'role_id' => $request->role_id,
            'context_id' => 1,
            'user_id' => $request->user,
            'modifier_id' => 1,
            'component' =>  'core',
            'item_id' => 0
        ]);
        
        return redirect()->back()->withSuccess("User <strong>$user->full_name</strong> added!");
    }

    public function roles_remove_users(Request $request)
    {
        // dd('Remove user', $request->all());
        $request->validate([
            'role_id' => 'required|integer',
            'user' => 'required|integer'
        ]);

        $user = User::find($request->user);

        $roleAssignment = RoleAssignment::where('role_id', $request->role_id)->where('user_id', $request->user)->first()->delete();
        
        return redirect()->back()->withSuccess("User <strong>$user->full_name</strong> removed!");
    }

    // Auth methods
    public function auth_methods(Request $request) 
    {
        $user = Auth::user();

        $users = User::all();

        return view('admin-v2.users.auth_methods.auth_methods', compact('user', 'users'));
    }    

    public function add_service() 
    {
        $user = Auth::user();

        $users = User::all();

        return view('admin-v2.users.auth_methods.add_service', compact('user', 'users'));

    }


    public function sanctioned_users(Request $request) 
    {
        $name = $request->name;

        $user = Auth::user();

        $users = User::where(function($query) use($name) {
            $query->where('full_name', 'like', '%' . $name . '%')
            ->orWhere('user_name', 'like', '%' . $name . '%')
            ->orWhere('email', 'like', '%' . $name . '%');
        })->where('banned', 1)->paginate(5);

        return view('admin-v2.users.sanctioned_users', compact('user', 'users', 'name'));
    }    

    // unsuspend
    public function unsuspend_user($id) 
    {
        $user = User::where('id', $id)->first();

        $user->gagged = 0;

        $user->save();

        return redirect()->back();
    }    

    function generate_random_point($centre, $minradius,$maxradius = 0,$randomradius = false)
    {
        $radius_earth = 6371; //km
        //Pick random distance within $distance;
        if ($randomradius && ($maxradius>$minradius)){
            $distance = rand($minradius,$maxradius);
        }
        else{
            $distance = $minradius;
        }

        //Convert degrees to radians.
        $centre_rads = array_map('deg2rad', $centre);
        //First suppose our point is the north pole.
        //Find a random point $distance miles away
        $lat_rads = (pi() / 2) - $distance / $radius_earth;
        $lng_rads = lcg_value() * 2 * pi();
        //($lat_rads,$lng_rads) is a point on the circle which is
        //$distance miles from the north pole. Convert to Cartesian
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);
        //Rotate that sphere so that the north pole is now at $centre.
        //Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];
        $rot = (pi() / 2) - $centre_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);
        //Rotate in z axis by $rot = $centre_rads[1]
        $rot = $centre_rads[1];
        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;
        //Finally convert this point to polar co-ords
        $lng_rads = atan2($x3, $y3);
        $lat_rads = asin($z3);
        return array_map('rad2deg', array($lat_rads, $lng_rads));
    }
}
