<?php

namespace App\Http\Controllers;

use App\Post;
use App\SharedPost;
use App\CommentLike;
use App\PostComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function sharePost(Request $request, Post $post)
    {
        $post->times_shared +=1;
        $post->save();

        dd($request->all(), $post->times_shared);
    }

    public function comment(Request $request, Post $post)
    {
        $comment = new PostComment;
        $comment->post_id = $post->id;
        $comment->commentor_id = $request->user;
        $comment->text = $request->text;
        $comment->save();

        return response()->json([
            'status' => 200,
            'message' => $comment
        ]);
    }

    public function likeComment(Request $request, PostComment $comment)
    {
        $commentLike = CommentLike::where('comment_id', $comment->id)->where('user_id', $request->user)->first();
        if ($commentLike) {
            $commentLike->delete();

            $commentLikes = CommentLike::where('comment_id', $comment->id)->count();

            return response()->json([
                'status' => 200,
                'message' => 'Like deleted.',
                'likes' => $commentLikes
            ]);
        }

        $commentLike = new CommentLike;
        $commentLike->comment_id = $comment->id;
        $commentLike->user_id = $request->user;
        $commentLike->save();

        $commentLikes = CommentLike::where('comment_id', $comment->id)->count();

        return response()->json([
            'status' => 200,
            'message' => $commentLike,
            'likes' => $commentLikes
        ]);
    }
}
