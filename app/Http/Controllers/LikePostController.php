<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostLike;
use Illuminate\Http\Request;
use Auth;

class LikePostController extends Controller
{
    public function likePost(Request $request, $id)
    {
        $postLike = PostLike::where('post_id', $id)->where('user_id', $request->user_id)->first();

        if ($postLike) {
            $postLike->delete();
            $likes = PostLike::where('post_id', $id)->count();
           
            return response()->json(
                [
                'message' => 'Post Unliked',
                'likes' => $likes
                ],
                200
            );
        } else {
            PostLike::create([
                'post_id' => $id,
                'user_id' => $request->user_id
            ]);
            $likes = PostLike::where('post_id', $id)->count();

            return response()->json(
                [
                    'message' => 'Post Liked',
                    'likes' => $likes
                ],
                200
            );
        }
    }
}
