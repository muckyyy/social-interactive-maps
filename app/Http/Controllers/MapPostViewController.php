<?php

namespace App\Http\Controllers;

use App\Post;
use App\MapPostView;
use Illuminate\Support\Facades\Auth;

class MapPostViewController extends Controller
{
    public function __invoke(Post $post)
    {
        $postSeen = MapPostView::where('user_id', Auth::user()->id)->where('post_id', $post->id)->first();

        if ($postSeen) {
            return;
        }

        $postSeen = MapPostView::create([
            'user_id' => Auth::user()->id,
            'post_id' => $post->id,
        ]);

        return response()->json($postSeen);
    }
}
