<?php

namespace App\Http\Controllers;

use App\Follower;
use Illuminate\Http\Request;

class FollowersController extends Controller
{
    public function create(Request $request)
    {
        $following = Follower::create($request->all());

        return response()->json($following);
    }

    public function destroy(Request $request)
    {
        $following = Follower::where('user_id', $request->user_id)->where('follower_id', $request->follower_id)->first();
        $following->delete();

        return response()->json($following);
    }
}
