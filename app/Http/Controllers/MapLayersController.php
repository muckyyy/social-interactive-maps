<?php

namespace App\Http\Controllers;

use App\MapLayer;
use Illuminate\Http\Request;

class MapLayersController extends Controller
{
    public function index(Request $request)
    {
        return response()->json(['map_layers' => MapLayer::get()]);
    }
}
