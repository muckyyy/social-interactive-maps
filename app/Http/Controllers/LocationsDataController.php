<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationsDataController extends Controller
{
    public function __invoke(Request $request)
    {
        $zoom = $request->mapZoom;
        $bounds = json_decode($request->mapBounds);
        $southWestBounds = $bounds->_southWest;
        $northEastBounds = $bounds->_northEast;

        $quadrants = $this->getMapQuadrants($bounds);
        
        $locationQuery = 'SELECT * from ( ';
        $numquadrants = count($quadrants);
        $counter = 0;
        foreach ($quadrants as $key => $q) {
            $counter ++;
            $locationQuery .= '(SELECT id, category_id, icon, name, rating, address, website, phone_number, Y(location) as lat, X(location) as lng from locations WHERE
                        Y(location) < '.$q['top_left']['lat'].' AND Y(location) > '.$q['bottom_right']['lat'].' AND X(location) < '.$q['bottom_right']['lng'].' AND X(location) > '.$q['top_left']['lng'] .' LIMIT 0,5 )
                 ';

            if ($counter < $numquadrants) {
                $locationQuery .= ' UNION ALL ';
            }
        }
        $locationQuery .=' ) as result';
        
        $locations = collect(DB::select($locationQuery));

        $locations->map(function ($location) {
            $category = Category::find($location->category_id);

            $location->icon_class = $category->icon_class;
            $location->icon_color = $category->icon_color;
            $location->icon_prefix = $category->icon_prefix;

            return $location;
        });

        $data = [
            'locations' => $locations
        ];

        return response()->json($data);
    }

    /*
     * Function that splits area xy number of quadrants
     * @bound expect 2 coordinates (lat,lng) for north east (upper right) and soth west (bottom left).
     * It returns coordinates for quadrants as array()
     *
     * @param  object $bound - must contain _southWest and _northEast properties
     * @return array         Numeric array of floats (lat/lng). First
     *                       element is latitude, second is longitude.
     */


    public function getMapQuadrants($bounds, $divisionnum = 3)
    {
        $sw = $bounds->_southWest;
        $ne = $bounds->_northEast;

        if (!isset($sw->lat) || !isset($sw->lng) || !isset($ne->lat) || !isset($sw->lng)) {
            return array('error' => 'Must pass valid parametars');
        }

        $partsnum = $divisionnum;
        $tileWidth = ($ne->lng - $sw->lng) / $partsnum;
        $tileHeight = ($ne->lat - $sw->lat) / $partsnum;
        $areas = array();
        $areacoordinates = array();
        $quadrants = array();
        $beginlat = $ne->lat;
        $beginlng = $sw->lng;

        for ($x=0;$x < $partsnum;$x++) {
            for ($y=0;$y<$partsnum;$y++) {
                $quadrants[] = array(
                'top_left' => array(
                    'lat'=> $beginlat,
                    'lng'=>$beginlng
                ),
                'top_right'=> array(
                    'lat'=> $beginlat ,
                    'lng'=>$beginlng + $tileWidth
                ),
                'bottom_left' => array(
                    'lat' => $beginlat - $tileHeight,
                    'lng' => $beginlng
                ),
                'bottom_right' => array(
                    'lat' => $beginlat - $tileHeight,
                    'lng' => $beginlng + $tileWidth
                )
            );

                $beginlng = $beginlng+$tileWidth;
            }
            //Update pointers
            $beginlng = $sw->lng;
            $beginlat = $beginlat - $tileHeight;
        }

        return $quadrants;
    }
}
