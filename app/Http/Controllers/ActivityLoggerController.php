<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Auth;

class ActivityLoggerController extends Controller
{
    public function index() {
        $user = Auth::user();
        $activities = Activity::latest()->paginate(10);
        return view('admin-v2.activity_logger.index', compact('activities', 'user'));
    }
}


