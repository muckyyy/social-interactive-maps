<?php

namespace App\Http\Controllers;

use App\Beacon;
use Illuminate\Http\Request;
use App\Category;
use App\Location;
use App\City;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\CategoryIcon;
use Image;
use App\LocationImage;
use App\LocationReview;
use App\CategoryCustomField;
use App\CustomLocationField;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{
    public function addLocation($type, $lat, $long)
    {
        $user = Auth::user();
        $categories = Category::where('parent_id', 0)->where('active', 1)->get();
        $locations = Location::get();
        $cities = City::take(30)->get();

        return view('location.add-location', compact('type', 'user', 'categories', 'locations', 'cities', 'lat', 'long'));
    }

    public function mapAddLocation(Request $request)
    {
        $location = Location::create([
            'name' => $request['name'],
            'location' => new Point($request->coordinates['lat'], $request->coordinates['lng']),
            'description' => $request['desciption'],
            'website' =>  $request['website'],
            'phone_number' => $request['phone_number'],
            'category_id' => $request['category'],
        ]);

        return response()->json(['location' => $location], 200);
    }

    public function getCategories(Request $request)
    {
        $keyword = $request->keyword;

        $categories = Category::where('name', 'like', '%' . $keyword . '%')->get();

        return response()->json($categories);
    }

    public function addLocationPost(Request $request)
    {
        $input = $request->except('_token');
        $user = Auth::user();
        $input['user_id'] = $user->id;

        $input['location'] = new Point($input['location-lat'], $input['location-long']);

        $location = Location::create($input);
        $locationID = $location->id;

        $categories = [];
        $categories['cat1'] = $input['cat1'];
        $categories['cat2'] = $input['cat2'];
        $categories['cat3'] = $input['cat3'];
        $categories['cat4'] = $input['cat4'];
        $categories['cat5'] = $input['cat5'];

        $query = CategoryCustomField::where('category_id', $categories['cat1']);

        foreach ($categories as $key => $category) {
            $query->orWhere('category_id', $category);
        }

        $fields = $query->get();

        return view('location.location-custom-fields', compact('user', 'fields', 'locationID'));
    }

    public function addCustomLocationFields($id, Request $request)
    {
        $input = $request->except('_token');

        foreach ($input as $key => $data) {
            CustomLocationField::create([
               'location_id' => $id,
               'name' => strtolower($key),
               'content' => $data
           ]);
        }

        \Session::flash('flash_message', "You have successfully added location!");
        return redirect('/');
    }

    public function getLocation($id)
    {
        $user = Auth::user();
        $location = Location::find($id);
        $images = LocationImage::where('location_id', $id)->get();

        $reviews = [];

        $reviews['all'] = LocationReview::where('location_id', $id)->count();
        $reviews['average'] = 0;
        $reviews['one'] = '';
        $reviews['two'] = '';
        $reviews['three'] = '';
        $reviews['four'] = '';
        $reviews['five'] = '';

        if ($reviews['all'] != 0) {
            $reviews['one'] = LocationReview::where('location_id', $id)->where('rating', 1)->count();
            $reviews['two'] = LocationReview::where('location_id', $id)->where('rating', 2)->count();
            $reviews['three'] = LocationReview::where('location_id', $id)->where('rating', 3)->count();
            $reviews['four'] = LocationReview::where('location_id', $id)->where('rating', 4)->count();
            $reviews['five'] = LocationReview::where('location_id', $id)->where('rating', 5)->count();

            $reviews['average'] = (($reviews['one'] * 1) + ($reviews['two'] * 2) + ($reviews['three'] * 3) + ($reviews['four'] * 4) + ($reviews['five'] * 5)) / $reviews['all'];
        }

        // TODO: need logic which locations to show
        $locations = Location::get();


        $reviewFromUser = LocationReview::where('location_id', $id)->get();
        //$reviewFromUser = LocationReview::where('location_id', $id)->where('user_id', Auth::user()->id)->first();
        return view('location.view', compact('user', 'location', 'locations', 'images', 'reviews', 'reviewFromUser'));
    }

    public function getAdminLocationIcons(Request $request)
    {
        $id = $request->input('id');

        return CategoryIcon::where('category_id', $id)->where('active', 1)->get();
    }

    public function addLocationImages($id, Request $request)
    {
        $files = $_FILES['images'];

        $destinationPath = public_path().'/img/locations/';

        foreach ($_FILES["images"]["error"] as $key => $error) {
            if ($error == UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["images"]["tmp_name"][$key];
                // basename() may prevent filesystem traversal attacks;
                // further validation/sanitation of the filename may be appropriate
                $name = basename(rand(11111, 99999).$_FILES["images"]["name"][$key]);
                move_uploaded_file($tmp_name, "$destinationPath/$name");

                LocationImage::create([
                    'location_id' => $id,
                    'user_id' => Auth::user()->id,
                    'image' => $name,
                    'status' => 'review'
                ]);
            }
        }

        \Session::flash('flash_message', "Images uploaded, soon as our admins approve them, they will show up for added location!");
        return redirect('/');
    }

    public function addLocationReview($id, Request $request)
    {
        $rating = LocationReview::where('location_id', $id)->where('user_id', Auth::user()->id)->first();
        if (empty($rating)) {
            $location = LocationReview::create([
                    'location_id' => $id,
                    'user_id' => Auth::user()->id,
                    'rating' => $request->rating
            ]);

            $reviews = [];

            $reviews['all'] = LocationReview::where('location_id', $id)->count();
            $reviews['average'] = 0;
            $reviews['one'] = '';
            $reviews['two'] = '';
            $reviews['three'] = '';
            $reviews['four'] = '';
            $reviews['five'] = '';

            if ($reviews['all'] != 0) {
                $reviews['one'] = LocationReview::where('location_id', $id)->where('rating', 1)->count();
                $reviews['two'] = LocationReview::where('location_id', $id)->where('rating', 2)->count();
                $reviews['three'] = LocationReview::where('location_id', $id)->where('rating', 3)->count();
                $reviews['four'] = LocationReview::where('location_id', $id)->where('rating', 4)->count();
                $reviews['five'] = LocationReview::where('location_id', $id)->where('rating', 5)->count();

                $reviews['average'] = (($reviews['one'] * 1) + ($reviews['two'] * 2) + ($reviews['three'] * 3) + ($reviews['four'] * 4) + ($reviews['five'] * 5)) / $reviews['all'];
            }

            $userReview = LocationReview::where('location_id', $id)->where('user_id', Auth::user()->id)->first();

            return response()->json(['location_id' => $id, 'user_id' => Auth::user()->id, 'rating' => $request->rating,'reviews' => $reviews]);
        }
        if ($rating->rating == 0) {
            $rating->rating = $request->rating;
            $rating->save();

            $reviews = [];

            $reviews['all'] = LocationReview::where('location_id', $id)->count();
            $reviews['average'] = 0;
            $reviews['one'] = '';
            $reviews['two'] = '';
            $reviews['three'] = '';
            $reviews['four'] = '';
            $reviews['five'] = '';

            if ($reviews['all'] != 0) {
                $reviews['one'] = LocationReview::where('location_id', $id)->where('rating', 1)->count();
                $reviews['two'] = LocationReview::where('location_id', $id)->where('rating', 2)->count();
                $reviews['three'] = LocationReview::where('location_id', $id)->where('rating', 3)->count();
                $reviews['four'] = LocationReview::where('location_id', $id)->where('rating', 4)->count();
                $reviews['five'] = LocationReview::where('location_id', $id)->where('rating', 5)->count();

                $reviews['average'] = (($reviews['one'] * 1) + ($reviews['two'] * 2) + ($reviews['three'] * 3) + ($reviews['four'] * 4) + ($reviews['five'] * 5)) / $reviews['all'];
            }

            $userReview = LocationReview::where('location_id', $id)->where('user_id', Auth::user()->id)->first();

            return response()->json(['location_id' => $id, 'user_id' => Auth::user()->id, 'rating' => $request->rating,'reviews' => $reviews]);
        }

        return response()->json(["response"=>"Already voted"]);
    }

    public function updateReview($id, Request $request)
    {
        $rating = LocationReview::where('location_id', $id)->where('user_id', Auth::user()->id)->first();

        $rating->rating = $request->rating;
        $rating->save();
        return response()->json(["response"=>"This is 0"]);
    }

    public function beacon($parameter)
    {
        $user = Auth::user();

        if (is_numeric($parameter)) {
            $beacon = Beacon::findOrFail($parameter);
        } else {
            $beacon = Beacon::where('name', $parameter)->first();
        }

        if (!$beacon) {
            return abort(404);
        }

        return view('beacons.index', compact('user', 'beacon'));
    }

    public function userReview($id, Request $request)
    {
        $userReview = $request->text;
        $location_full_review = LocationReview::where('location_id', $id)->where('user_id', Auth::user()->id)->first();
        $location_full_review->review = $userReview;
        $location_full_review->save();
        return redirect()->back();
    }
}
