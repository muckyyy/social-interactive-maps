<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class ToggleMapProfileController extends Controller
{
    public function __invoke(User $user)
    {
        if ($user->map_profile_display) {
            $user->map_profile_display = 0;
            $status = 'off';
        } else {
            $user->map_profile_display = 1;
            $status = 'on';
        }

        $user->save();

        // return redirect()->back()->withSuccess("Toggled displaying profile picture on map");
        return response()->json([
            'response' => 200,
            'message' => "Profile picture toggled $status ."
        ]);
    }
}
