<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShowPagesController extends Controller
{
    public function aboutUsPage()
    {
        return view('welcome-pages.about-us');
    }

    public function helpPage()
    {
        return view('welcome-pages.help');
    }

    public function termsPage()
    {
        return view('welcome-pages.terms');
    }

    public function privacyPage()
    {
        return view('welcome-pages.privacy');
    }

    public function sitemapPage()
    {
        return view('welcome-pages.sitemap');
    }

    public function contactUsPage()
    {
        return view('welcome-pages.contact-us');
    }
}
