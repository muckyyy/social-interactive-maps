<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Image;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\User;
use App\Post;
use App\PostLike;
use App\PostComment;
use App\CommentLike;
use App\Story;
use App\Country;
use App\City;
use App\HeaderCoverImage;
use App\Mail\EventSuccess;
use App\Mail\OtherEmail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\PostImage;
use App\Location;
use App\LocationImage;
use App\LocationReview;
use App\CategoryCustomField;
use App\CustomLocationField;

class UserController extends Controller
{
    public function index($parameter)
    {
        if (is_numeric($parameter)) {
            $profile = User::find($parameter);
        } else {
            $profile = User::where('user_name', $parameter)->first();
        }

        $user = Auth::user();

        $posts = Post::where('profile_id', $profile->id)->orderBy('id', 'DESC')->get();

        foreach ($posts as $key => $post) {
            $post['user_like'] = 'none';
            $likes = $post['likes'] = PostLike::where('post_id', $post->id)->get();

            foreach ($post['likes'] as $key => $like) {
                if ($like->user_id == Auth::user()->id && $like->status == 'like') {
                    $post['user_like'] = 'like';
                }
            }

            $post['comments'] = PostComment::join('users', 'users.id', '=', 'post_comments.commentor_id')
                                ->where('post_id', $post->id)
                                ->where('post_comments.parent_id', 0)
                                ->select('post_comments.*', 'users.email', 'users.created_at as userDate')
                                ->get();


            foreach ($post['comments'] as $key => $comment) {
                $post['comments'][$key]['sub_comments'] = PostComment::join('users', 'users.id', '=', 'post_comments.commentor_id')
                                                                       ->where('parent_id', $comment->id)
                                                                       ->select('post_comments.*', 'users.email', 'users.created_at as userDate')
                                                                       ->get();
            }

            $post['comment_count'] = $post['comments']->count();
        }
        $profile->about='I am a simple man, there is nothing so special about me. Except that I know laravel :)';
        $profile->save();

        $city = City::find($profile->city);
        $country = Country::where('id', $city->country)->first();
        $images = HeaderCoverImage::get();
        $added_images = PostImage::get();
        $id = $profile->id;

        return view('global-profile.index', compact('profile', 'user', 'id', 'city', 'country', 'images', 'posts', 'added_images'));
    }

    public function getUserDetails($id)
    {
        $user = User::find($id);

        $city = City::find($user->city);
        $cityName = $city ? $city->name : '';

        $country = $city ? Country::find($city->country)->name : '';
        $f = User::take(10)->get();  // TODO
        $friends = [];
        $lastPost = Post::where('user_id', $user->id)->orderBy('created_at', 'desc')->first();
        // $lastOnline = Cache::get("user-last-seen-$user->id");

        $lastOnline = $user->last_online;

        
        if (Carbon::now()->diffInMinutes($lastOnline) <= 2) {
            $lastSeen = "<span style='color:green'>Online</span>";
        } elseif (Carbon::now()->diffInMinutes($lastOnline) > 2 && Carbon::now()->diffInMinutes($lastOnline) < 61) {
            $lastSeen = "Last Seen: " . Carbon::now()->diffInMinutes($lastOnline) . " minutes ago";
        } else {
            $lastSeen = "Offline";
        }

        foreach ($f as $friend) {
            $img = $friend->profile_img ? "/uploads/$friend->profile_img" : "/img/no-image.png";

            $content = "<div class='followers-images'><a><img src='$img' style='width: 40px; height: 40px;'></a></div>";

            array_push($friends, $content);
        }

        return response()->json([
            'user_id' => $user->id,
            'name' => $user->full_name,
            'lastSeen' => $lastSeen,
            'about' => $user->about ?: 'Hello all, I am here to hang out and meet new friends. My hobbies are travelling, drinking coffee and of course hanging out with my friends on amaps.',
            'city' => $cityName,
            'country' => $country,
            'image' => $user->profile_img,
            'friends' => $friends,
            'friendsTotal' => User::count(),
            'lastPost' => $lastPost
        ]);
    }

    public function profileAbout($id)
    {
        $user = Auth::user();
        return view('global-profile.about', compact('user'));
    }

    public function profileFriends($id)
    {
        $user = Auth::user();
        return view('global-profile.friends', compact('user'));
    }

    public function profilePhotos($id)
    {
        $user = Auth::user();
        return view('global-profile.photos', compact('user'));
    }

    public function profileAlbum($id, $albumId)
    {
        $user = Auth::user();
        return view('global-profile.album', compact('user'));
    }

    public function profileVideo($id)
    {
        $user = Auth::user();
        return view('global-profile.videos', compact('user'));
    }

    public function createPost(Request $request)
    {
        if ($request->hasFile('img')) {
            $image_array = $request->file('img');
            $array_length = count($image_array);
            $image_size = 0;
            $all_images_together = 0;
            for ($i=0; $i<$array_length ; $i++) {
                $image_size = $image_array[$i]->getSize();
                $all_images_together = $image_size + $all_images_together;
            }

            if ($all_images_together < 52428800) {
                for ($i=0; $i<$array_length ; $i++) {
                    $image_size = $image_array[$i]->getSize();
                    $image_ext = $image_array[$i]->getClientOriginalExtension();
                    $new_image_name = rand(123456, 999999).".".$image_ext;
                    $destination_path = public_path('/uploads');
                    $image_array[$i]->move($destination_path, $new_image_name);
                    $postImage = new PostImage;
                    $postImage->image = $new_image_name;
                    $postImage->save();
                }
            } else {
                return back()->with('msg', 'Size of uploaded files exceeds 50 MB');
            }
        }

        $input = $request->except('_token');
        $input['user_id'] = Auth::user()->id;

        //get link
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $input['text'], $match);
        $link = implode(" ", $match[0]);

        if ($link) {
            $tags = $this->parseUrlOGTags($link);
        }

        if (isset($tags)) {
            if ($tags) {
                if (isset($tags['og:title'])) {
                    $input['og_title'] = $this->RemoveBS($tags['og:title']);
                }
                if (isset($tags['og:description'])) {
                    $input['og_description'] = $this->RemoveBS($tags['og:description']);
                }
                if (isset($tags['og:image'])) {
                    $input['og_image'] = $tags['og:image'];
                }
                if (isset($tags['og:url'])) {
                    $input['og_url'] = $tags['og:url'];
                } else {
                    $input['og_url'] = $link;
                }
            }
        }

        //file
        $destinationPath = public_path().'/img/posts/';
        $file = Input::file('media');

        if ($file) {
            $extension = Input::file('media')->getClientOriginalExtension();
            $fileName1 = rand(11111, 99999).'.'.$extension;
            Input::file('media')->move($destinationPath, $fileName1);
            Image::make($destinationPath.$fileName1)->fit(800, 800)->save($destinationPath.$fileName1);
            $input['media'] = $fileName1;
        }

        $post = new Post;

        Post::create($input);

        return redirect('/p/'.$input['profile_id']);
    }

    public function toggleLike(Request $request)
    {
        $post_id = $request->input('post_id');

        $checkPost = PostLike::where('post_id', $post_id)->where('user_id', Auth::user()->id)->first();

        if (empty($checkPost)) {
            PostLike::create([
                'post_id' => $post_id,
                'user_id' => Auth::user()->id,
                'status' => 'like'
            ]);
        } else {
            if ($checkPost->status == 'like') {
                PostLike::where('id', $checkPost->id)->update([
                    'status' => 'unlike'
                ]);
            } else {
                PostLike::where('id', $checkPost->id)->update([
                    'status' => 'like'
                ]);
            }
        }

        return $checkPost;
    }

    public function toggleCommentLike(Request $request)
    {
        $comment_id = $request->input('comment_id');

        $checkComment = CommentLike::where('comment_id', $comment_id)->first();

        if (empty($checkComment)) {
            CommentLike::create([
                'comment_id' => $comment_id,
                'user_id' => Auth::user()->id,
                'status' => 'like'
            ]);
        } else {
            if ($checkComment->status == 'like') {
                CommentLike::where('id', $checkComment->id)->update([
                    'status' => 'unlike'
                ]);
            } else {
                CommentLike::where('id', $checkComment->id)->update([
                    'status' => 'like'
                ]);
            }
        }

        return $checkComment;
    }

    public function createComment(Request $request, $post_id, $profile_id, $parent_id)
    {
        $text = $request->input('text');

        $comment = PostComment::create([
            'post_id' => $post_id,
            'commentor_id' => Auth::user()->id,
            'text' => $text,
            'parent_id' => $parent_id
        ]);

        return 200;
    }

    public function getAllPosts($id)
    {
        $posts = Post::join('users', 'users.id', '=', 'posts.user_id')
                        ->where('profile_id', $id)
                        ->orderBy('created_at', 'DESC')
                        ->select('users.email as userEmail', 'posts.*')
                        ->get();



        foreach ($posts as $key => $post) {
            $post['user_like'] = 'none';
            $likes = $post['likes'] = PostLike::where('post_id', $post->id)->orderBy('created_at', 'DESC')->get();

            foreach ($post['likes'] as $key => $like) {
                if ($like->user_id == Auth::user()->id && $like->status == 'like') {
                    $post['user_like'] = 'like';
                }
            }

            $post['comments'] = PostComment::join('users', 'users.id', '=', 'post_comments.commentor_id')
                                ->join('comment_likes', 'post_comments.id', '=', 'comment_likes.comment_id')
                                ->where('post_id', $post->id)
                                ->where('post_comments.parent_id', 0)
                                ->orderBy('created_at', 'DESC')
                                ->select('post_comments.*', 'users.email', 'users.created_at as userDate', 'comment_likes.status as likeStatus')
                                ->get();


            foreach ($post['comments'] as $key => $comment) {
                $post['comments'][$key]['sub_comments'] = PostComment::join('users', 'users.id', '=', 'post_comments.commentor_id')
                                                                       ->where('parent_id', $comment->id)
                                                                       ->orderBy('created_at', 'DESC')
                                                                       ->select('post_comments.*', 'users.email', 'users.created_at as userDate')
                                                                       ->get();
            }

            $post['comment_count'] = $post['comments']->count();
        }

        return $posts;
    }


    public function testooo(Request $request, $postId, $profileId, $commentId)
    {
        return $request->all();
    }

    public function file_get_contents_curl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }


    public function parseUrlOGTags($url)
    {
        $html = $this->file_get_contents_curl($url);


        libxml_use_internal_errors(true); // Yeah if you are so worried about using @ with warnings
        $doc = new \DomDocument();
        $doc->loadHTML($html);
        $xpath = new \DOMXPath($doc);
        $query = '//*/meta[starts-with(@property, \'og:\')]';
        $metas = $xpath->query($query);
        foreach ($metas as $meta) {
            $property = $meta->getAttribute('property');
            $content = $meta->getAttribute('content');
            $rmetas[$property] = $content;
        }
        return $rmetas;
    }

    public function RemoveBS($Str)
    {
        $StrArr = str_split($Str);
        $NewStr = '';
        foreach ($StrArr as $Char) {
            $CharNo = ord($Char);
            if ($CharNo == 163) {
                $NewStr .= $Char;
                continue;
            } // keep £
            if ($CharNo > 31 && $CharNo < 127) {
                $NewStr .= $Char;
            }
        }
        return $NewStr;
    }

    public function getPostLike($postId)
    {
        return PostLike::join('users', 'users.id', '=', 'post_likes.user_id')
                          ->where('post_likes.post_id', $postId)
                          ->select('users.*')
                          ->get();
    }

    public function createStoryText(Request $request)
    {
        $input = $request->all();

        $story = Story::create([
            'user_id' => Auth::user()->id,
            'text' => $input['text'],
            'background' => $input['background'],
            'type' => 'text',
            'story_location' => Auth::user()->displaylocation
        ]);

        \Session::flash('flash_message', "Story shared!");
        return redirect('/');
    }

    public function createStoryImg(Request $request)
    {
        $file = $request->file('story_img');

        $extension = $file->getClientOriginalExtension();
        $destinationPath = public_path().'/img/stories/';
        $fileName1 = rand(11111, 99999).'.'.$extension;

        Image::make($file->getRealPath())->fit(800, 800)->save($destinationPath.$fileName1);

        $story = Story::create([
            'user_id' => Auth::user()->id,
            'image' => $fileName1,
            'type' => 'image',
            'story_location' => Auth::user()->displaylocation
        ]);

        \Session::flash('flash_message', "Story shared!");
        return redirect('/');
    }

    public function testApi()
    {
        return 'lala';
    }

    public function sendEmail()
    {
        $data = array('name'=>"sitko");

        Mail::send(['text'=>'mails.event_success'], $data, function ($message) {
            $message->to('info@avatarsmap.com', 'morel se')->subject('Laravel Basic Testing Mail');
            $message->from('info@avatarsmap.com', 'sitko');
        });

        return 'RADI!';
    }

    public function send($name, $surname, $email, $message, $receiverEmail)
    {
        $objEmail = new \stdClass();
        $objEmail->name = $name;
        $objEmail->surname = $surname;
        $objEmail->email = $email;
        $objEmail->message = $message;
        $objEmail->sender = 'Avatars';

        $res = Mail::to($receiverEmail)->send(new EventSuccess($objEmail));

        return $res;
    }

    public function contactEmailSend(Request $request)
    {
        $input = $request->all();

        if (!$input['name'] || filter_var($input['email'], FILTER_VALIDATE_EMAIL) === false || !$input['message']) {
            $return = array('error'=>true,'errormessage'=>'Email not send, please fill in all fields');
            return Response()->json($return);
        }

        $data = new \stdClass();
        $data->name = $input['name'];
        $data->surname = $input['surname'];
        $data->message = $input['message'];
        $data->email = $input['email'];

        Mail::send(['html'=>'mails.contact_form'], ['email' => $data], function ($message) {
            $message->to('info@avatarsmap.com', 'AvatarsMap Contact form')->subject('Contact form submitted');
            $message->from('info@avatarsmap.com', 'AvatarsMap Contact form');
        });

        return Response()->json(array('error'=>false,'successmessage'=>'Thank you for your message'));
    }

    public function photoUpdate(Request $request)
    {
        if ($request->has('img')) {
            $image = $request->file('img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(150, 150)->save(public_path('/uploads/' . $filename));

            $user = Auth::user();
            $user->profile_img=$filename;
            $user->save();
        }
        return redirect("p/$user->id");
    }

    public function backgroundImageUpdate(Request $request, $id)
    {
        $user = User::find($id);

        $user->cover_img = $request->id;
        $user->save();

        return redirect()->back();
    }

    public function elevatorEmailSend($name, $surname, $email, $message, Request $request)
    {
        if (!$name || filter_var($email, FILTER_VALIDATE_EMAIL) === false || !$message) {
            $return = array('error'=>true,'errormessage'=>'Email not send, please validate your input');
            return Response()->json($return);
        }

        $data = new \stdClass();
        $data->name = $name;
        $data->surname = $surname;
        $data->message = $message;
        $data->email = $email;

        Mail::send(['html'=>'mails.elevator_pitch'], ['email'=> $data], function ($message) {
            $message->to('info@avatarsmap.com', 'AvatarsMape Elevator Pitch form')->subject('Contact form submitted');
            $message->from('info@avatarsmap.com', 'AvatarsMape Elevator Pitch form');
        });

        return Response()->json(array('error'=>false,'successmessage'=>'Success. Thank you for your message'));
    }

    public function getUserBasicInfo(User $user)
    {
        return response()->json($user);
    }
}
