<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Pin;
use App\MapPost;

class PinController extends Controller
{
	public function createPin(Request $request) {
		$input = $request->all();
		$input['user_id'] = Auth::user()->id;

		Pin::create($input);
		User::where('id', Auth::user()->id)->update([
			'lat' => $input['lat'],
			'long' => $input['long'],
			'last_message' => $input['description']
		]);
		return redirect('/');
	}

	public function createMapPost(Request $request) {
		$input = $request->all();
		MapPost::create([
			'user_id' => Auth::user()->id,
			'text' => $input['text']
		]);
		User::where('id', Auth::user()->id)->update([
			'last_message' => $input['text']
		]);
		\Session::flash('flash_message', "Thank you!");
		return redirect('/');
	}

	public function test() {
		$title = '';
		$dom = new \DOMDocument();
		$dom->loadHTMLFile('http://newton.newtonsoftware.com/career/CareerAtomFeed.action?clientId=8a7883d06937f5f701693a4776391aaf');
		return $dom;
	}
}
