<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\MapLayer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class MainAvatarController extends Controller
{
    public function __invoke(Request $request)
    {
        $u = Auth::user();

        $lat = $u->displaylocation->getLat();
        $long = $u->displaylocation->getLng();

        // TODO: Which layer for main user?
        $layer = MapLayer::where('short_name', 'other_avatars_layer')->first();
        $walking_distance = $layer->param12;

        //If user view is too far away we wont return anything
        $distance = $this->haversineGreatCircleDistance($lat, $long, $u->currentlocation->getLat(), $u->currentlocation->getLng());
        
        if ($distance > 20000) { //10 km
            return json_encode(array());
        }
        //Calculate random coordinates for circle given upper values
        $coords = array($u->currentlocation->getLat(), $u->currentlocation->getLng());

        $reach = $u->reach / 1000;

        // $sql = "SELECT
        //     id, user_name, full_name, email, profile_img, map_profile_display, last_action, Y(displaylocation) as lat, X(displaylocation) as lng,
        //     ACOS( SIN( RADIANS( Y(displaylocation) ) ) * SIN( RADIANS('" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
        //     * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS(  X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380
        //     AS `distance`
        //     FROM `users`
        //     WHERE
        //     ACOS( SIN( RADIANS(  Y(displaylocation) ) ) * SIN( RADIANS( '" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
        //     * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS( X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380 < '" . $reach . "'
        //     AND id = '" . $u->id  . "'
        //     HAVING distance < '" . $reach  . "'";

        $sql = "SELECT
            id, user_name, full_name, email, profile_img, map_profile_display, last_action, Y(displaylocation) as lat, X(displaylocation) as lng,
            ACOS( SIN( RADIANS( Y(displaylocation) ) ) * SIN( RADIANS('" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
            * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS(  X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380 
            AS `distance`
            FROM `users`
            WHERE id = '" . $u->id  . "'";
            

        $user = DB::select(DB::raw($sql))[0];

        $post = Post::where('user_id', $user->id)->where('profile_id', $user->id)->orderBy('created_at', 'desc')->first();

        $lastOnline = $user->last_action;
        $lastSeen = "<span class='ml-2' style='color: #777'>Offline</span>";

        if ($lastOnline) {
            if (Carbon::now()->diffInMinutes($lastOnline) <= 2) {
                $lastSeen = "<span class='ml-2' style='color: #279900'>Online</span>";
            }
            if (Carbon::now()->diffInMinutes($lastOnline) > 2 && Carbon::now()->diffInMinutes($lastOnline) < 61) {
                $lastSeen = "<span class='ml-2'>Last Seen: <span style='color: #279900'>" . Carbon::now()->diffInMinutes($lastOnline) . "</span> minutes ago</span>";
            }
        }

        $user->lastSeen = $lastSeen;
        $user->mainUser = true;

        if ($post) {
            $now = Carbon::now();
            $diff = $now->diffInMinutes($post->created_at);

            if ($diff < 16) {
                $user->post = $post;
            }
        }

        $dist = $this->addDistance($user->lat, $user->lng, $walking_distance);
        $user->walkToLat = $dist[0];
        $user->walkToLng = $dist[1];

        $data = [
            'user' => $user,
        ];

        return response()->json($data);
    }

    public function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    public function addDistance($lat, $lng, $distance)
    {
        $rand = mt_rand(1, 4);

        $earthRadius = 6371;

        $pi = M_PI;

        $m = (1 / ((2 * $pi / 360) * $earthRadius)) / 1000;  //1 meter in degree

        if ($rand == 1) {
            $new_latitude = $lat + ($distance * $m);
            $new_longitude = $lng + ($distance * $m) / cos($lat * ($pi / 180));
        } elseif ($rand == 2) {
            $new_latitude = $lat - ($distance * $m);
            $new_longitude = $lng + ($distance * $m) / cos($lat * ($pi / 180));
        } elseif ($rand == 2) {
            $new_latitude = $lat + ($distance * $m);
            $new_longitude = $lng - ($distance * $m) / cos($lat * ($pi / 180));
        } else {
            $new_latitude = $lat - ($distance * $m);
            $new_longitude = $lng - ($distance * $m) / cos($lat * ($pi / 180));
        }

        return array($new_latitude , $new_longitude);
    }
}
