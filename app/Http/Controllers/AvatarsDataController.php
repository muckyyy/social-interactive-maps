<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Beacon;
use App\MapLayer;
use Carbon\Carbon;
use App\UserStatus;
use App\MapPostView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AvatarsDataController extends Controller
{
    public function __invoke(Request $request)
    {
        $layer = MapLayer::where('short_name', $request->layer)->first();

        $walking_distance = $layer->param12;

        $user = User::find($request->user_id);
        $lat = $request->lat;
        $long = $request->lng;
        $limit = $request->limiter;

        //If user view is too far away we wont return anything
        $distance = $this->haversineGreatCircleDistance($lat, $long, $user->currentlocation->getLat(), $user->currentlocation->getLng());
        
        if ($distance > 20000) { //10 km
            return json_encode(array());
        }

        //Calculate random coordinates for circle given upper values
        $coords = array($user->currentlocation->getLat(), $user->currentlocation->getLng());
        
        $reach = $user->reach / 1000;

        $sql = "SELECT
            id, user_name, full_name, email, profile_img, map_profile_display, last_action, Y(displaylocation) as lat, X(displaylocation) as lng,
            ACOS( SIN( RADIANS( Y(displaylocation) ) ) * SIN( RADIANS('" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
            * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS(  X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380 
            AS `distance`
            FROM `users`
            WHERE
            ACOS( SIN( RADIANS(  Y(displaylocation) ) ) * SIN( RADIANS( '" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
            * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS( X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380 < '" . $reach . "' 
            AND id != '" . $user->id  . "'
            AND `last_action` >= DATE_SUB(NOW(),INTERVAL 15 MINUTE)
            HAVING distance < '" . $reach  . "'
            ORDER BY RAND() LIMIT $limit";

        $users = DB::select(DB::raw($sql));

        $index = 0;

        foreach ($users as $u) {
            $post = Post::where('user_id', $u->id)->where('profile_id', $u->id)->orderBy('created_at', 'desc')->first();

            $lastOnline = $u->last_action;
            $lastSeen = "<span class='ml-2' style='color: #777'>Offline</span>";

            if ($lastOnline) {
                if (Carbon::now()->diffInMinutes($lastOnline) <= 2) {
                    $lastSeen = "<span class='ml-2' style='color: #279900'>Online</span>";
                }
                if (Carbon::now()->diffInMinutes($lastOnline) > 2 && Carbon::now()->diffInMinutes($lastOnline) < 61) {
                    $lastSeen = "<span class='ml-2'>Last Seen: <span style='color: #279900'>" . Carbon::now()->diffInMinutes($lastOnline) . "</span> minutes ago</span>";
                }
            }

            if ($users[$index]->id == $u->id) {
                $users[$index]->lastSeen = $lastSeen;
            }

            if ($post) {
                $now = Carbon::now();
                $diff = $now->diffInMinutes($post->created_at);
                $postSeen = MapPostView::where('post_id', $post->id)->where('user_id', Auth::user()->id)->first();

                if ($diff < 16) {
                    $users[$index]->post = $post;
                    if ($postSeen) {
                        $users[$index]->post_seen = true;
                    } else {
                        $users[$index]->post_seen = false;
                    }
                }
            }

            $dist = $this->addDistance($u->lat, $u->lng, $walking_distance);
            $users[$index]->walkToLat = $dist[0];
            $users[$index]->walkToLng = $dist[1];

            $index++;
        }

        $data = [
            'users' => $users,
        ];

        return response()->json($data);
    }

    public function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    public function addDistance($lat, $lng, $distance)
    {
        $rand = mt_rand(1, 4);

        $earthRadius = 6371;

        $pi = M_PI;

        $m = (1 / ((2 * $pi / 360) * $earthRadius)) / 1000;  //1 meter in degree

        if ($rand == 1) {
            $new_latitude = $lat + ($distance * $m);
            $new_longitude = $lng + ($distance * $m) / cos($lat * ($pi / 180));
        } elseif ($rand == 2) {
            $new_latitude = $lat - ($distance * $m);
            $new_longitude = $lng + ($distance * $m) / cos($lat * ($pi / 180));
        } elseif ($rand == 2) {
            $new_latitude = $lat + ($distance * $m);
            $new_longitude = $lng - ($distance * $m) / cos($lat * ($pi / 180));
        } else {
            $new_latitude = $lat - ($distance * $m);
            $new_longitude = $lng - ($distance * $m) / cos($lat * ($pi / 180));
        }

        return array($new_latitude , $new_longitude);
    }
}
