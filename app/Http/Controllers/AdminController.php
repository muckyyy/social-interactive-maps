<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\City;
use App\Country;
use App\Region;
use App\TileServer;
use App\Category;
use App\AdminIcon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Image;
use App\Tag;
use App\CategoryCustomField;
use App\CategoryIcon;
use App\Beacon;
use App\Location;
use App\LocationField;
use App\LocationImage;
use App\LocationFieldGroup;

class AdminController extends Controller
{
    public function regionAdd(){
        $user = Auth::user();

        $region = new Region();


        $countries = Country::all();
        $regions = array();


        return view('admin.region.add', compact('user','region','countries','regions'));

    }

    public function regionEdit($id){
        $user = Auth::user();
        $region = Region::find($id);

        $countries = Country::all();

        return view('admin.region.edit', compact('user','countries','region'));
    }
    
    public function regionSave(Request $request){
        if ($request->input('action') == 'update'){
            $input = $request->all();
            $region = Region::find($input['id']);
            $region->update($input);

        }

        if ($request->input('action') == 'add'){
            $input = $request->all();
            Region::create($input);
        }

        return redirect('/admin-city');
    }

    public function getRegionsAjax($countryid,Request $request){
        $user = Auth::user();
        $country = Country::find($countryid);

        $regions = Region::where('country',$countryid)->orderBy('name','asc')->get();


        if ($request->ajax()) {
            return view('admin.region.load', compact('regions'));
        }


    }

    public function getCountryRegions($countryid){
        $user = Auth::user();
        $country = Country::find($countryid);

        $regions = Region::where('country',$countryid)->orderBy('name','asc')->get();
        $return = array();
        foreach($regions as $region){
            $return[] = array("value"=>$region->id,"label"=>$region->name);
        }
        
        if ($request->ajax()) {
            return response()->json($return);
        }
        //return $response->json($return);
    }

    public function countryEdit($id){
        $user = Auth::user();
        $country = Country::find($id);

        $regions = Region::where('country',$country->id);

        return view('admin.country.edit', compact('user','country','regions'));
    }

    public function countrySave(Request $request){
        if ($request->input('action') == 'update'){
            $input = $request->all();
            $region = Country::find($input['id']);
            $region->update($input);

        }

        if ($request->input('action') == 'add'){
            $input = $request->all();
            Country::create($input);
        }

        return redirect('/admin-city');
    }

    public function countryAdd(){
        $user = Auth::user();

        $country = new Country();



        $regions = array();


        return view('admin.country.add', compact('user','country'));

    }

    /* Admin V2 */

    public function admin() {
        $user = Auth::user();
        return view('admin-v2.index', compact('user'));
    }


    public function adminLvlIcons() {
        $user = Auth::user();
        $icons = AdminIcon::get();
        return view('admin-v2.lvl-icons', compact('user','icons'));
    }

    public function avatar_settings() {
        $user = Auth::user();
        return view('admin-v2.avatar_settings', compact('user'));        
    }

    // tile servers
    public function tile_server_management() {
        $user = Auth::user();
        $servers = TileServer::all();
        return view('admin-v2.tile-server-management', compact('user', 'servers'));        
    }

    public function tile_servers_add() {
        $user = Auth::user();
        return view('admin-v2.tile_servers_add', compact('user'));        
    }
     
    public function uploadAdminIcon(Request $request) {

        $destinationPath = public_path().'/img/admin-icons/';
        $file = Input::file('icon_file');
        $created = 0;

        if($file){
            $extension = Input::file('icon_file')->getClientOriginalExtension();
            $fileName1 = rand(11111,99999).'.'.$extension;
            Input::file('icon_file')->move($destinationPath, $fileName1);
            Image::make($destinationPath.$fileName1)->fit(200, 200)->save($destinationPath.$fileName1);
            $imgIcon = "<img src='/img/admin-icons/". $fileName1 ."'>";
            $input['icon'] = $fileName1;
            $input['tag'] = $imgIcon;
            AdminIcon::create($input);
            $created = 1;
        }

        if($created==0) {
            \Session::flash('flash_message_error', "Image not uploaded. Please try again.");
        } else {
            \Session::flash('flash_message', "You have successfully uploaded image!");
        }

        return redirect('/admin/lvl-icons');
    }

    // Search Cities
    public function getCities($country,$query)
    {
        $data = DB::table('cities')
                    ->where('cities.name', 'like', '%'.$query.'%')
                    ->join('countries', 'countries.id', '=', 'cities.country')
                    ->select('cities.id', 'cities.name', 'cities.region' ,'countries.name AS country')
                    ->take(20)
                    ->get();

        return response()->json($data);
    }

}
