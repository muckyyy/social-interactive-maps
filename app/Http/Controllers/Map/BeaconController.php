<?php

namespace App\Http\Controllers\Map;

use App\Beacon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BeaconController extends Controller
{
    public function index(Request $request)
    {
        $zoom = $request->mapZoom;
        $bounds = json_decode($request->mapBounds);
        $southWestBounds = $bounds->_southWest;
        $northEastBounds = $bounds->_northEast;

        $beaconQuery = 'SELECT id, icon, icon_size_x, icon_size_y, icon_ancor_point_x, icon_ancor_point_y, name, Y(display_location) as lat, X(display_location) as lng from beacons WHERE
                        Y(display_location) > ' . $southWestBounds->lat .
                        ' AND Y(display_location) < '. $northEastBounds->lat .
                        ' AND X(display_location) > '. $southWestBounds->lng .
                        ' AND X(display_location) < '. $northEastBounds->lng . ' limit 0,50 ';

        
        return response()->json(DB::select($beaconQuery));
    }
}
