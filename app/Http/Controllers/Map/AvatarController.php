<?php

namespace App\Http\Controllers\Map;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Beacon;
use App\Post;
use Illuminate\Support\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class AvatarController extends Controller
{
    public function index()
    {
        $users = User::whereNotNull('currentlocation')->with(['posts' => function ($query) {
            $query->where('updated_at', '>', Carbon::now()->subMinutes(60));
        }])->take(20)->get();

        foreach ($users as $u) {
            $loc = array($u->currentlocation->getLat(), $u->currentlocation->getLng());
            $saveLoc = $this->generate_random_point($loc, $u->privacy, $u->reach);
            $u->currentlocation = new Point($saveLoc[0], $saveLoc[1]);
        }
        return response()->json($users);
    }


    public function getavatars(Request $request)
    {
        $lat = $request['lat'];
        $long = $request['lng'];

        $user = Auth::user();

        //If user view is too far away we wont return anything
        $distance = $this->haversineGreatCircleDistance($lat, $long, $user->currentlocation->getLat(), $user->currentlocation->getLng());
        if ($distance > 20000) { //10 km
            return json_encode(array());
        }
        //Calculate random coordinates for circle given upper values
        $coords = array($user->currentlocation->getLat(), $user->currentlocation->getLng());
        $showcoord = array($user->displaylocation->getLat(), $user->displaylocation->getLng());

        if ($user->active_beacon) {
            $reach = Beacon::find($user->active_beacon)->beacon_range;
        } else {
            $reach = $user->reach / 1000;
        }

        // $sql = "SELECT id, full_name, X(displaylocation),Y(displaylocation), "
        //     . "( 6371.393 * acos( cos( radians('" . $user->currentlocation->getLng() . "') ) * cos( radians(X(displaylocation) ) ) * cos( radians( Y(displaylocation) ) - radians('" . $user->displaylocation->getLat() . "') ) + sin( radians('" . $user->currentlocation->getLng() . "') ) * sin( radians( X(displaylocation) ) ) ) ) AS distance "
        //     . "FROM users where id!='" . $user->id . "' HAVING distance < '" . $reach . "' ORDER BY RAND() LIMIT 0 , 20";
        // $sql = "SELECT
        //     `id`,
        //     `name`, X(displaylocation),Y(displaylocation),
        //     ACOS( SIN( RADIANS( Y(displaylocation) ) ) * SIN( RADIANS('" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
        //     * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS(  X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380 AS `distance`
        //     FROM `users`
        //     WHERE
        //     ACOS( SIN( RADIANS(  Y(displaylocation) ) ) * SIN( RADIANS( '" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
        //     * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS( X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380 < '" . $reach . "' and id!='.$user->id.'
        //     ORDER BY RAND() LIMIT 0 , 20";

        $sql = "SELECT
            `id`,
            `full_name`, 
            X(displaylocation),Y(displaylocation),
            ACOS( SIN( RADIANS( Y(displaylocation) ) ) * SIN( RADIANS('" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
            * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS(  X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380 
            AS `distance`
            FROM `users`
            WHERE
            ACOS( SIN( RADIANS(  Y(displaylocation) ) ) * SIN( RADIANS( '" . $coords[0] . "' ) ) + COS( RADIANS(  Y(displaylocation) ) )
            * COS( RADIANS( '" . $coords[0] . "' )) * COS( RADIANS( X(displaylocation) ) - RADIANS( '" . $coords[1] . "' )) ) * 6380 < '" . $reach . "' 
            AND id !='.$user->id.'
            HAVING distance < '" . $reach  . "'
            ORDER BY RAND() LIMIT 0 , 20";

        $users = DB::select(DB::raw($sql));

        $avatars = array();

        foreach ($users as $u) {
            $u = User::find($u->id);

            // $lastOnline = Cache::get("user-last-seen-$u->id");
            $lastOnline = $u->last_online;

            $lastSeen = Carbon::now()->diffInMinutes($lastOnline);

            if ($lastOnline && $lastSeen < 61) {
                $loc = array($u->currentlocation->getLat(), $u->currentlocation->getLng());
                $saveLoc = $this->generate_random_point($loc, $u->privacy, $u->reach);
                $u->displaylocation = new Point($saveLoc[0], $saveLoc[1]);
                $u->save();

                $post = Post::where('user_id', $u->id)->where('updated_at', '>', Carbon::now()->subMinutes(60))->first();
                
                $avatars[] = array(
                    'id' => $u->id,
                    'full_name' => $u->full_name,
                    'profile_img' => $u->profile_img,
                    'lat' => $u->displaylocation->getLat(),
                    'lng' => $u->displaylocation->getLng(),
                    'name' =>  $u->full_name,
                    'post' => $post,
                    'lastSeen' => $lastSeen
                );
            }
        }

        return response()->json([
            'avatars' => $avatars,
        ]);
    }

    public function generate_random_point($centre, $minradius, $maxradius = 0, $randomradius = false)
    {
        $radius_earth = 6371; //km
        if ($randomradius && ($maxradius > $minradius)) {
            $distance = rand($minradius, $maxradius);
        } else {
            $distance = $minradius;
        }
        $distance = $distance / 1000;
        $centre_rads = array_map('deg2rad', $centre);
        $lat_rads = (pi() / 2) - $distance / $radius_earth;
        $lng_rads = lcg_value() * 2 * pi();
        $x1 = cos($lat_rads) * sin($lng_rads);
        $y1 = cos($lat_rads) * cos($lng_rads);
        $z1 = sin($lat_rads);
       
        $rot = (pi() / 2) - $centre_rads[0];
        $x2 = $x1;
        $y2 = $y1 * cos($rot) + $z1 * sin($rot);
        $z2 = -$y1 * sin($rot) + $z1 * cos($rot);
        $rot = $centre_rads[1];

        $x3 = $x2 * cos($rot) + $y2 * sin($rot);
        $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
        $z3 = $z2;
        $lng_rads = atan2($x3, $y3);

        $lat_rads = asin($z3);

        return array_map('rad2deg', array($lat_rads, $lng_rads));
    }

    public function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
}
