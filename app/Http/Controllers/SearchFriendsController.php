<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchFriendsController extends Controller
{
    public function __invoke($keyword)
    {
        // TODO: Limit users to friends once we have friends functionality worked out
        $friends = User::where('full_name', 'like', '%' . $keyword . '%')->select('id', 'full_name', 'profile_img')->get();

        return response()->json([
            'status' => 200,
            'friends' => $friends
        ]);
    }
}
