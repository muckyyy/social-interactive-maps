<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CustomLocationField extends Model
{
	
    protected $fillable = [
        'location_id',
        'name',
        'content',
        'type',
        'status'
    ];
}
