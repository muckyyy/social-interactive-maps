<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryCustomField extends Model
{

    protected $fillable = [
    	'category_id',
    	'name',
    	'type',
    	'content',
    	'status'
    ];
}
