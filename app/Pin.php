<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Pin extends Model
{
	
    protected $fillable = [
    	'user_id',
    	'description',
    	'type',
    	'lat',
    	'long',
    	'approved'
    ];
}
