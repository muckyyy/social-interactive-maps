
<div class="bottom-pin-location-section">
    <i class="fas fa-times create-random-pin-close-btn"></i>
    <div class="col-xs-12">
        <div class="custom-add-status-section col-xs-12 col-sm-offset-3 col-sm-6 no-padding">
            <div class="custom-add-status-img">
                <img src="img/common/statusbg.jpg">
            </div>
            <div class="custom-add-status-text">
                <div class="custom-add-status-title">
                    Share your status
                </div>
                <form class="form-horizontal" method="POST" action="/create-user-status">
                    {{ csrf_field() }}
                    <textarea class="form-control custom-pin-textarea" id='pin-description' name='text' required="required" maxlength="250"></textarea>
                    <input type="submit" class="add-location-btn button-primary-color" style="margin-top: 15px;border: 0;line-height:40px;font-family: 'Roboto', sans-serif;font-weight:500;" value="Share">
                </form>
            </div>
        </div>
        <div class="custom-add-status-info">
            (Max 360 characters)
        </div>
    </div>
</div>

<script>
$('#statusBox').click(function(){
    $('.bottom-pin-location-section').fadeIn(500)
})
</script>