<div class="bottom-pin-location-section-story">

    <i class="fas fa-times create-random-pin-close-btn-story"></i>

    <div class="col-xs-12" style="margin-top: 45px;">
        <div class="custom-add-status-section col-xs-12 col-sm-offset-3 col-sm-6 no-padding">
            <div class="custom-add-status-img">
                <img src="img/common/storybg.jpg">
            </div>
            <div class="custom-add-status-text">

                <div class="story-default">
                    <div class="custom-add-status-title">
                        Add a story
                    </div>

                    <div id='story-text-option' class="custom-story-option col-xs-offset-2 col-xs-8 button-primary-color" style="margin-top:5px;">
                        Text
                    </div>
                   
                    <form id="form1" runat="server" method="POST" action="/create-story-img" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <label for="storyImg" class="custom-story-label button-primary-color">Image</label>
                        <input id="storyImg" name="story_img" style="visibility:hidden;" type="file">

                        <div class="custom-story-preview-section">
                            <img id="story-img-preview-img" src="#" alt="your image" />
                            <div class="col-xs-12">
                                <input type="submit" class="custom-story-preview-btn button-primary-color" name="Share">
                            </div>
                        </div>

                    </form>

                    {{-- <script>
                        function readURL(input) {
                               if (input.files && input.files[0]) {
                                   var reader = new FileReader();
                                   
                                   reader.onload = function (e) {
                                       $('#story-img-preview-img').attr('src', e.target.result);
                                       $('.custom-story-preview-section').fadeIn(300)
                                   }
                                   
                                   reader.readAsDataURL(input.files[0]);
                               }
                           }
                           
                           $("#storyImg").change(function(){
                               readURL(this);
                           });
                    </script> --}}

                </div>

                @include('status.story-text')


            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <form id='custom-pin-story-form' method="POST" action="/create-story-text">
            {{ csrf_field() }}
            <input id='custom-pin-story-input-text' type="hidden" name="text">
            <input id='custom-pin-story-input-bg' type="hidden" name="background">
        </form>
    </div>

</div>


{{-- <script>
$('.custom-pin-story').click(function(){
    $('.bottom-pin-location-section-story').fadeIn(500)
})
$('.create-random-pin-close-btn-story').click(function(){
    $('.bottom-pin-location-section-story').fadeOut(500)
})

//story text option
let storyText;
let storyImg;

$('#story-text-option').click(function(){
    $('.story-default').fadeOut(1000)
    $('.story-text-1').delay(1100).fadeIn(500)
})

$('#story-text-btn-1').click(function(){
    storyText = $('#story-textarea').val()
    $('.story-text-1').fadeOut(1000)
    $('.story-text-2').delay(1100).fadeIn(500)
    $('#custom-pin-story-input-text').val(storyText)
})

$('.story-text-bg-section').click(function(){
    storyImgFull = this.children[0].currentSrc
    storyImg = (this.children[0].currentSrc).slice(-16)
    $('.story-text-2').fadeOut(500)
    $('.story-text-3').delay(800).fadeIn(500)
    $('#story-img-preview').attr('src', storyImgFull)
    $('#story-text-preview').text(storyText)
    $('#custom-pin-story-input-bg').val(storyImg)
})

$('.story-preview-btn').click(function(){
    $('#custom-pin-story-form').submit()
})
</script> --}}