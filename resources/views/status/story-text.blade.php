
<div class="story-text-1">
    <div class="custom-add-status-title">
        Write a text
    </div>

    <textarea id='story-textarea' class="form-control" style="min-height: 70px;"></textarea>

    <div id='story-text-btn-1' class="add-location-btn button-primary-color" style="margin-top: 15px;border: 0;line-height:40px;font-family: 'Roboto', sans-serif;font-weight:500;">
        Next
    </div>
</div>


<div class="story-text-2">
    <div class="custom-add-status-title">
        Choose a background
    </div>

    <div class="col-xs-12 no-padding">
        <div class="story-text-bg-section">
            <img src="/img/common/story-text-4.png">
        </div>
        <div class="story-text-bg-section">
            <img src="/img/common/story-text-3.png">
        </div>
        <div class="story-text-bg-section">
            <img src="/img/common/story-text-2.png">
        </div>
        <div class="story-text-bg-section">
            <img src="/img/common/story-text-1.png">
        </div>
    </div>
</div>

<div class="story-text-3">

    <div class="story-preview">
        <img id='story-img-preview' src="">
        <div id="story-text-preview">

        </div>
    </div>

    <div class="story-preview-btn button-primary-color">
        Share
    </div>

</div>

<script>
    
</script>