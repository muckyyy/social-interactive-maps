<div class="bottom-pin-location-section-event">

    <i class="fas fa-times create-random-pin-close-btn-event"></i>



    <div class="col-xs-12">
        <div class="custom-add-status-section col-xs-12 col-sm-offset-3 col-sm-6 no-padding" style="height: 400px;">
            <div class="custom-add-status-img">
                <img src="img/common/alert-bg.jpg">
            </div>
            <div class="custom-add-status-text">
                <div class="custom-add-status-title">
                    Add Alert
                </div>

                <div class="col-xs-12">
                    
                    <div class="alert-li-section col-xs-12">
                        <div class="alert-icon col-xs-12">
                            <i class="fas fa-exclamation-triangle"></i>
                        </div>
                        <div class='alert-text col-xs-12'>
                            Danger
                        </div>
                    </div>

                    <div class="alert-li-section col-xs-12">
                        <div class="alert-icon col-xs-12">
                            <i class="fas fa-traffic-light"></i>
                        </div>
                        <div class='alert-text col-xs-12'>
                            Traffic jam
                        </div>
                    </div>

                    <div class="alert-li-section col-xs-12">
                        <div class="alert-icon col-xs-12">
                            <i class="fas fa-car-crash"></i>
                        </div>
                        <div class='alert-text col-xs-12'>
                            Accident
                        </div>
                    </div>

                    <div class="alert-li-section col-xs-12">
                        <div class="alert-icon col-xs-12">
                            <i class="fas fa-house-damage"></i>
                        </div>
                        <div class='alert-text col-xs-12'>
                            Natural disaster
                        </div>
                    </div>

                </div>

                <form class="form-horizontal" method="POST" action="/create-map-post">
                    {{ csrf_field() }}


                </form>
            </div>
        </div>
    </div>


</div>


<script>
$('.custom-pin-event').click(function(){
    $('.bottom-pin-location-section-event').fadeIn(500)
})
$('.create-random-pin-close-btn-event').click(function(){
    $('.bottom-pin-location-section-event').fadeOut(500)
})
</script>