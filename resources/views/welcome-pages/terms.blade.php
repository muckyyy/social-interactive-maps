@guest
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/navigation-2.css">
</head>
<div class="main-navigation-section col-xs-12 no-padding">   
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark small-screen">
        <a class="navbar-brand" href="#"></a>
        <a class="navbar-brand" href="#">
            <span>Already pynning? Log in here: </span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-button" aria-controls="mobile-button" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="mobile-button">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item pt-3">
                    <input type="email" class="form-control" id="mobile-email-input" aria-describedby="username" placeholder="username">
                </li>
                 <li class="nav-item pt-3">
                    <input type="password" class="form-control" id="mobile-pass-input" aria-describedby="password" placeholder="password">
                </li>
                <li class="nav-item">
                    <input type="image" src="/img/new-img/play-button-arrowhead.png" alt="Submit" style="width: 28px; height: 28px;">
                </li>
            </ul>
        </div>
    </nav>
    <div class="container big-screen">
        <div class="row">
            <div class="col-md-2">  
            </div>
            <div class="col-md-5 text-right pr-0 main-text-nav">
                <span style="color:yellow;">You're already pynning? Log in here: </span>
            </div>
            <div class="col-md-5" style="padding-right: 0;">
                <form id="login-form" class="form-horizontal" method="POST" action="{{ route('login') }}" style="margin-bottom:0;">
                    {{ csrf_field() }}
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="username" required autofocus style="display: inline; width: 33%;">
                    <input id="password" type="password" class="form-control" name="password" placeholder="password" required style="display: inline; width: 33%;">
                    <button type="submit" style="display: flex; align-items: center; padding:0; background-color: transparent; border-width: 0;">
                        <input type="image" src="/img/new-img/play-button-arrowhead.png" alt="Submit" style="width: 28px; height: 28px;">
                        <span class="go-button">Go</span>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@else 
<div class="main-navigation-section col-xs-12 no-padding">
    <div>
        @if(\Request::route()->getName() == 'home.index')
            <navigation :user="{{$user}}" :map-layers="{{$map_layers}}"></navigation>  
        @else 
            <navigation :user="{{$user}}"></navigation>  
        @endif
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>
@endguest
<h1>Terms page</h1>
<script>
    $('#guestLogin').click(function(){
        let loginForm = document.getElementById('custom-login-form');
        if(!loginForm.style.display || loginForm.style.display == 'none') {
            $('#custom-login-form').fadeIn(500)
        }
        else {
            $('#custom-login-form').fadeOut(500)
        }
    })
</script>