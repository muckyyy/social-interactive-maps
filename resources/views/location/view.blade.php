@extends('layouts.app') @section('per-page-css')
<link rel='stylesheet' href='/css/locations.css' />
<link rel='stylesheet' href='/css/global-profile.css' />

<link rel="stylesheet" type="text/css" href="/css/slick/slick.css" /> // Add the new slick-theme.css if you want the default styling
<link rel="stylesheet" type="text/css" href="/css/slick/slick-theme.css" />
<link rel='stylesheet' href='/css/leaflet/leaflet.css' /> @endsection @section('map-scripts')
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/css/slick/slick.min.js"></script>
    <style>
        #change-rating:hover {
            cursor: pointer;
            text-decoration: none;
        }
        .photos-tab-hover-effect:hover {
            filter: brightness(0.4);
            cursor: pointer;
        }
        #form-submit-review-none {
            display: none;
        }
        .stars-rated {
            color: #FBBC04;
        }
        .location-ratings {
            display: inline;
        }
        #not-rated-location-2 {
            display: none;
        }
        .form-one-not-reviewed {
            display: none;
        }
        .form-one-reviewed {
            display: none;
        }
        .noHover{
            pointer-events: none;
        }
    </style>
@endsection @section('content')
{{$currentTime = Carbon\Carbon::now()}}

<div class="lv-section col-xs-12 no-padding" style="margin-top: 90px;">
    <div class="container">
        <div class="col-lg-1"></div>
        <div class="col-lg-10" style="background-color: white; padding: 0;">
            <div class="lv-slick-carousel">
                <!--@foreach($images as $image)-->
                <!--<img src="/img/locations/{{$image->image}}">-->
                <!--@endforeach-->
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video11.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video10.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video12.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video7.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video6.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video13.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video14.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video15.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video17.jpg">
                <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video16.jpg">
            </div>
            <div style="border-top: 1px solid #ebebe0; margin:5px 0 0 10px;">
                <div class="lv-left-img col-xs-12 col-lg-1 no-padding" style="padding: 30px 20px;">
                    <img src="{{ !empty($location->icon) ? $location->icon:''}}" style="width: 90px; height: 90px;">
                </div>
                <div class="lv-left-info-title col-xs-12 col-lg-11" style="padding: 38px 40px; color: grey; font-weight: 400;">
                    <span style="font-size: 1.7em;">{{ !empty($location->name) ? $location->name:''}}</span>
                    <br>
                    <span style="font-size: 1em;">{{ !empty($location->address) ? $location->address:''}}</span>
                    <br>
                    <span style="font-size: 1em;">{{ !empty($location->postal_code) ? $location->postal_code:''}}</span>
                </div>
            </div>
            <div class="tab col-md-8" style="background-color: #F3F2F2; padding: 0; width: 71.325%;">
                <button class="tablinks active" onclick="openTab(event, 'tips')"><i class="fas fa-info-circle" style="padding-right: 8px;"></i>Reviews</button>
                <button class="tablinks" onclick="openTab(event, 'photos')"><i class="fas fa-camera" style="padding-right: 8px;"></i>Photos</button>
                <button class="tablinks" onclick="openTab(event, 'map')"><i class="fas fa-map-marker-alt" style="padding-right: 8px;"></i>Map</button>
            </div>
            @if ( is_null($location->rating) || ($location->rating) === 0 )
            <div class="col-md-2 text-center" style="padding: 0; margin-left: -18px; width: 17%;">
                <button class="btn-edit success-z" style="margin:5px 0 0 0;">No ratings</button>
            </div>
            @else
            <div class="col-md-2" style="padding: 0; margin-left: -4px; width: 17%;">
                <button class="btn-edit success-z" style="font-weight: bold; vertical-align: top;">{{$location->rating}}.0<sup style="padding-left: 5px; font-weight: normal;">/5</sup></button>
                <ul style="display: inline-block; list-style-type: none; padding: 0; margin: 4px 0 0 10px; line-height: 150%;">
                    <li style="font-size: 1.2em; font-weight: bold;">19</li>
                    <li><small>ratings</small></li>
                </ul>
            </div>
            @endif
            <div class="col-lg-2 text-right" style="padding: 0; width: 11%;">
                <div style="padding-top: 15px;">
                    <a href="">
                        <i class="far fa-thumbs-up" style="font-size: 1.7em; color:#0080a0; padding-right: 20px;"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="container">
        <div class="lv-main-section">
        
            <div class="col-lg-1"></div>
            @foreach($reviewFromUser as $userReview)
            
            <div class="lv-left-section col-lg-7 tabcontent" id="tips" style="margin:15px 0 0 8px; background-color: white;">
                <div class="col-xs-12 gp-main-section" id="form-submit-review-none">
                        <div class="col-xs-12 gp-main-section-li no-padding">
                            <div class="gp-main-section-top col-xs-12 no-padding">
                                @if (!($user->profile_img))
                                    <div class="gp-author-img">
                                        <img src='/img/no-image.png'>
                                    </div>
                                @else
                                    <div class="gp-author-img">
                                        <img src='/uploads/{{Auth::user()->profile_img}}'>
                                    </div>
                                @endif
                                <div class="gp-author-date">
                                    <div class="gp-author">
                                        {{$user->full_name}}
                                    </div>
                                    <div class="lv-main-review noHover" style="font-weight: 500;color: #FBBC04;">
                                        <span class="rating-add-to-review-box"></span>
                                        <i class="fas fa-star custom-review-star star-rating star-one" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star star-rating star-two" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star star-rating star-three" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star star-rating star-four" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star star-rating star-five" style="font-size:1em;"></i>
                                    </div>
                                </div>
                            </div>
                            <form class="form-horizontal form-one-not-reviewed" method="POST" action="/write-review/{{$location->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <div  class="gp-main-section-middle col-xs-12 no-padding" style="margin-top: 20px; text-align: justify;">
                                    <textarea class="form-submit-review" name="text" placeholder="Write your review here..." onfocus="this.placeholder=''" onblur="this.placeholder='Write your review here...'" style="width: 100%; height: 100px; padding: 15px 10px 15px 30px; border-color: #e6ecf5; border-radius: 5px;"></textarea>
                                    <div style="padding: 0; margin:0;">
                                        <input class="btn btn-primary button-primary-color custom-s-btn submit-button" type="submit" value="Submit" style="margin:0;">
                                    </div>
                                </div>
                            </form>
                            @if(!empty(\App\LocationReview::where('location_id', $location->id)->where('user_id', Auth::user()->id)->first()->review))
                             <form class="form-horizontal form-one-reviewed" method="POST" action="/write-review/{{$location->id}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <div class="gp-main-section-middle col-xs-12 no-padding" style="margin-top: 20px; text-align: justify;">
                                    <textarea class="form-submit-review" name="text" placeholder="Write your review here..." onfocus="this.placeholder=''" onblur="this.placeholder='Write your review here...'" style="width: 100%; height: 100px; padding: 15px 10px 15px 30px; border-color: #e6ecf5; border-radius: 5px;">{{\App\LocationReview::where('location_id', $location->id)->where('user_id', Auth::user()->id)->first()->review}}</textarea>
                                    <div style="padding: 0; margin:0;">
                                        <input class="btn btn-primary button-primary-color custom-s-btn submit-button" type="submit" value="Submit" style="margin:0;">
                                    </div>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                <div class="col-xs-12 gp-main-section">
                @if(is_null($userReview->review))
                    <div class="col-xs-12 gp-main-section-li no-padding">
                        <div class="gp-main-section-top col-xs-12 no-padding">
                            @if (!($user->profile_img))
                                <div class="gp-author-img">
                                    <img src='/img/no-image.png'>
                                </div>
                            @else
                                <div class="gp-author-img">
                                    <img src='/uploads/{{Auth::user()->profile_img}}'>
                                </div>
                            @endif
                            <div class="gp-author-date">
                                <div class="gp-author">
                                    {{$user->full_name}}
                                </div>
                                <div class="lv-main-review" style="font-weight: 500;color: #FBBC04;">
                                    {{$userReview->rating}}

                                    @if($userReview->rating == 1)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                    @elseif ($userReview->rating == 2)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                    @elseif ($userReview->rating == 3)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                    @elseif ($userReview->rating == 4)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                    @elseif ($userReview->rating == 5)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if((($userReview->user_id)==(Auth::user()->id)) && (!is_null($userReview->rating)) && (is_null($userReview->review)))
                        <form class="form-horizontal" method="POST" action="/write-review/{{$location->id}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                            <div class="gp-main-section-middle col-xs-12 no-padding" style="margin-top: 20px; text-align: justify;">
                                <textarea class="form-submit-review" name="text" placeholder="Write your review here..." onfocus="this.placeholder=''" onblur="this.placeholder='Write your review here...'" style="width: 100%; height: 100px; padding: 15px 10px 15px 30px; border-color: #e6ecf5; border-radius: 5px;">{{$userReview->review}}</textarea>
                                <div style="padding: 0; margin:0;">
                                    <input id="submit-button-2" class="btn btn-primary button-primary-color custom-s-btn" type="submit" value="Submit" disabled style="margin:0;">
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>
                @endif
                </div>
                 @foreach($reviewFromUser as $userReview)
                <div class="col-xs-12 gp-main-section">
                @if(!is_null($userReview->review))
                    <div class="col-xs-12 gp-main-section-li no-padding">
                        <div class="gp-main-section-top col-xs-12 no-padding">
                            @if (!($user->profile_img))
                                <div class="gp-author-img">
                                    <img src='/img/no-image.png'>
                                </div>
                            @else
                                <div class="gp-author-img">
                                    <img src="/uploads/{{\App\User::find($userReview->user_id)->profile_img}}">
                                </div>
                            @endif
                            <div class="gp-author-date">
                                <div class="gp-author">
                                    {{\App\User::find($userReview->user_id)->full_name}}
                                </div>
                                <div class="lv-main-review" style="font-weight: 500;color: #FBBC04;">
                                    {{$userReview->rating}}

                                    @if($userReview->rating == 1)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                    @elseif ($userReview->rating == 2)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                    @elseif ($userReview->rating == 3)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                    @elseif ($userReview->rating == 4)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star custom-review-star" style="font-size:1em;"></i>
                                    @elseif ($userReview->rating == 5)
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    @endif
                                </div>
                            </div>
                            <div class="gp-dots">
                                <i class="fas fa-ellipsis-h"></i>
                            </div>
                        </div>
                        <div class="gp-main-section-middle col-xs-12 no-padding" style="margin-top: 20px; text-align: justify;">
                            {{$userReview->review}}
                        </div>
                        <div class="gp-main-section-bottom col-xs-12 no-padding" style="margin-top: 20px;padding-top: 10px;">
                            <div class="gp-main-section-bottom-li">
                                <div style="float: left;">
                                    <i class="far fa-heart"></i>
                                </div>
                                <div class="gp-main-section-li-number">
                                    7
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                </div>
                @endforeach
            </div>
            @endforeach
            <!-- Left location section -->
            <div class="lv-left-section col-lg-7 tabcontent" id="photos" style="margin:15px 0 0 8px; background-color: white;">
                <div class="lv-left-info-title col-xs-12" style="padding: 13px;">
                </div>
                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video11.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video10.jpg">
                    </div>
                    <!--
                    <div class="lv-left-text col-xs-12 no-padding">
                        <div class="lv-left-name col-xs-12" style="text-transform: capitalize;">
                            {{$location->name}}
                        </div>
                        <div class="lv-left-rating col-xs-12">
                            <div class="review-stars col-xs-12 no-padding" style="margin-top: -3px;">
                                @if(empty($reviews['all'])) No Reviews @else @for($i=0;$i
                                <$reviews[ 'all'];$i++) <i class="fas fa-star custom-review-star" style="font-size:14px;color: #FBBC04;"></i>
                                    @endfor @for($i=0;$i

                                    < 5 - $reviews[ 'all'];$i++) <i class="fas fa-star custom-review-star" style="font-size: 14px;">
                                        </i>
                                        @endfor @endif
                            </div>
                            <div class="col-xs-12 no-padding custom-review-text" style="color: #fff;">
                                {{$reviews['all']}} users given average rate of {{round($reviews['average'],1)}}
                            </div>
                            <div style="float:left;margin-top:4px;font-size: 12px;">
                                @if($location->rating == NULL) @else - {{$location->rating}} reviews @endif
                            </div>
                        </div>
                    </div>
                -->
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video13.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video14.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video12.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video15.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video16.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video17.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video6.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video8.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video10.jpg">
                    </div>
                </div>

                <div class="col-xs-3 col-lg-3 no-padding image-paddings">
                    <div class="lv-left-img col-xs-12 no-padding">
                        <img class="photos-tab-hover-effect" src="https://html.crumina.net/html-olympus/img/video15.jpg">
                    </div>
                </div>
            </div>
            <div class="lv-left-section col-lg-7 tabcontent" id="map" style="margin:15px 0 0 8px; background-color: white;">
                <div class="col-md-12 no-padding image-paddings">
                    <div id="customLocationMap" style="border-radius: 5px;"></div>
                </div>
            </div>
            <!-- Left location section end -->
            <div class="col-xs-3 col-lg-3">
                <div class="lv-right-section col-xs-12 no-padding">
                    <div class="lv-left-info-title">
                        Location Info
                    </div>
                    <div class="lv-left-info-text">
                        <div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
                            <i class="fas fa-map-marker-alt"></i>{{$location->address}}
                            <br>
                            <span style="margin-left: 28px;">{{$location->postal_code}}</span>
                        </div>
                        <div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
                            <i class="fas fa-phone-volume"></i> {{$location->phone_number}}
                        </div>
                        <div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
                            <i class="fas fa-globe-europe"></i>
                            <a href="l/31">{{$location->website}}</a>
                        </div>
                    </div>
                </div>
            
                @if((empty(\App\LocationReview::where('location_id', $userReview->location_id)->where('user_id', Auth::user()->id)->first())))
                    
                @endif
                <div class="lv-left-review-section col-xs-12 no-padding">
                @if(!empty(\App\LocationReview::where('location_id', $userReview->location_id)->where('user_id', Auth::user()->id)->first()->user_id))
                @if(((\App\LocationReview::where('location_id', $userReview->location_id)->where('user_id', Auth::user()->id)->first()->user_id) == $user->id))
                    <div class="lv-left-info-title">
                        You voted
                    </div>
                    <div class="review-stars col-xs-12 no-padding location-rating-no-hover">
                        <div class="location-ratings" id="not-rated-location-2">
                            <i class="fas fa-star custom-review-star star-rating star-one"></i>
                            <i class="fas fa-star custom-review-star star-rating star-two"></i>
                            <i class="fas fa-star custom-review-star star-rating star-three"></i>
                            <i class="fas fa-star custom-review-star star-rating star-four"></i>
                            <i class="fas fa-star custom-review-star star-rating star-five"></i>
                        </div>
                        @if($userReview->rating==0)
                        <div class="location-ratings" id="not-rated-location">
                            <i class="fas fa-star custom-review-star star-rating star-one"></i>
                            <i class="fas fa-star custom-review-star star-rating star-two"></i>
                            <i class="fas fa-star custom-review-star star-rating star-three"></i>
                            <i class="fas fa-star custom-review-star star-rating star-four"></i>
                            <i class="fas fa-star custom-review-star star-rating star-five"></i>
                        </div>
                        @else
                        <div class="location-ratings" id="rated-location">
                        @for($i=0;$i<$userReview->rating;$i++)
                            <i class="fas fa-star custom-review-star stars-rated"></i>
                        @endfor
                        </div>
                        <div class="location-ratings" id="rated-location-2">
                        @for($i=0;$i< 5 - $userReview->rating;$i++)
                            <i class="fas fa-star custom-review-star"></i>
                        @endfor
                        </div>
                        @if ( !empty(\App\LocationReview::where('location_id', $userReview->location_id)->where('user_id', Auth::user()->id)->first()->rating) && ((\App\LocationReview::where('location_id', $userReview->location_id)->where('user_id', Auth::user()->id)->first()->updated_at)->diffInDays($currentTime)) >= 7)
                            <div>
                                <a id="change-rating" style="font-size:0.8em;"> Change your rating ?</a>
                            </div>
                        @endif
                        @endif
                    </div>
                @endif
                @endif
                @if(empty(\App\LocationReview::where('location_id', $userReview->location_id)->where('user_id', Auth::user()->id)->first()->user_id))
                    <div class="lv-left-info-title">
                        You did not vote
                    </div>
                    <div class="lv-left-info-title location-rating-no-hover">
                        <i class="fas fa-star custom-review-star star-rating star-one"></i>
                        <i class="fas fa-star custom-review-star star-rating star-two"></i>
                        <i class="fas fa-star custom-review-star star-rating star-three"></i>
                        <i class="fas fa-star custom-review-star star-rating star-four"></i>
                        <i class="fas fa-star custom-review-star star-rating star-five"></i>
                    </div>

                @endif

                    
                    <div class="lv-left-info-title custom-reviews-title col-xs-12 no-padding" style="margin-top: 5px;margin-bottom: -2px;">
                        Reviews
                    </div>
                    <div class="col-xs-12 no-padding custom-review-text">
                        <div>
                        {{$reviews['all']}} users given average rate of {{round($reviews['average'],1)}}
                        </div>
                    </div>
                    <div class="lv-left-review-summary col-xs-12 no-padding">
                        <div class="col-xs-12 no-padding">
                            <div class="lv-left-review-rating">
                                5
                            </div>
                            <div class="lv-left-review-bar progress">
                                <div class="progress-bar progress-bar-success custom-lv-left-review-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="custom-bar-five">
                                    <span class="sr-only">w</span>
                                    <span style="visibility: hidden;">{{$reviews['five']}}</span> 
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <div class="lv-left-review-rating">
                                4
                            </div>
                            <div class="lv-left-review-bar progress">
                                <div class="progress-bar progress-bar-success custom-lv-left-review-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="80" id="custom-bar-four">
                                    <span class="sr-only">w</span>
                                    <span style="visibility: hidden;">{{$reviews['four']}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <div class="lv-left-review-rating">
                                3
                            </div>
                            <div class="lv-left-review-bar progress">
                                <div class="progress-bar progress-bar-success custom-lv-left-review-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="60" id="custom-bar-three">
                                    <span class="sr-only">w</span> 
                                    <span style="visibility: hidden;">{{$reviews['three']}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <div class="lv-left-review-rating">
                                2
                            </div>
                            <div class="lv-left-review-bar progress">
                                <div class="progress-bar progress-bar-success custom-lv-left-review-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="40" id="custom-bar-two">
                                    <span class="sr-only">w</span>
                                    <span style="visibility: hidden;">{{$reviews['two']}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <div class="lv-left-review-rating">
                                1
                            </div>
                            <div class="lv-left-review-bar progress">
                                <div class="progress-bar progress-bar-success custom-lv-left-review-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="20" id="custom-bar-one">
                                    <span class="sr-only">w</span>
                                    <span style="visibility: hidden;">{{$reviews['one']}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="lv-right-section col-xs-12 no-padding">
                    <div class="lv-left-info-title">
                    </div>
                    <div class="lv-right-see-section col-xs-12 no-padding">
                        @foreach($images as $image)
                        <div class="lv-right-see-li col-xs-6">
                            <div class="lv-right-see-img col-xs-12 no-padding">
                                <img src="/img/locations/{{$image->image}}">
                            </div>
                            <div class="lv-right-see-title col-xs-12 no-padding">
                                {{substr($image->image, 5)}}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>

    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script type="text/javascript">
    $('.form-submit-review').keyup(function(){
    let formText = $.trim($(this).val());
    if (formText.length > 0) {
        $('.submit-button').css({'opacity':'1', 'transition':'opacity linear'});
        $(".submit-button").removeAttr("disabled");
    } else {
        $('.submit-button').css({'opacity':'0.5', 'transition':'opacity linear'});
        $(".submit-button").attr('disabled','disabled');
        }
    })

    $('.form-submit-review').keyup(function(){
    let formText = $.trim($(this).val());
    if (formText.length > 0) {
        $('#submit-button-2').css({'opacity':'1', 'transition':'opacity linear'});
        $("#submit-button-2").removeAttr("disabled");
    } else {
        $('#submit-button-2').css({'opacity':'0.5', 'transition':'opacity linear'});
        $("#submit-button-2").attr('disabled','disabled');
        }
    })
</script>
<script>
    function openTab(evt, name) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(name).style.display = "block";
        evt.currentTarget.className += " active";

        if(name === "map") {
            let location = {!! json_encode($location) !!};

            map = L.map('customLocationMap').setView([location.location.coordinates[1], location.location.coordinates[0]], 9)
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    id: id,
                    attribution: attribution
                }).addTo(map);

            let locations = {!! json_encode($locations) !!};

            locations.forEach(loc => {
                let iconSize;
                let className;

                locLat = loc.location.coordinates[1];
                locLng = loc.location.coordinates[0];

                if(location.id == loc.id) {
                    iconSize = [70, 70];
                    className = 'blinkingIcon';
                } else {
                    iconSize = [50, 50];
                }

                let locationMarker = L.marker([locLat, locLng], {
                    icon: new L.icon({
                        iconUrl: '/img/icons/location_shop.png',
                        iconSize: iconSize,
                        className: className
                    })
                });

                locationMarkers.addLayer(locationMarker);
            })
            locationMarkers.addTo(map);
        }
    }
</script>
<script>
    $(document).ready(function() {
        var getPhotosDiv = document.getElementById("tips");
        getPhotosDiv.style.display = "block";

        $('.lv-slick-carousel').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true
        });
    });

    var ratingDiv = document.getElementsByClassName("star-rating");
    var ratingClickedOne = false;
    var ratingClickedTwo = false;
    var ratingClickedThree = false;
    var ratingClickedFour = false;
    var ratingClickedFive = false;
    var changedRating; 
    $(".star-one").on({
        click: function(){
            ratingClickedOne = true;
            changedRating = 1;
            $('.star-one').addClass('reviewColor')
            $('.location-rating-no-hover').addClass('noHover')
            $(".rating-add-to-review-box").html('')
            $(".rating-add-to-review-box").append(changedRating);
            addReview(1)
        },
        mouseover: function(){
            $('.star-one').addClass('reviewColor')
        },
        mouseout: function(){
            if(!ratingClickedOne){
                $('.star-one').removeClass('reviewColor')
            }
        }
    });

    $(".star-two").on({
        click: function(){
            ratingClickedTwo = true;
            changedRating = 2;
            $('.star-one').addClass('reviewColor')
            $('.star-two').addClass('reviewColor')
            $('.location-rating-no-hover').addClass('noHover')
            $(".rating-add-to-review-box").html('')
            $(".rating-add-to-review-box").append(changedRating);
            addReview(2)
        },
        mouseover: function(){
            $('.star-one').addClass('reviewColor')
            $('.star-two').addClass('reviewColor')
        },
        mouseout: function(){
            if(!ratingClickedTwo){
                $('.star-one').removeClass('reviewColor')
                $('.star-two').removeClass('reviewColor')
            }
        }
    });

    $(".star-three").on({
        click: function(){
            ratingClickedThree = true;
            changedRating = 3;
            $('.star-one').addClass('reviewColor')
            $('.star-two').addClass('reviewColor')
            $('.star-three').addClass('reviewColor')
            $('.location-rating-no-hover').addClass('noHover')
            $(".rating-add-to-review-box").html('')
            $(".rating-add-to-review-box").append(changedRating);
            addReview(3)
        },
        mouseover: function(){
            $('.star-one').addClass('reviewColor')
            $('.star-two').addClass('reviewColor')
            $('.star-three').addClass('reviewColor')
        },
        mouseout: function(){
            if(!ratingClickedThree){
                $('.star-one').removeClass('reviewColor')
                $('.star-two').removeClass('reviewColor')
                $('.star-three').removeClass('reviewColor')
            }
        }
    });    

    $(".star-four").on({
        click: function(){
            ratingClickedFour = true;
            changedRating = 4;
            $('.star-one').addClass('reviewColor')
            $('.star-two').addClass('reviewColor')
            $('.star-three').addClass('reviewColor')
            $('.star-four').addClass('reviewColor')
            $('.location-rating-no-hover').addClass('noHover')
            $(".rating-add-to-review-box").html('')
            $(".rating-add-to-review-box").append(changedRating);
            addReview(4)
        },
        mouseover: function(){
            $('.star-one').addClass('reviewColor')
            $('.star-two').addClass('reviewColor')
            $('.star-three').addClass('reviewColor')
            $('.star-four').addClass('reviewColor')
        },
        mouseout: function(){
            if(!ratingClickedFour){
                $('.star-one').removeClass('reviewColor')
                $('.star-two').removeClass('reviewColor')
                $('.star-three').removeClass('reviewColor')
                $('.star-four').removeClass('reviewColor')
            }
        }
    }); 
    
    $(".star-five").on({
        click: function(){
            ratingClickedFive = true;
            changedRating = 5;
            $('.star-one').addClass('reviewColor')
            $('.star-two').addClass('reviewColor')
            $('.star-three').addClass('reviewColor')
            $('.star-four').addClass('reviewColor')
            $('.star-five').addClass('reviewColor')
            $('.location-rating-no-hover').addClass('noHover')
            $(".rating-add-to-review-box").html('')
            $(".rating-add-to-review-box").append(changedRating);
            addReview(5)
        },
        mouseover: function(){
            $('.star-one').addClass('reviewColor')
            $('.star-two').addClass('reviewColor')
            $('.star-three').addClass('reviewColor')
            $('.star-four').addClass('reviewColor')
            $('.star-five').addClass('reviewColor')
        },
        mouseout: function(){
            if(!ratingClickedFive){
                $('.star-one').removeClass('reviewColor')
                $('.star-two').removeClass('reviewColor')
                $('.star-three').removeClass('reviewColor')
                $('.star-four').removeClass('reviewColor')
                $('.star-five').removeClass('reviewColor')
            }
        }
    });

    $("#change-rating").click(function(){
        $('#rated-location').hide();
        $('#rated-location-2').hide();
        $('#not-rated-location-2').show();
        axios.get(`/update-location-review/${locationID}`, {params: { rating: changedRating }}).then(response=>{
        })
        /*$('.custom-review-star').removeClass('stars-rated')
        $('.stars-rated').css = ({"color":"grey"});*/
    });
    /*
    $("#change-rating").click(function(){
        var rating = 0;
        axios.get(`/update-location-review/${locationID}`, {params: { rating: rating }}).then(response=>{
            console.log(response.data);
            location.reload();
        })
    });
    */
    barOne = ("{{$reviews['one']}}" / "{{$reviews['all']}}") * 100
    barTwo = ("{{$reviews['two']}}" / "{{$reviews['all']}}") * 100
    barThree = ("{{$reviews['three']}}" / "{{$reviews['all']}}") * 100
    barFour = ("{{$reviews['four']}}" / "{{$reviews['all']}}") * 100
    barFive = ("{{$reviews['five']}}" / "{{$reviews['all']}}") * 100

    $('#custom-bar-one').css('width', barOne + '%')
    $('#custom-bar-two').css('width', barTwo + '%')
    $('#custom-bar-three').css('width', barThree + '%')
    $('#custom-bar-four').css('width', barFour + '%')
    $('#custom-bar-five').css('width', barFive + '%')

    locationID = '{{$location->id}}'



    function addReview(rating) {

        axios.get(`/add-location-review/${locationID}`, {params: { rating: rating }}).then(response=>{
            //need modifications
            $("#form-submit-review-none").show();
            if ($('#not-rated-location-2').length > 0) {
                $(".form-one-reviewed").show();
            }
            else {
                $(".form-one-not-reviewed").show();
            }
            
            let location_rating = response.data.rating;
            let location_review_one = response.data.reviews.one;
            let location_review_two = response.data.reviews.two;
            let location_review_three = response.data.reviews.three;
            let location_review_four = response.data.reviews.four;
            let location_review_five = response.data.reviews.five;
            let location_review_all = response.data.reviews.all;
            let location_review_average = response.data.reviews.average;
            
            let barOne = (location_review_one / location_review_all) * 100
            let barTwo = (location_review_two / location_review_all) * 100
            let barThree = (location_review_three / location_review_all) * 100
            let barFour = (location_review_four / location_review_all) * 100
            let barFive = (location_review_five / location_review_all) * 100
            if(rating==1) {
                $('.star-one').addClass('reviewColor')
                $('#custom-bar-one').css('width', barOne + '%')
            } else if (rating==2) {
                $('.star-one').addClass('reviewColor')
                $('.star-two').addClass('reviewColor')
                $('#custom-bar-two').css('width', barTwo + '%')
            } else if (rating==3) {
                $('.star-one').addClass('reviewColor')
                $('.star-two').addClass('reviewColor')
                $('.star-three').addClass('reviewColor')
                $('#custom-bar-three').css('width', barThree + '%')
            } else if (rating==4) {
                $('.star-one').addClass('reviewColor')
                $('.star-two').addClass('reviewColor')
                $('.star-three').addClass('reviewColor')
                $('.star-four').addClass('reviewColor')
                $('#custom-bar-four').css('width', barFour + '%')
            } else if (rating==5) {
                $('.star-one').addClass('reviewColor')
                $('.star-two').addClass('reviewColor')
                $('.star-three').addClass('reviewColor')
                $('.star-four').addClass('reviewColor')
                $('.star-five').addClass('reviewColor')
                $('#custom-bar-five').css('width', barFive + '%')
            }
            $('.custom-review-text').html('');
            $('.custom-review-text').append(
                    "<div>" + location_review_all + " users given rating with average rate of " + location_review_average + "</div>"
            )
        })
    }
</script>

@endsection