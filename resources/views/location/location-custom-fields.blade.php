@extends('layouts.app')

@section('per-page-css')
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
   <link href="/css/leaflet/leaflet.css" rel="stylesheet" />
   
@endsection

@section('content')
    <br/><br/><br/><br/><br/>
    <div class="container">

        <form class="form-horizontal" method="POST" action="/add-custom-location-fields/{{$locationID}}">
            {{ csrf_field() }}

            @foreach($fields as $key => $field)
            <div class="custom-location-fields col-xs-12 col-sm-6">
                <span style="font-weight: 600;text-transform: capitalize;">{{$field->name}}</span>
                <input class="form-control" type="{{$field->type}}" name="{{$field->name}}" value="{{$field->content}}">
            </div>
            @endforeach

            <div class="col-xs-12">
                <input class="btn btn-primary" type="submit" value="Next">
            </div>

        </form>
    </div>

@endsection

@section('map-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection
