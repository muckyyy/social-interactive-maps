@extends('layouts.app')

@section('per-page-css')
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
   <link href="/css/leaflet/leaflet.css" rel="stylesheet" />
   
@endsection

@section('content')

	<div class="navigation-section-stabilizer">

	<div class="container">
		<div class="col-xs-offset-3 col-xs-6 custom-add-location-section">

		<div class="custom-add-location-title col-xs-12">
			{{$type}} location
		</div>

		<div class="custom-add-location-form col-xs-12">

			<form class="form-horizontal" method="POST" action="/add-location-post">
			    {{ csrf_field() }}

			    <div class="col-xs-12 no-padding">
				    <div class="custom-form-input-title">
				    	Select category <span>*</span>
				    </div>
				    <select id='categories-section-1' class="form-control" style="margin-bottom:15px;padding-left: 5px;" name="category_id">
				    	<option value="">Select category</option>
				    	@foreach($categories as $category)
				    		<option value="{{$category->id}}">{{$category->name}}</option>
				    	@endforeach
				    </select>
				</div>

				<div class="category-1">

				</div>

				<div class="category-2">

				</div>

				<div class="category-3">

				</div>

				<div class="category-4">

				</div>

				<div class="category-5">

				</div>

				<br/>


				<script>

					$('#categories-section-1').on('change', function(){

						let id = $('#'+this.id+' option:selected').val()
						console.log('selected id', id)
						categoryAjax(id, '.category-1', 1, 'secondChild')
					})

					function secondChild(element) {
						let id = $('#'+element.id+' option:selected').val()
						categoryAjax(id, '.category-2', 2, 'thirdChild')
					}

					function thirdChild(element) {
						let id = $('#'+element.id+' option:selected').val()
						categoryAjax(id, '.category-3', 3, 'fourthChild')
					}

					function fourthChild(element) {
						let id = $('#'+element.id+' option:selected').val()
						categoryAjax(id, '.category-4', 4, 'fifthChild')
					}

					function fifthChild(element) {
						let id = $('#'+element.id+' option:selected').val()
						categoryAjax(id, '.category-5', 5, 'fifthChild')
					}

					function categoryAjax(id, element, status, functionName) {

						let appendHtml = ''
						let statusID = status+1

						for(let i=status; i<=5; i++){
							console.log('status', status)
							console.log('status i', i)
							$('.category-'+i).empty();
						}

						$(element).empty()

						$.ajax
						({
							dataType: 'json',
							type: "GET",
							url: "/get-child-categories",
							data: {"id":id},
								success: function(data)
								{
									console.log('id', id)
									if(data != '') {
										console.log('data', data)
										appendHtml += "<select id='categories-section-"+statusID+"' onchange='"+functionName+"(this)'' class='form-control custom-added-category' style='padding-left: 5px;'' name='category_id'>"
										appendHtml += "<option value=''>Select category</option>"
										data.forEach(function(item){
											appendHtml += "<option value='"+item.id+"''>"+item.name+"</option>"
										})
										appendHtml += "</select>"

										$(element).append(appendHtml)
									}
									for(let i=status; i<=5; i++){
										$('#valcat'+i).val('');
									}
									$('#valcat'+status).val(id)
								}
						});
					}
				</script>

				<input id='default-location-icon' type="hidden" name="icon">
				<input id='valcat1' type="hidden" name="cat1">
				<input id='valcat2' type="hidden" name="cat2">
				<input id='valcat3' type="hidden" name="cat3">
				<input id='valcat4' type="hidden" name="cat4">
				<input id='valcat5' type="hidden" name="cat5">

				<div class="col-xs-12 no-padding" style="margin-top: -10px;">
				    <div class="custom-form-input-title">
				    	City <span>*</span>
				    </div>
				    <select name="city_id" class="form-control custom-location-select">
				    	@foreach($cities as $city)
				    		<option value="{{$city->id}}">{{$city->name}}</option>
				    	@endforeach
				    </select>
				</div>

				<input type="hidden" name="type" value="{{$type}}">

				<div class="col-xs-12 no-padding">
				    <div class="custom-form-input-title">
				    	Name <span>*</span>
				    </div>
				    <input class="form-control" type="text" name="name" placeholder="Name" required="required">
				</div>

				<div class="col-xs-12 no-padding">
				    <div class="custom-form-input-title">
				    	Description <span>*</span>
				    </div>
				    <textarea class="form-control" name="description" required="required"></textarea>
				</div>

				<div class="col-xs-12 no-padding">
				    <div class="custom-form-input-title">
				    	Address <span>*</span>
				    </div>
				    <input class="form-control" type="text" name="address" placeholder="Addess" required="required">
				</div>

				<div class="col-xs-12 no-padding">
				    <div class="custom-form-input-title">
				    	Postal Code <span>*</span>
				    </div>
				    <input class="form-control" type="text" name="postal_code" placeholder="Postal Code" required="required">
				</div>

				<div class="col-xs-12 no-padding">
				    <div class="custom-form-input-title">
				    	Phone Number <span style="color: #00b6f1;">(optional)</span>
				    </div>
				    <input class="form-control" type="text" name="phone_number" placeholder="Phone Number">
				</div>


				<!-- add open closed fields-->

				<div class="col-xs-12 no-padding">
				    <div class="custom-form-input-title">
				    	Website <span style="color: #00b6f1;">(optional)</span>
				    </div>
				    <input class="form-control" type="text" name="website" placeholder="Website">
				</div>

                <div class="col-xs-12 no-padding">
                	<!--
                    <div class="custom-form-input-title">
                        Location on map:
                    </div>
                    <div id="location-map" style="width: 500px; height: 500px;"></div>
                    -->
                    <input type="hidden" name="location-lat" id="location-lat" value="{{$lat}}">
                    <input type="hidden" name="location-long" id="location-long" value="{{$long}}">

                </div>

				<button id='add-location-btn-public' class="add-location-btn" style="margin-top: 30px;border: 0;cursor: pointer;">
					Add location
				</button>

			</form>

		</div>

		</div>
	</div>

	</div>



    <script src="/js/map/location/add-location.js" rel="stylesheet" /> </script>

	<script>

		$( document ).ready(function() {
			$('.custom-location-select').select2();
		});

		//Upload icons
		getIcons();

		$('#location-category-admin').change(function(){

			getIcons();

		})

		function getIcons() {
			let locId = $('#location-category-admin option:selected').val();

			$.ajax
			({
				dataType: 'json',
				type: "GET",
				url: "/get-admin-location-icons",
				data: {"id":locId},
					success: function(data)
					{
						let appendHtml = ''
						$('.location-icons').empty()

						appendHtml += "<div class='col-xs-12 no-padding' style='margin-top: 10px;'>"
						appendHtml += "<div id='testt' class='admin-dropdown-title' style='font-weight:600;'>Category icon</div>"
						data.forEach(function(item){
							appendHtml += "<div class='admin-dropdown-icons' onclick='getDefaultIcon(this)'>"
								appendHtml += "<img src='/img/category-icons/"+item.icon+"'>"	
							appendHtml += "</div>"
							console.log('data',item)
						})
						
						appendHtml += "</div>"

						$('.location-icons').append(appendHtml)
						
					}
			});
		}

		function getDefaultIcon(element){
			let defaultImage = (element.children[0].src).slice(-10)
			defaultImage = defaultImage.replace('/', '');
			console.log(defaultImage)
			$('.admin-dropdown-icons').css('background', '#ffffff')
			element.style.background = '#3097d1'

			$('#default-location-icon').val(defaultImage)
		}
	</script>

@endsection

@section('map-scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection
