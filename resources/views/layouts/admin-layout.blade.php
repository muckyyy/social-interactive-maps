<!DOCTYPE html>
<html>
    <head>
        
        <!-- Title -->
        <title>Avatars map admin section</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href="admin-v2/assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="admin-v2/assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="admin-v2/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="admin-v2/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="admin-v2/assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/> 
        <link href="admin-v2/assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>  
        <link href="admin-v2/assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>  
        <link href="admin-v2/assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
        <link href="admin-v2/assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="admin-v2/assets/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/> 
        <link href="admin-v2/assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet" type="text/css"/>   
        <link href="admin-v2/assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>  
        <link href="admin-v2/assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>    
            
        <!-- Theme Styles -->
        <link href="admin-v2/assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
        <link href="admin-v2/assets/css/themes/green.css" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="admin-v2/assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        <script src="admin-v2/assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
        <script src="admin-v2/assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>
        
    </head>
    <body class="page-header-fixed">
        <div class="overlay"></div>

        @if (Session::has('flash_message'))
            <div class="master_message">{{ Session::get('flash_message') }}</div>
        @endif
        @if (Session::has('flash_message_error'))
            <div class="master_message_error">{{ Session::get('flash_message_error') }}</div>
        @endif

        <!-- Top nav -->
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="index.html" class="logo-text"><span>Avatars map</span></a>
                    </div>
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-left">
                                <li>        
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                                </li>
                                <li>        
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown"><i class="fa fa-bell"></i><span class="badge badge-success pull-right">3</span></a>
                                    <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                        <li><p class="drop-title">You have 3 pending tasks !</p></li>
                                        <li class="dropdown-menu-list slimscroll tasks">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="#">
                                                        <div class="task-icon badge badge-success"><i class="icon-user"></i></div>
                                                        <span class="badge badge-roundless badge-default pull-right">1min ago</span>
                                                        <p class="task-details">New user registered.</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="task-icon badge badge-danger"><i class="icon-energy"></i></div>
                                                        <span class="badge badge-roundless badge-default pull-right">24min ago</span>
                                                        <p class="task-details">Database error.</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="task-icon badge badge-info"><i class="icon-heart"></i></div>
                                                        <span class="badge badge-roundless badge-default pull-right">1h ago</span>
                                                        <p class="task-details">Reached 24k likes</p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="drop-all"><a href="#" class="text-center">All Tasks</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                        <span class="user-name">{{$user->name}}<i class="fa fa-angle-down"></i></span>
                                        <img class="img-circle avatar" src="assets/images/avatar1.png" width="40" height="40" alt="">
                                    </a>
                                    <ul class="dropdown-menu dropdown-list" role="menu">
                                        <li role="presentation">
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Top nav end -->



            <!-- Navigation left -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <div class="sidebar-header">
                        <div class="sidebar-profile">
                            <a href="javascript:void(0);" id="profile-menu-link">
                                <div class="sidebar-profile-image">
                                    <img src="assets/images/profile-menu-image.png" class="img-circle img-responsive" alt="">
                                </div>
                                <div class="sidebar-profile-details">
                                    <span>{{$user->name}}<br></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <ul class="menu accordion-menu">

                        <li class="active"><a href="index.html" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>Dashboard</p></a></li>

                        <li><a href="profile.html" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list"></span><p>Category management</p></a></li>

                        <li><a href="profile.html" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-user"></span><p>Avatar settings</p></a></li>

                        <li><a href="profile.html" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-th"></span><p>Tile server management</p></a></li>

                        <li><a href="profile.html" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-map-marker"></span><p>Map settings</p></a></li>

                        <li><a href="profile.html" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-map-marker"></span><p>Beacon administration</p></a></li>

                        <li><a href="profile.html" class="waves-effect waves-button"><span class="menu-icon icon-eye"></span><p>Locations management</p></a></li>

                        <li><a href="profile.html" class="waves-effect waves-button"><span class="menu-icon icon-users"></span><p>Users</p></a></li>
                    </ul>

                </div>
            </div>
            <!-- Navigation left end -->







            <div class="page-inner">
                <div class="page-title">
                    <h3>Dashboard</h3>
                </div>
                <div id="main-wrapper">
                    <div class="row">
                        OVDJE CONTENT IDE
                    </div>
                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s">2019 &copy; Avatars map</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->

        <div class="cd-overlay"></div>
    

        <!-- Javascripts -->
        <script src="admin-v2/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
        <script src="admin-v2/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="admin-v2/assets/plugins/pace-master/pace.min.js"></script>
        <script src="admin-v2/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="admin-v2/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="admin-v2/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="admin-v2/assets/plugins/switchery/switchery.min.js"></script>
        <script src="admin-v2/assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="admin-v2/assets/plugins/offcanvasmenueffects/js/classie.js"></script>
        <script src="admin-v2/assets/plugins/offcanvasmenueffects/js/main.js"></script>
        <script src="admin-v2/assets/plugins/waves/waves.min.js"></script>
        <script src="admin-v2/assets/plugins/3d-bold-navigation/js/main.js"></script>
        <script src="admin-v2/assets/plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="admin-v2/assets/plugins/jquery-counterup/jquery.counterup.min.js"></script>
        <script src="admin-v2/assets/plugins/toastr/toastr.min.js"></script>
        <script src="admin-v2/assets/plugins/flot/jquery.flot.min.js"></script>
        <script src="admin-v2/assets/plugins/flot/jquery.flot.time.min.js"></script>
        <script src="admin-v2/assets/plugins/flot/jquery.flot.symbol.min.js"></script>
        <script src="admin-v2/assets/plugins/flot/jquery.flot.resize.min.js"></script>
        <script src="admin-v2/assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="admin-v2/assets/plugins/curvedlines/curvedLines.js"></script>
        <script src="admin-v2/assets/plugins/metrojs/MetroJs.min.js"></script>
        <script src="admin-v2/assets/js/modern.js"></script>
        <script src="admin-v2/assets/js/pages/dashboard.js"></script>
        
    </body>
</html>