<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Social Maps V2</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/navigation.css') }}" rel="stylesheet">
    <link href="{{ asset('css/beacon.css') }}" rel="stylesheet">
    <script src="/js/leaflet/leaflet.js"></script>

    @yield('per-page-css')
    <link href="{{ asset('css/leaflet-override.css') }}" rel="stylesheet">

    <script src='/js/map/config.js?{{ time() }}'></script>
    <script src="/js/vue-common/vue-site-config.js"></script>

    @if(\Request::route()->getName() == "home.index")
    <style>
        body {overflow-y: hidden}
    </style>
    @endif
</head>

<body>
    <div id="app" class="position-relative" style="background: url('/img/geneva1.jpg'); background-size: contain;">
        @if (Session::has('flash_message'))
        <div class="master_message">{{ Session::get('flash_message') }}</div>
        @endif
        @if (Session::has('flash_message_error'))
        <div class="master_message_error">{{ Session::get('flash_message_error') }}</div>
        @endif

        @include('navigation.main')

        @yield('content')
    </div>

    <script src='/js/app.js'></script>
    
    @yield('map-scripts')

    <script>
        $('.master_message').delay(3000).fadeOut(1500);
        $('.master_message_error').delay(3000).fadeOut(1500);
    </script>

    <script>
        var leftMenuClosed = false
        $('.left-menu-close-btn').click(function() {
            if (leftMenuClosed) {
                $('.left-menu-section').removeClass('left-menu-closed')
                $('.left-menu-section').addClass('left-menu-opened')
                $('.left-menu-close-btn').empty()
                $('.left-menu-close-btn').html('<i class="fas fa-chevron-circle-left"></i>')
                $('.left-menu-ul').fadeIn(500)
                leftMenuClosed = false
            } else {
                $('.left-menu-section').removeClass('left-menu-opened')
                $('.left-menu-section').addClass('left-menu-closed')
                $('.left-menu-close-btn').empty()
                $('.left-menu-close-btn').html('<i class="fas fa-chevron-circle-right"></i>')
                $('.left-menu-ul').hide()
                leftMenuClosed = true
            }
        })

        var leftPublicMenuClosed = false
        $('.custom-right-public-section-close-btn').click(function() {
            if (leftPublicMenuClosed) {
                $('.custom-right-public-section').removeClass('left-menu-closed-public')
                $('.custom-right-public-section').addClass('left-menu-opened-public')
                $('.custom-right-public-section-close-btn').empty()
                $('.custom-right-public-section-close-btn').html('<i class="fas fa-chevron-circle-right"></i>')
                $('.left-menu-ul-public').show()
                leftPublicMenuClosed = false
            } else {
                $('.custom-right-public-section').removeClass('left-menu-opened-public')
                $('.custom-right-public-section').addClass('left-menu-closed-public')
                $('.custom-right-public-section-close-btn').empty()
                $('.custom-right-public-section-close-btn').html('<i class="fas fa-chevron-circle-left"></i>')
                $('.left-menu-ul-public').hide()
                leftPublicMenuClosed = true
            }
        })
        /*
        var rightMenuClosed = false
        $('.right-menu-close-btn').click(function() {
            if (rightMenuClosed) {
                $('.right-menu-section').removeClass('right-menu-closed')
                $('.right-menu-section').addClass('right-menu-opened')
                $('.right-menu-close-btn').empty()
                $('.right-menu-close-btn').html('<i class="fas fa-chevron-circle-right right-menu-section-btn"></i>')
                $('.right-menu-ul').show()
                rightMenuClosed = false
            } else {
                $('.right-menu-section').removeClass('right-menu-opened')
                $('.right-menu-section').addClass('right-menu-closed')
                $('.right-menu-close-btn').empty()
                $('.right-menu-close-btn').html('<i class="fas fa-chevron-circle-left"></i>')
                $('.right-menu-ul').hide()
                rightMenuClosed = true
            }
        })
        */

        $('.right-menu-close-btn').click(function() {
            $('.right-menu-section').removeClass('right-menu-opened')
            $('.right-menu-section').addClass('right-menu-closed')
            $('.right-menu-close-btn').empty()
            $('.right-menu-section-close-modal-btn').hide()
            $('.right-menu-ul').hide()
        })

        $('.right-menu-section-close-modal-btn').click(function() {
            $('.right-menu-section').fadeOut(500)
        })
    </script>

    <script>
        $('.custom-pin-btn').click(function() {
            $('.pin-menu-section').fadeIn(500)
            allowMovingPin = true
        })

        $('.create-pin-next-btn').click(function() {
            $('#pin-form-description').val($('#pin-description').val())
            $('#pin-form-type').val($('#pin-type').val())
            $('.add-pin-btn').fadeIn(1000)
            $('.cancel-pin-btn').fadeIn(1000)
            $('.pin-menu-section').fadeOut(500)
        })
    </script>

    <script>
        $('.custom-friends-icon').click(function() {
            $('.custom-friends-btn-dropdown').fadeToggle(500)
        })
    </script>

    {{-- GOOGLE ANALYTICS --}}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154370639-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-154370639-1');
    </script>

</body>

</html>