@extends('layouts.app')

@section('per-page-css')
    <link rel="stylesheet" href="/css/leaflet/leaflet.awesome-markers.css">
    <link rel='stylesheet' href='/css/leaflet/leaflet.css' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
@endsection

@section('content')
<main-section></main-section>
<input class="d-none" id="markerActioninput" value="">

<div id="notify-user-radius" class="success-message-radius alert alert-success" role="alert">
</div>
<!-- Pin menu popout section -->
<div class="pin-menu-section">
    <div class="col-xs-12 col-sm-offset-4 col-sm-4">
        <textarea class="form-control custom-pin-textarea" id='pin-description'>Description</textarea>
        <div class="btn btn-info create-pin-next-btn">
            Next
        </div>
    </div>
</div>
<form class="form-horizontal" method="POST" action="/create-pin">
    {{ csrf_field() }}
    <input type="hidden" id='pin-form-description' name="description">
    <input type="hidden" id='pin-form-type' name="type">
    <input type="hidden" id='pin-form-lat' name="lat">
    <input type="hidden" id='pin-form-long' name="long">
    <input class="add-pin-btn btn btn-success" type="submit" disabled="disabled" value="Add pin">
    <a href="/">
        <input class="cancel-pin-btn btn btn-danger" value="Cancel">
    </a>
</form>

<div id="search-navigation-toggle" class="left-menu-section" style="display: none; background: transparent;">
    <div class="left-menu-close-btn">
        <i class="fas fa-chevron-circle-left"></i>
    </div>
    <div class="left-menu-ul scrollbar_overflow-2 d-none searchResults" id="location-profile-show">

    </div>
</div>
<!-- map modal -->
<div class="container">
  <!-- Modal -->
    <modal 
        :open="true" 
        icon="fas fa-plus-circle"
        custom-close-class="locationsModal" 
        :footer="false" 
        class="locationsModal d-none"
    >
        <template v-slot:title>
            Add Location
        </template>
        <add-location class="pb-4" />
    </modal>
    <modal 
        :open="true" 
        icon="fas fa-comments"
        custom-close-class="mapPostModal" 
        :footer="false" 
        class="mapPostModal d-none"
    >
        <template v-slot:title>
           Post
        </template>
        <map-post class="pb-4" />
    </modal>

</div>
<div id="left-menu-section-toggle" class="left-menu-section">
    <online-users></online-users>   
    <div class="left-menu-ul scrollbar_overflow">
        <map-search class="mt-3"></map-search>
        <personal-feed :posts="{{ json_encode($posts) }}" />
    </div>
</div>

<!-- indicators -->
<div class="map-data-indicator">
    <span class="mr-1">
        <i class="fas fa-landmark"></i>: 
        <span id="locationsIndicator">0</span>
    </span>
    <span>
        <i class="fas fa-users"></i>: 
        <span id="usersIndicator">0</span>
    </span>
</div>

@include('map.map')

@endsection

@include('partials.data_define')

@section('map-scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        {{-- map layers --}}
    <script src="/js/map/map_layers/map_layers.js"></script>
    <script src="/js/map/map_layers/other_avatars_layer.js"></script>

    <script src="/js/map/beacons.js"></script>
    <script src="/js/map/avatars.js"></script>
    <script src="/js/map/main-avatar.js"></script>
    <script src="/js/map/map-settings.js"></script>
    <script src="/js/map/map-search.js"></script>
    <script src="/js/map/locations.js"></script>
    <script src='/js/leaflet/leaflet.js'></script>
    <script src="/js/leaflet/leaflet.awesome-markers.js"></script>
    <script src='/js/map/config.js?{{ time() }}'></script>
    <script src='/js/map/initialize.js?{{ time() }}'></script>
    <script src='/js/map/homepage-common.js'></script>
@endsection
