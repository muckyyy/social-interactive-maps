@extends('layouts.app')

@section('per-page-css')
    <link rel='stylesheet' href='/css/leaflet/leaflet.css' />
@endsection

@section('content')

	@include('partials.login_form')
	<!-- Right menu section -->
	<div class="right-menu-section">

		<div class="right-menu-section-close-modal-btn">
			<i class="fas fa-times"></i>
		</div>

	    <div class="right-menu-close-btn">
	        <i class="fas fa-chevron-circle-right right-menu-section-btn"></i>
	    </div>

	    <div class="right-menu-ul">

	        <div class="right-menu-ul-main-img">
	            <img src="https://media-cdn.tripadvisor.com/media/photo-s/0e/cc/0a/dc/restaurant-chocolat.jpg">
	            <div class="right-menu-ul-img-layout">
	                <div class="col-xs-12 right-menu-ul-layout-title">
	                    Super Duper Burgers
	                </div>
	                <div class="col-xs-12 right-menu-ul-layout-rating">
	                    5.0
	                    <i class="fas fa-star right-menu-ul-star"></i>
	                    <i class="fas fa-star right-menu-ul-star"></i>
	                    <i class="fas fa-star right-menu-ul-star"></i>
	                    <i class="fas fa-star right-menu-ul-star"></i>
	                    <i class="fas fa-star right-menu-ul-star"></i>
	                </div>
	                <div class="col-xs-12 right-menu-ul-subtext">
	                    Hamburger restaurant
	                </div>
	            </div>
	        </div>

	        <div class="right-menu-ul-info-section col-xs-12 no-padding">

	            <div class="right-menu-ul-info-title col-xs-12">
	                Info
	            </div>

	            <div class="right-menu-ul-info-li col-xs-12">
	                <i class="fas fa-map-marker-alt"></i>
	                721 Market St, San Francisco, CA 94103
	            </div>

	            <div class="right-menu-ul-info-li col-xs-12">
	                <i class="fas fa-globe-americas"></i>
	                superduperburgers.com
	            </div>

	            <div class="right-menu-ul-info-li col-xs-12">
	                <i class="fas fa-phone"></i>
	                +3482 122312312 
	            </div>

	            <div class="right-menu-ul-info-li col-xs-12">
	                <i class="far fa-clock"></i>
	                09:00AM - 05:00PM
	            </div> 
	            
	        </div>

	        <div class="right-menu-ul-photos-section col-xs-12 no-padding">

	            <div class="right-menu-ul-photos-title col-xs-12">
	                Photos
	            </div>

	            <div class="right-menu-ul-photos-ul col-xs-12 no-padding">

	                <div class="right-menu-ul-photos-li col-xs-6">
	                    <img src="/img/test/test-photo-1.jpeg">
	                </div>

	                <div class="right-menu-ul-photos-li col-xs-6">
	                    <img src="/img/test/test-photo-2.jpeg">
	                </div>

	                <div class="right-menu-ul-photos-li col-xs-6">
	                    <img src="/img/test/test-photo-3.jpeg">
	                </div>

	                <div class="right-menu-ul-photos-li col-xs-6">
	                    <img src="/img/test/test-photo-4.jpeg">
	                </div>

	                <div class="right-menu-ul-photos-li col-xs-6">
	                    <img src="/img/test/test-photo-5.jpeg">
	                </div>

	            </div>

	        </div>
	        

	    </div>

	</div>
	<!-- Right menu section end -->

    @include('map.map')

@endsection

@section('map-scripts')
    <script src='/js/leaflet/leaflet.js'></script>
    <script src='/js/map/pins.js'></script>


    <script src='/js/map/guest-map/guestConfig.js'></script>
    <script src='/js/map/guest-map/guestInitialize.js'></script>
    <script src='/js/map/guest-map/guestMapCommonJquery.js'></script>
@endsection
