<div class="user-profile-nav-section">

	<div class="user-profile-nav-menu-section">

		<a href="/profile">
			<div class="user-profile-nav-menu-li" id='user-profile'>
				General
			</div>
		</a>

		<a href="/profile/locations">
			<div class="user-profile-nav-menu-li" id='user-privacy'>
				Locations
			</div>
		</a>

		<a href="/profile/privacy">
			<div class="user-profile-nav-menu-li" id='user-privacy'>
				Privacy
			</div>
		</a>
		
	</div>

</div>

<script>
	let path = window.location.pathname

	if(path == '/profile'){
		$('#user-profile').css('background', '#247BA0');
		$('#user-profile').css('color', '#fff');
	}

	if(path == '/profile/privacy'){
		$('#user-privacy').css('background', '#247BA0');
		$('#user-privacy').css('color', '#fff');
	}
</script>