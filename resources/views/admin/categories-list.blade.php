@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection


@section('content')

	<div class="navigation-section-stabilizer">
	
	<div class="container">

		@include('admin.menu')

		<div class="main-admin-section col-xs-12">

			<div class="col-xs-8" style="margin-top: -15px;">
				<div class="admin-category-title">
					Categories list:
				</div>

				@foreach($categories as $category)
					<div class="category-li">
						
						<form class="form-horizontal" method="POST" action="/delete-category" style="float: right;">
						    {{ csrf_field() }}

						    <input type="hidden" value="{{$category->id}}" name="id">

						    @if($category->active == 0)
					    	<a href="/admin-active-category/{{$category->id}}/1">
					        	<span class="btn btn-danger admin-delete-btn" style="width: 100px;margin-left: 15px;">
					        		Deactive
					        	</span>
					    	</a>
					    	@else
					    	<a href="/admin-active-category/{{$category->id}}/0">
					        	<span class="btn btn-success admin-delete-btn" style="width: 100px;margin-left: 15px;">
					        		Active
					        	</span>
					    	</a>
					    	@endif

						    <input type="submit" class="btn btn-danger admin-delete-btn" value="Delete">

						    <a href="/admin-edit-category/{{$category->id}}">
						    	<span class="btn btn-success admin-delete-btn" style="width: 100px;margin-right: 15px;">Edit</span>
							</a>

							
						</form>

						<div class="category-icon-img-section">
							{!! $category->icon !!}
						</div>

						<div style="float: left;">
							{{$category->name}}
						</div>

						

					</div>
				@endforeach
			</div>

		</div>

	</div>
	</div>

@endsection