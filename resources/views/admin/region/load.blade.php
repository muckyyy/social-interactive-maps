<option value="0">Select region</option>
@foreach($regions as $region)
<option value="{{$region->id}}"
       @if (!empty($country))
       @if ($country->id == $region->country)
                                selected
       @endif
       @endif
>{{$region->name}}</option>
@endforeach
