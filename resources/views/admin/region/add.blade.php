@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/leaflet/leaflet.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="navigation-section-stabilizer">

<div class="container">

	@include('admin.menu')

	<div class="main-admin-section col-xs-12">

		<div class="col-xs-6">

			<div class="admin-category-title">
				Adding new region
			</div>

			<form class="form-horizontal" method="POST" action="/admin-region/save">
			    {{ csrf_field() }}

                <input type="text" class="form-control" name="name" placeholder="Name" required="required" value="{{$region->name}}">
                <select name="country" class="form-control" style="margin-top: 15px;">

			    	@foreach($countries as $country)
			    		<option class="admin-select-li" value="{{$country->id}}"
                                @if ($country->id == $region->country)
                                selected
                                @endif >{{$country->name}}</option>
			    	@endforeach
                </select> <br>

               
                <textarea name="about" cols="50" rows="5">{{$region->about}}</textarea><br><br>
                <input type="text" class="form-control" name="website" placeholder="Web" value="{{$region->website}}"><br>
                <input type="text" class="form-control" name="population" placeholder="Population" value="{{$region->population}}"><br>
                <input type="text" class="form-control" name="area" placeholder="Area" value="{{$region->area}}"><br>
                <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$region->phone}}"><br>
                <input type="hidden" name="icon" value="{{$region->icon}}">
                <input type="hidden" name="action" value="add">
                <input type="hidden" name="id" value="{{$region->id}}">

			    <input type="hidden" name="icon" id='admin-category-icon' required="required">
                

			    <input type="submit" class="btn btn-primary" value="Update" style="float:right;width:200px;margin-top: 15px;">

			</form>

		</div>

		

	</div>


</div>
</div>
@section('map-scripts')
    <script src='/js/leaflet/leaflet.js'></script>
    <script src='/js/admin/commonJs.js'></script>
    <script src='/js/admin/cityedit.js'></script>
@endsection

@endsection
