@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection

@section('content')

	<div class="navigation-section-stabilizer">
	
	<div class="container">

		@include('admin.menu')

		<div class="main-admin-section col-xs-12">

				<div class="col-xs-6">

					<div class="admin-category-title">
						Edit <span style="color: #3097d1;">{{$category->name}}</span> category:
					</div>

					<form class="form-horizontal" method="POST" action="/change-category-basics-post/{{$category->id}}" enctype="multipart/form-data">
					    {{ csrf_field() }}

					    <input type="text" class="form-control" name="name" placeholder="Name" required="required" value="{{$category->name}}">

					    <div class="file-icon-text" style="margin-top: 15px;">
					    	Parent category
					    </div>

					    <select name="parent_id" class="form-control">
					    	<option value="0">None (Master)</option>
					    	@foreach($categories as $cat)
					    	    <option value="{{$cat->id}}" {{ ($cat->id == $category->parent_id) ? 'selected="selected"' : '' }}> {{$cat->name }} </option>
					    	@endforeach
					    </select>

					    <input type="hidden" name="icon" id='admin-category-icon' required="required">

					    <div class="file-icon-section col-xs-12 no-padding">

					    	<div class="file-icon-text">
					    		Custom icon
					    	</div>

						    <input type="file" name="icon_file">
						</div>

						<div class="category-tags-section col-xs-12 no-padding" style="margin-top: 15px;font-size: 20px;font-weight: 600;color: #636b6f;">
							Tags
							<input type="tags" name="tags" class="form-control" value="{{$tagString}}" placeholder="For example #travel #epic #stuff">
						</div>

					    <input type="submit" class="btn btn-primary" value="Add" style="float:right;width:100%;margin-top: 30px;">

				</div>


				<div class="col-xs-6">

					<div class="admin-category-title">
						Stock icons:
					</div>

					<div class="category-icons">
						<div class="category-icon">
							<i class="fas fa-utensils"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-coffee"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-anchor"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-archway"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-bed"></i>
						</div>
						<div class="category-icon">
							<i class="fab fa-bitcoin"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-book"></i>
						</div>

						<div class="category-icon">
							<i class="fas fa-air-freshener"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-car"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-cart-arrow-down"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-charging-station"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-city"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-cocktail"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-dice"></i>
						</div>

						<div class="category-icon">
							<i class="fas fa-envelope"></i>
						</div>
						<div class="category-icon">
							<i class="fab fa-envira"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-fire-extinguisher"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-gamepad"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-gift"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-plane"></i>
						</div>

						<div class="category-icon">
							<i class="fab fa-atlassian"></i>
						</div>
						<div class="category-icon">
							<i class="fab fa-audible"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-camera-retro"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-basketball-ball"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-baseball-ball"></i>
						</div>

						<div class="category-icon">
							<i class="fas fa-cubes"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-drum"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-fire-extinguisher"></i>
						</div>
						<div class="category-icon">
							<i class="fas fa-flask"></i>
						</div>
						<div class="category-icon">
							<i class="fab fa-fort-awesome"></i>
						</div>

					</div>

				</div>

			</div>

				</form>

			</div>

		</div>

	</div>
	</div>

	@section('map-scripts')
	    <script src='/js/admin/commonJs.js'></script>
	@endsection

@endsection