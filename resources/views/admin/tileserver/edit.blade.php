@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/leaflet/leaflet.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="navigation-section-stabilizer">

<div class="container">

	@include('admin.menu')

	<div class="main-admin-section col-xs-12">

		<div class="col-xs-6">

			<div class="admin-category-title">
				Adding new tile server
			</div>

			<form class="form-horizontal" method="POST" action="/tile-servers/save">
			    {{ csrf_field() }}
                <input type="text" class="form-control" name="name" placeholder="Name" required="required" value="{{$server->name}}"> <br>
                <input type="text" class="form-control" name="domain" placeholder="Domain" required="required" value="{{$server->domain}}"><br>
                <input type="text" class="form-control" name="ip" placeholder="IP" required="required" value="{{$server->ip}}"><br>

                <input type="text" class="form-control" name="token" placeholder="Token" required="required" value="{{$server->token}}"><br>
                <input type="text" class="form-control" name="username" placeholder="Username" required="required" value="{{$server->username}}"><br>
                <input type="text" class="form-control" name="pw" placeholder="Password" required="required" value="{{$server->pw}}"><br>

                <input type="text" class="form-control" name="tilesurl" placeholder="Tile server url" required="required" value="{{$server->tilesurl}}"><br>
                <input type="checkbox" class="form-control" name="active" placeholder="Active" required="required" value="{{$server->active}}"><br>
                <input type="text" class="form-control" name="maxusers" placeholder="Maximum number of users" required="required" value="{{$server->maxusers}}"><br>
               
                 <input type="hidden" name="action" value="add" >
			    <input type="submit" class="btn btn-primary" value="Add" style="float:right;width:200px;margin-top: 15px;">

			</form>

		</div>

		

	</div>


</div>
</div>
@section('map-scripts')
    <script src='/js/leaflet/leaflet.js'></script>
    
    <script src='/js/admin/cityedit.js'></script>
@endsection

@endsection
