@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">

    <link rel='stylesheet' href='/css/leaflet/leaflet.css' />

@endsection

@section('content')

<div class="navigation-section-stabilizer">

<div class="container">

	@include('admin.menu')

    <div class="col-xs-9" style="margin-top: 30px;">
    

	<div class="main-admin-section col-xs-12">
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Domain</th>
                    <th>IP</th>
                    <th>Active</th>
                    <th>Current user count</th>
                   
                </tr>
            </thead>
            <tbody>
                @foreach($servers as $server)
                <tr>
                    <td><span>{{$server->id}}</span></td>
                    <td><a href="/admin-city/edit/{{$server->id}}">{{$server->name}}</a></td>
                    <td>{{$server->domain}}</td>
                    <td>{{$server->ip}}</td>
                    <td>{{$server->active}}</td>
                    <td>0</td>
                </tr>
                @endforeach
               
            </tbody>
        </table>

        <a href="/tile-servers-add">
			<div class="btn btn-primary" id='user-privacy'>
				Add new server
			</div>
		</a>

		
        
	</div>
    </div>
	

</div>
</div>
@section('map-scripts')
    <script src='/js/admin/commonJs.js'></script>
    
@endsection

@endsection
