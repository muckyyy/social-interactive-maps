@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection

@section('content')

	<div class="navigation-section-stabilizer">

	<div class="container">

		@include('admin.menu')

		<div class="main-admin-section col-xs-12">

			<div class="col-xs-6" style="margin-top: -15px;">
				<div class="admin-category-title" style="text-transform: capitalize;">
					Category {{$category->name}} {!! $category->icon !!} 
					<a href="/admin-change-categort-basics/{{$category->id}}" style="margin-top: 0px;margin-right: 15px;">
						<span class="btn btn-success custom-fields-edit-btn" style="margin-left: 15px;">Edit</span> 
					</a>
				</div>

				@if(!empty($parentCategory))
				<div class="edit-subcategories">
					<div class="edit-subcategory-title">
						Parent category:
					</div>

					<div class="admin-custom-field-section" style="margin-bottom: 5px;"> 
						&bull; {{$parentCategory->name}}
					</div>

				</div>

				@else
				<div class="edit-subcategories">
					<div class="edit-subcategory-title">
						Parent category:
					</div>

					<div class="admin-custom-field-section" style="margin-bottom: 5px;"> 
						&bull; No parent category</br>
						( this category is master category)
					</div>
				</div>
				@endif

				@if(!$subCategories->isEmpty())
				<div class="edit-subcategories">
					<div class="edit-subcategory-title">
						Subcategories:
					</div>

					@foreach($subCategories as $subcat)
					<div class="admin-custom-field-section" style="margin-bottom: 5px;"> 
						&bull; {{$subcat->name}}
					</div>
					@endforeach
				</div>

				@else
				<div class="edit-subcategories">
					<div class="edit-subcategory-title">
						Subcategories:
					</div>

					<div class="admin-custom-field-section" style="margin-bottom: 5px;"> 
						&bull; No subcategories
					</div>
				</div>
				@endif

				@if(!$customFields->isEmpty())
				<div class="edit-subcategories">
					<div class="edit-subcategory-title">
						Custom fields:
					</div>

					@foreach($customFields as $field)
					<div class="admin-custom-field-section"> 
						&bull; {{$field->name}} 


						<form class="form-horizontal" method="POST" action="/delete-custom-field/{{$field->id}}/{{$category->id}}" style="float: right;">
						    {{ csrf_field() }}

						<input type="submit" class="btn btn-danger custom-fields-edit-btn" value="Delete">

						</form>

						<a href="/admin-change-custom-field/{{$field->id}}/{{$category->id}}" style="float: right;margin-top: 0px;margin-right: 15px;">
							<span class="btn btn-success custom-fields-edit-btn" style="margin-left: 15px;">Edit</span> 
						</a>

						<div class="admin-custom-field-text">
							Type : {{$field->type}} <br/>
							Value : 
							@if($field->type == 'image')
								<img src="/img/categories/{{$field->content}}">
							@else
								<span>{{$field->content}}</span>
							@endif
							
						</div>
					</div>
					@endforeach
				</div>

				@else

					<div class="edit-subcategories">
						<div class="edit-subcategory-title">
							Custom fields:
						</div>

						<div class="admin-custom-field-section"> 
							&bull; No custom fields
						</div>
					</div>

				@endif


				<div class="edit-subcategories">
					<div class="edit-subcategory-title">
						Category tags:
					</div>


					@if(empty($tagString))

					<div class="admin-custom-field-section"> 
						&bull; No tags for this category
					</div>

					@else

					<div class="admin-custom-field-section"> 
						{{$tagString}} 
					</div>

					@endif

				</div>

				

			</div>

		</div>

	</div>
	</div>

@endsection