@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection

@section('content')

	<div class="navigation-section-stabilizer">
	
	<div class="container">

		@include('admin.menu')

		<div class="main-admin-section col-xs-12">

			<form class="form-horizontal" method="POST" action="/change-custom-field-post/{{$customField->id}}/{{$categoryId}}" enctype="multipart/form-data">
			    {{ csrf_field() }}

			    <input class="form-control" type="text" name="name" placeholder="Field name" value="{{$customField->name}}" required="required" style="margin-bottom: 15px;">

			    <div style="font-size: 16px;font-weight: 600;">
			    	Choose type
			    </div>
			    <select class="form-control" name="type" id="typeselector" style="margin-bottom: 15px;">
			    	<option value="text">Text</option>
			    	<option value="largeText">Large text</option>
			    	<option value="number">Number</option>
			    	<option value="image">Image</option>
			    </select>

			    <div class="field-type" id="text">
			    	<input class="form-control" type="text" name="text" value="{{$customField->content}}" placeholder="Insert text...">
			    </div>

			    <div class="field-type" id="largeText">
			    	<textarea class="form-control" name="largeText">{{$customField->content}}</textarea>
			    </div>

			    <div class="field-type" id="number">
			    	<input class="form-control" type="number" value="{{$customField->content}}" name="number" placeholder="Insert number...">
			    </div>

			    <div class="field-type" id="image">
			    	<input class="form-control" type="file" name="image">
			    </div>
			    
			    <input class="btn btn-primary" type="submit" value="Submit" style="margin-top: 30px;width: 100%;">

			</form>

		</div>

	</div>
	</div>

	<script>
		$(function() {
		        $('#typeselector').change(function(){
		            $('.field-type').hide();
		            $('#' + $(this).val()).show();
		        });
		    });
	</script>

@endsection