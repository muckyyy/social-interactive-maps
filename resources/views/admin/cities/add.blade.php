@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/leaflet/leaflet.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="navigation-section-stabilizer">

<div class="container">

	@include('admin.menu')

	<div class="main-admin-section col-xs-12">

		<div class="col-xs-6">

			<div class="admin-category-title">
				Adding new city
			</div>

			<form class="form-horizontal" method="POST" action="/admin-city/save">
			    {{ csrf_field() }}

                <input type="text" class="form-control" name="name" placeholder="Name" required="required" value="{{$city->name}}">
                <select name="country" id="country-select" class="form-control" style="margin-top: 15px;">
                    <option value="">Choose country</option>
			    	@foreach($countries as $country)
			    		<option class="admin-select-li" value="{{$country->id}}"
                                @if ($country->id == $city->country)
                                selected
                                @endif >{{$country->name}}</option>
			    	@endforeach
			    </select>

                <select name="region" id="region-select" class="form-control" style="margin-top: 15px;">

                    <option value="">Please choose country first</option>
                
			    	
			    </select>
                <br>
                <textarea name="about" cols="50" rows="5">{{$city->about}}</textarea><br>
                <input type="text" class="form-control" name="website" placeholder="Web" value="{{$city->website}}"><br>
                <input type="text" class="form-control" name="population" placeholder="Population" value="{{$city->population}}"><br>
                <input type="text" class="form-control" name="area" placeholder="Area" value="{{$city->area}}"><br>
                <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$city->phone}}"><br>
                <input type="hidden" name="icon" value="{{$city->icon}}">
                <input type="hidden" name="action" value="add" id="city-action">
                <input type="hidden" name="id" value="{{$city->id}}">
                <input type="hidden" name="lat" id="city-lat" value="{{$city->lat}}">
                <input type="hidden" name="long" id="city-long" value="{{$city->long}}">
			    <input type="hidden" name="icon" id='admin-category-icon' required="required">
                <div id="city-map-add" style="height: 200px">

                </div>

			    <input type="submit" class="btn btn-primary" value="Update" style="float:right;width:200px;margin-top: 15px;">

			</form>

		</div>

		

	</div>


</div>
</div>
@section('map-scripts')
    <script src='/js/leaflet/leaflet.js'></script>
    
    <script src='/js/admin/cityedit.js'></script>
@endsection

@endsection
