@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection

@section('content')

	<div class="navigation-section-stabilizer">
	<div class="container">

		@include('admin.menu')

		<div class="main-admin-section col-xs-12">

			<div class="col-xs-12" style="margin-top: -15px;">
				<div class="progress">
				  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="66"
				  aria-valuemin="0" aria-valuemax="100" style="width:66%">
				  2/3
				  </div>
				</div>
			</div>

			<div class="col-xs-6">
				<div class="admin-category-title">
					Add custom field:
				</div>

				<form class="form-horizontal" method="POST" action="/add-custom-field/{{$id}}" enctype="multipart/form-data">
				    {{ csrf_field() }}

				    <input class="form-control" type="text" name="name" placeholder="Field name" required="required" style="margin-bottom: 15px;">

				    <div style="font-size: 16px;font-weight: 600;">
				    	Choose type
				    </div>
				    <select class="form-control" name="type" id="typeselector" style="margin-bottom: 15px;">
				    	<option value="text">Text</option>
				    	<option value="largeText">Large text</option>
				    	<option value="number">Number</option>
				    	<option value="image">Image</option>
				    </select>

				    <div class="field-type" id="text">
				    	<input class="form-control" type="text" name="text" placeholder="Insert text...">
				    </div>

				    <div class="field-type" id="largeText">
				    	<textarea class="form-control" name="largeText">Insert text...</textarea>
				    </div>

				    <div class="field-type" id="number">
				    	<input class="form-control" type="number" name="number" placeholder="Insert number...">
				    </div>

				    <div class="field-type" id="image">
				    	<input class="form-control" type="file" name="image">
				    </div>
				    
				    <input class="btn btn-primary" type="submit" value="Add" style="margin-top: 30px;width: 100%;">

				</form>

			</div>

			<a href="/admin-category-icons/{{$id}}">
				<div class="col-xs-12" style="margin-top: 60px;">
					If you dont want to add more field you can skip this step and click finish
					<div class="btn btn-success" style="width: 100%;margin-top: 15px;">
						Next
					</div>
				</div>
			</a>

		</div>

	</div>
	</div>

	<script>
		$(function() {
		        $('#typeselector').change(function(){
		            $('.field-type').hide();
		            $('#' + $(this).val()).show();
		        });
		    });
	</script>

@endsection