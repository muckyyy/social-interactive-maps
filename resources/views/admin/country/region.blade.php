<option value="0">Select region</option>
@foreach($regions as $region)
<option value="{{$region->id}}"
       @if ($city->region == $region->id)
                                selected
                                @endif
>{{$region->name}}</option>
@endforeach
