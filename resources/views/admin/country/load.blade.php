
@foreach($cities as $city)
<tr>
    <td><span>{{$city->icon}}</span></td>
    <td><a href="/admin-city/edit/{{$city->id}}">{{$city->name }}</a></td>
    <td><a href="/admin-region/edit/{{$city->region}}">{{$city->regionname }}</a></td>
    <td><a href="/admin-country/edit/{{$city->country}}">{{$city->countryname }}</a></td>
    
</tr>
@endforeach
<tr><td colspan="4"><div id="pagination">{{{ $cities->links() }}}</div></td></tr>