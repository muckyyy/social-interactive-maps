@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/leaflet/leaflet.css') }}" rel="stylesheet">
@endsection

@section('content')

<div class="navigation-section-stabilizer">

<div class="container">

	@include('admin.menu')

	<div class="main-admin-section col-xs-12">

		<div class="col-xs-6">

			<div class="admin-category-title">
				Country: {{$country->name}}
			</div>

			<form class="form-horizontal" method="POST" action="/admin-country/save">
			    {{ csrf_field() }}

                <input type="text" class="form-control" name="name" placeholder="Name" required="required" value="{{$country->name}}">
                <Br>
                <input type="text" class="form-control" name="website" placeholder="Web" value="{{$country->website}}"><br>
                <input type="text" class="form-control" name="population" placeholder="Population" value="{{$country->population}}"><br>
                <input type="text" class="form-control" name="area" placeholder="Area" value="{{$country->area}}"><br>
                <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$country->phone}}"><br>
                <input type="hidden" name="icon" value="{{$country->icon}}">
                <input type="hidden" name="action" value="add">
                <input type="hidden" name="id" value="{{$country->id}}">
			    <input type="hidden" name="icon" id='admin-category-icon' required="required">
               

			    <input type="submit" class="btn btn-primary" value="Update" style="float:right;width:200px;margin-top: 15px;">

			</form>

		</div>

		

	</div>


</div>
</div>
@section('map-scripts')
    <script src='/js/leaflet/leaflet.js'></script>
    <script src='/js/admin/commonJs.js'></script>
    <script src='/js/admin/cityedit.js'></script>
@endsection

@endsection
