@extends('layouts.app')

@section('per-page-css')
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">

    <link rel='stylesheet' href='/css/leaflet/leaflet.css' />

@endsection

@section('content')

<div class="navigation-section-stabilizer">

<div class="container">

	@include('admin.menu')

    <div class="col-xs-9" style="margin-top: 30px;">
    <form action="/search" method="POST" role="search">
        {{ csrf_field() }}
        <div class="input-group">
            <input type="text" class="form-control" name="query" id="cities-search"
                placeholder="Search users"> <span class="input-group-btn">
                <button type="submit" class="btn btn-default">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div>
    </form>

	<div class="main-admin-section col-xs-12">
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Icon</th>
                    <th>Name</th>
                    <th>Region</th>
                    <th>Country</th>
                    
                </tr>
            </thead>
            <tbody id="cities">
               @include('admin.cities.load')
            </tbody>
        </table>

        <a href="/admin-city/add">
			<div class="btn btn-primary" id='user-privacy'>
				Add new City
			</div>
		</a>

        <a href="/admin-region/add">
			<div class="btn btn-primary" id='user-privacy'>
				Add new Region
			</div>
		</a>

        <a href="/admin-country/add">
			<div class="btn btn-primary" id='user-privacy'>
				Add new Country
			</div>
		</a>
		
        
	</div>
    </div>
	

</div>
</div>
@section('map-scripts')
    <script src='/js/admin/commonJs.js'></script>
    
@endsection

@endsection
