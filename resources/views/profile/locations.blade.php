@extends('layouts.app')

@section('content')

	<div class="navigation-section-stabilizer custom-user-profile-location-section col-xs-12 no-padding">

	    @include('navigation.user-profile-nav')

	    <div class="user-profile-container">

	    	<div class="custom-user-profile-location-list-titles col-xs-12">
	    			<div class="col-xs-3">
		    			Name
		    		</div>
		    		<div class="col-xs-3">
		    			Address
		    		</div>
		    		<div class="col-xs-3">
		    			Postal code
		    		</div>
	    		</div>
	    	
	    	@foreach($locations as $location)

	    		<div class="custom-user-profile-location-list-section col-xs-12">
	    			<div class="col-xs-3">
		    			{{$location->name}}
		    		</div>
		    		<div class="col-xs-3">
		    			{{$location->address}}
		    		</div>
		    		<div class="col-xs-3">
		    			{{$location->postal_code}}
		    		</div>
	    		</div>

	    	@endforeach

	    </div>

	</div>

@endsection
