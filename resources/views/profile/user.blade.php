@extends('layouts.app')

@section('content')

    <div class="navigation-section-stabilizer col-xs-12 no-padding">

    @include('navigation.user-profile-nav')

    <div class="user-profile-container">

    	<div class="col-xs-12" style="border-bottom: 1px solid #eee;padding-bottom: 5px;text-transform: capitalize;">
    		@if($user->name == NULL && $user->lname == NULL)
    			<b>anonymous</b> | User level: <b>{{$user->type}}</b>
    		@else
    			<b>{{$user->name}} {{$user->last_name}}</b> | User level: <b>{{$user->type}}</b>
    		@endif

    		<div style="margin-top: 5px;" class="col-xs-12 no-padding">
	    		For your profile to go to standard mode, you need to fill fields below:
	    	</div>
    	</div>


    	@if($user->type == 'basic')
    	<div class="col-xs-12">

    		<form class="form-horizontal" method="POST" action="/update-profile">
    		    {{ csrf_field() }}

    		    <div class="col-xs-6 no-padding">

	    		    <input type="text" style="margin-top: 15px;" class="form-control" name="name" placeholder="First name" required="required">

	    		    <input type="text" style="margin-top: 15px;" class="form-control" name="last_name" placeholder="Last name" required="required">

                    <div>
                        <label for="selectDate" style="margin-top: 15px;">Date of Birth:</label>
                        <br/>
                        <select id="selectDate" name="day" style="width:auto;float: left;margin-right: 15px;" class="form-control selectWidth">
                            @for ($i = 1; $i <= 31; $i++)
                            <option class="">{{$i}}</option>
                            @endfor
                        </select>
                        <select id="selectMonth" name="month" style="width:auto;float: left;margin-right: 15px;" class="form-control selectWidth">
                            @for ($i = 1; $i <= 12; $i++)
                            <option class="">{{$i}}</option>
                            @endfor
                        </select>
                        <select id="selectYear" name="year" style="width:auto;float: left;" class="form-control selectWidth">
                            @for ($i = 2019; $i >= 1900; $i--)
                            <option class="">{{$i}}</option>
                            @endfor
                        </select>
                    </div>

                    <div style="margin-top: 50px;">
    	    		    <input type="submit" class="btn btn-primary" value="Update">
                    </div>

	    		</div>

    		</form>

    		<div class="col-xs-12">

    		</div>

    	</div>
    	@endif


    	@if($user->type == 'standard')
    	<div class="col-xs-12">

    		<form class="form-horizontal" method="POST" action="/update-standard-profile">
    		    {{ csrf_field() }}

    		    <div class="col-xs-6 no-padding">

	    		    Upload profile picture

                    <br/>
	    		    <input type="submit" style="margin-top: 15px;" class="btn btn-primary" value="Update">

	    		</div>

    		</form>

    		<div class="col-xs-12">

    		</div>

    	</div>
    	@endif

    </div>
    </div>

    <script>
    	var x = document.getElementById("demo");

    	function getLocation() {
    	  if (navigator.geolocation) {
    	    navigator.geolocation.getCurrentPosition(showPosition);
    	  } else {
    	    x.innerHTML = "Geolocation is not supported by this browser.";
    	  }
    	  console.log(navigator.geolocation)
    	}

    	function showPosition(position) {
    	  x.innerHTML = "Latitude: " + position.coords.latitude + 
    	  "<br>Longitude: " + position.coords.longitude; 
    	}

    </script>

@endsection
