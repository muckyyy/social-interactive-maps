@extends('layouts.app')

@section('content')

	<div class="global-profile-section">
		
		<div class="global-profile-header">
			<img src="/img/profile-cover.jpg">
		</div>

		<div class="container">

			<div class="global-profile-img">
				<img src="/img/profile-img.jpeg">
			</div>

			<div class="global-bottom-section">

				<div class="global-bottom-left-section">
					<div>
						{{$selectedUser->name}} {{$selectedUser->last_name}}
					</div>
					<div>
						{{$selectedUser->email}}
					</div>
					<div>
						{{$selectedUser->date_of_birth}}
					</div>
					<div>
						{{$selectedUser->type}}
					</div>
				</div>

				<div class="global-bottom-right-section">
					@foreach($selectedUserMessages as $message)
					<div class="global-bottom-right-section-message"> 
						{{$message->description}}
					</div>
					<div class="global-bottom-right-section-date">
						{{$message->created_at}}
					</div>
					@endforeach
				</div>

			</div>

		</div>




	</div>

@endsection
