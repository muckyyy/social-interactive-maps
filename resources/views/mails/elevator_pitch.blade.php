Hello <i>Avatars</i>,
<p>This is a email is from elevator pitch form.</p>
 
<p><u>Details:</u></p>
 
<div>
    <p><b>Name:</b>&nbsp;{{ $email->name }}</p>
    <p><b>Surname:</b>&nbsp;{{ $email->surname }}</p>
    <p><b>Email:</b>&nbsp;{{ $email->email }}</p>
    <p><b>Message:</b>&nbsp;{{ $email->message }}</p>
</div>
<br/>
<br/>
Thank You,
<br/>
<i>{{ $email->email }}</i>