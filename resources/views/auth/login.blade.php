@extends('layouts.app')

@section('per-page-css')
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
@endsection

@section('content')
   <div class="container main-container">
        <div class="row">
            <div class="col-md-5 logo-section">
                <img src="/img/new-img/new-logo.png" style="height:400px;">
                <h1 class="text-center" style="background-color: #2c2c38; padding-bottom:0.3em;">pynn.it</h1>
            </div>
            <div class="col-md-5 reg-form">
                <form method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <h4 style="">Sign up</h4>

                    <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}" style="margin-bottom: 5px;">
                        <div class="col-md-12">
                            <input required id="full_name" type="text" class="form-control" name="full_name" value="{{ old('full_name') }}" placeholder="Full name" >
                            @if ($errors->has('full_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('full_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}" style="margin-bottom: 5px;">
                        <div class="col-md-12">
                            <input required id="user_name" type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" placeholder="Username">
                            @if ($errors->has('user_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}" style="margin-bottom: 5px;">
                        <div class="col-md-12">
                            <input required id="password" type="password" class="form-control" name="password" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group" style="margin-bottom: 5px;">
                        <div class="col-md-12">
                            <input required id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" >
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" style="margin-bottom: 5px;">
                        <div class="col-md-12">
                            <input required id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }} text-center" style="margin-bottom: 5px;">
                        <div class="col-md-12" style="width:67%; margin:auto;">
                            <select required onchange='enableCitySearch(event)' class="form-control" name="country" id="country">
                                <option value="{{ old('country') ? old('country') : '' }}">{{ old('country') ? \App\Country::find(old('country'))->name : 'Choose country...' }}</option>
                                @foreach($countries as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('country'))
                                <span class="help-block">
                                    <strong class='text-danger'>{{ $errors->first('country') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }} register-form-padding" style="margin-bottom:0;"> 
                        <div class="col-md-12">
                            <input required id='findCities' type="text" class="form-control disabled" placeholder='Start typing...' value="{{ old('city_id') ? \App\City::find(old('city_id'))->name : '' }}">
                            @if ($errors->has('city_id'))
                                <span class="help-block">
                                    <strong class='text-danger'>{{ $errors->first('city_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <input id="cityid" type="text" name='city_id' class='d-none' value="{{ old('city_id') ? \App\City::find(old('city_id'))->id : '' }}">
                    <div class="text-center">
                        <input class="add-location-btn mt-2 button-register" type="submit" value="Register">
                    </div>
                    <p class="or-edit">or</p>
                    <div class="social-input">
                        <a class="btn btn-lg btn-social btn-facebook mb-2">
                            <img src="/img/new-img/facebook.svg">
                            Login with Facebook
                        </a>
                            <br>
                        <a class="btn btn-lg btn-social btn-facebook mb-2">
                            <img src="/img/new-img/search.svg" style="margin-left: -0.9em;">
                            Login with Google
                        </a>
                    </div>
                </form>
                <form class='d-none' id='getCities' action="/api/cities/{keyword}" method='get'>
                    {{ csrf_field() }}
                    <input name='keyword' type="text">
                </form>
            </div>
            <div class="col-md-2"></div>
        </div>
   </div>

    <div class="container footer-bottom" 
        style="position: absolute; bottom: 0; left:0;position: fixed; bottom: 0; left: 0; z-index: 99999; width: 100%; background: #161621;"
    >
        <div class="row text-center" style="padding: 1em 0;">
            <div class="col-md-2"></div>
            <div class="col-md-8 bottom-links">
                <a href="/about-us" class="pr-5 text-white">About us</a>
                <a href="/help" class="pr-5 text-white">Help</a>
                <a href="/terms" class="pr-5 text-white">Terms</a>
                <a href="/privacy" class="pr-5 text-white">Privacy</a>
                <a href="/sitemap" class="pr-5 text-white">Sitemap</a>
                <a href="/contact-us" class="text-white">Contact us</a>
            </div>
            <div class="col-md-2"></div>
        </div>
        
    <script src="{{ asset('js/register.js') }}" rel="text/javascript"></script>
    <script>
    function enableCitySearch(e) {
        if(e.target.value) {
        document.getElementById('findCities').classList.remove('disabled');
        } 
        else {
            document.getElementById('findCities').classList.add('disabled');
            document.getElementById('findCities').value = "";
            document.getElementById('cityid').value = "";
        }
    }
    </script>

</div>
@endsection