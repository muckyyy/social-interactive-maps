@extends('layouts.app')
<style>
.register-form-padding {
    margin-bottom: 5px;
    padding: 5px 0;
}
.main-navigation-section {
    display: none;
    }
.register-style {
    background: #237aa0;
    color: #fff !important; 
    padding: 10px 0;'
}
.panel-body-mod {
    padding-left: 2em;
    padding-right: 2em; 
    padding-top:1em; 
    background-color: #fff;"
}
.panel-body-mod form {
    text-align: center; 
    padding-bottom:2em;
}
.navigation-section-stabilizer {
    margin-top: 2em !important;
}
</style>
@section('content')

<div class="navigation-section-stabilizer">

<div id='register' class="container" style="margin:0; padding: 0; width:100%; max-width: none;">
    <div class="row" style="margin:0;">
       <!-- @include('partials.login_form') -->
       <div class="col-md-3 col-lg-4"></div>
        <div class="col-md-6 col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading custom-form-title register-style">Register</div>

                <div class="panel-body panel-body-mod">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }} register-form-padding">
                            <label for="user_name" class="col-md-12 custom-login-label control-label">User Name</label>

                            <div class="col-md-12">
                                <input required id="user_name" type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" >

                                @if ($errors->has('user_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }} register-form-padding">
                            <label for="full_name" class="col-md-12 custom-login-label control-label">Full Name</label>

                            <div class="col-md-12">
                                <input required id="full_name" type="text" class="form-control" name="full_name" value="{{ old('full_name') }}" >

                                @if ($errors->has('full_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} register-form-padding">
                            <label for="email" class="col-md-12 custom-login-label control-label">E-Mail Address</label>

                            <div class="col-md-12">
                                <input required id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} register-form-padding">
                            <label for="password" class="col-md-12 custom-login-label control-label">Password</label>

                            <div class="col-md-12">
                                <input required id="password" type="password" class="form-control" name="password"  >
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group register-form-padding">
                            <label for="password-confirm" class="col-md-12 custom-login-label control-label">Confirm Password</label>

                            <div class="col-md-12">
                                <input required id="password-confirm" type="password" class="form-control" name="password_confirmation"  >
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }} register-form-padding">
                            <label for="country" class="col-md-12 custom-login-label control-label">Country</label>
                            <div class="col-md-12">
                                <select required onchange='enableCitySearch(event)' class="form-control" name="country" id="country"=""> <!-- HERE I DELETED ="" after i="country", while transforming code to bootstrap 4 -->
                                    <option value="{{ old('country') ? old('country') : '' }}">{{ old('country') ? \App\Country::find(old('country'))->name : 'Choose country...' }}</option>
                                    @foreach($countries as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong class='text-danger'>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }} register-form-padding">
                            <label for="city" class="col-md-12 custom-login-label control-label">City</label>
                            <div class="col-md-12">
                                <input required id='findCities' type="text" class="form-control disabled" placeholder='Start typing...' value="{{ old('city_id') ? \App\City::find(old('city_id'))->name : '' }}">
                                @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong class='text-danger'>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <!-- city input, hidden -->
                         <input id="cityid" type="text" name='city_id' class='d-none' value="{{ old('city_id') ? \App\City::find(old('city_id'))->id : '' }}">

                        <input class="add-location-btn mt-2" style="border: 0;margin-top: 1em;" type="submit" value="Submit">

                    </form>

                    <!-- get cities form -->
                    <form class='d-none' id='getCities' action="/api/cities/{keyword}" method='get'>
                        {{ csrf_field() }}
                        <input name='keyword' type="text">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-4"></div>
</div>
<script src="{{ asset('js/register.js') }}" rel="text/javascript"></script>

<script>
function enableCitySearch(e) {
    if(e.target.value) {
       document.getElementById('findCities').classList.remove('disabled');
    } 
    else {
        document.getElementById('findCities').classList.add('disabled');
        document.getElementById('findCities').value = "";
        document.getElementById('cityid').value = "";
    }
}
</script>

</div>
@endsection
