<div class="global-profile">

	<div class="container">
		
		<!-- Profile header -->
		<div class="gp-header-section">
			<img src="https://html.crumina.net/html-olympus/img/top-header1.jpg">

			<div class="gp-header-main-img">
				<img src="https://html.crumina.net/html-olympus/img/author-main1.jpg">
			</div>

			<div class="header-menu">
				<a href="/p/1">
					<div class="header-menu-li">
						Timeline
					</div>
				</a>
				<a href="/profile/1/about">
					<div class="header-menu-li">
						About
					</div>
				</a>
				<a href="/profile/1/friends">
					<div class="header-menu-li">
						Friends
					</div>
				</a>
				<a href="/profile">
					<div class="header-menu-li header-menu-li-name" style="">
						Nerazim Pros
						<div class="header-menu-li-text">
							Photographer
						</div>
					</div>
				</a>
				<a href="/profile/1/photos">
					<div class="header-menu-li">
						Photos
					</div>
				</a>
				<a href="/profile/1/videos">
					<div class="header-menu-li">
						Videos
					</div>
				</a>
				<div class="header-menu-li">
					...
				</div>
			</div>
		</div>
		<!-- Profile header end -->

	</div>

</div>
