<script>

var profileId = '{{$id}}'
console.log('test',profileId)

var app2 = new Vue({
	el: '#user-posts',
	data: {
		check: true,
		posts: null,
		text: '',
		subText: '',
		likes: null,
		likesActive: false
	},

	methods: {

		toggleLike(data, self, active) {

			self = this
			console.log('clicked')
			$.ajax
			({
				type: "GET",
				url: "/toggle-like",
				data: {post_id: data},
					success: function(data)
					{
						console.log('toggle like')
						console.log('before active', active)
						active = !active
						console.log('before active', active)
						self.getPosts()
					}
			});
		},

		toggleCommentLike(data, self, active) {

			self = this
			console.log('clicked')
			$.ajax
			({
				type: "GET",
				url: "/toggle-comment-like",
				data: {comment_id: data},
					success: function(data)
					{
						console.log('toggle like')
						console.log('before active', active)
						active = !active
						console.log('before active', active)
						self.getPosts()
					}
			});
		},

		getPostLike(postId) {
			self = this
			axios
			  .get(apiUrl+'get-post-likes/'+postId)
			  .then(response => {
			  		self.likes = response.data
			  		console.log('likes', self.likes)
			  })
			  this.likesActive = true
			  console.log('likes active', this.likesActive)
		},

		toggleReply(id) {
			$('#sub-reply'+id).fadeToggle(200)
		},

		submitSubCommebt(postId, id, commentId, text){
			console.log('stats', text, postId, id, commentId)

			let self = this

			axios.post('/create-comment/'+postId+'/'+id+'/'+commentId, {
			    text: text
			  })
			  .then(function (response) {
			    console.log(response);
			    self.getPosts()
			  })
			  .catch(function (error) {
			    console.log(error);
			  });
		},

		getPosts() {
            return; // dont need this function for now
			axios
			  .get(apiUrl+'get-all-posts/'+profileId)
			  .then(response => {
			  		this.posts = response.data

			  		for(let i=0; i<response.data.length;i++){
			  			if(response.data[i].user_like == 'like'){
			  				response.data[i].isActive = true
			  			} else {
			  				response.data[i].isActive = false
			  			}
			  			
			  			//comments
			  			for(let j=0; j<response.data[i].comments.length;j++){
			  				response.data[i].comments[j].isActive = false
			  				if(response.data[i].comments[j].likeStatus == 'like'){
			  					response.data[i].comments[j].isActive = true
			  				} else {
			  					response.data[i].comments[j].isActive = false
			  				}

			  				//subcomments
			  				for(let z=0; z<response.data[i].comments[j].length;z++){
			  					response.data[i].comments[j].sub_comments[z].isActive = false
			  					if(response.data[i].comments[j].sub_comments[z].likeStatus == 'like'){
			  						response.data[i].comments[j].sub_comments[z].isActive = true
			  					} else {
			  						response.data[i].comments[j].sub_comments[z].isActive = false
			  					}
			  				}


			  			}
			  		}
			  		console.log('data', this.posts)
			  })
		}

	},

	mounted () {
	  this.getPosts()
	}
})
</script>