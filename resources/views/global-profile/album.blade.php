@extends('layouts.app')

@section('per-page-css')
    <link rel='stylesheet' href='/css/global-profile.css' />
@endsection

@section('content')

	@include('global-profile.navigation')

	<div class="container" style="margin-bottom: 60px;">

		<div class="friends-search-bar-section col-xs-12">
			<div class="custom-friends-header-name col-xs-8">
				Best Vacations Album (177 Photos) <a href="/profile/1/photos"><span style="color: #1ba6d2;">(Return to Albums)</span></a>
			</div>
			<div class="col-xs-4 no-padding">
				<input class="form-control" type="text" placeholder="Search friends" style="height: 40px;line-height: 40px;margin-top: 15px;">
				<i class="fas fa-search custom-search-friends-icon"></i>
			</div>
		</div>

		<div class="as-section col-xs-12 no-padding">

			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item1.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item2.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item3.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item4.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item5.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item6.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item7.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item8.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item9.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item10.jpg">
			<img class="as-li-img" src="https://html.crumina.net/html-olympus/img/photo-item11.jpg">

		</div>


		<div class="as-carousel col-xs-12 no-padding">
			<i class="far fa-times-circle as-close-icon"></i>

			<img src="https://html.crumina.net/html-olympus/img/photo-item5.jpg">
		</div>



	</div>

	<script>
		$('.as-li-img').click(function(){
			$('.as-carousel').fadeIn(500)
		})
		$('.as-close-icon').click(function(){
			$('.as-carousel').fadeOut(500)
		})
	</script>

@endsection

@section('map-scripts')

@endsection
