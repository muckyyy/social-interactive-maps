
<script src="https://cdn.jsdelivr.net/npm/croppie@2.6.4/croppie.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/croppie@2.6.4/croppie.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<link rel='stylesheet' href='/css/global-profile-navigation.css' />

<div class="global-profile" id="global-profile-11">
    <div class="container">
      <!-- Modal -->
      <div class="modal fade" id="modal-photo-upload" role="dialog" style="top: 5%;">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
              <h4 class="modal-title">Upload profile image</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="upload-demo-croppie">
                    <div class="col-lg-12 text-center">
                        <div id="upload-demo"></div>
                    </div>
                    <!--
                    <div class="col-lg-6">
                    <div id="preview-crop-image" style="width:260px; height:260px;background-color: grey; margin: auto; padding: 30px;"></div>
                    </div>-->
                </div>
                <div class="row">
                     <div class="col-md-12">
                        <div id="upload-profile-img-area">
                            <div class="area">
                                <input type="file" id="image_file" style="color:transparent;" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4" id="upload-demo-croppie-button">
                        <button class="btn btn-primary btn-block upload-image" style="margin-top:3%;">Save
                        </button>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" onClick="clearImage()">Cancel</button>
            </div>
          </div>
        <!-- End of modal content -->
        </div>
      </div>
    </div>


      <div class="container">
          <!-- Modal -->
          <div class="modal fade" id="modal-photo-upload-2" role="dialog" style="top: 10%;">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                  <h4 class="modal-title">Change profile image</h4>
                </div>
                <div class="modal-body" style="display: flex; flex-direction: row; justify-content: center;">
                     <div style="margin-right: 15px; border:1px solid #cccccc;  border-radius: 5px;">
                        <button id="upload-a-photo-button" class="btn btn-secondary" type="submit" data-target="#modal-photo-upload" data-toggle="modal" onclick="closeModal()" style="color: grey;"><i class="far fa-folder" style="padding-right: 5px;"></i>Upload a photo</button>
                    </div>
                      <div style="margin-left: 15px;">
                        <button class="btn btn-secondary button-primary-color" type="submit" data-target="#modal-take-a-photo" data-toggle="modal" onclick="startCam()"><i class="fas fa-camera" style="padding-right: 5px;"></i>Take a photo</button>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="close-button">Cancel</button>
                </div>
              </div>
      
            </div>
        </div>
    </div>

     <div class="container">
          <!-- Modal -->
          <div class="modal fade" id="modal-take-a-photo" role="dialog" style="top: 10%;">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                  <h4 class="modal-title">Take photo using cam</h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="take-photo-using-cam">
                        <div class="col-md-2"></div>
                        <div class="col-md-8" style="padding: 3px; border: 1px dashed blue; margin: auto;">
                            <div id="my_camera"></div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="row" id="preview-photo-using-cam">
                        <div class="col-md-2"></div>
                        <div class="col-md-8" id="results" style="padding: 3px;"></div> 
                        <div class="col-md-2"></div>
                    </div>
                    <div class="row" id="take-snapshot-button">
                        <div class="col-md-12 text-center" style="padding-top: 15px;">
                            <input type=button value="Take snapshot" onClick="take_snapshot()">
                            <input type="hidden" name="image" class="image-tag">
                        </div>
                    </div>
                    <div class="row"  style="padding-top: 15px;">
                        <div class="col-md-3"></div>
                        <div class="col-md-3 text-center" id="submit-cam-button" style="padding: 0;">
                            <button class="btn button-primary-color" onClick="storeTakenImg()">Submit</button>
                        </div>
                        <div class="col-md-3 text-center" id="retake-photo" style="padding: 0;">
                             <button class="btn btn-info" onClick="retakePhoto()">Retake photo ?</button>
                        </div>
                         <div class="col-md-3"></div>
                    </div>
                    <div class="row" id="cam-photo-crop">
                                <div class="col-md-12 text-center">
                                    <div id="upload-demo-3"></div>
                                </div>
                    </div>
                    <div class="row" id="cam-photo-crop-button">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <button class="btn btn-primary button-primary-color btn-block upload-image-2">Save</button>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="close-button" onClick="turnCamOff()">Cancel</button>
                </div>
              </div>
            </div>
        </div>
    </div>


    <div class="container">
        @if ( $profile->id == \Auth::user()->id )
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog" style="position: absolute; top: 10%; left: 0; right: 0; margin: auto;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                        <h4 class="modal-title">Background texture</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div style="display: grid; grid-template-columns: repeat(3,1fr);">
                                @foreach($images as $image)
                                    <div class="thumbnail">
                                        <img src="{{$image->name}}" alt="" width="100%" height="100%">
                                        <div class="caption">
                                            <span style="text-align: center;"></span>
                                            <div style="text-align: center;">
                                                <form action='{{ url("/background-image-update/$user->id") }}' method="post" enctype="multipart/form-data" style="text-align: center;"> {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $image->id }}">
                                                    <button class="btn btn-primary button-primary-color" type="submit">Apply</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <!-- Profile header -->
        @if(App\HeaderCoverImage::find($profile->cover_img))
        <div class="gp-header-section d-flex" style="{{ 'background: url(' .  App\HeaderCoverImage::find($profile->cover_img)->name . ')' }}">
        @else 
        <div class="gp-header-section d-flex">
        @endif
            <div class='d-flex profile-img-and-text'>
            @if ( $profile->id == \Auth::user()->id )
                <div class="profile-image-nav" style="position: relative;" data-target="#modal-photo-upload-2" data-toggle="modal">
                    @if (!($profile->profile_img))
                        <img src="/img/no-image.png" alt="profile photo" style="width: 150px; height: 150px;">
                    @else
                        <img src="/uploads/{{$profile->profile_img}}" alt="profile photo" style="width: 150px; height: 150px;">
                    @endif
                        <div class="img-hover">
                            <a style="text-decoration: none; color: #f1f1f1;">
                                <i class="fas fa-camera" style="padding:5px;"></i>Photo
                            </a>
                        </div>
                </div>
            @else
                <div class="profile-image-nav" style="position: relative;">
                    @if (!($profile->profile_img))
                    <img src="/img/no-image.png" alt="profile photo" style="width: 150px; height: 150px;">
                    @else
                    <img src="/uploads/{{$profile->profile_img}}" alt="profile photo" style="width: 150px; height: 150px;">
                    @endif
                    <div>
                        <a style="text-decoration: none; color: #f1f1f1;">
                            <i class="fas fa-camera" style="padding:5px;"></i>Photo
                        </a>
                    </div>
                </div>
            @endif
                <div class="d-flex flex-direction-column profileMainDesc header-menu">
                    <span class="name">{{$profile->full_name}}
                        <i class="fas fa-user-check" style="color: #3DB024; padding-left: 20px;"></i>
                        <span style="color: #3DB024; padding-left: 3px; font-size: 14px;">Verified</span>
                    </span>
                    <ul class="unordered-list-1" style="padding-top: 5px;">
                        <li>
                            <a href="/p/1"><i class="far fa-address-card"></i>21 about</a>
                        </li>
                        <li>
                            <a href="/profile/1/about"><i class="far fa-address-card"></i>34 about</a>
                        </li>
                        <li>
                            <a href="/profile/1/friends"><i class="fas fa-users"></i>65 friends</a>
                        </li>
                    </ul>
                    <ul class="unordered-list-1">
                        <li>
                            <a href="/profile/1/profile"><i class="far fa-user"></i></i>18 profile</a>
                        </li>
                        <li>
                            <a href="/profile/1/photos"><i class="far fa-image"></i>91 photos</a>
                        </li>
                        <li>
                            <a href="/profile/1/videos"><i class="fas fa-film"></i>73 videos</a>
                        </li>
                    </ul>
                    <div style="padding-top: 10px;">
                        <a href="" style="margin: 0; text-decoration: none;">
                            <i class="fas fa-location-arrow" style="padding-right: 5px;"></i>{{$city->name}}, {{$country->name}}
                        </a>
                        <p style="margin: 0;">
                            <i class="fas fa-angle-double-right" style="padding-right: 5px;"></i>
                            Don't worry, be happy ...
                            <i class="fas fa-angle-double-left" style="padding-left: 5px;"></i>
                        </p>
                    </div>
                </div>

            </div>
            <div class="user-profile-right-side">
                <div class="unordered-list-2">
                    <li style="padding-right: 10px;">
                        <a href="" style="color: blue;">
                            <i class="fas fa-medal" style="padding-right: 6px;"></i>
                        </a>
                    </li>
                    <li style="padding-right: 10px;">
                        <a href="" style="color: red;">
                            <i class="fas fa-medal" style="padding-right: 6px;"></i>
                        </a>
                    </li>
                    <li style="padding-right: 10px;">
                        <a href="" style="color: green;">
                            <i class="fas fa-medal" style="padding-right: 6px;"></i>
                        </a>
                    </li>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary button-primary-color btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-h" style="vertical-align: bottom;"></i>
                            <span class="caret" style="margin-left:8px;"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-medal" style="padding: 5px;"></i>More medals
                                </a>
                            </li>
                            @if ( $profile->id == \Auth::user()->id )
                            <li>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal">
                                    <i class="fas fa-camera" style="padding:5px;"></i>Background
                                </a>
                            </li>
                            @endif
                        </div>
                    </div>
                </div>
                @if ( $profile->id !== \Auth::user()->id )
                <div class="btn-group" style="display:flex; align-items:flex-end; padding: 0 20px 15px 0;">
                    {{-- <button type="button" class="btn btn-primary button-primary-color dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> --}}
                    @if (\App\Follower::where('user_id', \Auth::user()->id)->where('follower_id', $profile->id)->where('approved', true)->first())  
                        <button type="button" class="btn btn-primary button-primary-color btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user-friends" style="padding-right: 6px;"></i>Following
                            <span class="caret" style="margin-left:5px;"></span> 
                            <span class="sr-only">Toggle Dropdown</span> 
                        </button>
                    @elseif (\App\Follower::where('user_id', \Auth::user()->id)->where('follower_id', $profile->id)->where('approved', false)->first())  
                        <button type="button" class="btn btn-primary button-primary-color btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user-friends" style="padding-right: 6px;"></i>Follow Request Sent
                            <span class="caret" style="margin-left:5px;"></span> 
                            <span class="sr-only">Toggle Dropdown</span> 
                        </button> 
                    @else 
                        <button onclick="sendFollowRequest()" type="button" class="btn btn-primary button-primary-color">
                            <i class="fas fa-user" style="padding-right: 6px;"></i>Follow
                            {{-- <span class="caret" style="margin-left:5px;"></span> --}}
                            {{-- <span class="sr-only">Toggle Dropdown</span> --}}
                        </button>
                    @endif
                    <div class="dropdown-menu" style="padding: 0">
                        <li>
                            <a onclick="sendUnFollowRequest()" class="dropdown-item btn-danger btn-rounded" href="#">
                                <i class="fas fa-user-minus mr-1 p-2"></i>
                                @if(\App\Follower::where('user_id', \Auth::user()->id)->where('follower_id', $profile->id)->where('approved', true)->first())
                                    Unfollow
                                @else 
                                    Cancel Request
                                @endif
                            </a>
                        </li>
                        {{-- <li>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user-plus" style="padding: 5px;"></i>Send friend request
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user-friends" style="padding: 5px;"></i>Send follow request
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-thumbtack" style="padding:5px;"></i>Follow now
                            </a>
                        </li> --}}
                    </div>
                </div>
                @endif
                @if ( $profile->id !== \Auth::user()->id )
                <div class="btn-group" style="display:flex; align-items:flex-end; padding: 0 20px 15px 0;">
                    <button type="button" class="btn btn-primary button-primary-color">
                        <i class="fas fa-envelope" style="padding-right: 6px;"></i>Message
                        <span style="margin-left:5px;"></span>
                    </button>
                </div>
                @endif
                @if ( $profile->id !== \Auth::user()->id )
                <div class="btn-group" style="display:flex; align-items:flex-end; padding: 0 20px 15px 0;">
                    <button type="button" class="btn btn-default btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span><i class="fas fa-ellipsis-h" style="vertical-align: bottom;"></i></span>
                        <span class="caret" style="margin-left:5px;"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-ban" style="padding:5px;"></i>Block<br>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-comment-slash" style="padding:5px;"></i>Report
                            </a>
                        </li>
                    </div>
                </div>
                @endif
            </div>
            <!-- Profile header end -->
        </div>
    </div>
</div>

     <script type="text/javascript">
        function turnCamOff() {
            Webcam.reset();
        }
    </script>

    <script type="text/javascript">
        // follow functionality
        function sendFollowRequest() {
            let user_id = {{\Auth::user()->id}};
            let follower_id = {{$profile->id}};
            let data = {
                user_id: user_id,
                follower_id: follower_id
            }

            axios.post('/api/send-follow-request', data).then(() => {
                location.reload();
            })
        }

        function sendUnFollowRequest() {
            let user_id = {{\Auth::user()->id}};
            let follower_id = {{$profile->id}};
            let data = {
                user_id: user_id,
                follower_id: follower_id
            }

            axios.delete('/api/send-follow-request/destroy', {
                params: {
                    user_id: user_id,
                    follower_id: follower_id
                }
            }).then(() => {
                location.reload();
            })
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var takenImg;

         var resize2 = $('#upload-demo-3').croppie({
            enableExif: true,
            enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 300,
                height: 300,
                type: 'circle' //square
            },
            boundary: {
                width: 450,
                height: 350
            }
        });

        function startCam() {
            document.getElementById("close-button").click();
                Webcam.set({
                width: 390,
                height: 290,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
            Webcam.attach( '#my_camera' );
        }
        
        function retakePhoto() {
            $("#retake-photo").css("display","none");
            $("#preview-photo-using-cam").css("display","none");
            $("#submit-cam-button").css("display","none");
            $("#take-photo-using-cam").css("display","block");
            $("#take-snapshot-button").css("display","block");
        }

        function take_snapshot() {
            Webcam.snap( function(data_uri) {
                $("#take-photo-using-cam").css("display","none");
                $("#take-snapshot-button").css("display","none");
                $("#submit-cam-button").css("display","block");
                $("#preview-photo-using-cam").css("display","block");
                $("#retake-photo").css("display","block");
                $(".image-tag").val(data_uri);
                takenImg = data_uri;
                document.getElementById('results').innerHTML = '<img style="margin:auto;" src="'+data_uri+'"/>';
            });
        }

          function storeTakenImg() {
            $("#retake-photo").css("display","none");
            $("#submit-cam-button").css("display","none");
            $("#preview-photo-using-cam").css("display","none");
            $("#cam-photo-crop").css("display","block");
            $("#cam-photo-crop-button").css("display","block");
            resize2.croppie('bind', {
            url: takenImg
        });
        }

         $('.upload-image-2').on('click', function (ev) {
          resize2.croppie('result', {
            type: 'canvas',
            size: 'viewport'
          }).then(function (takenImg) {
            html = '<img src="' + takenImg + '" />';
            $("#preview-crop-image").html(html);
            $("#upload-success").html("Images cropped and uploaded successfully.");
            $("#upload-success").show();
            $.ajax({
            url: "{{route('croppie.upload-image')}}",
            type: "POST",
            data: {"image":takenImg},
            success: function (data) {
                Webcam.reset();
                setTimeout(location.reload(true), 3000);
                }
            });
          });
        });
    </script>
   
    <script type="text/javascript">
        function closeModal() {
            document.getElementById("close-button").click();
        }
    </script>

    <script type="text/javascript">

        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });

        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 300,
                height: 300,
                type: 'circle' //square
            },
            boundary: {
                width: 450,
                height: 350
            }
        });


        $('#image_file').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            $("#upload-profile-img-area").css("display","none");
            $("#upload-demo-croppie").css("display","block");
            $("#upload-demo-croppie-button").css("display","block");
            reader.readAsDataURL(this.files[0]);
        });

        function clearImage() {
           location.reload();
        }

        $('.upload-image').on('click', function (ev) {
          resize.croppie('result', {
            type: 'canvas',
            size: 'viewport'
          }).then(function (img) {
            html = '<img src="' + img + '" />';
            $("#preview-crop-image").html(html);
            $("#upload-success").html("Images cropped and uploaded successfully.");
            $("#upload-success").show();
            $.ajax({
            url: "{{route('croppie.upload-image')}}",
            type: "POST",
            data: {"image":img},
            success: function (data) {
                setTimeout(location.reload(true), 3000);
                }
            });
          });
        });
    </script>

    <script type="text/javascript">
        $(function() {
                $("#upload_link").on('click', function(e) {
                e.preventDefault();
                $("#img1:hidden").trigger('click');
                });
        });
    </script>

    <script type="text/javascript">
        if(document.getElementById("img1")) {
            document.getElementById("img1").onchange = function() {
                document.getElementById("profile-photo-upload").submit();
            };
        }
    </script>

    <script type="text/javascript">
        var upload = document.getElementById('image_file');

        function onFile() {
            var me = this,
                file = upload.files[0],
                name = file.name.replace(/\.[^/.]+$/, '');

            if (file.type === '' ||
                file.type === 'image/jpg' ||       
                file.type === 'image/jpeg' ||
                file.type === 'image/png') {
                if (file.size < (3000 * 1024)) {
                    upload.parentNode.className = 'area uploading';
                } else {
                    window.alert('File size is too large, please ensure you are uploading a file of less than 3MB');
                }
            } else {
                window.alert('File type ' + file.type + ' not supported');
            }
        }

        upload.addEventListener('dragenter', function (e) {
            upload.parentNode.className = 'area dragging';
        }, false);

        upload.addEventListener('dragleave', function (e) {
            upload.parentNode.className = 'area';
        }, false);

        upload.addEventListener('dragdrop', function (e) {
            onFile();
        }, false);

        upload.addEventListener('change', function (e) {
            onFile();
        }, false);
    </script>