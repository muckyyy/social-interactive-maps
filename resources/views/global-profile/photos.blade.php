@extends('layouts.app')

@section('per-page-css')
    <link rel='stylesheet' href='/css/global-profile.css' />
@endsection

@section('content')

	@include('global-profile.navigation')

	<div class="container" style="margin-bottom: 60px;">

		<div class="friends-search-bar-section col-xs-12">
			<div class="custom-friends-header-name col-xs-8">
				Test's Pr0 Albums (17)
			</div>
			<div class="ps-header-btn no-padding">
				Create Album +
			</div>
		</div>

		<div class="ps-main-section col-xs-12 no-padding">

			<a href="/profile/1/album/1">
				<div class="col-xs-3" style='margin-bottom:15px;'>
					<div class="ps-li">
						<div class="ps-li-header-img">
							<img src="https://html.crumina.net/html-olympus/img/photo-album1.jpg">
						</div>
						<div class="ps-li-text col-xs-12">
							Some Awesome Vacation
							<div class="ps-li-date col-xs-12">
								Last Added: 2 hours ago
							</div>
							<div class="ps-comment-num col-xs-12 no-padding">
								<div class="col-xs-4">
									17
								</div>
								<div class="col-xs-4">
									57
								</div>
								<div class="col-xs-4">
									7
								</div>
							</div>
							<div class="ps-comment-text col-xs-12 no-padding">
								<div class="col-xs-4">
									Photos
								</div>
								<div class="col-xs-4">
									Comments
								</div>
								<div class="col-xs-4">
									Shares
								</div>
							</div>
						</div>
					</div>
				</div>
			</a>

			<a href="/profile/1/album/1">
				<div class="col-xs-3" style='margin-bottom:15px;'>
					<div class="ps-li">
						<div class="ps-li-header-img">
							<img src="https://html.crumina.net/html-olympus/img/photo-album2.jpg">
						</div>
						<div class="ps-li-text col-xs-12">
							Some Awesome Vacation 2
							<div class="ps-li-date col-xs-12">
								Last Added: 2 hours ago
							</div>
							<div class="ps-comment-num col-xs-12 no-padding">
								<div class="col-xs-4">
									17
								</div>
								<div class="col-xs-4">
									57
								</div>
								<div class="col-xs-4">
									7
								</div>
							</div>
							<div class="ps-comment-text col-xs-12 no-padding">
								<div class="col-xs-4">
									Photos
								</div>
								<div class="col-xs-4">
									Comments
								</div>
								<div class="col-xs-4">
									Shares
								</div>
							</div>
						</div>
					</div>
				</div>
			</a>

			<a href="/profile/1/album/1">
				<div class="col-xs-3" style='margin-bottom:15px;'>
					<div class="ps-li">
						<div class="ps-li-header-img">
							<img src="https://html.crumina.net/html-olympus/img/photo-album3.jpg">
						</div>
						<div class="ps-li-text col-xs-12">
							Some Awesome Vacation 3
							<div class="ps-li-date col-xs-12">
								Last Added: 2 hours ago
							</div>
							<div class="ps-comment-num col-xs-12 no-padding">
								<div class="col-xs-4">
									17
								</div>
								<div class="col-xs-4">
									57
								</div>
								<div class="col-xs-4">
									7
								</div>
							</div>
							<div class="ps-comment-text col-xs-12 no-padding">
								<div class="col-xs-4">
									Photos
								</div>
								<div class="col-xs-4">
									Comments
								</div>
								<div class="col-xs-4">
									Shares
								</div>
							</div>
						</div>
					</div>
				</div>
			</a>

			<a href="/profile/1/album/1">
				<div class="col-xs-3" style='margin-bottom:15px;'>
					<div class="ps-li">
						<div class="ps-li-header-img">
							<img src="https://html.crumina.net/html-olympus/img/photo-album4.jpg">
						</div>
						<div class="ps-li-text col-xs-12">
							Some Awesome Vacation 4
							<div class="ps-li-date col-xs-12">
								Last Added: 2 hours ago
							</div>
							<div class="ps-comment-num col-xs-12 no-padding">
								<div class="col-xs-4">
									17
								</div>
								<div class="col-xs-4">
									57
								</div>
								<div class="col-xs-4">
									7
								</div>
							</div>
							<div class="ps-comment-text col-xs-12 no-padding">
								<div class="col-xs-4">
									Photos
								</div>
								<div class="col-xs-4">
									Comments
								</div>
								<div class="col-xs-4">
									Shares
								</div>
							</div>
						</div>
					</div>
				</div>
			</a>

			<a href="/profile/1/album/1">
				<div class="col-xs-3" style='margin-bottom:15px;'>
					<div class="ps-li">
						<div class="ps-li-header-img">
							<img src="https://html.crumina.net/html-olympus/img/photo-album5.jpg">
						</div>
						<div class="ps-li-text col-xs-12">
							Some Awesome Vacation 5
							<div class="ps-li-date col-xs-12">
								Last Added: 2 hours ago
							</div>
							<div class="ps-comment-num col-xs-12 no-padding">
								<div class="col-xs-4">
									17
								</div>
								<div class="col-xs-4">
									57
								</div>
								<div class="col-xs-4">
									7
								</div>
							</div>
							<div class="ps-comment-text col-xs-12 no-padding">
								<div class="col-xs-4">
									Photos
								</div>
								<div class="col-xs-4">
									Comments
								</div>
								<div class="col-xs-4">
									Shares
								</div>
							</div>
						</div>
					</div>
				</div>
			</a>

		</div>


	</div>

@endsection

@section('map-scripts')

@endsection
