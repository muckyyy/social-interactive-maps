@extends('layouts.app')

@section('per-page-css')
    <link rel='stylesheet' href='/css/global-profile.css' />
@endsection

@section('content')

	@include('global-profile.navigation')

	<div class="container" style="margin-bottom: 60px;">
		
		<div class="friends-search-bar-section col-xs-12">
			<div class="custom-friends-header-name col-xs-8">
				Test's Pr0 Freinds (77)
			</div>
			<div class="col-xs-4 no-padding">
				<input class="form-control" type="text" placeholder="Search friends" style="height: 40px;line-height: 40px;margin-top: 15px;">
				<i class="fas fa-search custom-search-friends-icon"></i>
			</div>
		</div>

		<div class="fs-list-section col-xs-12 no-padding">

			<div class="col-xs-3 fs-li">
				<div class="fs-header-img col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/friend1.jpg">
				</div>
				<div class="fs-profile-img">
					<img src="https://html.crumina.net/html-olympus/img/avatar5.jpg">
				</div>
				<div class="fs-text col-xs-12">
					Sitko Dotapro
					<div class="fs-location col-xs-12">
						Sarajevo
					</div>
				</div>
			</div>

			<div class="col-xs-3 fs-li">
				<div class="fs-header-img col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/friend2.jpg">
				</div>
				<div class="fs-profile-img">
					<img src="https://html.crumina.net/html-olympus/img/avatar6.jpg">
				</div>
				<div class="fs-text col-xs-12">
					Sitko Dotapro
					<div class="fs-location col-xs-12">
						Sarajevo
					</div>
				</div>
			</div>

			<div class="col-xs-3 fs-li">
				<div class="fs-header-img col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/friend3.jpg">
				</div>
				<div class="fs-profile-img">
					<img src="https://html.crumina.net/html-olympus/img/avatar7.jpg">
				</div>
				<div class="fs-text col-xs-12">
					Sitko Dotapro
					<div class="fs-location col-xs-12">
						Sarajevo
					</div>
				</div>
			</div>

			<div class="col-xs-3 fs-li">
				<div class="fs-header-img col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/friend4.jpg">
				</div>
				<div class="fs-profile-img">
					<img src="https://html.crumina.net/html-olympus/img/avatar8.jpg">
				</div>
				<div class="fs-text col-xs-12">
					Sitko Dotapro
					<div class="fs-location col-xs-12">
						Sarajevo
					</div>
				</div>
			</div>

			<div class="col-xs-3 fs-li">
				<div class="fs-header-img col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/friend5.jpg">
				</div>
				<div class="fs-profile-img">
					<img src="https://html.crumina.net/html-olympus/img/avatar9.jpg">
				</div>
				<div class="fs-text col-xs-12">
					Sitko Dotapro
					<div class="fs-location col-xs-12">
						Sarajevo
					</div>
				</div>
			</div>

		</div>

	</div>

@endsection

@section('map-scripts')

@endsection
