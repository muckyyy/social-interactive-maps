@extends('layouts.app')

@section('per-page-css')
    <link rel='stylesheet' href='/css/global-profile.css' />
@endsection

@section('content')

	@include('global-profile.navigation')

	<div class="container" style="margin-bottom: 60px;">

		<div class="friends-search-bar-section col-xs-12">
			<div class="custom-friends-header-name col-xs-8">
				Test's Pr0 Videos (17)
			</div>
			<div class="ps-header-btn no-padding">
				Add New Video +
			</div>
		</div>

		<div class="vs-videos-section col-xs-12 no-padding">

			<div class="col-xs-3">
				<div class="vs-video-li col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/video10.jpg">
					<div class="vs-video-li-text col-xs-12">
						Sea Dance Festival
						<div class="vs-video-li-time col-xs-12 no-padding">18:17</div>
					</div>
				</div>
			</div>

			<div class="col-xs-3">
				<div class="vs-video-li col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/video10.jpg">
					<div class="vs-video-li-text col-xs-12">
						Sea Dance Festival
						<div class="vs-video-li-time col-xs-12 no-padding">18:17</div>
					</div>
				</div>
			</div>

			<div class="col-xs-3">
				<div class="vs-video-li col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/video10.jpg">
					<div class="vs-video-li-text col-xs-12">
						Sea Dance Festival
						<div class="vs-video-li-time col-xs-12 no-padding">18:17</div>
					</div>
				</div>
			</div>

			<div class="col-xs-3">
				<div class="vs-video-li col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/video10.jpg">
					<div class="vs-video-li-text col-xs-12">
						Sea Dance Festival
						<div class="vs-video-li-time col-xs-12 no-padding">18:17</div>
					</div>
				</div>
			</div>

			<div class="col-xs-3">
				<div class="vs-video-li col-xs-12 no-padding">
					<img src="https://html.crumina.net/html-olympus/img/video10.jpg">
					<div class="vs-video-li-text col-xs-12">
						Sea Dance Festival
						<div class="vs-video-li-time col-xs-12 no-padding">18:17</div>
					</div>
				</div>
			</div>

		</div>


	</div>

@endsection

@section('map-scripts')

@endsection
