@extends('layouts.app')

@section('per-page-css')
    <link rel='stylesheet' href='/css/global-profile.css' />
@endsection

@section('content')

	@include('global-profile.navigation')

	<div class="container" style="margin-bottom: 60px;">

		<!-- Profile info section -->
		<div class="col-xs-12 col-md-4 no-padding" style="padding-right: 15px;">

			<div class="col-xs-12 global-profile-timeline-left no-padding">
				<div class="gp-block-title">
					Profile Info
				</div>

				<div class="gp-block-content">

					<div class="gp-block-content-li gp-about-me-li col-xs-12 no-padding">
						<div class="gp-block-content-li-title col-xs-4 no-padding" style="float: left;">About me:</div>
						<div class="col-xs-8" style="float: left;">
							Hi, I’m James, I’m 36 and I work as a Digital Designer for the “Daydreams” Agency in Pier 56.
						</div>
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-12 no-padding">
						<div class="gp-block-content-li-title col-xs-4 no-padding" style="float: left;">Birthday:</div>
						<div class="col-xs-8" style="float: left;">
							December 14th, 1987
						</div>
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-12 no-padding">
						<div class="gp-block-content-li-title col-xs-4 no-padding" style="float: left;">Birthplace:</div>
						<div class="col-xs-8" style="float: left;">
							Rajvosa
						</div>
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-12 no-padding">
						<div class="gp-block-content-li-title col-xs-4 no-padding" style="float: left;">Gender:</div>
						<div class="col-xs-8" style="float: left;">
							Male
						</div>
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-12 no-padding">
						<div class="gp-block-content-li-title col-xs-4 no-padding" style="float: left;">Status:</div>
						<div class="col-xs-8" style="float: left;">
							Married
						</div>
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-12 no-padding">
						<div class="gp-block-content-li-title col-xs-4 no-padding" style="float: left;">Phone Number:</div>
						<div class="col-xs-8" style="float: left;">
							(044) 234 - 4369 - 8957
						</div>
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-12 no-padding">
						<div class="gp-block-content-li-title col-xs-4 no-padding" style="float: left;">Joined:</div>
						<div class="col-xs-8" style="float: left;">
							April 31, 2014
						</div>
					</div>


				</div>
			</div>

		</div>
		<!-- Profile info section end -->

		<!-- Hobbies section -->
		<div class="col-xs-12 col-md-8 no-padding">

			<div class="col-xs-12 global-profile-timeline-left no-padding">
				<div class="gp-block-title">
					Hobbies and Interests
				</div>

				<div class="gp-block-content">

					<div class="gp-block-content-li gp-about-me-li col-xs-6 no-padding" style="padding-right: 15px;">
						<div class="gp-block-content-li-title no-padding"">Hobbies:</div>
						I like to ride the bike to work, swimming, and working out. I also like reading design magazines, go to museums, and binge watching a good tv show while it’s raining outside.
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-6 no-padding" style="padding-left: 15px;">
						<div class="gp-block-content-li-title no-padding"">Hobbies:</div>
						I like to ride the bike to work, swimming, and working out. I also like reading design magazines, go to museums, and binge watching a good tv show while it’s raining outside.
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-6 no-padding" style="padding-right: 15px;">
						<div class="gp-block-content-li-title no-padding"">Favourite TV Shows:</div>
						Breaking Good, RedDevil, People of Interest, The Running Dead, Found, American Guy.
					</div>

					<div class="gp-block-content-li gp-about-me-li col-xs-6 no-padding" style="padding-left: 15px;">
						<div class="gp-block-content-li-title no-padding"">Favourite TV Shows:</div>
						Breaking Good, RedDevil, People of Interest, The Running Dead, Found, American Guy.
					</div>


				</div>
			</div>

		</div>
		<!-- Hobbies section end -->

	</div>

@endsection

@section('map-scripts')

@endsection
