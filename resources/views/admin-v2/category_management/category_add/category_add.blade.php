@extends('admin-v2.index')

@section('content')
<div id="category-add" class="container">
    <div class="main-admin-section col-xs-12">

        <div>
            <div class="progress rounded">
            <div class="progress-bar progress-bar-success bg-admin" role="progressbar" aria-valuenow="50"
            aria-valuemin="0" aria-valuemax="100" style="width:50%">
            1/2
            </div>
            </div>
        </div>

        @include('partials.notifications')
        <div>
             <div class="admin-category-title mb-2">
                Add new category:
            </div>
            <form class="form-horizontal" method="POST" action="{{ route('category.add') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

              <input type="text" class="form-control rounded" name="name" required="required" value="{{ old('name') ?: '' }}" placeholder="Category name...">

                <div class="file-icon-text mb-2 mt-4">
                    Parent category
                </div>
                <select name="parent_id" class="form-control rounded">
                    <option value="">Choose parent category...</option>
                    <option value="0">None (Master)</option>
                    @foreach($categories as $category)
                        <option class="admin-select-li" value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>

                <input type="hidden" name="icon" id='admin-category-icon' required="required">

                <div class="file-icon-section col-xs-12 no-padding mt-4">

                    <div class="file-icon-text mb-2">
                        Custom icon
                    </div>

                    <input type="file" name="icon_file">
                </div>

                <div class="file-icon-text col-xs-12 no-padding" style="margin-top: 15px;">
                    <div class='col-xs-12 no-padding mb-1'>
                        Force Child:
                    </div>
                    <input style="width: 30%;" class="form-control col-xs-2" type="checkbox" name="force_child" value="1" checked="checked">
                </div>

                <div class="category-tags-section col-xs-12 no-padding" style="margin-top: 15px;font-size: 20px;font-weight: 600;color: #636b6f;">
                    Tags
                    <input type="tags" name="tags" class="form-control rounded mt-2" placeholder="For example #travel #epic #stuff">
                </div>
                <input type="submit" class="btn btn-admin" value="Add" style="float:right;width:100%;margin-top: 30px;">
        </div>

        </form>
    </div>
</div>
@endsection