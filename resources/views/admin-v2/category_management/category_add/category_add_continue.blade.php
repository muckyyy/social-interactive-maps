@extends('admin-v2.index')

@section('content')
<div id="category_add_continue" class="navigation-section-stabilizer">
    <div class="container">
        <div class="main-admin-section col-xs-12">
            <div>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100"
                    aria-valuemin="0" aria-valuemax="100" style="width:100%">
                    2/2
                    </div>
                </div>
            </div>

            @include('partials.notifications')
            <div>
                <form class="form-horizontal" method="POST" action="{{ route('category.add_groups') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div style="font-size: 16px;font-weight: 600; margin-bottom: 10px;">
                        Choose category field groups
                    </div>
                    <input type="text" name="category_id" class="d-none" value="">
                    <select class="form-control rounded mb-4" name="group_id" id="typeselector" onchange="selectInput()">
                        <option value="">Choose location field group...</option>
                        @foreach ($parentGroups as $group)
                            <option disabled value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                        @foreach ($locationFieldGroups as $group )
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endforeach
                        <input name="category_id" type="text" class="d-none" value="{{ $category->id }}">
                    </select>
                    
                    <div style="width:100%;" class="d-flex justify-content-center mt-4">
                        <input style="width:100px" class="btn btn-admin rounded" type="submit" value="Add">
                        <a style="width:100px" class="btn btn-admin rounded ml-4" href="{{ route('category_management') }}">Finish</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection