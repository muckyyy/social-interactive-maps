@extends('admin-v2.index')

@section('content')
    <div id="category-icons">
        <div class="navigation-section-stabilizer">
            <div class="container">
        
                <div class="category-icons-title" style="margin-bottom: 30px;">
                    Add icons for category <span>"{{$category->name}}"</span>
                </div>
        
                <div class="col-xs-12 admin-icons-list">
        
                    @for($i=1;$i<=43;$i++)
                    <div class="admin-icon">
                        <img src="/img/category-icons/icon{{$i}}.svg" />
                    </div>
                    @endfor
                    
                </div>
        
                <button id='icon-admin-btn' class="col-xs-offset-4 col-xs-4 btn btn-admin" style="margin-top: 60px;">
                    FINISH
                </button>
        
            </div>
        </div>
    </div>
        
@section('admin_scripts')    
    <script>
        var iconSet = []
        var categoryId = '{{$id}}'

        document.getElementById("icon-admin-btn").disabled = true;

        $('.admin-icon').click(function(e){

            if($(this).hasClass("iconActive")) {

                let tempIcon = (e.currentTarget.children[0].currentSrc).slice(-10)
                tempIcon = tempIcon.replace('/', '');

                for(let i=0; i<iconSet.length; i++){
                    if(iconSet[i] == tempIcon) {
                        iconSet.splice(i, 1);
                    }
                }
                $(this).removeClass('iconActive')

            } else {
                $(this).addClass('iconActive')
                let addIcon = (e.currentTarget.children[0].currentSrc).slice(-10)
                addIcon = addIcon.replace('/', '');
                iconSet.push(addIcon)	
            }
            if(iconSet == null || iconSet == ''){
                document.getElementById("icon-admin-btn").disabled = true;
            } else {
                document.getElementById("icon-admin-btn").disabled = false;
            }
        })
        //Upload icons
        $('#icon-admin-btn').click(function(){

            $.ajax
            ({
                dataType: 'json',
                type: "GET",
                url: "/admin-category-icons-post",
                data: {"iconSet": iconSet, "id":categoryId},
                    success: function(data)
                    {
                        console.log('data',data)
                        window.location.href = "/admin/category_management";
                    }
            });

        })
    </script>  
@endsection  <!-- Scripts  -->   
@endsection  <!-- Content  -->  