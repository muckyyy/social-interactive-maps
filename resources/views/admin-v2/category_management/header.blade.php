<div class="admin_header d-flex">
    <div>
    <a href="{{ url('admin/category_management') }}" class="{{ strpos( url()->current(), 'location_fields_groups') ? '' : (strpos( url()->current(), 'locations_fields') ? '' : 'active') }}">
            Category List
        </a>
    </div>
    <div>
    <a href="{{ url('admin/category_management/location_fields_groups') }}" class="{{ strpos( url()->current(), 'location_fields_groups') ? 'active' : '' }}">
            Location fields groups
        </a>
    </div>
    <div>
        <a href="{{ url('admin/category_management/locations_fields') }}" class="{{ strpos( url()->current(), 'locations_fields') ? 'active' : '' }}">
        Location fields
        </a>
    </div>
</div>
