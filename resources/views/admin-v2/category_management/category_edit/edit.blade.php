@extends('admin-v2.index')

@section('content')
<div id="category-edit" class="container">
    <div class="main-admin-section col-xs-12">
        <div class="col-xs-9" style="padding: 0 15px">
            <div class="admin-category-title">
                <div>
                    <span>Category</span>
                    <div>
                        <span>{{$category->name}} {!! $category->icon !!}</span> 
                    </div> 
                </div>
            </div>

            @if(!empty($parentCategory))
            <div class="edit-subcategories">
                <div class="edit-subcategory-title">
                    Parent category:
                </div>
                <div class="admin-custom-field-section" style="margin-bottom: 5px;"> 
                     {{$parentCategory->name}}
                </div>
            </div>

            @else
            <div class="edit-subcategories">
                <div class="edit-subcategory-title">
                    Parent category:
                </div>

                <div class="admin-custom-field-section" style="margin-bottom: 5px;" title="This category is master category"> 
                    No parent category
                </div>
            </div>
            @endif

            @if(!$subCategories->isEmpty())
            <div class="edit-subcategories">
                <div class="edit-subcategory-title">
                    Subcategories:
                </div>
                @foreach($subCategories as $subcat)
                <div class="admin-custom-field-section" style="margin-bottom: 5px;"> 
                     {{$subcat->name}}
                </div>
                @endforeach
            </div>
            @else
            <div class="edit-subcategories">
                <div class="edit-subcategory-title">
                    Subcategories:
                </div>

                <div class="admin-custom-field-section" style="margin-bottom: 5px;"> 
                     No subcategories
                </div>
            </div>
            @endif

            <div class="edit-subcategories">
                <div class="edit-subcategory-title">
                    Category tags:
                </div>

                @if(empty($tagString))
                    <div class="admin-custom-field-section"> 
                         No tags for this category
                    </div>

                    @else

                    <div class="admin-custom-field-section"> 
                        {{$tagString}} 
                    </div>
                @endif
            </div>
            {{-- @if(!$customFields->isEmpty())
            <div class="edit-custom-fields">
                <div class="edit-subcategory-title">
                    Custom fields:
                </div>
                {{-- @foreach($customFields as $field)
                <div class="admin-custom-field-section"> 
                    <div style="display: grid">
                        <div class="d-flex">
                            <span class="title">Name:</span>
                            <span>{{$field->name}}</span>  
                        </div>
                        <div class="d-flex">
                            <span class="title">Type:</span>
                            <span>{{$field->type}}</span>  
                        </div>
                        <div class="d-flex">
                            <span class="title">Value:</span>
                            <span>
                                @if($field->type == 'image')
                                    <img src="/img/categories/{{$field->content}}">
                                @else
                                    <span>{{$field->content}}</span>
                                @endif    
                            </span>  
                        </div>    
                    </div>
                    <form class="d-flex" method="POST" action="/delete-custom-field/{{$field->id}}/{{$category->id}}">
                        {{ csrf_field() }}
                        <a href="/admin/category_management/change_custom_field/{{$field->id}}/{{$category->id}}">
                            <span class="btn btn-success custom-fields-edit-btn">Edit</span> 
                        </a>
                        <input type="submit" class="btn btn-danger custom-fields-edit-btn" value="Delete">
                    </form>
                </div>
                @endforeach --}}
            {{-- </div>
            @else
                <div class="edit-subcategories">
                    <div class="edit-subcategory-title">
                        Custom fields:
                    </div>

                    <div class="admin-custom-field-section"> 
                         No custom fields
                    </div>
                </div>
            @endif --}} 
            <a id="editCategoryButton" href="/admin-change-categort-basics/{{$category->id}}">
                <span class="btn btn-success custom-fields-edit-btn">
                    Edit
                </span> 
            </a>
        </div>
    </div>
</div>
@endsection