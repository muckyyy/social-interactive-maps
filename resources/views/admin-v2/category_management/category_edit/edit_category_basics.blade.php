@extends('admin-v2.index')

@section('content')
<div id="category-edit-custom" class="container">
	<div class="main-admin-section col-xs-12" style="margin-bottom:20px">
			<div class="col-xs-12">
				<div class="admin-category-title">
					Edit category <span style="color: #3097d1;">{{$category->name}}</span>:
                </div>

				<form id="editCategory" class="form-horizontal" method="POST" action="/change-category-basics-post/{{$category->id}}" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="file-icon-text" style="margin-top: 15px;">
						Name
					</div>
					<input type="text" class="form-control" name="name" placeholder="Name" required="required" value="{{$category->name}}">

					<div class="file-icon-text" style="margin-top: 15px;">
						Parent category
					</div>

					<select name="parent_id" class="form-control">
						<option value="0">None (Master)</option>
						@foreach($categories->where('parent_id', 0) as $cat)
							<option value="{{$cat->id}}" {{ ($cat->id == $category->parent_id) ? 'selected="selected"' : '' }}> {{$cat->name }} </option>
						@endforeach
					</select>

					<input type="hidden" name="custom_icon" id='admin-category-icon' required="required">

					<div class="file-icon-section col-xs-12 no-padding">
						<div class="file-icon-text">
							Icon prefix
						</div>

                        <input type="text" name="icon_prefix" class="form-control" value="{{$category->icon_prefix ?: 'fa'}}">
                        <small>Example: 'glyphicon' or 'fa'"</small>
                    </div>
                    
                    <div class="file-icon-section col-xs-12 no-padding">
						<div class="file-icon-text">
							Icon class
						</div>

						<input type="text" name="icon_class" class="form-control" placeholder="Example: store, school, mountain">
                    </div>
                    
                    <div class="file-icon-section col-xs-12 no-padding">
						<div class="file-icon-text">
							Icon color
                        </div>
                        @php 
                            $colors = ['darkred', 'lightred', 'orange', 'beige', 'green', 'darkgreen', 'lightgreen', 'blue', 'darkblue', 'lightblue', 'purple', 'darkpurple', 'pink', 'cadetblue', 'white', 'gray', 'lightgray', 'black'];
                        @endphp    

                        <select name="icon_color"  class="form-control">
                            @if(!$category->icon_color)
                                <option value="">Select color...</option>
                            @endif

                            @foreach($colors  as $color)
                                <option value="{{$color}}" {{ ($color == $category->icon_color) ? 'selected="selected"' : '' }}> {{$color }} </option>
                            @endforeach
					</select>
					</div>

					<div class="file-icon-section col-xs-12 no-padding">
						<div class="file-icon-text">
							Custom icon
						</div>

						<input type="file" name="icon_file">
					</div>

					<div class="category-tags-section col-xs-12 no-padding">
						<div class="file-icon-text">
							Tags
						</div>
						<input type="tags" name="tags" class="form-control" value="{{$tagString}}" placeholder="For example #travel #epic #stuff">
					</div>
			</div>


			<div class="col-xs-12">

				<div class="admin-category-icons-title">
					Stock icons:
				</div>

				<div class="category-icons">
					<div class="category-icon">
						<i class="fas fa-utensils"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-coffee"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-anchor"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-archway"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-bed"></i>
					</div>
					<div class="category-icon">
						<i class="fab fa-bitcoin"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-book"></i>
					</div>

					<div class="category-icon">
						<i class="fas fa-air-freshener"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-car"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-cart-arrow-down"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-charging-station"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-city"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-cocktail"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-dice"></i>
					</div>

					<div class="category-icon">
						<i class="fas fa-envelope"></i>
					</div>
					<div class="category-icon">
						<i class="fab fa-envira"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-fire-extinguisher"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-gamepad"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-gift"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-plane"></i>
					</div>

					<div class="category-icon">
						<i class="fab fa-atlassian"></i>
					</div>
					<div class="category-icon">
						<i class="fab fa-audible"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-camera-retro"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-basketball-ball"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-baseball-ball"></i>
					</div>

					<div class="category-icon">
						<i class="fas fa-cubes"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-drum"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-fire-extinguisher"></i>
					</div>
					<div class="category-icon">
						<i class="fas fa-flask"></i>
					</div>
					<div class="category-icon">
						<i class="fab fa-fort-awesome"></i>
					</div>
				</div>
			</div>
		</div>
	</form>
		<input onclick="document.getElementById('editCategory').submit()" type="submit" class="submitButton btn btn-admin"  value="Edit">
</div>

	@section('admin_scripts')
		<script src='/js/admin/commonJs.js'></script>
	@endsection

@endsection
