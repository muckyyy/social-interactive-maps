@extends('admin-v2.index')

@section('content')
<div id="category_edit_custom_fields" class="container">
    <div class="main-admin-section col-xs-12">
        <form class="form-horizontal" method="POST" action="/change-custom-field-post/{{$customField->id}}/{{$categoryId}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="title">
                Custom field name:
            </div>
            <input class="form-control" type="text" name="name" placeholder="Field name" value="{{$customField->name}}" required="required" style="margin-bottom: 15px;">

            <div class="title">
                Choose type
            </div>
            <select class="form-control" name="type" id="typeselector" onchange="showTypeField()">
                <option value="text">Text</option>
                <option value="largeText">Large text</option>
                <option value="number">Number</option>
                <option value="image">Image</option>
            </select>

            <div class="field-type" id="text">
                <input class="form-control" type="text" name="text" value="{{$customField->content}}" placeholder="Insert text...">
            </div>

            <div class="field-type" id="largeText">
                <textarea class="form-control" name="largeText">{{$customField->content}}</textarea>
            </div>

            <div class="field-type" id="number">
                <input class="form-control" type="number" value="{{$customField->content}}" name="number" placeholder="Insert number...">
            </div>

            <div class="field-type" id="image">
                <input class="form-control" type="file" name="image">
            </div>
            
            <input class="btn btn-admin" type="submit" value="Submit" style="margin-top: 30px;width: 100%; background: #3fb9a0; border-radius: 5px;">
        </form>
    </div>
</div>
@endsection
@section('admin_scripts')    
    <script>
        function hideAll() {
            document.getElementById('text').style.display = 'none';
            document.getElementById('largeText').style.display = 'none';
            document.getElementById('number').style.display = 'none';
            document.getElementById('image').style.display = 'none';
        }

        function showTypeField() {
            let typeselector = document.getElementById('typeselector');
            let text = document.getElementById('text');
            let largeText = document.getElementById('largeText');
            let number = document.getElementById('number');
            let image = document.getElementById('image');

            hideAll();

            setTimeout(() => {
                if(typeselector.value == 'text') {
                    text.style.display = 'block';
                }

                if(typeselector.value == 'largeText') {
                    largeText.style.display = 'block';
                }

                if(typeselector.value == 'number') {
                    number.style.display = 'block';
                }

                if(typeselector.value == 'image') {
                    image.style.display = 'block';
                }
            }, 10)
        }
    </script>
@endsection    