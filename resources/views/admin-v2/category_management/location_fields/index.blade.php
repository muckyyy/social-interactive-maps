@extends('admin-v2.index')

@section('content')
    <div id="category-management">
        @include('admin-v2.category_management.header')
        <div class="addNewGroup d-flex justify-content-end">
            <a href="{{ route('location_fields.create') }}" class="btn btn-admin rounded">Add new location field</a>
        </div>
        @include('partials.notifications')
        <table class="table table-responsive" id="category_location_groups" style="width:100% !important;">
            <tr class="p-4">
                <th class="p-2">Name</th>
                <th class="p-2">Short name</th>
                <th class="p-2">Description</th>
                <th class="p-2">Group</th>
                <th class="p-2">Data Type</th>
                <th class="p-2">Number of fields</th>
                <th class="p-2">Actions</th>
            </tr>
            @foreach($locations as $location)
            <tr class='even'>
                <td>{{ $location->name }}</td>
                <td>{{ $location->short_name }}</td>
                <td class="collapsed" title='{{ $location->description }}'>{{ $location->description }}</td>
                <td class="collapsed" title='{{ \App\LocationFieldGroup::where('id', $location->group_id)->first()->name }}'>
                    {{ \App\LocationFieldGroup::where('id', $location->group_id)->first()->name }}
                </td>
                <td>{{ $location->data_type }}</td>
                <td>
                    {{ $location->noOfFields }}
                </td>
                <td class="actions d-flex justify-content-center">
                    <div class="d-flex">
                        <a color='#35baa0' href="{{ url("admin/category_management/locations_fields/edit/{$location->id}")}}">
                            <i class="text-info far fa-edit"></i>
                        </a> 
                        <form method="post" action="{{ url("admin/category_management/locations_fields/delete/{$location->id}") }}">
                            {{ csrf_field() }}
                            <button style='background:none; border:none' type='submit' title="delete" class="text-danger fas fa-trash-alt"></button>
                        </form>
                    </div>    
                </td>
            </tr>
            @endforeach
        </table>
        {{ $locations->links() }}
    </div>
@endsection









