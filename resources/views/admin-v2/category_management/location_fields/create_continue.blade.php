@extends('admin-v2.index')

@section('content')
<div id="category_add_continue" class="navigation-section-stabilizer">
    <div class="ml-4">
        @include('admin-v2.category_management.header')
    </div>

    <div class="container">
        <div class="main-admin-section col-xs-12">
            <div>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100"
                    aria-valuemin="0" aria-valuemax="100" style="width:100%">
                    2/2
                    </div>
                </div>
            </div>
            <div>
                <div style="font-size: 18px;font-weight: 600; margin-bottom: 10px; text-align:center">
                    Creating a new '<span style="text-transform:capitalize">{{ $type }}</span>' type location field:
                </div>
                @include('partials.notifications')
                <form class="form-horizontal" method="POST" action="{{ route('location_fields.store') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="shortName">Short name(unique)</label>
                        <input id="shortName" name="short_name" type="text" class="form-control" placeholder="Short name..." value="{{ old('short_name') }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input id="name" name="name" type="text" class="form-control" placeholder="Name..." value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        <label for="field_description">Field description</label>
                        <textarea class="form-control" name="description" cols="30" rows="10" placeholder="Field Description...">
                            {{ old('description') ?: '' }}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="is_admin">Is admin?</label>
                        <select name="is_admin" id="is_admin" class="form-control">
                            <option selected  value="{{ old('is_admin') ?: '' }}">{{ old('is_admin') == null ? 'Choose...' : (old('is_admin') ? 'Yes' : 'No' ) }}</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="show_on_creation">Show on creation?</label>
                        <select name="show_on_creation" id="show_on_creation" class="form-control">
                            <option selected  value="{{ old('show_on_creation') ?: '' }}">{{ old('show_on_creation') == null ? 'Choose...' : (old('show_on_creation') ? 'Yes' : 'No' ) }}</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="group_id">Group</label>
                        <select name="group_id" id="category_id" class="form-control">
                            <option selected  value="{{ old('group_id') ?: '' }}">{{ old('group_id') ?: 'Choose...' }}</option>
                            @foreach(\App\LocationFieldGroup::get() as $group)
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sort_order">Sort order</label>
                        <select name="sort_order" id="sort_order" class="form-control">
                            <option selected  value="{{ old('sort_order') ?: '' }}">{{ old('sort_order') ?: 'Choose...' }}</option>
                            <option value="ASC">ASC</option>
                            <option value="DESC">DESC</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="required">Is this field required?</label>
                        <select name="required" id="required" class="form-control">
                            <option selected  value="{{ old('required') ?: '' }}">{{ old('required') == null ? 'Choose...' : (old('required') ? 'Yes' : 'No' )  }}</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="unique">Should data be unique?</label>
                        <select name="unique" id="unique" class="form-control">
                            <option selected  value="{{ old('unique') ?: '' }}">{{ old('unique') == null ? 'Choose...' : (old('unique') ? 'Yes' : 'No' )  }}</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="locked">Is this field locked?</label>
                        <select name="locked" id="locked" class="form-control">
                            <option selected  value="{{ old('locked') ?: '' }}">{{ old('locked') == null ? 'Choose...' : (old('locked') ? 'Yes' : 'No' )  }}</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="visible">Whos is this page visible to?</label>
                        <select name="visible" id="visible" class="form-control">
                            <option selected  value="{{ old('visible') ?: '' }}">{{ old('visible') ?: 'Choose...' }}</option>
                            <option value="Everyone">Everyone</option>
                            <option value="User">User</option>
                            <option value="Admin">Admin</option>
                        </select>
                    </div>
                    <div class="d-none">
                        <input type="text" name="data_type" value="{{ $type }}">
                        <input type="text" name="map_form" value="{{ $type == 'map' ? '1' : '0' }}">
                    </div>

                    <div id='location_field_specific_details' class="form-group">
                        <span>
                            Specific Settings
                        </span>
                    </div>

                    {{-- Type Text --}}
                    @if($type == 'text')
                    <div class="form-group">
                        <label >Min value</label>
                        <input class="form-control" type="number" name='param_1' value="{{ old('param_1') }}">
                    </div>
                    <div class="form-group">
                        <label for="text_max_value">Max value</label>
                        <input class="form-control" type="number" name='param_2' value="{{ old('param_2') }}">
                    </div>
                    <div class="form-group">
                        <label for="text_min_value">Input type</label>
                        <select name="param_3" id="text_input_type" class="form-control">
                            <option selected  value="{{ old('param_3') ?: '' }}">{{ old('param_3') ?: 'Choose...' }}</option>
                            <option value="text">Text</option>
                            <option value="number">Number</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="default_data">Default value</label>
                        <input  class="form-control"  type="text" name="param_4" id="default_data" placeholder='Type in default value' value="{{ old('param_4') }}">
                    </div>
                    @endif

                    {{-- Type Text Area --}}
                    @if($type == 'text_area')
                    <div class="form-group">
                        <label for="text_area_min_value">Min value</label>
                        <input class="form-control" type="number" name='param_1' value="{{ old('param_1') }}">
                    </div>
                    <div class="form-group">
                        <label for="text_area_max_value">Max value</label>
                        <input class="form-control" type="number" name='param_2' value="{{ old('param_2') }}">
                    </div>
                    <div class="form-group">
                        <label for="text_area_row_num">Row number</label>
                        <input class="form-control" type="number" name='param_3' value="{{ old('param_3') }}">
                    </div>
                    <div class="form-group">
                        <label for="text_area_col_num">Column number</label>
                        <input class="form-control" type="number" name='param_4' value="{{ old('param_4') }}">
                    </div>
                    @endif

                    {{-- Type Date --}}
                    @if($type == 'date')
                    <div class="form-group">
                        <label for="date_min_value">Min value</label>
                        <input class="form-control" type="date" name='param_1' value="{{ old('param_1') }}">
                    </div>
                    <div class="form-group">
                        <label for="date_max_value">Max value</label>
                        <input class="form-control" type="date" name='param_2' value="{{ old('param_2') }}">
                    </div>
                    <div class="form-group">
                        <label for="date_type">Date type</label>
                         <select name="param_3" id="date_type" class="form-control">
                            <option selected  value="{{ old('param_3') ?: '' }}">{{ old('param_3') ?: 'Choose...' }}</option>
                            <option value="YYYY">YYYY</option>
                            <option value="YYYY/MM">YYYY/MM</option>
                            <option value="DD/MM/YYY">DD/MM/YYY</option>
                            <option value="DD/MM/YYY_H:m">DD/MM/YYY H:m</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="default_value">Default value</label>
                        <input class="form-control"  type="text" name="param_4" id="default_value" placeholder='Type in default value' value="{{ old('param_4') }}">
                    </div>
                    @endif

                    {{-- Type Select --}}
                    @if($type == 'select')
                    <div class="form-group">
                        <label for="menu_options">Menu options (seperate with space)</label>
                        <textarea class="form-control" name="param_1" id="menu_options" cols="30" rows="10">
                            {{ old('param_1') }}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="select_type">Select type</label>
                        <select name="param_2" id="select_type" class="form-control">
                            <option selected  value="{{ old('param_2') ?: '' }}">{{ old('param_2') ?: 'Choose...' }}</option>
                            <option value="single_select">Single select</option>
                            <option value="multi_select">Multi select</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="default_value">Default value</label>
                        <input class="form-control"  type="text" name="param_3" id="default_value" placeholder='Type in default value' value="{{ old('param_3') }}">
                    </div>
                    @endif
                    
                    <input class="btn btn-admin" type="submit" value="Save" style="margin-top: 30px;width: 100%; border-radius: 5px; padding: 10px">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection