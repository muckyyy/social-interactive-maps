@extends('admin-v2.index')

@section('content')
<div id="category_add_continue" class="navigation-section-stabilizer">
    <div class="ml-4">
        @include('admin-v2.category_management.header')
    </div>
    <div class="container">
        <div class="main-admin-section col-xs-12">
            <div>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50"
                    aria-valuemin="0" aria-valuemax="100" style="width:50%">
                    1/2
                    </div>
                </div>
            </div>
            <div>
                <div style="font-size: 16px;font-weight: 600; margin-bottom: 10px; text-align:center">
                    Add location field:
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" method="GET" action="{{ route('location_fields.create_continue') }}" enctype="multipart/form-data">

                    <div style="font-size: 16px;font-weight: 600; margin-bottom: 10px;">
                        Choose location field type
                    </div>
                    <select required class="form-control" name="type" id="typeselector" onchange="selectInput()">
                        <option value="">Field type...</option>
                        @foreach ( $field_types as $type )
                            <option value="{{ $type }}">{{ $type == 'text_area' ? 'text area' : $type }}</option>
                        @endforeach
                    </select>
                    
                    <input class="btn btn-admin" type="submit" value="Next" style="margin-top: 30px;width: 100%;">
                </form>
            </div>

        </div>
    </div>
</div>
@endsection