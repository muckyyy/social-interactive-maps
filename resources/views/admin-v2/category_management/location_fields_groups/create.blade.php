@extends('admin-v2.index')

@section('content')
 <div class="add_new_service map_settings" style="width:100%;">
    @include('admin-v2.category_management.header')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(session('success'))
       <div class="alert alert-success">
        <ul>
            <li>{!! session('success') !!}</li>
        </ul>
    </div>
    @endif                              
    <form method="post" action="{{ url('admin/category_management/location_fields_groups/create') }}" class="container">
        {{ csrf_field() }}
        <div class="title">
            <span>Create location field group</span>
        </div>
        <div class="form-group d-flex">
            <label>Name</label>
            <input name="name" class="form-control" type="text" required value='{{ old('full_name') }}'>
        </div>
        <div class="form-group d-flex">
            <label style="width:22%">Visible</label>
            <input name="visible" class="form-control" type="checkbox">
        </div>
        <div class="buttonHolder">
            <button type="submit" class="btn btn-admin btn-rounded">Create</a>
        </div>
    </form>
</div>
@endsection
