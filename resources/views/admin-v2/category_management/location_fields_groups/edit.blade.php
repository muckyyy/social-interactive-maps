@extends('admin-v2.index')

@section('content')
 <div class="add_new_service map_settings">
    @include('admin-v2.category_management.header')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(session('success'))
       <div class="alert alert-success">
        <ul>
            <li>{!! session('success') !!}</li>
        </ul>
    </div>
    @endif                              
    <form method="post" action="{{ url("admin/category_management/location_fields_groups/update/{$locationFieldGroup->id}") }}" class="container">
        {{ csrf_field() }}
        <div class="title">
            <span>Edit: {{ $locationFieldGroup->name }}</span>
        </div>
        <div class="form-group d-flex">
            <label>Name</label>
            <input name="name" class="form-control" type="text" required value='{{ $locationFieldGroup->name }}'>
        </div>
        <div class="form-group d-flex">
            <label style="width:22%">Visible</label>
            @if($locationFieldGroup->visible)
                <input name="visible" class="form-control" type="checkbox" checked>
            @else 
                <input name="visible" class="form-control" type="checkbox">
            @endif
        </div>
        <div class="buttonHolder">
            <a href="{{ url('admin/category_management/location_fields_groups') }}" class="btn btn-admin btn-rounded">Back</a>
            <button type="submit" class="btn btn-admin btn-rounded">Edit</button>
        </div>
    </form>
</div>
@endsection
