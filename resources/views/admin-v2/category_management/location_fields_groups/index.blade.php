@extends('admin-v2.index')

@section('content')
    <div id="category-management">
        @include('admin-v2.category_management.header')
        @include('partials.notifications')     
        <div class="addNewGroup d-flex justify-content-end">
            <a href="{{ url('admin/category_management/location_fields_groups/create') }}" class="btn btn-admin rounded">Add new group</a>
        </div>
        <table id="category_location_groups">
            <tr>
                <th>Group</th>
                <th>Number of fields</th>
                <th>Actions</th>
            </tr>
            @foreach($locationFieldGroups as $group)
            <tr class='even'>
                <td>{{ $group->name }}</td>
                <td>
                    {{ $group->noOfFields ?: '/' }}
                </td>
                <td class="actions d-flex justify-content-center">
                    <div class="d-flex">
                        <a color='#35baa0' href="{{ url("admin/category_management/location_fields_groups/edit/{$group->id}")}}">
                            <i class="text-info far fa-edit"></i>
                        </a> 
                        @if($group->visible)
                            <form method="post" action="{{ url("admin/category_management/location_fields_groups/change_visibility/{$group->id}") }}">
                                {{ csrf_field() }}
                                <input name='visible' class="d-none" type="text" value='0'>
                                <button style='background:none; border:none' type="submit"  title="toggle to visible" class="far fa-eye"></button>
                            </form>
                        @else 
                            <form method="post" action="{{ url("admin/category_management/location_fields_groups/change_visibility/{$group->id}") }}">
                                {{ csrf_field() }}
                                <input name='visible' class="d-none" type="text" value='1'>
                                <button style='background:none; border:none' type="submit"  title="toggle to invisible" class="far fa-eye-slash"></button>
                            </form>
                        @endif    
                        <form method="post" action="{{ url("admin/category_management/location_fields_groups/delete/{$group->id}") }}">
                            {{ csrf_field() }}
                            <button style='background:none; border:none' type='submit' title="delete" class="text-danger fas fa-trash-alt"></button>
                        </form>
                        <div class="ordering">
                            @if($group->ordering > $minOrder)
                                <a style="cursor:pointer" onclick="document.getElementById('reorderInput' + {{ $group->id }}).value = 'up';document.getElementById('reorder' + {{ $group->id }}).submit()">
                                    <i class="fas fa-arrow-up"></i>
                                </a>
                            @else 
                                {{-- just a placeholder to keep content aligned nicely --}}
                                <a class="invisible fas fa-arrow-up"></a>    
                            @endif
                            @if($group->ordering < $maxOrder)
                                <a style="cursor:pointer" onclick="document.getElementById('reorderInput' + {{ $group->id }}).value = 'down';document.getElementById('reorder' + {{ $group->id }}).submit()">
                                    <i class="fas fa-arrow-down"></i>
                                </a>
                            @else 
                                {{-- just a placeholder to keep content aligned nicely --}}
                                <a class="invisible fas fa-arrow-down"></a>    
                            @endif
                            <form method="post" action="{{ url("admin/category_management/location_fields_groups/reorder/{$group->id}") }}" id="{{ 'reorder' . $group->id }}" class="d-none">
                                {{ csrf_field() }}
                                <input id='{{ 'reorderInput' . $group->id }}' name="reorder" type="text" value="">
                            </form>
                        </div>
                    </div>    
                </td>
            </tr>
            @endforeach
        </table>
        {{ $locationFieldGroups->links() }}
    </div>
@endsection











