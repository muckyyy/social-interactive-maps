@extends('admin-v2.index')

@section('content')
    <div id="category-management" style="width:100%;">
        @include('admin-v2.category_management.header')
        @include('partials.notifications')
        <table class="map_settings" id="category-management_table">
            <tr>
                <th></th>
                <th>Name</th>
                <th>Path</th>
                <th>Actions</th>
            </tr>
            @foreach($categories as $category)
            <tr>
                <td>{!! $category->icon !!}</td>
                <td>{{ $category->name }}</td>
                @php
                    $parentCategory = App\Category::where('id', $category->parent_id)->first();
                    if($parentCategory) {
                        $parentCategoryName = explode(' ', $parentCategory['name'] );
                        $parentCategoryName = $parentCategoryName[0];
                        $parentCategoryName = str_replace(',', '', $parentCategoryName);
                    }

                    $childCategory = explode(' ', $category->name);
                    $childCategory = $childCategory[0];
                    $childCategory = str_replace(',', '', $childCategory);
                @endphp
                <td>
                    @if(!$category->parent_id == 0)
                        <span class="imgHolder">
                            {!! $parentCategory->icon !!}
                        </span> 
                        {{ $parentCategoryName }}
                        /
                        <span class="imgHolder">
                            {!! $category->icon !!}
                        </span>
                        {{ $childCategory }}
                    @else 
                        /    
                    @endif
                </td>
                <td class="actions">  
                    <form class="form-horizontal" method="POST" action="{{ url("delete-category/$category->id") }}">
                        {{ csrf_field() }}
                        <a style='color: #35baa0'  title="edit" class="far fa-edit" href="/admin/category_management/admin-edit-category/{{$category->id}}"></a>
                        @if($category->active == 0)
                            <a class="inactive" title="inactive" href="{{ url("admin-active-category/$category->id/1") }}">
                                <i class="text-danger far fa-times-circle"></i>
                            </a>
                        @else
                            <a class="active" title="active" href="{{ url("admin-active-category/$category->id/0") }}">
                                <i class="text-info far fa-check-circle"></i>
                            </a>
                        @endif
                        <button style='border:none; background:none; padding: 0' type="submit" class="text-danger far fa-trash-alt"></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
         {{ $categories->links() }}
        <div class="button-holder">
            <a href="{{ url('admin/category_management/category_add') }}" class="btn btn-admin">
                Add a new category
            </a>
        </div>  
    </div>
@endsection







