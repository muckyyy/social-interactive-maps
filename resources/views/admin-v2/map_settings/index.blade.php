@extends('admin-v2.index')

@section('content')
    <div class="map_settings">
        {{-- Lower Layer --}}
        @include('admin-v2.map_settings.partials.header')
        @include('partials.notifications')
        <table id="map_setting_table">
            <tr>
                <th>Layer name</th>
                <th>Description</th>
                <th>Active</th>
                <th>Actions</th>
            </tr>
            @foreach($map_layers as $layer)
            <tr class='even'>
                <td>{{ $layer->display_name }}</td>
                <td>
                   {{ $layer->description }}
                </td>
                <td>
                    @if($layer->active)
                    <span class='fa fa-check text-info' ></span>
                    @else
                    <span class='fa fa-times text-danger' ></span>
                    @endif
                </td>
                <td class="d-flex justify-content-center">
                    @if($layer->layer_type == 'landing_map')
                    <a href="{{ url("/admin/map_settings/$layer->short_name") }}">
                        <i title="edit" class="fas fa-edit"></i>
                    </a>
                    @else 
                     <a href="{{ url("/admin/map_settings/guest_map_layers/$layer->short_name") }}">
                        <i title="edit" class="fas fa-edit"></i>
                    </a>
                    @endif
                    <form method="post" action="{{ url('admin/map_settings/toggle_active/' . $layer->id) }}">
                        {{ csrf_field() }}
                        @if($layer->active)
                            <button title="toggle off" class="fas fa-toggle-on text-success" style="border:none; background: none"></button>
                        @else
                            <button title="toggle on" class="fas fa-toggle-off text-danger" style="border:none; background: none"></button>
                        @endif
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
@endsection
