@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
<div class="map_settings" id="locations_layer">
    @include('admin-v2.map_settings.partials.header')
    
    <!-- Settings Layer -->
    <div id='bottom-layer'>
        <div class="main-title">
            Locations Layer
        </div>
            <div class='d-flex'>
                <div class="title">
                    <span>
                        Active Zoom range
                    </span>
                </div>
                <div class='content sliders'>
                    <div id="locationsSlider"></div>
                </div>
            </div>
            <div class='d-flex'>
                <div class="title">
                    <span>
                        Location base reach radius
                    </span>
                </div>
                <div class='content sliders'>
                    <div id="locationBaseReach"></div>
                </div>
            </div>
            <div class="d-flex">
                <div class="title">
                    Number of quadrants on screen
                </div>
                <div class="content dropdowns">
                    <div class='form-group'>
                        <select name="quadrantNumber" class='form-control'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="d-flex">
                <div class="title">
                    Number of icons per quadrant
                </div>
                <div class="content dropdowns">
                    <div class='form-group'>
                        <select name="quadrantIconNumber" class='form-control'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="d-flex">
                <div class="title">
                    Number of cashed pages
                </div>
                <div class="content dropdowns">
                    <div class='form-group'>
                        <select name="cashedPagesNumber" class='form-control'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="d-flex">
                <div class="title">
                    Base icon size, anchor
                </div>
                <div class="content dropdowns">
                    <div class='form-group'>
                        <select name="baseIconSize" class='form-control'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="d-flex">
                <div class="title">
                    Data request delay(in seconds)
                </div>
                <div class="content dropdowns">
                    <div class='form-group'>
                        <select name="dataRequestDelay" class='form-control'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
// Locations Layer
$( function() {
    $( "#locationsSlider" ).slider({
      range: true,
      min: 0,
      max: 18,
      values: [ 2, 16 ],
      slide: function( event, ui ) {
        let sliders = document.getElementsByClassName('ui-slider-handle');
        let sliderMin = sliders[0];
        let sliderMax = sliders[1];
        sliderMin.innerHTML = ui.values[ 0 ];
        sliderMax.innerHTML = ui.values[ 1 ];
      }
    });
    let sliders = document.getElementsByClassName('ui-slider-handle');
    let sliderMin = sliders[0];
    let sliderMax = sliders[1];
    sliderMin.innerHTML = 2;
    sliderMax.innerHTML = 16;
  });

// Maxi Icons Slider
$( function() {
    $( "#locationBaseReach" ).slider({
    range: "max",
    min: 500,
    max: 5000,
    value: 1000,
    slide: function( event, ui ) {
        $('#locationBaseReach .ui-slider-handle').text(ui.value);
    }
    });
    $( "#locationBaseReach .ui-slider-handle" ).text( $( "#locationBaseReach" ).slider( "value" ) );
});
</script>
