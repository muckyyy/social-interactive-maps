@extends('admin-v2.index')

@section('content')
    <div class="map_settings">
        {{-- Lower Layer --}}
        @include('admin-v2.map_settings.partials.header')
        <table id="map_setting_table">
            <tr>
                <th>Layer name</th>
                <th>Description</th>
                <th>Active</th>
                <th>Actions</th>
            </tr>
            @foreach($map_layers as $layer)
            <tr class='even'>
                <td>{{ $layer->display_name }}</td>
                <td>
                   {{ $layer->description }}
                </td>
                <td>
                    @if($layer->active)
                    <span class='fa fa-check text-info' ></span>
                    @else 
                    <span class='fa fa-times text-danger' ></span>
                    @endif
                </td>
                <td>
                   <a href="{{ url("/admin/map_settings/$layer->short_name") }}">
                        <i title="edit" class="fas fa-edit"></i>
                    </a> 
                    <!-- TODO -->
                   <!-- <a href='/'>
                        <i title="move up" class="fas fa-arrow-alt-circle-up"></i>
                    </a>
                   <a href='/'>
                        <i title="move down" class="fas fa-arrow-alt-circle-down"></i>
                    </a> -->
                </td>
            </tr>
            @endforeach
        </table>    
@endsection        
        