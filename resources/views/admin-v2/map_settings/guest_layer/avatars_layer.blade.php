@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
<div class="map_settings">
    @include('admin-v2.map_settings.partials.header')
    
    <!-- Settings Layer -->
    <div id='bottom-layer'>
        <div class="main-title">
            Avatars Layer
        </div>
            <div class='d-flex'>
                <div class="title">
                    <span>
                        Active Zoom range
                    </span>
                </div>
                <div class='content sliders'>
                    <div id="otherAvatarsLayerSlider"></div>
                </div>
            </div>
            <div class="d-flex">
                <div class="title">
                    Avatar icon size
                </div>
                <div class="content dropdowns">
                    <div class='form-group'>
                        <span class='coordinates'>x</span>
                        <select name="anchorIconSize" class='form-control'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <div class='form-group'>
                        <span class='coordinates'>y</span>
                        <select class='form-control'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="d-flex">
                <div class="title">
                    Avatar anchor position
                </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <span class='coordinates'>x</span>
                    <select name="anchorPosition" class='form-control'>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </div>
                <div class='form-group'>
                    <span class='coordinates'>y</span>
                    <select class='form-control'>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </div>
            </div>
            </div>
            <div class="d-flex">
                <div class="title">
                    Display duration
                </div>
                <div class="content dropdowns">
                    <div class='form-group'>
                        <select name="displayDuration" class='form-control'>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class='d-flex'>
                <div class="title">
                    <span>
                        Max number of icons per page
                    </span>
                </div>
                <div class='content sliders'>
                    <div id="maxIconsSlider"></div>
                </div>
            </div>
            <div class='d-flex'>
                <div class="title">
                    <span>
                        Number of cached pages
                    </span>
                </div>
                <div class='content sliders'>
                    <div id="cachedPagesNoSlider"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
// Other Layer Slider
$( function() {
    $( "#otherAvatarsLayerSlider" ).slider({
      range: true,
      min: 0,
      max: 18,
      values: [ 2, 16 ],
      slide: function( event, ui ) {
        let sliders = document.getElementsByClassName('ui-slider-handle');
        let sliderMin = sliders[0];
        let sliderMax = sliders[1];
        sliderMin.innerHTML = ui.values[ 0 ];
        sliderMax.innerHTML = ui.values[ 1 ];
      }
    });
    let sliders = document.getElementsByClassName('ui-slider-handle');
    let sliderMin = sliders[0];
    let sliderMax = sliders[1];
    sliderMin.innerHTML = 2;
    sliderMax.innerHTML = 16;
  });

// Maxi Icons Slider
$( function() {
    $( "#maxIconsSlider" ).slider({
    range: "max",
    min: 0,
    max: 500,
    value: 100,
    slide: function( event, ui ) {
        $('#maxIconsSlider .ui-slider-handle').text(ui.value);
    }
    });
    $( "#maxIconsSlider .ui-slider-handle" ).text( $( "#maxIconsSlider" ).slider( "value" ) );
});
// Cached Pages Slider
$( function() {
    $( "#cachedPagesNoSlider" ).slider({
    range: "max",
    min: 0,
    max: 10,
    value: 2,
    slide: function( event, ui ) {
        $('#cachedPagesNoSlider .ui-slider-handle').text(ui.value);
    }
    });
    $( "#cachedPagesNoSlider .ui-slider-handle" ).text( $( "#cachedPagesNoSlider" ).slider( "value" ) );
});
</script>
