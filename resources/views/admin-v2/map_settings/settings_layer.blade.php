@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
    <div class="map_settings">
        @include('admin-v2.map_settings.partials.header')
        @include('partials.notifications')

        <!-- Settings Layer -->
        <form id='bottom-layer' method='post' action="{{ route('map_settings.settings_layer.store') }}">
            {{ csrf_field() }}
            <div class="main-title">
                Settings Layer
            </div>
                <div class='d-flex'>
                    <div class="title">
                        <span>
                            Short Name
                    </div>
                    <div class='form-group'>
                        <input type="text" name='short_name' class="form-control rounded" value="{{ $layer->short_name }}">
                    </div>
                </div>
                <div class='d-flex'>
                    <div class="title">
                        <span>
                            Display Name
                    </div>
                    <div class='form-group'>
                        <input type="text" name='display_name' class="form-control rounded" value="{{ $layer->display_name }}">
                    </div>
                </div>
                <div class='d-flex'>
                    <div class="title">
                        <span>
                            Description
                    </div>
                    <div class='form-group'>
                        <input type="text" name='description' class="form-control rounded" value="{{ $layer->description }}">
                    </div>
                </div>
                <div class='d-flex'>
                    <div class="title">
                        <span>
                            Base privacy radius in meters(min value 500, max value 10 000)
                        </span>
                    </div>
                    <div class='content sliders'>
                        <div id="lowerLayerSlider"></div>
                        <input id="privacy_radius" type="number" class='d-none' name='privacy_radius' value="{{ $layer->param13 }}">
                    </div>
                </div>
                <div class='d-flex'>
                    <div class="title">
                        <span>Base reach radius in meters(min value 500, max value 10 000)</span>
                    </div>
                    <div class='content sliders'>
                        <div id="upperLayerSlider"></div>
                        <input id="reach_radius" type="number" class='d-none' name='reach_radius' value="{{ $layer->param14 }}">
                    </div>
                </div>
                <div class='d-flex'>
                    <div class="title">
                        <span>
                            Initial Zoom
                        </span>
                    </div>
                    <div class='content sliders'>
                        <div id="initialZoom"></div>
                        <input id="initial_zoom" type="number" class='d-none' name="initial_zoom" value="{{ $layer->param3 }}">
                    </div>
                </div>
                <div class="d-flex">
                    <div class="title">
                        Avatar icon size
                    </div>
                    <div class="content dropdowns">
                        <div class='form-group'>
                            <span class='coordinates'>x</span>
                            <input name="icon_size_x" class='form-control rounded' value="{{ $layer->param1 }}">
                        </div>
                        <div class='form-group'>
                            <span class='coordinates'>y</span>
                            <input name="icon_size_y" class='form-control rounded' value="{{ $layer->param2 }}">
                        </div>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="title">
                        Avatar anchor position
                    </div>
                <div class="content dropdowns">
                    <div class='form-group'>
                        <span class='coordinates'>x</span>
                        <input name="icon_ancor_x" class='form-control rounded' value="{{ $layer->param15 }}">
                    </div>
                    <div class='form-group'>
                        <span class='coordinates'>y</span>
                        <input name="icon_ancor_y" class='form-control rounded' value='{{ $layer->param16 }}'>
                    </div>
                </div>
                </div>
                <div class="d-flex">
                    <div class="title">
                        Avatar refresh position(seconds)
                    </div>
                    <div class="content dropdowns">
                        <div class='form-group'>
                            <input name="avatar_refresh_position" class='form-control rounded' value='{{ $layer->param17 }}'>
                        </div>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="title">
                        Inactivity Time
                    </div>
                    <div class="content dropdowns">
                        <div class='form-group'>
                            <input name="inactivity_time" class='form-control rounded' value='{{ $layer->param23 }}'>
                        </div>
                    </div>
                </div>
                <div class='text-center'>
                    <button type="submit" class='btn btn-info btn-rounded'>Save</button>
                </div>
            </form>
        </div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    // Lower Layer
    $( function() {
        $( "#lowerLayerSlider" ).slider({
        range: "max",
        min: 500,
        max: 10000,
        value: document.getElementById('privacy_radius').value,
        slide: function( event, ui ) {
            $('#lowerLayerSlider .ui-slider-handle').text(ui.value);
            $('#privacy_radius').val(ui.value);
        }
        });
        $( "#lowerLayerSlider .ui-slider-handle" ).text( $( "#lowerLayerSlider" ).slider( "value" ) );
    });
    // Upper Layer
    $( function() {
        $( "#upperLayerSlider" ).slider({
        range: "max",
        min: 500,
        max: 10000,
        value: document.getElementById('reach_radius').value,
        slide: function( event, ui ) {
            $('#upperLayerSlider .ui-slider-handle').text(ui.value);
            $('#reach_radius').val(ui.value);
        }
        });
        $( "#upperLayerSlider .ui-slider-handle" ).text( $( "#upperLayerSlider" ).slider( "value" ) );
    });

    $( function() {
        $( "#initialZoom" ).slider({
        range: "max",
        min: 1,
        max: 18,
        value: document.getElementById('initial_zoom').value,
        slide: function( event, ui ) {
            $('#initialZoom .ui-slider-handle').text(ui.value);
            document.getElementById('initial_zoom').value = ui.value;
        }
        });
        $( "#initialZoom .ui-slider-handle" ).text( $( "#initialZoom" ).slider( "value" ) );
    });
</script>
