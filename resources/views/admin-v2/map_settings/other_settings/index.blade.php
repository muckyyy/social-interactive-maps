@extends('admin-v2.index')

@section('content')
<div class="map_settings">
    @include('admin-v2.map_settings.partials.header')
    <div id='bottom-layer'>
        <div class="d-flex">
            <div class="title">
                Starting zoom registered
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <select name="startingZoomRegistered" class='form-control'>
                        <option selected value="">Select [0-18]</option>
                        @php
                            for ($x = 0; $x <= 18; $x++) {
                               echo "<option value='$x'>$x</option>";
                            }  
                        @endphp
                    </select>
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Starting zoom guest(if not specified)
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <select name="startingZoomGuest" class='form-control'>
                        <option selected value="">Select [0-18]</option>
                        @php
                            for ($x = 0; $x <= 18; $x++) {
                                echo "<option value='$x'>$x</option>";
                            }  
                        @endphp
                    </select>
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Wheel pixel per zoom level
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                   <input class="form-control" type="number" min="10" max="150" step="1" placeholder="Zoom level..." >
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Zoom delta
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                   <input class="form-control" type="number"  min="0.00" max="1.00" step="0.01" placeholder="Zoom delta...">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection        
        