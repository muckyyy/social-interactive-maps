<div class="admin_header d-flex">
    <div>
        @if(strpos(url()->current(), 'other_settings') || strpos(url()->current(), 'guest_map_layers'))
            <a href="{{ url('/admin/map_settings') }}" class="map_settings_table_header">
        @else 
            <a href="{{ url('/admin/map_settings') }}" class="active map_settings_table_header">
        @endif

        Registered Users Map Layers
        </a>
    </div>
    <div>
        <a href="{{ url('/admin/map_settings/guest_map_layers') }}" class="{{ strpos(url()->current(), 'guest_map_layers') ? 'active' : '' }} map_settings_table_header">
            Guest user map layers
        </a>
    </div>
    <div>
        <a href="{{ url('/admin/map_settings/other_settings') }}" class="{{ strpos(url()->current(), 'other_settings') ? 'active' : '' }} map_settings_table_header">
            Other settings
        </a>
    </div>
</div>

