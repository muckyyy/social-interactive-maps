@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
<div class="map_settings" id="locations_layer">
    @include('admin-v2.map_settings.partials.header')
    @include('partials.notifications')

    <!-- Settings Layer -->
    <form id='bottom-layer' method='post' action="{{ route('map_settings.beacon_layer.store') }}">
        {{ csrf_field() }}
        <div class="main-title">
            Beacon Layer
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Short Name
                </span>
            </div>
            <div class='form-group'>
                <input type="text" name='short_name' class="form-control rounded" value="{{ $layer->short_name }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Display Name
                </span>
            </div>
            <div class='form-group'>
                <input type="text" name='display_name' class="form-control rounded" value="{{ $layer->display_name }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Description
                </span>
            </div>
            <div class='form-group'>
                <input type="text" name='description' class="form-control rounded" value="{{ $layer->description }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Active Zoom range
                </span>
            </div>
            <div class='content sliders'>
                <div id="beaconSlider"></div>
                <input id="activeZoomMin" type="text" name="zoom_range_min" class="d-none" value="{{ $layer->param4 }}">
                <input id="activeZoomMax" type="text" name="zoom_range_max" class="d-none" value="{{ $layer->param5 }}">
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Max number of icons
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <input name="max_icons" class='form-control rounded' value="{{ $layer->param6 }}">
                </div>
            </div>
        </div>
        <div class='text-center'>
            <button type="submit" class='btn btn-info btn-rounded'>Save</button>
        </div>
    </form>
</div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
$( function() {
    let minZoom = document.getElementById('activeZoomMin').value;
    let maxZoom = document.getElementById('activeZoomMax').value;
    $( "#beaconSlider" ).slider({
        range: true,
        min: 0,
        max: 18,
        values: [ minZoom, maxZoom ],
        slide: function( event, ui ) {
            console.log(ui.values[0], ui.values[1])
            $("#beaconSlider .ui-slider-handle" )[0].innerText = ui.values[0];
            document.getElementById('activeZoomMin').value = ui.values[0];
            $("#beaconSlider .ui-slider-handle" )[1].innerText = ui.values[1];
            document.getElementById('activeZoomMax').value = ui.values[1];
            console.log(document.getElementById('activeZoomMax').value);
        }
    });
    $("#beaconSlider .ui-slider-handle" )[0].innerText = $("#beaconSlider" ).slider( "values", 0 );
    $("#beaconSlider .ui-slider-handle" )[1].innerText = $("#beaconSlider" ).slider( "values", 1 );
});
</script>
