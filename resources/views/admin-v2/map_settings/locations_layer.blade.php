@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
<div class="map_settings" id="locations_layer">
    @include('admin-v2.map_settings.partials.header')
    @include('partials.notifications')

    <!-- Settings Layer -->
    <form id='bottom-layer' method='post' action="{{ route('map_settings.locations_layer.store') }}">
        {{ csrf_field() }}
        <div class="main-title">
            Locations Layer
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Short Name
                </span>
            </div>
            <div class='form-group'>
                <input type="text" name='short_name' class="form-control rounded" value="{{ $layer->short_name }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Display Name
                </span>
            </div>
            <div class='form-group'>
                <input type="text" name='display_name' class="form-control rounded" value="{{ $layer->display_name }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Description
                </span>
            </div>
            <div class='form-group'>
                <input type="text" name='description' class="form-control rounded" value="{{ $layer->description }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Active Zoom range
                </span>
            </div>
            <div class='content sliders'>
                <div id="locationsSlider"></div>
                <input id="activeZoomMin" type="text" name="active_zoom_min" class="d-none" value="{{ $layer->param4  }}">
                <input id="activeZoomMax" type="text" name="active_zoom_max" class="d-none" value="{{ $layer->param5  }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Location base reach radius
                </span>
            </div>
            <div class='content sliders'>
                <div id="locationBaseReach"></div>
                <input id="reachRadius" type="text" name="reach_radius" class="d-none" value="{{ $layer->param14  }}">
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Number of quadrants on screen
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <select class='form-control rounded' name="number_of_quadrants">
                        <option disabled value="">{{ $layer->param20 }}</option>
                        <option value="1x1">1x1</option>
                        <option value="1x2">1x2</option>
                        <option value="1x3">1x3</option>
                        <option value="1x4">1x4</option>
                        <option value="1x5">1x5</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Number of icons per quadrant
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <input name="icons_per_quadrant" class='form-control rounded' value="{{ $layer->param21 }}">
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Number of cashed pages
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <input name="cached_pages" class='form-control rounded' value="{{ $layer->param7 }}">
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Base icon size
            </div>
            <div class="content dropdowns flex">
                <div class='form-group'>
                    <input name="icon_size_x"  class='form-control rounded' value="{{ $layer->param1 }}">
                </div>
                <div class='form-group'>
                    <input name="icon_size_y"  class='form-control rounded' value="{{ $layer->param2 }}">
                </div>
            </div>
        </div>

        <div class="d-flex">
            <div class="title">
                Base icon anchor size
            </div>
            <div class="content dropdowns flex">
                <div class='form-group'>
                    <input name="icon_ancor_x"  class='form-control rounded' value="{{ $layer->param15 }}">
                </div>
                <div class='form-group'>
                    <input name="icon_ancor_y"  class='form-control rounded' value="{{ $layer->param16 }}">
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Data request delay(in seconds)
            </div>
            <div class="content dropdowns">

                <div class='form-group'>
                    <input name="data_request_delay" class='form-control rounded' value="{{ $layer->param22 }}">
                </div>
            </div>
        </div>
        <div class='text-center'>
                <button type="submit" class='btn btn-info btn-rounded'>Save</button>
            </div>
        </form>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $( function() {
        let minZoom = document.getElementById('activeZoomMin').value;
        let maxZoom = document.getElementById('activeZoomMax').value;
        $( "#locationsSlider" ).slider({
            range: true,
            min: 0,
            max: 18,
            values: [ minZoom, maxZoom ],
            slide: function( event, ui ) {
                console.log(ui.values[0], ui.values[1])
                $("#locationsSlider .ui-slider-handle" )[0].innerText = ui.values[0];
                document.getElementById('activeZoomMin').value = ui.values[0];
                $("#locationsSlider .ui-slider-handle" )[1].innerText = ui.values[1];
                document.getElementById('activeZoomMax').value = ui.values[1];
                console.log(document.getElementById('activeZoomMax').value);
            }
        });
        $("#locationsSlider .ui-slider-handle" )[0].innerText = $("#locationsSlider" ).slider( "values", 0 );
        $("#locationsSlider .ui-slider-handle" )[1].innerText = $("#locationsSlider" ).slider( "values", 1 );
    });


// Maxi Icons Slider
    $( function() {
        $( "#locationBaseReach" ).slider({
            range: "max",
            min: 0,
            max: 500,
            value: document.getElementById('reachRadius').value,
            slide: function( event, ui ) {
                $('#locationBaseReach .ui-slider-handle').text(ui.value);
                document.getElementById('reachRadius').value = ui.value;
            }
        });
        $( "#locationBaseReach .ui-slider-handle" ).text( $( "#locationBaseReach" ).slider( "value" ) );
    });
</script>
