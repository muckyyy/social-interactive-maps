@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
<div class="map_settings">
    @include('admin-v2.map_settings.partials.header')
    @include('partials.notifications')

    <!-- Settings Layer -->
    <form id='bottom-layer' method='post' action="{{ route('map_settings.friends_layer.store') }}">
        {{ csrf_field() }}
        <div class="main-title">
            Friends Avatars Layer
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Short Name
            </div>
            <div class='form-group'>
                <input type="text" name='short_name' class="form-control rounded" value="{{ $layer->short_name }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Display Name
            </div>
            <div class='form-group'>
                <input type="text" name='display_name' class="form-control rounded" value="{{ $layer->display_name }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Description
            </div>
            <div class='form-group'>
                <input type="text" name='description' class="form-control rounded" value="{{ $layer->description }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Active Zoom range
                </span>
            </div>
            <div class='content sliders'>
                <div id="friendsLayerSlider"></div>
                <input id='activeZoomMin' type="number" name="active_zoom_range_min" class='d-none' value="{{ $layer->param4 }}">
                <input id='activeZoomMax' type="number" name="active_zoom_range_max" class='d-none' value="{{ $layer->param5 }}">
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Avatar icon size
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <span class='coordinates'>x</span>
                    <input name="icon_size_x" class='form-control rounded' value="{{ $layer->param1 }}">
                </div>
                <div class='form-group'>
                    <span class='coordinates'>y</span>
                    <input name="icon_size_y" class='form-control rounded' value="{{ $layer->param2 }}">
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Avatar anchor position
            </div>
        <div class="content dropdowns">
            <div class='form-group'>
                <span class='coordinates'>x</span>
                <input name="icon_ancor_x" class='form-control rounded' value="{{ $layer->param15 }}">
            </div>
            <div class='form-group'>
                <span class='coordinates'>y</span>
                <input name="icon_ancor_y" class='form-control rounded' value="{{ $layer->param16 }}">
            </div>
        </div>
        </div>
        <div class="d-flex">
            <div class="title">
                Display duration
            </div>
            <div class="content dropdowns">
                <div class='form-group'>
                    <input name="display_duration" class='form-control rounded' value="{{ $layer->param18 }}">
                </div>
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Max number of icons per page
                </span>
            </div>
            <div class='content sliders'>
                <div id="maxIconsSlider"></div>
                <input id="max_icons" type="number" class='d-none' name="max_icons" value="{{ $layer->param6 }}">
            </div>
        </div>
        <div class='d-flex'>
            <div class="title">
                <span>
                    Number of cached pages
                </span>
            </div>
            <div class='content sliders'>
                <div id="cachedPagesNoSlider"></div>
                <input id="cached_pages" type="number" class='d-none' name="cached_pages" value="{{ $layer->param7 }}">
            </div>
        </div>
        <div class='text-center'>
            <button type="submit" class='btn btn-info btn-rounded'>Save</button>
        </div>
    </form>
</div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
// Friends Layer
  $( function() {
    let minZoom = document.getElementById('activeZoomMin').value;
    let maxZoom = document.getElementById('activeZoomMax').value;
    $( "#friendsLayerSlider" ).slider({
      range: true,
      min: 0,
      max: 18,
      values: [ minZoom, maxZoom ],
      slide: function( event, ui ) {
          console.log(ui.values[0], ui.values[1])
          $("#friendsLayerSlider .ui-slider-handle" )[0].innerText = ui.values[0];
          document.getElementById('activeZoomMin').value = $("#friendsLayerSlider" ).slider( "values", 0 );
          document.getElementById('activeZoomMax').value = $("#friendsLayerSlider" ).slider( "values", 1 );
          $("#friendsLayerSlider .ui-slider-handle" )[1].innerText = ui.values[1];
      }
    });
    $("#friendsLayerSlider .ui-slider-handle" )[0].innerText = $("#friendsLayerSlider" ).slider( "values", 0 );
    $("#friendsLayerSlider .ui-slider-handle" )[1].innerText = $("#friendsLayerSlider" ).slider( "values", 1 );
  });

// Maxi Icons Slider
$( function() {
    $( "#maxIconsSlider" ).slider({
    range: "max",
    min: 0,
    max: 500,
    value: document.getElementById('max_icons').value,
    slide: function( event, ui ) {
        $('#maxIconsSlider .ui-slider-handle').text(ui.value);
        document.getElementById('max_icons').value = ui.value;
    }
    });
    $( "#maxIconsSlider .ui-slider-handle" ).text( $( "#maxIconsSlider" ).slider( "value" ) );
});
// Cached Pages Slider
$( function() {
    $( "#cachedPagesNoSlider" ).slider({
    range: "max",
    min: 0,
    max: 10,
    value: document.getElementById('cached_pages').value,
    slide: function( event, ui ) {
        $('#cachedPagesNoSlider .ui-slider-handle').text(ui.value);
        document.getElementById('cached_pages').value = ui.value;
    }
    });
    $( "#cachedPagesNoSlider .ui-slider-handle" ).text( $( "#cachedPagesNoSlider" ).slider( "value" ) );
});
</script>
