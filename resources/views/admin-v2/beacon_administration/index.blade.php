@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/> 
@endsection

@section('content')
    <div id="beacon_administration" style="width:100%;">
        @include('admin-v2.beacon_administration.partials.header')
        @include('partials.notifications')
        <form id="addNewBeacon" action="" method="">
            <a href="{{ route('beacon_administration.create') }}" class="btn btn-admin">Add New Beacon</a>
        </form>
        <table class="admin_table" id="beacon_administration_table">
            <tr>
                <th></th>
                <th>Beacon Name</th>
                <th>Active Users(last 15 min)</th>
                <th>Actions</th>
            </tr>
            @foreach($beacons as $beacon)
            <tr class='even'>
                <td>
                    <img style='max-height: 22px' src="{!! $beacon->icon  !!}">
                </td>
                <td>
                   {{ $beacon->name }}
                </td>
                <td>
                    15
                </td>
                <td class="actions d-flex justify-content-center">
                    <a href="{{ route('beacon_administration.edit', ['id' => $beacon->id]) }}">
                        <i title="settings" class="fas fa-cog"></i>
                    </a> 
                    <a href='#'>
                        <i title="view" class="far fa-eye"></i>
                    </a>
                    <form action="{{ route('beacon_administration.destroy', ['id' => $beacon->id]) }}" method='post'>
                        {{ csrf_field() }}
                        <button type='submit' title="delete" class="fas fa-trash-alt" style='background: none; border: none'></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table> 
        {{ $beacons->links() }}
    </div>
@endsection



