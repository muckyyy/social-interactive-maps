@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>

   <style>
       .mapPlaceholder {
            height: 50px;
            width: 300px;
            background: url(/img/beacons/map_bg.jpg);
            border-radius: 6px;
            filter: opacity(0.5);
            cursor: pointer;
        }
        .page-inner {
            height: 1062px !important;
        }
   </style>
@endsection

@section('content')
    <div id="beacon_administration" class='container-fluid'>
        @include('admin-v2.beacon_administration.partials.header')
        @include('partials.notifications')
        <div id="beacon_administration_public" class="row d-flex">
            <form class="col-6 container ml-0" style="text-align:left" id="addNewBeacon" action="{{ route('beacon_administration.store') }} " method="post" enctype="multipart/form-data" autocomplete="off">
                {{ csrf_field() }}
                <div class="form-group d-flex">
                    <span>Beacon Name</span>
                    <input style="width:300px" class="form-control" id="beacon_name" name="name" type="text" placeholder='Beacon Name...' value="{{ old('beacon_name') ?: '' }}">
                </div>
                <div class="form-group d-flex">
                    <span>Beacon Description</span>
                    <input style="width:300px" class="form-control" id="beacon_description" name="description" type="text" placeholder='Beacon Description...' value="{{ old('description') ?: '' }}">
                </div>
                <!-- city input, hidden -->
                <input id="cityid" type="text" name='city_id' class='d-none' value="{{ old('city_id') ? \App\City::find(old('city_id'))->id : '' }}">
                <div class="form-group d-flex">
                    <span>City</span>
                    <input autocomplete="disabled" id='findCities' class="form-control" placeholder='Start typing...' value="{{ old('city_id') ? \App\City::find(old('city_id'))->name : '' }}">
                </div>
                <div class="form-group d-flex">
                    <span>Location</span>
                    <div id='location' class='mapPlaceholder' onclick="showMap('location')"></div>
                    <input class='d-none' id='locationLat' name='locationLat' type="text">
                    <input class='d-none' id='locationLng' name='locationLng' type="text">
                </div>
                <div class="form-group d-flex">
                    <span>Display Location</span>
                    <div id='displayLocation' class='mapPlaceholder' onclick="showMap('displayLocation')"></div>
                    <input class='d-none' id='displayLocationLat' name='displayLocationLat' type="text">
                    <input class='d-none' id='displayLocationLng' name='displayLocationLng' type="text">
                </div>
                <div class="form-group d-flex sliders">
                    <span>Beacon Administrative Range</span>
                    <i class="sliderTitle">500</i>
                    <div id="upperLayerSlider" class="ui-slider-handle"></div>
                    <i class="sliderTitle">10 000</i>
                    <input id="beaconAdministrativeRange" name="administrative_range" type="text" class="d-none" value="{{ old('administrative_range') ?: 2000 }}">
                </div>
                <div class="form-group d-flex sliders">
                    <span>Beacon Reach Range </span>
                    <i class="sliderTitle">500</i>
                    <div id="lowerLayerSlider" class="ui-slider-handle"></div>            
                    <i class="sliderTitle">10 000</i>
                    <input id="beaconRange" name="beacon_range" type="text" class="d-none" value="{{ old('beacon_range') ?: 2000 }}">
                </div>
                <div class="form-group d-flex">
                    <span>Upload icon for beacon</span>
                    <input class="form-control" id="beacon_upload_icon" name="icon" type="file">
                </div>
                <div class="form-group d-flex">
                    <span>Beacon popup message</span>
                    <input class="form-control" id="beacon_popup_text" name="popup" type="text" placeholder='Message...' value="{{ old('popup') }}">
                </div>
                <div id="beacon_size" class="form-group d-flex">
                    <span>Beacon Size</span>
                    <i>x</i>
                    <input class="form-control" id="beacon_size_x" name="icon_size_x" type="number" value="{{ old('icon_size_x') }}">
                    <i>y</i>
                    <input class="form-control" id="beacon_size_y" name="icon_size_y" type="number" value="{{ old('icon_size_y') }}">
                </div>
                <div id="beacon_icon" class="form-group d-flex">
                    <span>Beacon Icon Ancor Point</span>
                    <i>x</i>
                    <input class="form-control" id="beacon_icon_x" name="icon_ancor_point_x" type="number" value="{{ old('icon_ancor_point_x') }}">
                    <i>y</i>
                    <input class="form-control" id="beacon_icon_y" name="icon_ancor_point_y" type="number" value="{{ old('icon_ancor_point_y') }}">
                </div>
              <div class="beacon_checkboxes mb-4">
                    <div>
                        @if(old('allow_image_upload') && old('allow_image_upload') == true )
                            <input name="allow_image_upload" type="checkbox" checked>
                        @else 
                            <input name="allow_image_upload" type="checkbox">
                        @endif
                        <label for="allow_image_upload">Allow image upload</label>
                    </div>
                    <div>
                        @if(old('enable_places') && old('enable_places') == true )
                         <input name="enable_places" type="checkbox" checked>
                        @else 
                            <input name="enable_places" type="checkbox">
                        @endif
                        <label for="enable_places">Enable places</label>
                    </div>
                    <div>
                        @if(old('enable_events') && old('enable_events') == true )
                            <input name="enable_events" type="checkbox" checked>
                        @else 
                            <input name="enable_events" type="checkbox">
                        @endif
                        <label for="enable_events">Enable events</label>
                    </div>
                    <div>
                        @if(old('enable_groups') && old('enable_groups') == true )
                            <input name="enable_groups" type="checkbox" checked>
                        @else
                            <input name="enable_groups" type="checkbox">
                        @endif
                        <label for="enable_groups">Enable groups</label>
                    </div>
                    <div>
                        @if(old('public') && old('public') == true )
                            <input name="public" type="checkbox" checked>
                        @else 
                            <input name="public" type="checkbox">
                        @endif
                        <label for="public">Public</label>
                    </div>
                </div>
                <div class='text-center'>
                    <form action="{{ route('beacon_administration.store') }}" method='post'>
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-admin rounded mt-4 py-2 px-4">Save</button>
                    </form>
                </div>
            </form>
            <div id='map_container'>
                @include('admin-v2.beacon_administration.partials.map')
                <div class='d-flex justify-content-center align-items-center mt-4' style='height: 50px'>
                    <span id='saveCoordinates' class='btn btn-submit mx-2 px-4 py-2'>
                        Save
                    </span>
                    <span id='closeMap' class='btn btn-danger mx-2 px-4 py-2' onclick='hideMap(1000)'>
                        Cancel
                    </span>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    let coordinate_location = false;
    let display_location = false;
    // save map coordinates
    window.onload = function () {
        document.getElementById('saveCoordinates').addEventListener('click', (e) => {
            let lat = map.getCenter().lat;
            let lng = map.getCenter().lng;
            if(coordinate_location) {
                let locationLat = document.getElementById('locationLat').value = lat;
                let locationLng = document.getElementById('locationLng').value = lng;
                console.log('LOCATION:::',locationLat, locationLng )
            }
            if(display_location) {
                let dispLocationLat = document.getElementById('displayLocationLat').value = lat;
                let dispLocationLng = document.getElementById('displayLocationLng').value = lng;
                console.log('DISP LOCATION:::', dispLocationLat, dispLocationLat )
            }
            hideMap(1000);
        })
    }

    function hideMap(t = 0) {
        coordinate_location = false;
        display_location = false;
        $('#map_container').hide(t);
    }

    $( document ).ready(function() {
        hideMap();
    });

    // open map
    function showMap(e) {
        if(e == 'location') {
            coordinate_location = true;
        }
        if( e == 'displayLocation') {
            display_location = true;
        }
        $('#map_container').fadeIn(1000);
    }

    // ---------------------------
    $( function() {
        $( "#lowerLayerSlider" ).slider({
        range: "max",
        min: 500,
        max: 10000,
        value: document.getElementById('beaconRange').value,
        slide: function( event, ui ) {
            $('#lowerLayerSlider .ui-slider-handle').text(ui.value);
            document.getElementById('beaconRange').value = ui.value;
        }
        });
        $( "#lowerLayerSlider .ui-slider-handle" ).text( $( "#lowerLayerSlider" ).slider( "value" ) );
    });

    $( function() {
        $( "#upperLayerSlider" ).slider({
        range: "max",
        min: 500,
        max: 10000,
        value: document.getElementById('beaconAdministrativeRange').value,
        slide: function( event, ui ) {
            $('#upperLayerSlider .ui-slider-handle').text(ui.value);
            document.getElementById('beaconAdministrativeRange').value = ui.value;
        }
        });
        $( "#upperLayerSlider .ui-slider-handle" ).text( $( "#upperLayerSlider" ).slider( "value" ) );
    });

    // FIND CITIES
    $( document ).ready(function() {
    $( "#findCities" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "/searchCities/" + $('#country').val() + '/' +request.term ,
          dataType: "json",
         
          success: function( data ) {
                response($.map(data, function(v,i){
                    return {
                        label: v.name + ', ' + v.country,
                        value: v.name,
                        cityid: v.id,  
                        };
                }));
            }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#findCities').val(ui.item.value);
        $('#cityid').val(ui.item.cityid);
        
        return false;
        
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
    
     if ($('#country').val() == 0){
        
        $('#city').autocomplete('disable');
        
    }
    $('#country').on('change',function(){
        if($(this).val()==0){
           $('#city').autocomplete('disable');
        }
        else{
            $('#city').autocomplete('enable');
        }
    });
});
</script>