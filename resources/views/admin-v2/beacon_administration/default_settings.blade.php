@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
    <div id="beacon_administration">
        @include('admin-v2.beacon_administration.partials.header')
        <div class="form-group d-flex">
            <span>Beacon Image</span>
            <input class="form-control" id="beacon_upload_icon" name="beacon_icon" type="file">
        </div>
        <div class="form-group d-flex sliders">
            <span>Beacon Administrative Range</span>
            <i class="sliderTitle">500</i>
            <div id="upperLayerSlider" class="ui-slider-handle"></div>
            <i class="sliderTitle">10 000</i>
        </div>
        <div class="form-group d-flex sliders">
            <span>Beacon avatar Range </span>
            <i class="sliderTitle">500</i>
            <div id="lowerLayerSlider" class="ui-slider-handle"></div>            
            <i class="sliderTitle">10 000</i>
        </div>
 
        <div id="beacon_size" class="form-group d-flex">
            <span>Beacon Size</span>
            <i>x</i>
            <input class="form-control" id="beacon_size_x" name="beacon_size_x" type="number">
            <i>y</i>
            <input class="form-control" id="beacon_size_y" name="beacon_size_y" type="number">
        </div>
        <div id="beacon_icon" class="form-group d-flex">
            <span>Beacon Anchor</span>
            <i>x</i>
            <input class="form-control" id="beacon_icon_x" name="beacon_size_x" type="number">
            <i>y</i>
            <input class="form-control" id="beacon_icon_y" name="beacon_size_y" type="number">
        </div>
        <div class="beacon_checkboxes">
            <div>
                <input name="allow_image_upload" type="checkbox">
                <label for="allow_image_upload">Allow image upload</label>
            </div>
            <div>
                <input name="enable_places" type="checkbox">
                <label for="enable_places">Enable places</label>
            </div>
            <div>
                <input name="enable_events" type="checkbox">
                <label for="enable_events">Enable events</label>
            </div>
        </div>
    </div>    
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    // ---------------------------
    $( function() {
        $( "#lowerLayerSlider" ).slider({
        range: "max",
        min: 500,
        max: 10000,
        value: 2000,
        slide: function( event, ui ) {
            $('#lowerLayerSlider .ui-slider-handle').text(ui.value);
        }
        });
        $( "#lowerLayerSlider .ui-slider-handle" ).text( $( "#lowerLayerSlider" ).slider( "value" ) );
    });

    $( function() {
        $( "#upperLayerSlider" ).slider({
        range: "max",
        min: 500,
        max: 10000,
        value: 2000,
        slide: function( event, ui ) {
            $('#upperLayerSlider .ui-slider-handle').text(ui.value);
        }
        });
        $( "#upperLayerSlider .ui-slider-handle" ).text( $( "#upperLayerSlider" ).slider( "value" ) );
    });
</script>
@endsection