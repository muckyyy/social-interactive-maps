<div class="admin_header d-flex">
    <div>
    <a href="{{ url('admin/beacon_administration') }}" class="{{ strpos( url()->current(), 'local_beacons') ? '' : (strpos( url()->current(), 'default_settings') ? '' : 'active') }}">
            Beacons
        </a>
    </div>
    <div>
        <a href="{{ url('admin/beacon_administration/default_settings') }}" class="{{ strpos( url()->current(), 'default_settings') ? 'active' : '' }}">
            Beacon default settings
        </a>
    </div>
</div>
