<div style='position: relative'>
	<div id="mapid"></div>
	<div class="d-flex justify-content-center align-items-center" id="coordinates">
		<span id="lat" class='mr-2'></span>
		<span id="lng" class='ml-4'></span>
	</div>
</div>

<script>
	// variables used in map
    let beaconReachRange;
	let reachRadius; 
	let beacon;
	let displayBeacon;
	let activeGelolocation = true;
	let activeDisplayLocation = false;

	function alterDisplay(e) {
		if(e.target.id == 'dispLocBtn') {
			activeGelolocation = false;
			activeDisplayLocation = true;
			$('#dispLocBtn').addClass('d-none');
			$('#geoLocBtn').removeClass('d-none');
			map.setView([displayLocation.coordinates[1], displayLocation.coordinates[0]]);
		}
		if(e.target.id == 'geoLocBtn') {
			activeGelolocation = true;
			activeDisplayLocation = false;
			$('#geoLocBtn').addClass('d-none');
			$('#dispLocBtn').removeClass('d-none');
			map.setView([geolocation.coordinates[1], geolocation.coordinates[0]]);
		}

	}

	let editPage = document.getElementById('editBeacon');
	if (editPage) {
		var map = L.map('mapid', { attributionControl: false }).setView([geolocation.coordinates[1], geolocation.coordinates[0]], 12);

		document.getElementById('lat').innerText = map.getCenter().lat.toFixed(4);
		document.getElementById('lng').innerText = map.getCenter().lng.toFixed(4);

		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map);

		var LeafIcon = L.Icon.extend({
			options: {
				iconSize:     [80, 81],
				iconAnchor:   [22, 94],
				popupAnchor:  [-3, -76]
			}
		});

		function changeRange(radius, e) {
			if(radius == 'reach') {
				reachRange = e;
				reachRadius.setRadius(e)
			}
			if(radius == 'administrative') {
				administrativeRange = e;
				beaconReachRange.setRadius(e)
			}
		}

		function changePopupMessage(e) {
			beaconPopup = e.target.value;
			beacon.bindPopup(beaconPopup).openPopup();
		}

		function changeIconSize(e, c) {
			let newIcon = beacon.options.icon;
			if(c == 'x') {
				newIcon.options.iconSize[0] = e.target.value;
			}
			if(c == 'y') {
				newIcon.options.iconSize[1] = e.target.value;
			}
			beacon.setIcon(newIcon);
		}

		function changeIconAncorPoint(e, c) {
			let newIcon = beacon.options.icon;
			if(c == 'x') {
				newIcon.options.iconAnchor[0] = e.target.value;
			}
			if(c == 'y') {
				newIcon.options.iconAnchor[1] = e.target.value;
			}
			beacon.setIcon(newIcon);
		}

		$('#beacon_upload_icon').change( function(event) {
			let file    = document.querySelector('input[type=file]').files[0];
			let reader  = new FileReader();
			map.removeLayer(beacon);

			reader.onloadend = function () {
				icon = new LeafIcon({iconUrl: reader.result, iconSize: [iconSizeX, iconSizeY], iconAnchor: [iconAncorX, iconAncorY],});
				beacon = L.marker([geolocation.coordinates[1], geolocation.coordinates[0]], {icon: icon}).addTo(map).bindPopup(beaconPopup);
			}

			if (file) {
				reader.readAsDataURL(file);
				icon = new LeafIcon({iconUrl: reader.result, iconSize: [iconSizeX, iconSizeY], iconAnchor: [iconAncorX, iconAncorY],});
				beacon = L.marker([geolocation.coordinates[1], geolocation.coordinates[0]], {icon: icon}).addTo(map).bindPopup(beaconPopup);
			} 
		});

		let img = document.getElementById('beaconImage').src; 
		
        let icon = new LeafIcon({iconUrl: img, iconSize: [iconSizeX, iconSizeY], iconAnchor: [iconAncorX, iconAncorY]});
		
        let displayIcon = new LeafIcon({iconUrl: img, iconSize: [iconSizeX, iconSizeY], iconAnchor: [iconAncorX, iconAncorY]});
		
        beacon = L.marker([geolocation.coordinates[1], geolocation.coordinates[0]], {icon: icon}).addTo(map).bindPopup(beaconPopup).openPopup();
		
        displayBeacon = L.marker([displayLocation.coordinates[1], displayLocation.coordinates[0]], {icon: icon, opacity: 0.6}).addTo(map);
		
        beaconReachRange = L.circle([geolocation.coordinates[1], geolocation.coordinates[0]], {radius: reachRange, color: 'rgba(51,136,255, 1)', opacity: 1}).addTo(map);
		
        reachRadius = L.circle([geolocation.coordinates[1], geolocation.coordinates[0]], {radius: administrativeRange, color: 'red', opacity: 1}).addTo(map);

		// sliders
		$( function() {
			$( "#lowerLayerSlider" ).slider({
				range: "max",
				min: 500,
				max: 10000,
				value: document.getElementById('beaconRange').value,
				slide: function( event, ui ) {
					$('#lowerLayerSlider .ui-slider-handle').text(ui.value);
					document.getElementById('beaconRange').value = ui.value;
					changeRange('reach', ui.value);
				}
			});
			$( "#lowerLayerSlider .ui-slider-handle" ).text( $( "#lowerLayerSlider" ).slider( "value" ) );
		});

		$( function() {
			$( "#upperLayerSlider" ).slider({
				range: "max",
				min: 500,
				max: 10000,
				value: document.getElementById('beaconAdministrativeRange').value,
				slide: function( event, ui ) {
					$('#upperLayerSlider .ui-slider-handle').text(ui.value);
					document.getElementById('beaconAdministrativeRange').value = ui.value;
					changeRange('administrative', ui.value);
				}
			});
			$( "#upperLayerSlider .ui-slider-handle" ).text( $( "#upperLayerSlider" ).slider( "value" ) );
		});

		// edit page map move
		map.addEventListener('move', function(ev) {
			let lat = map.getCenter().lat;
			let lng =  map.getCenter().lng;
			document.getElementById('lat').innerText = lat.toFixed(4);
			document.getElementById('lng').innerText = lng.toFixed(4);

			if(beacon && activeGelolocation) {
				$('#locationDisplay').text(lat.toFixed(12) + ' ' + lng.toFixed(12));	
				$('#locationLat').val(lat);	
				$('#locationLng').val(lng);	
				geolocation.coordinates[0] = lng;
				geolocation.coordinates[1] = lat;
				
				map.removeLayer(beacon);
				map.removeLayer(beaconReachRange);
				map.removeLayer(reachRadius);

				beacon = new L.marker([lat, lng], {icon: icon}).addTo(map).bindPopup(beaconPopup);
				beaconReachRange = L.circle([lat, lng], {radius: administrativeRange,  color: 'red', opacity: 1}).addTo(map);
				reachRadius = L.circle([lat, lng], {radius: reachRange, color: 'rgba(51,136,255, 1)', opacity: 1}).addTo(map);
			}

			if(beacon && activeDisplayLocation) {
				$('#displayLocationDisplay').text(lat.toFixed(12) + ' ' + lng.toFixed(12));	
				$('#displocationLat').val(lat);	
				$('#displocationLng').val(lng);	
				displayLocation.coordinates[0] = lng;
				displayLocation.coordinates[1] = lat;


				if(displayBeacon) {
					map.removeLayer(displayBeacon);
				}
				displayBeacon = new L.marker([lat, lng], {icon: displayIcon, opacity: 0.7}).addTo(map);
			}
		});
	}
	else {
		let geolocation = [8.67, 50.09]
		var map = L.map('mapid', { attributionControl: false }).setView([geolocation[1], geolocation[0]], 12);

		document.getElementById('lat').innerText = map.getCenter().lat.toFixed(4);
		document.getElementById('lng').innerText = map.getCenter().lng.toFixed(4);

		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map);

		var LeafIcon = L.Icon.extend({
			options: {
				iconSize:     [80, 81],
				iconAnchor:   [22, 94],
				popupAnchor:  [-3, -76]
			}
		});


		var pin = new LeafIcon({iconUrl: '/img/pins/pngfuel.com.png'});
		var marker = L.marker([geolocation[1], geolocation[0]], {icon: pin}).addTo(map).bindPopup('Hi there!').openPopup();

		// current coordinates
		map.addEventListener('move', function(ev) {
			let lat = map.getCenter().lat;
			let lng =  map.getCenter().lng;
			document.getElementById('lat').innerText = lat.toFixed(4);
			document.getElementById('lng').innerText = lng.toFixed(4);
			if(marker) {
				map.removeLayer(marker)
				marker = new L.marker([lat, lng], {icon: pin}).addTo(map).on('click', openPopup);
			}
		});
	}

	function openPopup() {
		if(beacon) {
			if(beaconPopup && beaconPopup != '') {
				beacon.bindPopup(beaconPopup).openPopup();
			}
			else {
				beacon.bindPopup('Sample popup text').openPopup();
			}
		} 
		else {
			marker.bindPopup('Sample popup text').openPopup();
		} 
	}
</script>