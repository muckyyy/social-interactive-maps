@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    {{-- <link rel='stylesheet' href='/css/leaflet/leaflet.css' /> --}}

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="  crossorigin=""/>

    <style>
        .title span {
            font-weight: bold;
            font-size: 16px;
        }
        .page-inner {
            height: 1062px !important;
        }
    </style>
@endsection

@section('content')
    <div id="beacon_administration">
        @include('admin-v2.beacon_administration.partials.header')
        @include('partials.notifications')
        <div id="beacon_administration_public" class="row d-flex">
            <form class="col-6 container ml-0" style="text-align:left" id="editBeacon" action="{{ route('beacon_administration.update', [$beacon->id]) }} " method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="title mb-4 ml-3">
                    <span>Editing beacon: {{ $beacon->name }}</span>
                </div>
                <div class="form-group d-flex">
                    <span>Beacon Name</span>
                    <input style="width:300px" class="form-control" id="beacon_name" name="name" type="text" placeholder='Beacon Name...' value="{{ old('beacon_name') ?: $beacon->name }}">
                </div>
                <div class="form-group d-flex">
                    <span>Beacon Description</span>
                    <input style="width:300px" class="form-control" id="beacon_description" name="description" type="text" placeholder='Beacon Description...' value="{{ old('desciption') ?: $beacon->description }}">
                </div>
                <!-- city input, hidden -->
                <input id="cityid" type="text" name='city_id' class='d-none' value="{{ old('city_id') ? \App\City::find(old('city_id'))->id : App\City::find($beacon->city_id)->id }}">
                <div class="form-group d-flex">
                    <span>City</span>
                    <input id='findCities' 
                           type="text" 
                           class="form-control" 
                           placeholder='Start typing...' 
                           value="{{ old('city_id') ? \App\City::find(old('city_id'))->name : App\City::find($beacon->city_id)->name }}">
                </div>
                <div class="form-group d-flex">
                    <span>Location</span>
                    <input id='locationLat' type="text" name='locationLat' class="d-none">
                    <input id='locationLng' type="text" name='locationLng' class="d-none">
                    <span id='locationDisplay' class='d-flex' style='white-space:nowrap; font-weight: bold; min-width: 210px'>
                        @php 
                            $array = explode(" ",(string) $beacon->geolocation);
                            $rarray = array_reverse($array);
                            echo $newstring = implode(" ", $rarray);
                        @endphp
                    </span>
                    <button type="button" id="geoLocBtn" onclick='alterDisplay(event)' style='background: #3388ff; color: #fff; height: 30px; width: 70px; font-size: 12px; border: none' class='btn-rounded d-none'>Edit</button>
                </div>
                <div class="form-group d-flex">
                    <span>Display Location</span>
                    <input id='displocationLat' type="text" name='displocationLat' class="d-none">
                    <input id='displocationLng' type="text" name='displocationLng' class="d-none">
                    <span id='displayLocationDisplay' class='d-flex' style='white-space:nowrap; font-weight: bold; min-width: 210px'>
                        @php 
                            $array = explode(" ",(string) $beacon->display_location);
                            $rarray = array_reverse($array);
                            echo $newstring = implode(" ", $rarray);
                        @endphp
                    </span>
                    <button type="button" id="dispLocBtn" onclick='alterDisplay(event)' style='background: #3388ff; color: #fff; height: 30px; width: 70px; font-size: 12px; border: none' class='btn-rounded'>Edit</button>
                </div>
                <div class="form-group d-flex sliders">
                    <span>Beacon Administrative Range</span>
                    <i class="sliderTitle">500</i>
                    <div id="upperLayerSlider" class="ui-slider-handle"></div>
                    <i class="sliderTitle">10 000</i>
                    <input id="beaconAdministrativeRange" name="administrative_range" type="text" class="d-none" value="{{ old('administrative_range') ?: $beacon->administrative_range }}">
                </div>
                <div class="form-group d-flex sliders">
                    <span>Beacon Reach Range </span>
                    <i class="sliderTitle">500</i>
                    <div id="lowerLayerSlider" class="ui-slider-handle"></div>            
                    <i class="sliderTitle">10 000</i>
                    <input id="beaconRange" name="beacon_range" type="text" class="d-none" value="{{ old('beacon_range') ?: $beacon->beacon_range }}">
                </div>
                <div id='editBeaconIcon' class="form-group d-flex">
                    <span>Beacon Icon</span>
                    <img id='beaconImage' style='max-height: 40px' src="{!! $beacon->icon  !!}">
                    <span id='changeIconButton' class='btn rounded ml-2' style='background: #3388ff; color: #fff; height: 30px; width: 70px; font-size: 12px'>Change?</span>
                </div>
                <div id='editBeaconIconUpload' class="d-none form-group d-flex">
                    <span>Upload icon for beacon</span>
                    <input class="form-control" id="beacon_upload_icon" name="icon" type="file">
                </div>
                <div class="form-group d-flex">
                    <span>Beacon popup message</span>
                    <input onkeyup='changePopupMessage(event)' class="form-control" id="beacon_popup_text" name="popup" type="text" placeholder='Message...' value="{{ old('popup') ?: $beacon->popup }}">
                </div>
                <div id="beacon_size" class="form-group d-flex">
                    <span>Beacon Size</span>
                    <i>x</i>
                    <input onkeyup="changeIconSize(event, 'x')" onchange="changeIconSize(event, 'x')" class="form-control" id="beacon_size_x" name="icon_size_x" type="number" value="{{ old('icon_size_x') ?:   $beacon->icon_size_x }}">
                    <i>y</i>
                    <input onkeyup="changeIconSize(event, 'y')" onchange="changeIconSize(event, 'y')" class="form-control" id="beacon_size_y" name="icon_size_y" type="number" value="{{ old('icon_size_y') ?:   $beacon->icon_size_y }}">
                </div>
                <div id="beacon_icon" class="form-group d-flex">
                    <span>Beacon Icon Ancor Point</span>
                    <i>x</i>
                    <input onkeyup="changeIconAncorPoint(event, 'x')" onchange="changeIconAncorPoint(event, 'x')" class="form-control" id="beacon_icon_x" name="icon_ancor_point_x" type="number" value="{{ old('icon_ancor_point_x') ?:   $beacon->icon_ancor_point_x }}">
                    <i>y</i>
                    <input onkeyup="changeIconAncorPoint(event, 'y')" onchange="changeIconAncorPoint(event, 'y')" class="form-control" id="beacon_icon_y" name="icon_ancor_point_y" type="number" value="{{ old('icon_ancor_point_y') ?:  $beacon->icon_ancor_point_y }}">
                </div>
                <div class="beacon_checkboxes mb-4">
                    <div>
                        {{-- Check these conditions, not working properly --}}
                        @if($beacon->allow_image_upload) 
                            <input name="allow_image_upload" type="checkbox" checked>
                        @elseif( !old('allow_image_upload') )
                            <input name="allow_image_upload" type="checkbox">    
                        @else 
                        <input name="allow_image_upload" type="checkbox">
                        @endif
                        <label for="allow_image_upload">Allow image upload</label>
                    </div>
                    <div>
                        @if($beacon->enable_places)
                         <input name="enable_places" type="checkbox" checked>
                        @elseif( !old('enable_places') )
                            <input name="enable_places" type="checkbox">    
                        @else 
                        <input name="enable_places" type="checkbox">
                        @endif
                        <label for="enable_places">Enable places</label>
                    </div>
                    <div>
                        @if($beacon->enable_events)
                            <input name="enable_events" type="checkbox" checked>
                        @elseif( !old('enable_events') )
                            <input name="enable_events" type="checkbox">    
                        @else 
                        <input name="enable_events" type="checkbox">
                        @endif
                        <label for="enable_events">Enable events</label>
                    </div>
                    <div>
                        @if($beacon->enable_groups)
                            <input name="enable_groups" type="checkbox" checked>
                        @elseif( !old('enable_groups') )
                            <input name="enable_groups" type="checkbox">    
                        @else
                        <input name="enable_groups" type="checkbox">
                        @endif
                        <label for="enable_groups">Enable groups</label>
                    </div>
                    <div>
                        @if($beacon->public)
                            <input name="public" type="checkbox" checked>
                        @elseif( !old('public') )
                            <input name="public" type="checkbox">    
                        @else 
                        <input name="public" type="checkbox">
                        @endif
                        <label for="public">Public</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-admin rounded mt-4 px-4 py-2">Save</button>
            </form>
            <div class="col-6" style='max-height:600px'>
                @include('admin-v2.beacon_administration.partials.map')
            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    // beacon data
    let geolocation = {!! json_encode($beacon->geolocation) !!};
    let displayLocation = {!! json_encode($beacon->display_location) !!};
    let iconSizeX = {!! json_encode($beacon->icon_size_x) !!}
    let iconSizeY = {!! json_encode($beacon->icon_size_y) !!}
    let iconAncorX = {!! json_encode($beacon->icon_ancor_point_x) !!}
    let iconAncorY = {!! json_encode($beacon->icon_ancor_point_y) !!}
    let beaconPopup = {!! json_encode($beacon->popup) !!}
    let administrativeRange = {!! json_encode($beacon->administrative_range) !!};
    let reachRange = {!! json_encode($beacon->beacon_range) !!};

    window.onload = function() {
        document.getEl
        document.getElementById('changeIconButton').addEventListener('click', function() {
            this.classList.add('d-none');
            document.getElementById('editBeaconIcon').classList.add('d-none');
            document.getElementById('editBeaconIconUpload').classList.remove('d-none');
        })
    }

    // FIND CITIES
$( document ).ready(function() {
    $( "#findCities" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "/searchCities/" + $('#country').val() + '/' +request.term ,
          dataType: "json",
         
          success: function( data ) {
                response($.map(data, function(v,i){
                    return {
                        label: v.name + ', ' + v.country,
                        value: v.name,
                        cityid: v.id,  
                        };
                }));
            }
        });
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#findCities').val(ui.item.value);
        $('#cityid').val(ui.item.cityid);
        
        return false;
        
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
    
     if ($('#country').val() == 0){
        
        $('#city').autocomplete('disable');
        
    }
    $('#country').on('change',function(){
        if($(this).val()==0){
           $('#city').autocomplete('disable');
            
        }
        else{
            $('#city').autocomplete('enable');
        }
    });
});
</script>