@extends('admin-v2.index')

@section('per-page-css')
    <style> 
        .usersContainer {
            max-height: 500px;
            overflow: auto;
            width: 100% !important;
        }

        .addUsersButton {
            width: 100px;
        }

        .highlighted {
            color: #3fb9a0;
            font-weight: bold;
            text-decoration: underline !important;
        }
    </style>
@endsection


@section('content')
 <div id="admin_users" class="add_new_service map_settings">
    @include('admin-v2.users.partials.header')
    @include('partials.notifications')
    <div class="row d-flex justify-content-evenly p-4">
         <div method="post" action="{{ url('admin/users/create_user') }}" class="container col-5 border rounded shadow p-4">
            {{ csrf_field() }}
            <div class="title">
                <span>Context users</span>
            </div>
            <div class="scrollBar usersContainer d-flex flex-direction-column align-items-start pl-4">
                @foreach ($users as $user)
                    <a onclick="removeUser(event)" class="p-1 m-1 pointer h5 text-decoration-none" id="{{ "user$user->id"  }}">{{ $user->full_name }} ({{ $user->email }})</a>
                @endforeach
            </div>
            <form class="d-none" id="addUserForm" action="{{ route('dashboard.users.role_assignments.add_users') }}" method="post">
                {{ csrf_field() }}
                <input id="addUserInput" name="user" type="text" value="">
            </form>
        </div>
        <div class="container col-2 d-grid justify-content-center">
            <div>
                <button disabled id="addBtn"  onclick="document.getElementById('addUserForm').submit();" class="addUsersButton btn btn-admin rounded m-auto p-2">
                    <i class="fas fa-caret-left"></i>
                    Add
                </button>
            </div>    
            <div>
                <button disabled id="removeBtn"  onclick="document.getElementById('removeUserForm').submit();" class="addUsersButton btn btn-admin rounded m-auto p-2 mt-4">
                    Remove
                    <i class="fas fa-caret-right"></i>
                </button>
            </div>    
        </div>
        <div method="post" action="{{ url('admin/users/create_user') }}" class="container col-5 border rounded shadow p-4">
            {{ csrf_field() }}
            <div class="title">
                <span>All users</span>
            </div>
            <div class="scrollBar usersContainer d-flex flex-direction-column align-items-start pl-4">
                @foreach ($contextUsers as $user)
                    <a onclick="addUser(event)" class="p-1 m-1 pointer h5 text-decoration-none" id="{{ "contextUser$user->id"  }}">{{ $user->full_name }} ({{ $user->email }})</a>
                @endforeach
            </div>
            <form class="d-none" id="removeUserForm" action="{{ route('dashboard.users.role_assignments.remove_users') }}" method="post">
                {{ csrf_field() }}
                <input id="removeUserInput" name="user" type="text" value="">
            </form>
        </div>
    </div>
@endsection

<script>
function disableButtons() {
    document.getElementById('addBtn').disabled = true;
    document.getElementById('removeBtn').disabled = true;
}


function addUser(e) {
    disableButtons();
    document.getElementById('addBtn').disabled = false;
    $('.highlighted').removeClass('highlighted');
    e.target.classList.add('highlighted');
    let id = e.target.id;
    let i = id.match(/\d+/g).map(Number);
    let userId = i[0];
    document.getElementById('addUserInput').value = userId;
}
function removeUser(e) {
    disableButtons();
    document.getElementById('removeBtn').disabled = false;
    $('.highlighted').removeClass('highlighted');
    e.target.classList.add('highlighted');
    let id = e.target.id;
    let i = id.match(/\d+/g).map(Number);
    let userId = i[0];
    document.getElementById('removeUserInput').value = userId;
}

</script>



