@extends('admin-v2.index')

@section('content')
    <div id="admin_users" class="map_settings">
        @include('admin-v2.users.partials.header')
        @include('partials.notifications')

        <table id="users_table">
            <tr>
                <th>Role</th>
                <th>Context</th>
                <th>User</th>
                <th>Modifier</th>
                <th>Component</th>
                <th>Item</th>
                <th>Actions</th>
            </tr>
                @foreach($roleAssignments as $assignment)
                <tr>
                    <td>
                        {{ \App\Role::where('id', $assignment->role_id)->first()->name }}  
                    </td>
                    <td>
                        {{ \App\Context::where('id', $assignment->context_id)->first()->context_level }}  
                    </td>
                    <td>
                        {{ \App\User::where('id', $assignment->user_id)->first()->full_name }} 
                    </td>
                    <td>
                        {{ $assignment->modifier_id }}
                    </td>
                    <td>
                        {{ $assignment->component }}
                    </td>
                    <td>
                        {{ $assignment->item_id }}
                    </td>
                    @if(\App\Context::where('id', $assignment->context_id)->first()->context_level == 10)
                    <td class="actions d-flex justify-content-center">
                        <div class="d-flex">
                            <a href="{{ route('dashboard.users.role_assignments.edit', $assignment->id) }}">
                                <i class="text-info far fa-edit"></i>
                            </a> 
                            <a style='color:#329883'  class="ml-2" href="{{ route('dashboard.users.role_assignments.add_users') }}">
                                <i class="fas fa-user-plus"></i>
                            </a>
                            <form method="post" action="{{ route('dashboard.users.role_assignments.delete', $assignment->id) }}">
                                {{ csrf_field() }}
                                <button style='background:none; border:none' type='submit' title="delete" class="text-danger fas fa-trash-alt"></button>
                            </form>
                        </div> 
                    </td>
                    @else 
                    <td>
                        <form method="post" action="{{ route('dashboard.users.role_assignments.delete', $assignment->id) }}">
                            {{ csrf_field() }}
                            <button style='background:none; border:none' type='submit' title="delete" class="text-danger fas fa-trash-alt"></button>
                        </form></td>
                    @endif   
                </tr>
                @endforeach
        </table>   
    </div>     
@endsection        


