@extends('admin-v2.index')

@section('content')
 <div class="add_new_service map_settings">
    @include('admin-v2.users.partials.header')
    @include('partials.notifications')
    <form method="post" action="{{ route('dashboard.users.role_assignments.update', $roleAssignment->id) }}" class="container">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
         <div class="title">
            <span>Editing a role assignment</span>
        </div>
        <div class="form-group d-flex">
            <label>Role</label>
            <select class="form-control rounded" name="role_id" id="">
                <option selected value="{{ old('role_id') ? $roles->where('id', old('role_id'))->first()->id : $roles->where('id', $roleAssignment->role_id)->first()->id }}">
                    {{ old('role_id') ? $roles->where('id', old('role_id'))->first()->name  : $roles->where('id', $roleAssignment->role_id)->first()->name }}
                </option>
                @foreach ($roles as $role )
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group d-flex">
            <label>Context level</label>
            <select class="form-control rounded" name="context_id" id="">
                <option selected value="{{ old('context_id') ? $contexts->where('id', old('context_id'))->first()->id : $contexts->where('id', $roleAssignment->context_id)->first()->id }}">
                    {{ old('context_id') ? $contexts->where('id', old('context_id'))->first()->context_level  : $contexts->where('id', $roleAssignment->context_id)->first()->context_level }}</option>
                @foreach ($contexts as $context )
                    <option value="{{ $context->id }}">{{ $context->context_level}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group d-flex">
            <label>User</label>
            <select class="form-control rounded" name="user_id" id="">
                <option selected value="{{ old('user_id') ? $users->find(old('user_id'))->id : $users->find($roleAssignment->user_id)->id  }}">
                    {{ old('user_id') ? $users->find(old('user_id'))->user_name : $users->find($roleAssignment->user_id)->user_name }}</option>
                @foreach ( $users as $user )
                    <option value="{{ $user->id }}">{{ $user->user_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group d-flex">
            <label>Modifier</label>
            <select class="form-control rounded" name="modifier_id" id="">
                <option selected value="{{ old('modifier_id') ?: $roleAssignment->modifier_id }}">{{ old('modifier_id') ?: $roleAssignment->modifier_id }}</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
        </div>
        <div class="form-group d-flex">
            <label>Component</label>
            <select class="form-control rounded" name="component" id="">
                <option selected value="{{ old('component') ?: $roleAssignment->component }}">{{ old('component') ?: $roleAssignment->component }}</option>
                <option value="core">core</option>
            </select>
        </div>
        <div class="form-group d-flex">
            <label>Item</label>
            <select class="form-control rounded" name="item_id" id="">
                <option selected value="{{ old('item_id') ?:  $roleAssignment->item_id }}">{{ old('item_id') ?:  $roleAssignment->item_id }}</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
        </div>
        <div class="buttonHolder">
            <button type="submit" class="btn btn-admin rounded">Save</button>
        </div>
    </form>
</div>
@endsection













 












 
