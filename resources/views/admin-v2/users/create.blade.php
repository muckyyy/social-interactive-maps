@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

    <style> 
    .sliders {
        width: 80%;
    }
    </style>
@endsection

@section('content')
 <div id="admin_users" class="add_new_service map_settings">
    @include('admin-v2.users.partials.header')
    @include('partials.notifications')
    <form id="createUser" method="post" action="{{ url('admin/users/create_user') }}" class="container">
        {{ csrf_field() }}
        <div class="title">
            <span>Create a new user</span>
        </div>
        <div class="form-group d-flex">
            <label>Full Name</label>
            <input name="full_name" class="form-control" type="text" required value='{{ old('full_name') }}'>
        </div>
        <div class="form-group d-flex">
            <label>User Name</label>
            <input name="user_name" class="form-control" type="text" required value='{{ old('user_name') }}'>
        </div>
        <div class="form-group d-flex">
            <label>Email</label>
            <input name="email" class="form-control" type="text" required value='{{ old('email') }}'>
        </div>
        <div class="form-group d-flex">
            <label>Password</label>
            <input name="password" class="form-control" type="password" required value='{{ old('password') }}'>
        </div>
        <div class="form-group d-flex">
            <label>Confirm Password</label>
            <input name="password_confirmation" class="form-control" type="password" required value='{{ old('password_confirmation') }}'>
        </div>
        <div class="form-group d-flex">
            <label>About</label>
            <textarea class="form-control" name="about"  cols="30" rows="10" required>{{ old('about') }}</textarea>
        </div>
        <div class="form-group d-flex">
            <label>Date of birth</label>
            <input name="birth_date" class="form-control" type="date" value='{{ old('birth_date') }}'>
        </div>
         <div class="form-group d-flex">
            <label>Profile Image</label>
            <input name="profile_img" class="form-control" type="file" value='{{ old('profile_img') }}'>
        </div>
        <div class="form-group d-flex">
            <label>Cover Image</label>
            <input name="cover_img" class="form-control" type="file" value='{{ old('cover_img') }}'>
        </div>
        <div class="form-group d-flex">
            <label>Gender</label>
            <select class="form-control" name="gender" >
                <option selected disabled value="Choose Gender...">Choose Gender...</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
        </div>
        <div class="form-group d-flex">
            <label>City</label>
            <select class="form-control" name="city">
                <option selected disabled value="">Select user city...</option>
                @foreach($cities as $city)
                <option value="{{ $city->id }}">
                        {{ $city->name }}
                    </option> 
                @endforeach
            </select>
        </div>
        <div class='form-group d-flex'>
            <label>
                Privacy radius
            </label>
            <div class='content sliders'>
                <div id="lowerLayerSlider"></div>
                <input id="privacyRadius" name="privacy" type="number" style="display:none">
            </div>
        </div>
        <div class='form-group d-flex'>    
            <label>Reach radius</label>
            <div class='content sliders'>
                <div id="upperLayerSlider"></div>
                <input id="reachRadius" name="reach" type="number" style="display:none">
            </div>
        </div>
        <div class="form-group d-flex">
            <label>Verified</label>
            <input name="verified" class="form-control" type="checkbox">
        </div>
        <div class="form-group d-flex">
            <label>Admin</label>
            <input name="is_admin" class="form-control" type="checkbox">
        </div>
        <div class="buttonHolder">
            <a onclick="document.getElementById('createUser').submit()" class="btn btn-admin">Save</a>
            <a href="{{ url()->previous() }}" class="btn btn-admin">Back</a>
        </div>
    </form>
</div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    // Lower Layer
    $( function() {
        let privacyRadius = document.getElementById('privacyRadius').value;
        $( "#lowerLayerSlider" ).slider({
        range: "max",
        min: 500,
        max: 10000,
        value: 2000,
        slide: function( event, ui ) {
            $('#lowerLayerSlider .ui-slider-handle').text(ui.value);
            document.getElementById('privacyRadius').value = ui.value;
            console.log(document.getElementById('privacyRadius').value);
        }
        });
        $( "#lowerLayerSlider .ui-slider-handle" ).text( $( "#lowerLayerSlider" ).slider( "value" ) );
        document.getElementById('privacyRadius').value = 2000;
    });
    // Upper Layer
    $( function() {
        $( "#upperLayerSlider" ).slider({
        range: "max",
        min: 500,
        max: 10000,
        value: 2000,
        slide: function( event, ui ) {
            $('#upperLayerSlider .ui-slider-handle').text(ui.value);
            document.getElementById('reachRadius').value = ui.value;
            console.log(document.getElementById('reachRadius').value);
        }
        });
        document.getElementById('reachRadius').value = 2000;
        $( "#upperLayerSlider .ui-slider-handle" ).text( $( "#upperLayerSlider" ).slider( "value" ) );
    });
</script>