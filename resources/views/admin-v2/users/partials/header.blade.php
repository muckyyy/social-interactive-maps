<div class="admin_header d-flex">
    <div>
        <a href="{{ url('admin/users') }}" class="{{ strpos(url()->current(), 'users/create') ? 'active' : (\Request::route()->getName() == 'dashboard.users' ? 'active' : '') }}">
            All users 
        </a>
    </div>
    <div>
        <a href="{{ url('admin/users/registered_invitations') }}" class="{{  strpos(url()->current(), 'registered_invitations') ? 'active' : '' }}">
            Registration Invitations
        </a>
    </div>
    <div>
        <a href="{{ route('dashboard.users.roles.index') }}" class="{{  strpos(url()->current(), 'roles') ? 'active' : '' }}">
            Roles
        </a>
    </div>
    {{-- <div>
        <a href="{{ url('admin/users/role_assignments') }}" class="{{  strpos(url()->current(), 'role_assignments') ? 'active' : '' }}">
            Role assignments
        </a>
    </div> --}}
    <div>
        <a href="{{ url('admin/users/sanctioned_users') }}" class="{{  strpos(url()->current(), 'sanctioned_users') ? 'active' : '' }}">
            Sanctioned users
        </a>
    </div>
    <div>
        <a href="{{ url('admin/users/auth_methods') }}" class="{{  strpos(url()->current(), 'auth_methods') ? 'active' : '' }}">
            Authentication methods
        </a>
    </div>
</div>
    
    