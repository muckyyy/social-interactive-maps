@extends('admin-v2.index')

@section('content')
    <div id="admin_users" class="auth_methods map_settings">
        @include('admin-v2.users.partials.header')
        <table id="users_table">
            <tr>
                <th>Name</th>
                <th>Configured</th>
                <th>Allow login</th>
                <th>Users count</th>
                <th>Actions</th>
            </tr>
                <tr>
                    <td class="social_media_icons">
                        <div>
                            <i class="fab fa-facebook-f" style="color:#3097D1"></i>
                            Facebook
                        </div>
                    </td>
                    <td>
                       <input type="checkbox" checked='true'>
                    </td>
                    <td>
                       <input type="checkbox" checked='true'>
                    </td>
                    <td>
                        22
                    </td>
                    <td class="d-flex justify-content-center">                        
                        <div>
                            <a title="Settings" href="#" class="fas fa-cog"></a>
                        </div>
                        <div>
                            <a title="Oauth Endpoints" href="#" class="fas fa-bars"></a>
                        </div>
                        <div>
                            <a title="Field mappings" href="#" class="fas fa-user"></a>
                        </div>
                        <div>
                            <a title="Delete" href="#" class="fas fa-trash-alt"></a>
                        </div>
                        <div>
                            <a title="Hide" href="#" class="far fa-eye"></a>
                        </div>
                        <div>
                            <a title="Move down" href="#" class="fas fa-arrow-down"></a>
                        </div>
                        {{-- <a title="Move up" href="#" class="fas fa-arrow-up"></a> --}}
                    </td>
                </tr>
                <tr>
                    <td class="social_media_icons">
                        <div>
                            <i class="fab fa-google" style="color:#DB4437"></i>
                            Google
                        </div>
                    </td>
                    <td>
                       <input type="checkbox" checked='true'>
                    </td>
                    <td>
                       <input type="checkbox" checked='true'>
                    </td>
                    <td>
                        15
                    </td>
                    <td class="d-flex justify-content-center">                        
                        <div>
                            <a title="Settings" href="#" class="fas fa-cog"></a>
                        </div>
                        <div>
                            <a title="Oauth Endpoints" href="#" class="fas fa-bars"></a>
                        </div>
                        <div>
                            <a title="Field mappings" href="#" class="fas fa-user"></a>
                        </div>
                        <div>
                            <a title="Delete" href="#" class="fas fa-trash-alt"></a>
                        </div>
                        <div>
                            <a title="Hide" href="#" class="far fa-eye"></a>
                        </div>
                         <div class="upDownArrows">
                            <a title="Move up" href="#" class="fas fa-arrow-up"></a>
                            <a title="Move down" href="#" class="fas fa-arrow-down"></a>
                        </div>
                        {{-- <a title="Move up" href="#" class="fas fa-arrow-up"></a> --}}
                    </td>
                </tr>
                <tr>
                    <td class="social_media_icons">
                        <div>
                            <i class="fab fa-twitter" style="color:#3097D1"></i>
                            Twitter
                        </div>
                    </td>
                    <td>
                       <input type="checkbox" checked='true'>
                    </td>
                    <td>
                       <input type="checkbox" checked='true'>
                    </td>
                    <td>
                        12
                    </td>
                    <td class="d-flex justify-content-center">                        
                        <div>
                            <a title="Settings" href="#" class="fas fa-cog"></a>
                        </div>
                        <div>
                            <a title="Oauth Endpoints" href="#" class="fas fa-bars"></a>
                        </div>
                        <div>
                            <a title="Field mappings" href="#" class="fas fa-user"></a>
                        </div>
                        <div>
                            <a title="Delete" href="#" class="fas fa-trash-alt"></a>
                        </div>
                        <div>
                            <a title="Hide" href="#" class="far fa-eye"></a>
                        </div>
                        <div>
                            <a title="Move up" href="#" class="fas fa-arrow-up"></a>
                        </div>
                        {{-- <a title="Move up" href="#" class="fas fa-arrow-up"></a> --}}
                    </td>
                </tr>
        </table>   
        <div class="buttonHolder">
            <a href="{{ url('admin/users/auth_methods/add_service') }}" class="btn btn-admin">Add New Service</a>
        </div>
    </div>     
@endsection        
