@extends('admin-v2.index')

@section('content')
 <div id="admin_users" class="add_new_service map_settings">
    @include('admin-v2.users.partials.header')
    <div class="container">
        <div class="title">
            <span >Add new service</span>
        </div>
        <div class="form-group d-flex">
            <label >Name</label>
            <input name="name" class="form-control" type="text">
        </div>
        <div class="form-group d-flex">
            <label>Client ID</label>
            <input name="clientId" class="form-control" type="text">
        </div>
        <div class="form-group d-flex">
            <label>Client secret</label>
            <input name="clientSecret" class="form-control" type="text">
        </div>
        <div class="form-group d-flex">
            <label>Scopes included for offline access</label>
            <input name="scopes" class="form-control" type="text">
        </div>
        <div class="form-group d-flex">
            <label>Additional parameters</label>
            <input name="additParams" class="form-control" type="text">
        </div>
        <div class="form-group d-flex">
            <label>Service base URL</label>
            <input name="serviceURL" class="form-control" type="text">
        </div>
        <div class="form-group d-flex">
            <label>Login domains</label>
            <input name="domains" class="form-control" type="text">
        </div>
         <div class="form-group d-flex">
            <label>Service logo</label>
            <input name="logo" class="form-control" type="file">
        </div>
        <div class="form-group d-flex">
            <input name="show" class="form-control" type="checkbox">
            <span>Show on login</span>
        </div>
        <div class="form-group d-flex">
            <input name="requreEmail" class="form-control" type="checkbox">
            <span>Require email</span>
        </div>
        <div class="buttonHolder">
            <a href="" class="btn btn-admin">Save</a>
            <a href="{{ url()->previous() }}" class="btn btn-admin">Back</a>
        </div>
    </div>
</div>
@endsection