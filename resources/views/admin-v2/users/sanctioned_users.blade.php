@extends('admin-v2.index')

@section('content')
    <div id="admin_users" class="map_settings">
        @include('admin-v2.users.partials.header')
        <form method="get" action="/admin/users/sanctioned_users" class="form-group" id="search_users">
            <input id="filterUsersInput" name="name" onkeyup="submitForm(event)" type="text" class="form-control" placeholder="Search Sanctioned Users..." value="{{ $name }}" autofocus>
            <i class="fas fa-search"></i>
        </form>
        <table id="users_table">
            <tr>
                <th></th>
                <th>User Name</th>
                <th>Full Name</th>
                <th>Primary email</th>
                <th>Actions</th>
            </tr>
            @foreach($users as $user)
                <tr class='even'>
                    <td class="imgTableCell">
                       @if($user->profile_img)
                          {!! $user->profile_img !!}
                        @else 
                            <img src="/img/common/avatar2.jpg" alt="">  
                        @endif
                    </td>
                    <td>
                        {{ $user->user_name }}
                    </td>
                    <td>
                        {{ $user->full_name }}
                    </td>
                    <td>
                        {{ $user->email }}
                    </td>
                    <td>
                    <form id="unsuspend_user" method="post" action="{{ url('/admin/users/sanctioned_users/' . $user->id . '/unsuspend') }}">
                         {{csrf_field()}}
                        {{ method_field('PATCH') }}
                        <a title="unsuspend" class="fas fa-user-check" onclick="document.getElementById('unsuspend_user').submit();">
                        </a>
                    </form>
                    </td>
                </tr>
            @endforeach
        </table>   
        {{ $users->links() }}
    </div>     
@endsection        

<script>
    window.onload = function() {
    $("[autofocus]").on("focus", function() {
        if (this.setSelectionRange) {
        var len = this.value.length * 2;
        this.setSelectionRange(len, len);
        } else {
        this.value = this.value;
        }
        this.scrollTop = 999999;
    }).focus();
}

    // submit form on enter and on backspace/delete if input is empty
    function submitForm(event) {
        var code = (event.keyCode ? event.keyCode : event.which);
        if(code == 13) { //Enter keycode
            document.getElementById('search_users').submit();
        }

        if(code == 8 || code == 46) {
            let input = document.getElementById('filterUsersInput');
            console.log(input.value.length);
            if(input.value.length == 0) {
                document.getElementById('search_users').submit();
            }
        }
    }
</script>