@extends('admin-v2.index')

@section('content')
    <div id="admin_users" class="map_settings">
        @include('admin-v2.users.partials.header')
        <div class="addNewRole d-flex justify-content-end">
            <a href="{{ route('dashboard.users.roles.add') }}" class="btn btn-admin rounded">Add a new role</a>
        </div>
        @include('partials.notifications')
        <table id="users_table">
            <tr>
                <th>Role</th>
                <th>Short Name</th>
                <th>Context Level</th>
                <th>Description</th>
                <th>Number of users</th>
                <th>Action</th>
            </tr>
            @foreach($roles as $role)
                <tr>
                    <td>
                        {{ $role->name }}  
                    </td>
                    <td>
                        {{ $role->shortname }}  
                    </td>
                    <td>
                        {{ $role->contextlevel }}  
                    </td>
                    <td>
                        {{ $role->description }}  
                    </td>
                    <td>
                        WHICH TABLE REALATES?
                    </td>
                    <td class="d-flex justify-content-center">
                        <a title="edit" href="{{ route('dashboard.users.roles.edit', ['id' => $role->id]) }}" class="fas fa-edit"></a>
                        @if($role->contextlevel == 10 && $role->shortname != 'guest')
                            <a style='color:#329883'  class="ml-2" href="{{ route('dashboard.users.roles.add_users', ['role_id' => $role->id]) }}">
                                <i class="fas fa-user-plus"></i>
                            </a>
                        @endif  
                        <a href="#">
                            <i class="fas fa-key ml-1 mr-1"></i>
                        </a>
                        <form method="POST" action="{{ route('dashboard.users.roles.delete', $role->id) }}">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button style='border:none; background:none; padding: 0' type="submit" class="ml-1 text-danger far fa-trash-alt"></button>
                        </form>
                    </td>
                </tr>
            @endforeach    
        </table>  
        {{ $roles->links() }} 
    </div>     
@endsection        
