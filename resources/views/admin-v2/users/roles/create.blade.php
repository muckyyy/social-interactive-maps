@extends('admin-v2.index')

@section('content')
 <div class="add_new_service map_settings">
    @include('admin-v2.users.partials.header')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(session('success'))
       <div class="alert alert-success">
        <ul>
            <li>{{session('success')}}</li>
        </ul>
    </div>
    @endif
    <form method="post" action="{{ route('dashboard.users.roles.add') }}" class="container">
        {{ csrf_field() }}
         <div class="title">
            <span>Add a new role</span>
        </div>
        <div class="form-group d-flex">
            <label>Role Name</label>
            <input name="name" class="form-control" type="text" required placeholder="Role Name..." value='{{ old('name') }}'>
        </div>
        <div class="form-group d-flex">
            <label>Short Name</label>
            <input name="shortname" class="form-control" type="text" required placeholder="Role Short Name..." value='{{ old('shortname') }}'>
        </div>
        <div class="form-group d-flex">
            <label>Description</label>
            <textarea class="form-control" name="description" id="" cols="30" rows="10" style="resize: none;">
                {{ old('description') }}
            </textarea>
        </div>
        <div class="form-group d-flex">
            <label>Context level</label>
            <select name="contextlevel" id="" class="form-control">
                <option value="{{ old('contextlevel')?: 'Choose context level' }}">{{ old('contextlevel')?: 'Choose context level' }}</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
                <option value="50">50</option>
            </select>
        </div>
        <div class="buttonHolder">
            <button type="submit" class="btn btn-admin rounded">Save</button>
        </div>
    </form>
</div>
@endsection













 
