@extends('admin-v2.index')

@section('content')
 <div class="add_new_service map_settings">
    @include('admin-v2.users.partials.header')
    @include('partials.notifications')
    <form method="post" action="{{ route('dashboard.users.roles.update', $role->id) }}" class="container">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
         <div class="title">
            <span>Editing a role {{ $role->name }}</span>
        </div>

        {{-- edit( description, short name, context level) --}}
        <div class="form-group d-flex">
            <label>Role Name</label>
            <input name="name" class="form-control" type="text" required placeholder="Role Name..." value='{{ $role->name }}'>
        </div>
        <div class="form-group d-flex">
            <label>Short Name</label>
            <input name="shortname" class="form-control" type="text" required placeholder="Role Short Name..." value='{{ $role->shortname }}'>
        </div>
        <div class="form-group d-flex">
            <label>Description</label>
            <textarea class="form-control" name="description" id="" cols="30" rows="10" style="resize: none;">
                {{ $role->description }}
            </textarea>
        </div>
        <div class="form-group d-flex">
            <label>Context level</label>
            <select class="form-control" name="contextlevel" id="">
                <option selected value="{{ $role->contextlevel }}">{{ $role->contextlevel }}</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
                <option value="50">50</option>
            </select>
        </div>
        <div class="buttonHolder">
            <button type="submit" class="btn btn-admin rounded">Save</button>
        </div>
    </form>
</div>
@endsection













 
