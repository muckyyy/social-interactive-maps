@extends('admin-v2.index')

@section('per-page-css')
    <style> 
        .usersContainer {
            max-height: 500px;
            overflow: auto;
            width: 100% !important;
        }

        .addUsersButton {
            width: 100px;
        }

        .highlighted {
            color: #3fb9a0;
            font-weight: bold;
            text-decoration: underline !important;
        }
    </style>
@endsection

@section('content')
 <div id="admin_users" class="add_new_service map_settings">
    @include('admin-v2.users.partials.header')
    @include('partials.notifications')
    <div class="row d-flex justify-content-evenly p-4">
         <div class="container col-5 border rounded shadow p-4">
            <div class="title">
                <span>Available users</span>
            </div>
            {{-- filter user inputs --}}
             <div class="form-group  mb-4">
                <input id="potentialUsers" type="text" class="form-control"  placeholder='Search users...' value="{{ $potentialUserFilter }}" onkeyup="filterUsers(event)">
            </div>
            <div class="scrollBar usersContainer d-flex flex-direction-column align-items-start pl-4">
                @foreach ($contextUsers as $user)
                    <a onclick="removeUser(event)" class="p-1 m-1 pointer h5 text-decoration-none" id="{{ "contextUser$user->id"  }}">{{ $user->full_name }} ({{ $user->email }})</a>
                @endforeach
            </div>
            <form class="d-none" id="removeUserForm" action="{{ route('dashboard.users.roles.remove_users', ['role_id' => $role_id]) }}" method="post">
                {{ csrf_field() }}
                <input id="removeUserInput" name="user" type="text" value="">
            </form>
        </div>
        <div class="container col-2 d-grid justify-content-center">
            <div>
                <button disabled id="addBtn"  onclick="document.getElementById('addUserForm').submit();" class="addUsersButton btn btn-admin rounded m-auto p-2">
                    <i class="fas fa-caret-left"></i>
                    Add
                </button>
            </div>    
            <div>
                <button disabled id="removeBtn"  onclick="document.getElementById('removeUserForm').submit();" class="addUsersButton btn btn-admin rounded m-auto p-2 mt-4">
                    Remove
                    <i class="fas fa-caret-right"></i>
                </button>
            </div>    
        </div>
        <div method="post" action="{{ url('admin/users/create_user') }}" class="container col-5 border rounded shadow p-4">
            <div class="title">
                <span>Potential users</span>
            </div>
            {{-- filter user inputs --}}
            <div class="form-group mb-4">
                <input id="users" type="text" class="form-control"  placeholder='Search users...' value="{{ $filter }}" onkeyup="filterUsers(event)">
            </div>
            <div class="scrollBar usersContainer d-flex flex-direction-column align-items-start pl-4">
                @foreach ($users as $user)
                    <a onclick="addUser(event)" class="p-1 m-1 pointer h5 text-decoration-none" id="{{ "user$user->id"  }}">{{ $user->full_name }} ({{ $user->email }})</a>
                @endforeach
            </div>
            <form class="d-none" id="addUserForm" action="{{ route('dashboard.users.roles.add_users', ['role_id' => $role_id]) }}" method="post">
                {{ csrf_field() }}
                <input id="addUserInput" name="user" type="text" value="">
            </form>
        </div>
        {{-- Filter form --}}
        <form id="filterForm" class="form-group  mb-4 d-none" method="get" action="{{ route('dashboard.users.roles.add_users') }}">
            <input name="role_id" type="text" class="form-control"  placeholder='Search users...' value="{{ $role_id }}">
            <input id="usersInput" name="filter" type="text" class="form-control"  placeholder='Search users...' value="{{ $filter }}">
            <input id="potentialUsersInput" name="potentialUserFilter" type="text" class="form-control"  placeholder='Search users...' value="{{ $potentialUserFilter }}">
        </form>
    </div>
@endsection

<script>
function disableButtons() {
    document.getElementById('addBtn').disabled = true;
    document.getElementById('removeBtn').disabled = true;
}


function addUser(e) {
    disableButtons();
    document.getElementById('addBtn').disabled = false;
    $('.highlighted').removeClass('highlighted');
    e.target.classList.add('highlighted');
    let id = e.target.id;
    let i = id.match(/\d+/g).map(Number);
    let userId = i[0];
    document.getElementById('addUserInput').value = userId;
}
function removeUser(e) {
    disableButtons();
    document.getElementById('removeBtn').disabled = false;
    $('.highlighted').removeClass('highlighted');
    e.target.classList.add('highlighted');
    let id = e.target.id;
    let i = id.match(/\d+/g).map(Number);
    let userId = i[0];
    document.getElementById('removeUserInput').value = userId;
}

// submit filter users form
function filterUsers(e) {
    let code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) { //Enter keycode 
        if(e.target.id == 'users') {
            document.getElementById('usersInput').value = e.target.value;
        }
        if(e.target.id == 'potentialUsers') {
            document.getElementById('potentialUsersInput').value = e.target.value;
        }
        document.getElementById('filterForm').submit();
    } 
    else if(code == 8 || code == 46) {
        if(e.target.id == 'users') {
            if(e.target.value.length > 0) return;
            document.getElementById('usersInput').value = e.target.value;
        }
        if(e.target.id == 'potentialUsers') {
            if(e.target.value.length > 0) return;
            document.getElementById('potentialUsersInput').value = e.target.value;
        }
        document.getElementById('filterForm').submit();
    }
}

</script>



