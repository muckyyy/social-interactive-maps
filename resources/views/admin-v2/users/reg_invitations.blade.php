@extends('admin-v2.index')

@section('content')
    <div id="admin_users" class="map_settings">
        @include('admin-v2.users.partials.header')
        <div class="d-flex">
            <form method="get" action="/admin/users" class="form-group" id="search_users">
                <input id="filterUsersInput" name="name" onkeyup="submitForm(event)" type="text" class="form-control" placeholder="Search..." value="{{ $name }}" autofocus>
                    <i class="fas fa-search"></i>
            </form>
            <div id="reg_invitation_checkboxes">
                <input class="checkbox" type="checkbox" checked='true'>
                <span class="labels">
                    All Status
                </span>
                <input class="checkbox" type="checkbox">
                <span class="labels">
                    Used
                </span>
                <input class="checkbox" type="checkbox">
                <span class="labels">
                    Pending
                </span>
                <input class="checkbox" type="checkbox">
                <span class="labels">
                    Expired
                </span>
            </div>
        </div>
        <div class="d-flex dates">
            <div class="form-group">
                <select class="form-control">
                    <option value="Select">Select</option>
                    <option value="Select">Value 1</option>
                    <option value="Select">Value 2</option>
                    <option value="Select">Value 3</option>
                </select>    
            </div>
            <div class="d-flex">
               <span class="labels">From:</span>
               <input class="" type="date">
               <input class="checkbox" type="checkbox" checked='true'>
               <span class="labels">use</span>
            </div>
            <div class="d-flex">
               <span class="labels">To:</span>
               <input class="" type="date">
                <input class="checkbox" type="checkbox" checked='true'>
               <span class="labels">use</span>
            </div>
        </div> 
        <div class="buttonHolder">
            <a href="" class="btn btn-admin">Show Results</a>    
        </div>   
           {{-- table --}}
        <table id="users_table">
            <tr>
                <th>Status</th>
                <th>Invitation code</th>
                <th>Invitation sent by</th>
                <th>Invitation sent time</th>
                <th>Invitation to (email)</th>
                <th>Invitation used (time)</th>
                <th>Invitation valid until</th>
                <th>Action</th>
            </tr>
                <tr class='even'>
                    <td>
                        Pending  
                    </td>
                    <td>
                       hyIsaf325DW
                    </td>
                    <td>
                        Armin B.
                    </td>
                    <td>
                        10 Sep 2019
                    </td>
                    <td>
                        Edin Sita
                    </td>
                    <td>
                        21 Sep 2019
                    </td>
                    <td>
                        05 Oct 2019
                    </td>
                    <td>
                        <a title="resend" href="#">
                            <i class="fas fa-redo"></i>
                        </a> 
                    </td>
                </tr>
        </table>   
        <div id="newInvite" class="buttonHolder">
            <a href="" class="btn btn-admin">New Invite</a>    
        </div>
    </div>     
@endsection        

<script>
    window.onload = function() {
    // remove checker class
    let elements = document.getElementsByClassName('checker');

    while(elements.length > 0){
        elements[0].classList.remove('checker');
    }

    $("[autofocus]").on("focus", function() {
        if (this.setSelectionRange) {
        var len = this.value.length * 2;
        this.setSelectionRange(len, len);
        } else {
        this.value = this.value;
        }
        this.scrollTop = 999999;
    }).focus();
}

    // submit form on enter and on backspace/delete if input is empty
    function submitForm(event) {
        var code = (event.keyCode ? event.keyCode : event.which);
        if(code == 13) { //Enter keycode
            document.getElementById('search_users').submit();
        }

        if(code == 8 || code == 46) {
            let input = document.getElementById('filterUsersInput');
            console.log(input.value.length);
            if(input.value.length == 0) {
                document.getElementById('search_users').submit();
            }
        }
    }
</script>