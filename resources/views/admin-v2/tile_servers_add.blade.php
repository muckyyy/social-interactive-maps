@extends('admin-v2.index')

@section('content')
    <div id="add-tile-server" class="container">
        <div class="main-admin-section col-xs-12 d-flex justify-content-center">
            <div class="col-xs-6">
                <div class="admin-category-title">
                    Add a new tile server
                </div>
                <form class="form-horizontal" method="POST" action="/tile-servers/save">
                    {{ csrf_field() }}
                    <input type="text" class="form-control" name="name" placeholder="Name" required="required"> <br>
                    <input type="text" class="form-control" name="domain" placeholder="Domain" required="required"><br>
                    <input type="text" class="form-control" name="ip" placeholder="IP"><br>
    
                    <input type="text" class="form-control" name="token" placeholder="Token"><br>
                    <input type="text" class="form-control" name="username" placeholder="Username"><br>
                    <input type="text" class="form-control" name="pw" placeholder="Password"><br>
    
                    <input type="text" class="form-control" name="tilesurl" placeholder="Tile server url" required="required" ><br>
                    <label class="d-flex">Status:
                        <div id="checkboxHolder" class="d-flex">
                            <input onchange="checkServerStatus()" id="server_status" type="checkbox" class="form-control" name="active"><br>
                            <span id="server_status_text" class="inActive">Inactive</span>
                        </div>
                    </label> 
                    <br>
                    <input type="text" class="form-control" name="maxusers" placeholder="Maximum number of users" required="required" ><br>
                    
                    <input type="hidden" name="action" value="add" >
                    <div class="button-holder d-flex justify-content-center">
                       <input type="submit" class="btn btn-admin" value="Add">
                    </div>   
                </form>
            </div>
        </div>
    </div>
@endsection

<script>
    function checkServerStatus() {
        let server_status = document.getElementById('server_status');
        let server_status_text = document.getElementById('server_status_text');
        
        if(server_status.checked) {
            server_status_text.classList.remove('inactive');
            server_status_text.classList.add('active');
            server_status_text.innerText = 'Active';
        } 
        else {
            server_status_text.classList.add('inactive');
            server_status_text.classList.remove('active');
            server_status_text.innerText = 'Inactive';
        }
    }
</script>


