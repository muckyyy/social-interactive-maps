@extends('admin-v2.index')

@section('content')
    <div id="tile-server-management" style="width:100%;">
        <table>
            <thead>
                <th class="title">ID</th> 
                <th class="title">Name</th> 
                <th class="title">Domain</th>    
                <th class="title">IP</th>  
                <th class="title">Active</th>   
                <th class="title">Current user count</th>  
            </thead>
            <tbody>
                @foreach($servers as $server)
                    <tr>
                        <td class="value">{{ $server->id }}</td>  
                        <td class="value">{{ $server->name }}</td>   
                        <td class="value">{{ $server->domain }}</td>  
                        <td class="value">{{ $server->ip }}</td>  
                        @if($server->active)
                            <td class="value" style="color: green">&#10004;</td>  
                        @else
                            <td class="value" style="color: red; font-weight: bold">&#10005;</td>   
                        @endif   
                        <td class="value">{{ $server->maxusers }}</td>  
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="button-holder">
        <a href="{{ url('admin/tile-server-management/tile_servers_add') }}" class="btn btn-admin">
                Add new server
            </a>
        </div>  
    </div>
@endsection


