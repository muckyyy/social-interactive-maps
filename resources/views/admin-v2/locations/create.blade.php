@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
@endsection

@section('content')
    <div class='container-fluid'>
        @include('admin-v2.locations.partials.header')
        @include('partials.notifications')
        <div  class="row d-flex">
            <form class="col-6 container ml-0" style="text-align:left" action="{{ route('locations_management.store') }} " method="post" enctype="multipart/form-data" autocomplete="off">
                {{ csrf_field() }}
                <div class="form-group d-flex align-items-center">
                    <span class='inputLabel mr-4'>Location Name</span>
                    <div style="position: relative">
                        <input 
                            autocomplete="disabled" 
                            id='searchLocationDuplicatesInput' 
                            onkeyup="searchData(event, 'check-location-duplicates')" 
                            class="input form-control" 
                            type="text" placeholder='Location Name...' 
                            value="{{ old('name') ?: '' }}">
                        <input id="locationName" class="d-none" name="name" type="text" value="{{ old('name') ?: '' }}">
                        <div class="d-none searchResults" id="locationDuplicates"></div>
                    </div>
                </div>
                <div class="form-group d-flex align-items-center">
                    <span class='inputLabel mr-4'>Description</span>
                    <textarea placeholder='Description...' name="description" class="textarea form-control" cols="30" rows="5">
                        {{ old('description') ?: '' }}
                    </textarea>
                </div>
                <div class="form-group d-flex align-items-center">
                    <span class='inputLabel mr-4'>Category</span>
                    <div style="position: relative">
                        <input 
                            autocomplete="disabled" 
                            id='searchCategoriesInput' 
                            onkeyup="searchData(event, 'category')" 
                            class="input form-control" 
                            type="text" placeholder='Category...' 
                            value="{{ old('category_id') ? \App\Category::find('category_id')->name : '' }}">
                        <input id="categoryId" class="d-none" name="category" type="text" value="{{ old('category_id') ?: '' }}">
                        <div class="d-none searchResults" id="categories"></div>
                    </div>
                </div>
                <div class="form-group d-flex align-items-center">
                    <span class='inputLabel mr-4' >Parent Location</span>
                    <div style="position: relative">
                        <input 
                            autocomplete="disabled" 
                            onkeyup="searchData(event, 'location')" 
                            class="input form-control" 
                            id="searchLocationsInput" 
                            type="text" 
                            placeholder='Parent Location...' 
                            value="{{ old('parent_location') ? \App\Location::find('parent_location')->name : '' }}">
                        <input id="parentLocationId" class="d-none" name="parent_location" type="text" value="{{ old('parent_location') ?: '' }}">
                        <div class="d-none searchResults" id="locations"></div>
                    </div>    
                </div>
                <div class="form-group d-flex align-items-center">
                    <span class='inputLabel mr-4'>City</span>
                    <div style="position: relative">
                        <input 
                        autocomplete="disabled" 
                        id='searchCitiesInput' 
                        onkeyup="searchData(event, 'city')" 
                        class="input form-control" 
                        type="text" placeholder='City...' 
                        value="{{ old('city_id') ? \App\City::find('city_id')->name : '' }}">
                        <input id="cityId" class="d-none" name="city" type="text" value="{{ old('city_id') ?: '' }}">
                        <div class="d-none searchResults" id="cities"></div>
                    </div>    
                </div>                
                <div class="form-group d-flex align-items-center">
                    <span class='inputLabel mr-4'>Icon</span>
                    <input id="locations_upload_icon" class="input form-control" name="icon" type="file" placeholder='Icon' value="{{ old('icon') ?: '' }}">
                </div>                
                <div class="form-group d-flex align-items-center">
                    <span class='inputLabel mr-4'>Geopoint</span>
                    <input id="chooseCoordinates" onclick="showMap()" class="input form-control" type="text" placeholder="{{ old('locationLat') ? old('locationLat') . ' ' . old('locationLng') : '' }}" value="">
                    <input class='d-none' id='locationLat' name='locationLat' type="text" value="{{ old('locationLat') }}">
                    <input class='d-none' id='locationLng' name='locationLng' type="text" value="{{ old('locationLng') }}">
                    <!-- <span id="showChosenCoords"></span> -->
                </div>
                <div class="form-group d-flex align-items-center">
                    <span  class='inputLabel mr-4'>Address</span>
                    <input class="input form-control" name="address" type="text" placeholder='Address...' value="{{ old('address') ?: '' }}">
                </div>                
                <div class="form-group d-flex align-items-center">
                    <span  class='inputLabel mr-4'>Post Code</span>
                    <input class="input form-control" name="post_code" type="number" placeholder='Post Code' value="{{ old('post_code') ?: '' }}">
                </div>                
                <div class="form-group d-flex align-items-center">
                    <span  class='inputLabel mr-4'>Phone Number</span>
                    <input class="input form-control" name="phone" type="phone" placeholder='Phone...' value="{{ old('phone') ?: '' }}">
                </div>                
                <div class="form-group d-flex align-items-center">
                    <span  class='inputLabel mr-4'>Images</span>
                    <input class="input form-control" name="images" multiple type="file" placeholder='Images...' value="">
                </div>
                <div class="form-group d-flex align-items-center">
                    <span  class='inputLabel mr-4'>Website</span>
                    <input class="input form-control" name="website" type="text" placeholder='Website...' value="{{ old('website') ?: '' }}">
                </div>
                <div class="form-group d-flex align-items-center">
                    <span  class='inputLabel mr-4'>Type</span>
                    <input class="input form-control" name="type" type="text" placeholder='Type...' value="{{ old('type') ?: '' }}">
                </div>
                <div style="width: 400px" class="text-center">
                    <button type="submit" class="btn btn-admin rounded mt-4 py-2 px-4">Save</button>
                </div>
            </form>
            <div id='map_container'>
                @include('admin-v2.locations.partials.map')
                <div class='d-flex justify-content-center align-items-center mt-4' style='height: 50px'>
                    <span id='saveCoordinates' class='btn btn-submit mx-2 px-4 py-2'>
                        Save
                    </span>
                    <span id='closeMap' class='btn btn-danger mx-2 px-4 py-2' onclick='hideMap(1000)'>
                        Cancel
                    </span>
                </div>
            </div>
        </div>
    </div>
@endsection

   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

   <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

   <script>
    function hideMap(t = 0) {
        coordinate_location = false;
        display_location = false;
        $('#map_container').hide(t);
    }

    $( document ).ready(function() {
        hideMap();
    });

    // open map
    function showMap() {
        $('#map_container').fadeIn(1000);
    }

    let duplicateLocation;
    let categories;
    let cities;

    function searchData(e, option) {
        if(option == 'check-location-duplicates') {
            if (e.target.value.length == 0) {
                $('#locationDuplicates').addClass('d-none');
                return; 
            }

            axios.get(`/api/check-location-duplicates`, { params: {
                keyword: e.target.value
            }}).then(res => {
                locations = res.data;
                $('#locationDuplicates').html('');

                $('#locationDuplicates').append('<h4>Possible duplicates?</h4>');

                locations.forEach(location => {
                    $('#locationDuplicates').removeClass('d-none');
                    $('#locationDuplicates').append(
                        `
                            <a target="_blank" href="/l/${location.id}" class='d-flex align-items-center pointer'>
                                <span class='mr-2'>
                                    <img style='width:20px; height:20px;' style src="${location.icon}" /> 
                                </span> 
                                <span class='h5'>${location.name}</span>
                            </a>
                        `
                        )
                });
            });
        } else if(option == 'category') {
            if (e.target.value.length == 0) {
                $('#categories').addClass('d-none');
                return; 
            }
            axios.get(`/api/search-categories`, { params: {
                keyword: e.target.value
            }}).then(res => {
                categories = res.data;
                $('#categories').html('');

                categories.forEach(category => {
                    $('#categories').removeClass('d-none');
                    $('#categories').append(
                        `
                            <div onclick="selectCategory(${category.id})" class='d-flex align-items-center pointer'>
                                <span style='width:20px; height:20px; margin-bottom: -6px;' class='mr-2'>${category.icon || ''}</span> 
                                <span class='h5'>${category.name}</span>
                            </div>
                        `
                        )
                });
            })
        } else if(option == 'location') {
            if (e.target.value.length == 0) {
                $('#locations').addClass('d-none');
                return; 
            }
            axios.get(`/api/search-locations`, { params: {
                keyword: e.target.value
            }}).then(res => {
                locations = res.data;
                $('#locations').html('');

                locations.forEach(location => {
                    $('#locations').removeClass('d-none');
                    $('#locations').append(
                        `
                            <div onclick="selectLocation(${location.id})" class='d-flex align-items-center pointer'>
                                <span class='mr-2'>
                                    <img style='width:20px; height:20px; margin-bottom: -6px;' style src="${location.icon}" /> 
                                </span> 
                                <span class='h5'>${location.name}, ${location.city}</span>
                            </div>
                        `
                        )
                });
            })
        } 
        else {
            if (e.target.value.length == 0) {
                $('#cities').addClass('d-none');
                return; 
            }
            axios.get(`/api/search-cities`, { params: {
                keyword: e.target.value
            }}).then(res => {
                cities = res.data;
                $('#cities').html('');

                cities.forEach(city => {
                    $('#cities').removeClass('d-none');
                    $('#cities').append(
                        `
                            <div onclick="selectCity(${city.id})" class='d-flex align-items-center pointer searchDataResults'>
                                <span style='width:20px; height:20px; margin-bottom: -6px;' class='mr-2'>${city.icon || ''}</span> 
                                <span class='h5'>${city.name}</span>
                            </div>
                        `
                        )
                });
            })
        }
    }

    function selectCategory(id) {
        let category = categories.filter(c => {
            return c.id == id;
        });
        document.getElementById('categoryId').value = id;
        document.getElementById('searchCategoriesInput').value = category[0].name;
        $('#categories').addClass('d-none');
    }

    function selectCity(id) {
        let city = cities.filter(c => {
            return c.id == id;
        });
        document.getElementById('cityId').value = id;
        document.getElementById('searchCitiesInput').value = city[0].name;
        $('#cities').addClass('d-none');
    }

    function selectLocation(id) {
        let location = locations.filter(c => {
            return c.id == id;
        });
        console.log(location[0]);
        document.getElementById('parentLocationId').value = id;
        document.getElementById('searchLocationsInput').value = location[0].name;
        $('#locations').addClass('d-none');
    }

    window.onload = function () {
        document.getElementById('saveCoordinates').addEventListener('click', (e) => {
            let lat = map.getCenter().lat;
            let lng = map.getCenter().lng;
            let locationLat = document.getElementById('locationLat').value = lat;
            let locationLng = document.getElementById('locationLng').value = lng;
            let coordinatesInput = document.getElementById('chooseCoordinates');
            coordinatesInput.placeholder = lat + ' ' + lng;
            coordinatesInput.classList.add('input-border-primary');
            // document.getElementById('showChosenCoords').innerText = lat + ' ' + lng;
            hideMap(1000);
        })
    }
   </script>


