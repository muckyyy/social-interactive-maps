@extends('admin-v2.index')

@section('content')
    <div id="beacon_administration" style="width:100%;">
        @include('admin-v2.locations.partials.header')
        @include('partials.notifications')
        <div class="text-right">
            <a href="{{ route('locations_management.create') }}" class="btn btn-admin btn-rounded">Add New Location</a>
        </div>
        <table class="admin_table">
            <tr>
                <th style='width:80px'>Icon</th>
                <th>Location Name</th>
                <th>City</th>
                <th>Geopoint</th>
                <th style="width: 150px">Actions</th>
            </tr>
            <tbody>
                @forelse($locations as $location)
                    <tr>
                        <td><img src="{{$location->icon}}"></td>
                        <td>{{$location->name}}</td>
                        <td>{{ \App\City::find($location->city_id)->name}}</td>
                        <td>{{$location->location}}</td>
                        <td class="actions">
                            <div class="d-flex justify-content-center">
                                <a href="{{ route('locations_management.edit', $location->id) }}">
                                    <i title="settings" class="fas fa-edit"></i>
                                </a> 
                                <form action="{{ route('locations_management.destroy', $location->id) }}" method='post'>
                                    {{ csrf_field() }}
                                    <button type='submit' title="delete" class="fas fa-trash-alt" style='background: none; border: none'></button>
                                </form>
                            </div>
                        </td>
                    </tr> 
                @empty
                    <tr>
                        <td>No Locations yet</td>
                    </tr>    
                @endforelse    
            </tbody>
        </table> 
        {{ $locations->links() }}
    </div>
@endsection



