<div style='position: relative'>
	<div id="mapid"></div>
	<div class="d-flex justify-content-center align-items-center" id="coordinates">
		<span id="lat" class='mr-2'></span>
		<span id="lng" class='ml-4'></span>
	</div>
</div>
<script>
	let geolocation = [8.67, 50.09]

	var map = L.map('mapid', { attributionControl: false }).setView([geolocation[1], geolocation[0]], 12);
    
    document.getElementById('lat').innerText = map.getCenter().lat.toFixed(4);
	document.getElementById('lng').innerText = map.getCenter().lng.toFixed(4);
  
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    var LeafIcon = L.Icon.extend({
        options: {
            iconSize:     [80, 81],
            iconAnchor:   [22, 94],
            popupAnchor:  [-3, -76]
        }
    });

    var pin = new LeafIcon({iconUrl: '/img/pins/pngfuel.com.png'});
	var marker = L.marker([geolocation[1], geolocation[0]], {icon: pin}).addTo(map);

    // current coordinates
    map.addEventListener('move', function(ev) {
        let lat = map.getCenter().lat;
        let lng =  map.getCenter().lng;
        document.getElementById('lat').innerText = lat.toFixed(4);
        document.getElementById('lng').innerText = lng.toFixed(4);
        if(marker) {
            map.removeLayer(marker)
            marker = new L.marker([lat, lng], {icon: pin}).addTo(map);
        }
    });
</script>