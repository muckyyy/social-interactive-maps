<!DOCTYPE html>
<html>
    <head>
        <!-- Title -->
        <title>Avatars map admin section</title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href="{{asset('admin-v2/assets/plugins/pace-master/themes/blue/pace-theme-flash.css')}}" rel="stylesheet"/>
        <link href="{{asset('admin-v2/assets/plugins/uniform/css/uniform.default.min.css')}}" rel="stylesheet"/>
        <link href="{{asset('admin-v2/assets/plugins/line-icons/simple-line-icons.css')}}" rel="stylesheet" type="text/css"/> 
        <link href="{{asset('admin-v2/assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css')}}" rel="stylesheet" type="text/css"/>  
        <link href="{{asset('admin-v2/assets/plugins/waves/waves.min.css')}}" rel="stylesheet" type="text/css"/>  
        <link href="{{asset('admin-v2/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin-v2/assets/plugins/3d-bold-navigation/css/style.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('admin-v2/assets/plugins/slidepushmenus/css/component.css')}}" rel="stylesheet" type="text/css"/> 
        <link href="{{asset('admin-v2/assets/plugins/weather-icons-master/css/weather-icons.min.css')}}" rel="stylesheet" type="text/css"/>   
        <link href="{{asset('admin-v2/assets/plugins/metrojs/MetroJs.min.css')}}" rel="stylesheet" type="text/css"/>  
        <link href="{{asset('admin-v2/assets/plugins/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>   
        
        {{-- bootstrap --}}
        <link href="{{asset('admin-v2/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        
        <!-- Theme Styles -->
        <link href="{{ asset('admin-v2/assets/css/modern.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('admin-v2/assets/css/themes/green.css')}}" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('admin-v2/assets/css/custom.css')}}" rel="stylesheet" type="text/css"/>
        
        <script src="{{ asset('admin-v2/assets/plugins/3d-bold-navigation/js/modernizr.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/offcanvasmenueffects/js/snap.svg-min.js') }}"></script>

        <link href="{{ asset('css/admivV2.css') }}" rel="stylesheet">

        

        @if(strpos(url()->current(), 'beacon_administration'))
            {{-- LEAFLET MAP FOR BEACON ADMINISTRATION SETTINGS --}}
            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
            <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
            <style>.page-inner { height: 1020px !important;}</style>
        @endif

        <style>
            .page-footer {
                position: fixed;
                bottom: 0;
            }
        </style>
    
        @yield('per-page-css')

    </head>
    <body class="page-header-fixed">
        <div class="overlay"></div>

        @if (Session::has('flash_message'))
            <div class="master_message">{{ Session::get('flash_message') }}</div>
        @endif
        @if (Session::has('flash_message_error'))
            <div class="master_message_error">{{ Session::get('flash_message_error') }}</div>
        @endif

        <!-- Top nav -->
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner w-100">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="{{ url('/') }}" class="logo-text"><span>Avatar map</span></a>
                    </div>
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-left" style="display:inline;">
                                <li>        
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                                </li>
                                <li>        
                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                                </li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right" style="display:inline;">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown" style="float:right;"><i class="fa fa-bell"></i><span class="badge badge-success pull-right">3</span></a>
                                    <ul class="dropdown-menu title-caret dropdown-lg" role="menu">
                                        <li><p class="drop-title">You have 3 pending tasks !</p></li>
                                        <li class="dropdown-menu-list slimscroll tasks">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="#">
                                                        <div class="task-icon badge badge-success"><i class="icon-user"></i></div>
                                                        <span class="badge badge-roundless badge-default pull-right">1min ago</span>
                                                        <p class="task-details">New user registered.</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="task-icon badge badge-danger"><i class="icon-energy"></i></div>
                                                        <span class="badge badge-roundless badge-default pull-right">24min ago</span>
                                                        <p class="task-details">Database error.</p>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="task-icon badge badge-info"><i class="icon-heart"></i></div>
                                                        <span class="badge badge-roundless badge-default pull-right">1h ago</span>
                                                        <p class="task-details">Reached 24k likes</p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="drop-all"><a href="#" class="text-center">All Tasks</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                                        <span class="user-name">{{Auth::user()->name}}</span>
                                        @if (!(Auth::user()->profile_img))
                                            <img class="img-circle avatar" src="/img/no-image.png" width="40" height="40" alt="">
                                        @else
                                            <img class="img-circle avatar" src="/uploads/{{Auth::user()->profile_img}}" width="40" height="40" alt="">
                                        @endif
                                    </a>
                                    <ul class="dropdown-menu dropdown-list" role="menu">
                                        <li role="presentation">
                                            <a href="{{ route('home.index') }}">Home</a>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                {{-- <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li> --}}
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Top nav end -->



            <!-- Navigation left -->
            <div id="adminSidebar" class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <div class="sidebar-header">
                        <div class="sidebar-profile">
                            <a href="javascript:void(0);" id="profile-menu-link">
                                <div class="sidebar-profile-image">
                                    @if (!(Auth::user()->profile_img))
                                        <img src="/img/no-image.png" class="img-circle img-responsive" alt="">
                                    @else
                                        <img src="/uploads/{{Auth::user()->profile_img}}" class="img-circle img-responsive" alt="">
                                    @endif
                                </div>
                                <div class="sidebar-profile-details">
                                    <span>{{Auth::user()->name}}<br></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <ul class="menu accordion-menu">

                        <li class="{{ \Request::route()->getName() == 'dashboard' ? 'active' : '' }}">
                            <a href="{{ url('admin') }}" class="waves-effect waves-button">
                                <span class="menu-icon glyphicon glyphicon-home"></span>
                                <p>Dashboard</p>
                            </a>
                        </li>

                        <li class="{{ strpos(url()->current(), 'category_management') ? 'active' : '' }}">
                            <a href="{{ url('admin/category_management') }}" class="waves-effect waves-button">
                                <span class="menu-icon glyphicon glyphicon-list"></span>
                                <p>Category management</p>
                            </a>
                        </li>

                        {{-- <li class="{{ \Request::route()->getName() == 'avatar_settings' ? 'active' : '' }}">
                            <a href="{{ url('admin/avatar_settings') }}" class="waves-effect waves-button">
                                <span class="menu-icon glyphicon glyphicon-user"></span>
                                <p>Avatar settings</p>
                            </a>
                        </li> --}}

                        <li class="{{ \Request::route()->getName() == 'lvl-icons' ? 'active' : '' }}">
                            <a href="{{ url('admin/lvl-icons') }}" class="waves-effect waves-button">
                                <span class="menu-icon glyphicon glyphicon-user"></span>
                                <p>Icons segment</p>
                            </a>
                        </li>
                        
                        <li class="{{ \Request::route()->getName() == 'tile-server-management' ? 'active' : (\Request::route()->getName() == 'tile_servers_add' ? 'active' : '') }}">
                            <a href="{{ url('admin/tile-server-management') }}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-th"></span><p>Tile server management</p></a>
                        </li>

                        @if(\Request::is('admin/map_settings'))
                            <li class="active">
                        @elseif(\Request::is('admin/map_settings/*'))
                            <li class="active">
                        @else 
                            <li>    
                        @endif
                            <a href="{{ url('admin/map_settings') }}" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-map-marker"></span><p>Map settings</p></a>
                        </li>

                        <li class="{{ strpos(url()->current(), 'beacon_administration') ? 'active' : '' }}">
                            <a href="{{ url('admin/beacon_administration') }}" class="waves-effect waves-button">
                                <span class="menu-icon glyphicon glyphicon-map-marker"></span>
                                <p>Beacon administration</p>
                            </a>
                        </li>

                        <li class="{{ strpos(url()->current(), 'locations') ? 'active' : '' }}">
                            <a href="{{ url('admin/locations') }}" class="waves-effect waves-button">
                            <span class="menu-icon icon-eye"></span>
                            <p>Locations management</p></a>
                        </li>

                        <li class="{{ strpos(url()->current(), 'admin/users') ? 'active' : '' }}">
                            <a href="{{ url('admin/users') }}" class="waves-effect waves-button">
                                <span class="menu-icon icon-users"></span>
                                <p>Users</p>
                            </a>
                        </li>
                        <li class="{{ strpos(url()->current(), 'admin/activity') ? 'active' : '' }}">
                            <a href="{{ url('admin/activity') }}" class="waves-effect waves-button">
                                <span class="menu-icon icon-bar-chart"></span>
                                <p>Activity</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <!-- Navigation left end -->
        <div class="page-inner">
                <div class="page-title">
                    <h3>
                        @if(\Request::route()->getName() == 'dashboard')
                            Dashboard
                        @elseif(str_contains(url()->current(), '/category_management'))  
                            Category Management
                        @elseif(\Request::route()->getName() == 'tile-server-management')
                            Tile Server Management
                        @elseif(\Request::route()->getName() == 'lvl-icons')   
                            Icons Segment
                        @elseif(str_contains(url()->current(), '/map_settings'))   
                            Map Settings
                        @elseif(str_contains(url()->current(), '/beacon_administration'))
                            Beacon Administration
                        @elseif(str_contains(url()->current(), '/admin/users'))   
                            Users
                        @elseif(str_contains(url()->current(), '/admin/activity'))
                            Activity
                        @endif
                    </h3>
                </div>
                <div id="main-wrapper">
                    <div id="admin-v2-pages" class="row">
                        @yield('content')
                    </div>
                </div><!-- Main Wrapper -->
                <div class="page-footer" style="text-align: center; padding-bottom: 0;">
                    <p class="no-s">{{ date('Y') }} &copy; Avatars map</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->

        <div class="cd-overlay"></div>

        <!-- Javascripts -->
        <script src="{{ asset('admin-v2/assets/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/pace-master/pace.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/jquery-blockui/jquery.blockui.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/uniform/jquery.uniform.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/offcanvasmenueffects/js/classie.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/offcanvasmenueffects/js/main.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/waves/waves.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/3d-bold-navigation/js/main.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/waypoints/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/jquery-counterup/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/flot/jquery.flot.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/flot/jquery.flot.time.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/flot/jquery.flot.symbol.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/flot/jquery.flot.resize.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/curvedlines/curvedLines.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/plugins/metrojs/MetroJs.min.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/js/modern.js') }}"></script>
        <script src="{{ asset('admin-v2/assets/js/pages/dashboard.js') }}"></script>

        @yield('admin_scripts')

        <script>
            $('.master_message').delay(3000).fadeOut(1500);
            $('.master_message_error').delay(3000).fadeOut(1500);
        </script>

        @yield('admin-scripts')
        
    </body>
</html>