@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
    <div id="avatarSettings" class="sliders container slider-pages">
        <div class="title">
            <span> Reach Slider </span>
        </div>
        <div id="reachSlider">
            <div id="reach-handle" class="ui-slider-handle"></div>
        </div>

        <div class="title">
            <span> Privacy Slider </span>
        </div>
        <div id="privacySlider">
            <div id="privacy-handle" class="ui-slider-handle"></div>
        </div>
        <div class="buttonHolder">
            <button class="btn btn-admin">Save</button>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $( function() {
        // Reach Slider
        
        let handle = $( "#reach-handle" );
        $( "#reachSlider" ).slider({
        create: function() {
            handle.text( $( this ).slider( "value" ) );
        },
        slide: function( event, ui ) {
            handle.text( ui.value );
        }
        });
    });

    $( function() {
         // Privacy Slider
         let handle = $( "#privacy-handle" );
        $( "#privacySlider" ).slider({
        create: function() {
            handle.text( $( this ).slider( "value" ) );
        },
        slide: function( event, ui ) {
            handle.text( ui.value );
        }
        });
    });
</script>