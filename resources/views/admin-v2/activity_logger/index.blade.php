@extends('admin-v2.index')

@section('content')
    <div class="map_settings">
        <table>
            <tr>
                <th>Log name</th>
                <th>Descrioption</th>
                <th>Subject Id</th>
                <th>subject Type</th>
                <th>Causer id</th>
                <th>Causer Type</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
            @foreach($activities as $activity)
                <tr>
                    <td>{{ $activity->log_name }}</td>
                    <td>{{ $activity->description }}</td>
                    <td>{{ $activity->subject_id }}</td>
                    <td>{{ $activity->subject_type }}</td>
                    <td>{{ $activity->causer_id }}</td>
                    <td>{{ $activity->causer_type }}</td>
                    <td>{{ $activity->created_at }}</td>
                    <td>{{ $activity->updated_at }}</td>
                </tr>
            @endforeach
        </table>  
        <div class='text-center'>
            {{ $activities->links() }}
        </div> 
    <div>
@endsection    

