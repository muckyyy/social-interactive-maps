@extends('admin-v2.index')

@section('per-page-css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
@endsection

@section('content')
    
    <div class="icon-wraper-section">

        <div class="custom-icons-section">

            <div class="custom-icons-title" style="margin-top:15px;margin-bottom:10px;font-size:20px;font-weight:600;clear:both;">
                HIGH RESOLUTION ICONS
            </div>

            <div class="custom-icons">
                <img src="/svg/flash_on.svg" title="flash_on.svg"/>
                <img src="/svg/flash_off.svg" title="flash_off.svg"/>
                <img src="/svg/flash_auto.svg" title="flash_auto.svg"/>
                <img src="/svg/panorama.svg" title="panorama.svg"/>
                <img src="/svg/landscape.svg" title="landscape.svg"/>
                <img src="/svg/night_landscape.svg" title="night_landscape.svg"/>
                <img src="/svg/sports_mode.svg" title="sports_mode.svg"/>
                <img src="/svg/portrait_mode.svg" title="portrait_mode.svg"/>
                <img src="/svg/night_portrait.svg" title="night_portrait.svg"/>
                <img src="/svg/close_up_mode.svg" title="close_up_mode.svg"/>
                <img src="/svg/selfie.svg" title="selfie.svg"/>
                <img src="/svg/gallery.svg" title="gallery.svg"/>
                <img src="/svg/stack_of_photos.svg" title="stack_of_photos.svg"/>
                <img src="/svg/add_image.svg" title="add_image.svg"/>
                <img src="/svg/edit_image.svg" title="edit_image.svg"/>
                <img src="/svg/remove_image.svg" title="remove_image.svg"/>
                <img src="/svg/compact_camera.svg" title="compact_camera.svg"/>
                <img src="/svg/multiple_cameras.svg" title="multiple_cameras.svg"/>
                <img src="/svg/camera.svg" title="camera.svg"/>
                <img src="/svg/slr_back_side.svg" title="slr_back_side.svg"/>
                <img src="/svg/old_time_camera.svg" title="old_time_camera.svg"/>
                <img src="/svg/camera_addon.svg" title="camera_addon.svg"/>
                <img src="/svg/camera_identification.svg" title="camera_identification.svg"/>
                <img src="/svg/start.svg" title="start.svg"/>
                <img src="/svg/clapperboard.svg" title="clapperboard.svg"/>
                <img src="/svg/film.svg" title="film.svg"/>
                <img src="/svg/camcorder.svg" title="camcorder.svg"/>
                <img src="/svg/camcorder_pro.svg" title="camcorder_pro.svg"/>
                <img src="/svg/webcam.svg" title="webcam.svg"/>
                <img src="/svg/integrated_webcam.svg" title="integrated_webcam.svg"/>
                <img src="/svg/rotate_camera.svg" title="rotate_camera.svg"/>
                <img src="/svg/switch_camera.svg" title="switch_camera.svg"/>
                <img src="/svg/photo_reel.svg" title="photo_reel.svg"/>
                <img src="/svg/film_reel.svg" title="film_reel.svg"/>
                <img src="/svg/cable_release.svg" title="cable_release.svg"/>

                <img src="/svg/home.svg" title="home.svg"/>
                <img src="/svg/icons8_cup.svg" title="icons8_cup.svg"/>
                <img src="/svg/globe.svg" title="globe.svg"/>
                <img src="/svg/ok.svg" title="ok.svg"/>
                <img src="/svg/checkmark.svg" title="checkmark.svg"/>
                <img src="/svg/cancel.svg" title="cancel.svg"/>
                <img src="/svg/synchronize.svg" title="synchronize.svg"/>
                <img src="/svg/refresh.svg" title="refresh.svg"/>
                <img src="/svg/download.svg" title="download.svg"/>
                <img src="/svg/upload.svg" title="upload.svg"/>
                <img src="/svg/empty_trash.svg" title="empty_trash.svg"/>
                <img src="/svg/full_trash.svg" title="full_trash.svg"/>
                <img src="/svg/folder.svg" title="folder.svg"/>
                <img src="/svg/opened_folder.svg" title="opened_folder.svg"/>
                <img src="/svg/file.svg" title="file.svg"/>
                <img src="/svg/document.svg" title="document.svg"/>
                <img src="/svg/audio_file.svg" title="audio_file.svg"/>
                <img src="/svg/image_file.svg" title="image_file.svg"/>
                <img src="/svg/video_file.svg" title="video_file.svg"/>
                <img src="/svg/print.svg" title="print.svg"/>
                <img src="/svg/music.svg" title="music.svg"/>
                <img src="/svg/share.svg" title="share.svg"/>
                <img src="/svg/cursor.svg" title="cursor.svg"/>
                <img src="/svg/puzzle.svg" title="puzzle.svg"/>
                <img src="/svg/unlock.svg" title="unlock.svg"/>
                <img src="/svg/lock.svg" title="lock.svg"/>
                <img src="/svg/idea.svg" title="idea.svg"/>
                <img src="/svg/no_idea.svg" title="no_idea.svg"/>
                <img src="/svg/link.svg" title="link.svg"/>
                <img src="/svg/broken_link.svg" title="broken_link.svg"/>
                <img src="/svg/rating.svg" title="rating.svg"/>
                <img src="/svg/like_placeholder.svg" title="like_placeholder.svg"/>
                <img src="/svg/like.svg" title="like.svg"/>
                <img src="/svg/dislike.svg" title="dislike.svg"/>
                <img src="/svg/info.svg" title="info.svg"/>
                <img src="/svg/about.svg" title="about.svg"/>
                <img src="/svg/picture.svg" title="picture.svg"/>
                <img src="/svg/clock.svg" title="clock.svg"/>
                <img src="/svg/alarm_clock.svg" title="alarm_clock.svg"/>
                <img src="/svg/address_book.svg" title="address_book.svg"/>
                <img src="/svg/contacts.svg" title="contacts.svg"/>
                <img src="/svg/news.svg" title="news.svg"/>
                <img src="/svg/bookmark.svg" title="bookmark.svg"/>
                <img src="/svg/binoculars.svg" title="binoculars.svg"/>
                <img src="/svg/search.svg" title="search.svg"/>
                <img src="/svg/ruler.svg" title="ruler.svg"/>
                <img src="/svg/services.svg" title="services.svg"/>
                <img src="/svg/settings.svg" title="settings.svg"/>
                <img src="/svg/support.svg" title="support.svg"/>
                <img src="/svg/frame.svg" title="frame.svg"/>
                <img src="/svg/menu.svg" title="menu.svg"/>
                <img src="/svg/key.svg" title="key.svg"/>
                <img src="/svg/calendar.svg" title="calendar.svg"/>
                <img src="/svg/calculator.svg" title="calculator.svg"/>
                <img src="/svg/minus.svg" title="minus.svg"/>
                <img src="/svg/plus.svg" title="plus.svg"/>

                <img src="/svg/graduation_cap.svg" title="graduation_cap.svg"/>
                <img src="/svg/briefcase.svg" title="briefcase.svg"/>
                <img src="/svg/business.svg" title="business.svg"/>
                <img src="/svg/signature.svg" title="signature.svg"/>
                <img src="/svg/safe.svg" title="safe.svg"/>
                <img src="/svg/advertising.svg" title="advertising.svg"/>
                <img src="/svg/businessman.svg" title="businessman.svg"/>
                <img src="/svg/businesswoman.svg" title="businesswoman.svg"/>
                <img src="/svg/manager.svg" title="manager.svg"/>
                <img src="/svg/online_support.svg" title="online_support.svg"/>
                <img src="/svg/assistant.svg" title="assistant.svg"/>
                <img src="/svg/customer_support.svg" title="customer_support.svg"/>
                <img src="/svg/reading_ebook.svg" title="reading_ebook.svg"/>
                <img src="/svg/reading.svg" title="reading.svg"/>
                <img src="/svg/neutral_trading.svg" title="neutral_trading.svg"/>
                <img src="/svg/bullish.svg" title="bullish.svg"/>
                <img src="/svg/bearish.svg" title="bearish.svg"/>
                <img src="/svg/high_priority.svg" title="high_priority.svg"/>
                <img src="/svg/medium_priority.svg" title="medium_priority.svg"/>
                <img src="/svg/low_priority.svg" title="low_priority.svg"/>
                <img src="/svg/make_decision.svg" title="make_decision.svg"/>
                <img src="/svg/decision.svg" title="decision.svg"/>
                <img src="/svg/good_decision.svg" title="good_decision.svg"/>
                <img src="/svg/neutral_decision.svg" title="neutral_decision.svg"/>
                <img src="/svg/bad_decision.svg" title="bad_decision.svg"/>
                <img src="/svg/approve.svg" title="approve.svg"/>
                <img src="/svg/disapprove.svg" title="disapprove.svg"/>
                <img src="/svg/podium_without_speaker.svg" title="podium_without_speaker.svg"/>
                <img src="/svg/podium_with_speaker.svg" title="podium_with_speaker.svg"/>
                <img src="/svg/podium_with_audience.svg" title="podium_with_audience.svg"/>
                <img src="/svg/video_projector.svg" title="video_projector.svg"/>
                <img src="/svg/statistics.svg" title="statistics.svg"/>
                <img src="/svg/collaboration.svg" title="collaboration.svg"/>
                <img src="/svg/voice_presentation.svg" title="voice_presentation.svg"/>
                <img src="/svg/conference_call.svg" title="conference_call.svg"/>
                <img src="/svg/comments.svg" title="comments.svg"/>
                <img src="/svg/faq.svg" title="faq.svg"/>
                <img src="/svg/reuse.svg" title="reuse.svg"/>
                <img src="/svg/organization.svg" title="organization.svg"/>
                <img src="/svg/department.svg" title="department.svg"/>
                <img src="/svg/library.svg" title="library.svg"/>
                <img src="/svg/shop.svg" title="shop.svg"/>
                <img src="/svg/self_service_kiosk.svg" title="self_service_kiosk.svg"/>
                <img src="/svg/donate.svg" title="donate.svg"/>
                <img src="/svg/currency_exchange.svg" title="currency_exchange.svg"/>
                <img src="/svg/debt.svg" title="debt.svg"/>
                <img src="/svg/sales_performance.svg" title="sales_performance.svg"/>
                <img src="/svg/invite.svg" title="invite.svg"/>
                <img src="/svg/money_transfer.svg" title="money_transfer.svg"/>
                <img src="/svg/feedback.svg" title="feedback.svg"/>
                <img src="/svg/approval.svg" title="approval.svg"/>
                <img src="/svg/paid.svg" title="paid.svg"/>
                <img src="/svg/in_transit.svg" title="in_transit.svg"/>
                <img src="/svg/shipped.svg" title="shipped.svg"/>
                <img src="/svg/package.svg" title="package.svg"/>
                <img src="/svg/planner.svg" title="planner.svg"/>
                <img src="/svg/overtime.svg" title="overtime.svg"/>
                <img src="/svg/leave.svg" title="leave.svg"/>
                <img src="/svg/expired.svg" title="expired.svg"/>
                <img src="/svg/process.svg" title="process.svg"/>
                <img src="/svg/diploma_1.svg" title="diploma_1.svg"/>
                <img src="/svg/diploma_2.svg" title="diploma_2.svg"/>
                <img src="/svg/business_contact.svg" title="business_contact.svg"/>
                <img src="/svg/survey.svg" title="survey.svg"/>
                <img src="/svg/inspection.svg" title="inspection.svg"/>
                <img src="/svg/rules.svg" title="rules.svg"/>
                <img src="/svg/todo_list.svg" title="todo_list.svg"/>
                <img src="/svg/ratings.svg" title="ratings.svg"/>
                <img src="/svg/questions.svg" title="questions.svg"/>
                <img src="/svg/answers.svg" title="answers.svg"/>
                <img src="/svg/fine_print.svg" title="fine_print.svg"/>
                <img src="/svg/candle_sticks.svg" title="candle_sticks.svg"/>
                <img src="/svg/serial_tasks.svg" title="serial_tasks.svg"/>
                <img src="/svg/parallel_tasks.svg" title="parallel_tasks.svg"/>
                <img src="/svg/tree_structure.svg" title="tree_structure.svg"/>
                <img src="/svg/org_unit.svg" title="org_unit.svg"/>
                <img src="/svg/privacy.svg" title="privacy.svg"/>
                <img src="/svg/disclaimer.svg" title="disclaimer.svg"/>
                <img src="/svg/callback.svg" title="callback.svg"/>
                <img src="/svg/service_mark.svg" title="service_mark.svg"/>
                <img src="/svg/registered_trademark.svg" title="registered_trademark.svg"/>
                <img src="/svg/trademark.svg" title="trademark.svg"/>
                <img src="/svg/copyright.svg" title="copyright.svg"/>
                <img src="/svg/copyleft.svg" title="copyleft.svg"/>
                <img src="/svg/sound_recording_copyright.svg" title="sound_recording_copyright.svg"/>
                <img src="/svg/butting_in.svg" title="butting_in.svg"/>
                <img src="/svg/multiple_inputs.svg" title="multiple_inputs.svg"/>
                <img src="/svg/collect.svg" title="collect.svg"/>
                <img src="/svg/internal.svg" title="internal.svg"/>
                <img src="/svg/external.svg" title="external.svg"/>
                <img src="/svg/vip.svg" title="vip.svg"/>
                <img src="/svg/light_at_the_end_of_tunnel.svg" title="light_at_the_end_of_tunnel.svg"/>
                <img src="/svg/entering_heaven_alive.svg" title="entering_heaven_alive.svg"/>
            </div>

        </div>

        <div class="add-new-icon-section">

            <div class="custom-icons-title" style="margin-top:30px;margin-bottom:10px;font-size:20px;font-weight:600;clear:both;">
                ADD CUSTOM ICON
            </div>

            <div class="custom-upload-img">

                <form class="form-horizontal" method="POST" action="/upload-admin-icon" enctype="multipart/form-data">
                    {{ csrf_field() }}

                        <input class="form-control" type="file" name="icon_file">

                        <input style="margin-top: 15px;border-radius: 8px;" class="btn btn-admin" type="submit" value="Upload">
                </form>
            </div>

            <div class="custom-icons-title" style="margin-top:30px;margin-bottom:10px;font-size:20px;font-weight:600;clear:both;">
                CUSTOM ICONS LIST
            </div>

            <div class="custom-admin-icon-list">
                @foreach($icons as $icon)
                    {!! $icon->tag !!}
                @endforeach
            </div>

        </div>

    </div>




@endsection

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>