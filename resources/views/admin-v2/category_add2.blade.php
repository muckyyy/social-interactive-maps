@extends('admin-v2.index')

@section('content')
    <div id="category-add" class="container">
        <h1>Add a category</h1>

        <div class="main-admin-section col-xs-12">
            <form class="form-horizontal" method="POST" action="/add-category" enctype="multipart/form-data">
                <div class="col-xs-6">
                    {{ csrf_field() }}
                    <div class="file-icon-text" style="margin-top: 15px;">
                        Category Name
                    </div>

                    <input type="text" class="form-control" name="name" placeholder="Name" required="required">

                    <div class="file-icon-text" style="margin-top: 15px;">
                        Parent category
                    </div>

                    <select name="parent_id" class="form-control">
                        <option value="0">None (Master)</option>
                        @foreach($categories as $category)
                            <option class="admin-select-li" value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
    
                    <input type="hidden" name="icon" id='admin-category-icon' required="required">
    
                    <div class="file-icon-section col-xs-12 no-padding">
    
                        <div class="file-icon-text">
                            Custom icon
                        </div>
    
                        <input type="file" name="icon_file">
                    </div>
    
                    <div class="file-icon-text col-xs-12 no-padding" style="margin-top: 15px;">
                        <div class='col-xs-12 no-padding'>
                            Force Child:
                        </div>
                        <input style="width: 30%;" class="form-control col-xs-2" type="checkbox" name="force_child" value="1" checked="checked">
                    </div>
    
                    <div class="category-tags-section col-xs-12 no-padding" style="margin-top: 15px;font-size: 20px;font-weight: 600;color: #636b6f;">
                        Tags
                        <input type="tags" name="tags" class="form-control" placeholder="For example #travel #epic #stuff">
                    </div>
                    {{-- <input type="submit" class="btn btn-admin" value="Add" style="float:right;width:100%;margin-top: 30px;"> --}}
                </div>

                <div class="col-xs-6">
                    <div class='col-xs-12 no-padding' style="margin-top: 15px;">
                        Add custom field:
                    </div>
        
                    <input class="form-control" type="text" name="name" placeholder="Field name" required="required" style="margin-bottom: 15px;">

                    <div style="font-size: 16px;font-weight: 600;">
                        Choose type
                    </div>
                    <select class="form-control" name="type" id="typeselector" style="margin-bottom: 15px;">
                        <option value="text">Text</option>
                        <option value="largeText">Large text</option>
                        <option value="number">Number</option>
                        <option value="image">Image</option>
                    </select>

                    <div class="field-type" id="text">
                        <input class="form-control" type="text" name="text" placeholder="Insert text...">
                    </div>

                    <div class="field-type" id="largeText">
                        <textarea class="form-control" name="largeText">Insert text...</textarea>
                    </div>

                    <div class="field-type" id="number">
                        <input class="form-control" type="number" name="number" placeholder="Insert number...">
                    </div>

                    <div class="field-type" id="image">
                        <input class="form-control" type="file" name="image">
                    </div>
                </div>
                <span class="submitBtn">
                    <input class="btn btn-admin " type="submit" value="Add">
                </span>
            </form>
        </div>  
        <div class="navigation-section-stabilizer">
            <div class="container">
                <div class="category-icons-title" style="margin-bottom: 30px;">
                    Add icons for category {{$category->name}}
                </div>
                <div class="col-xs-12 admin-icons-list">
                    @for( $i = 1; $i <= 43; $i++)
                        <div class="admin-icon">
                            <img src="/img/category-icons/icon{{$i}}.svg" />
                        </div>
                    @endfor
                </div>
                <button id='icon-admin-btn' class="col-xs-offset-4 col-xs-4 btn btn-admin" style="margin-top: 60px;">
                    FINISH
                </button>
            </div>
        </div>
            
{{-- <script>

    document.getElementById("icon-admin-btn").disabled = true;

    $('.admin-icon').click(function(e){

        if($(this).hasClass("iconActive")) {

            let tempIcon = (e.currentTarget.children[0].currentSrc).slice(-10)
            tempIcon = tempIcon.replace('/', '');

            for(let i=0; i<iconSet.length; i++){
                if(iconSet[i] == tempIcon) {
                    iconSet.splice(i, 1);
                }
            }
            $(this).removeClass('iconActive')

        } else {
            $(this).addClass('iconActive')
            let addIcon = (e.currentTarget.children[0].currentSrc).slice(-10)
            addIcon = addIcon.replace('/', '');
            iconSet.push(addIcon)	
        }
        console.log('iconset', iconSet)
        if(iconSet == null || iconSet == ''){
            document.getElementById("icon-admin-btn").disabled = true;
        } else {
            document.getElementById("icon-admin-btn").disabled = false;
        }
    })
    $('#icon-admin-btn').click(function(){
        $.ajax
        ({
        dataType: 'json',
        type: "GET",
        url: "/admin-category-icons-post",
        data: {"iconSet": iconSet, "id":categoryId},
            success: function(data)
            {
                console.log('data',data)
                window.location.href = "/admin";
            }
        });
    })
</script> --}}

@endsection


