<script>
    let user = {};
    user = {!!auth()->user() !!};
    let locations = {!!json_encode($locations->toArray()) !!};
    let userPost = {!!json_encode($userPost) !!}
    let mainUserLastSeen = {!!json_encode($lastSeen) !!}
    let city = {!!json_encode($city) !!}
    let map_layers = {!!json_encode($map_layers) !!};

    let other_avatars_layer;
    map_layers.forEach(layer => {
        if (layer.short_name == 'other_avatars_layer') {
            other_avatars_layer = layer;
        }
    })
</script>