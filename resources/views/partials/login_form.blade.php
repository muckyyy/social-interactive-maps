<div id='custom-login-form' class="panel panel-default col-md-6 col-lg-4 col-md-offset-8 login-panel-first-page">
    <div class="panel-heading custom-form-title border-bottom">Login</div>

    <div class="panel-body" style="padding:1em;">
        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" style="margin-bottom: 5px;">

                <label for="email" class="col-md-12 custom-login-label control-label">E-Mail Address</label>

                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-12 custom-login-label control-label">Password</label>

                <div class="col-md-12">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input class="add-location-btn" style="border: 0;margin-top: 15px;" type="submit" value="Login">
                </div>
            </div>
            
            @if (config('app.env') !== 'production' || app()->environment() !== 'production')
                <div class="custom-world-link col-md-12" style="text-decoration: none;;">
                    <span style="color: #484848; text-decoration: none;">Don't have an account? </span>
                    <a href="/register" style="color:#247BA0;">Sign up!</a><br />
                    <span style="color: #484848;">or</span>
                </div>
            @endif
            <a href="/guest-view">
                <div class="custom-world-link col-md-12" style="text-decoration: none; margin-top: -2px;">
                    Explore world map
                </div>
            </a>
        </form>
    </div>
</div>
<script>
  var x = location.pathname;
  if(x !== "/login") {
      $("#custom-login-form").hide();
  }
</script>