 @if ($errors->any())
    <div style='margin: 20px 0; border-radius: 5px' class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{!! $error  !!}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('success'))
    <div style='margin: 20px 0; border-radius: 5px' class="alert alert-success">
        <ul>
            <li>{!! session('success') !!}</li>
        </ul>
    </div>
@endif