@extends('layouts.app')

@section('per-page-css')
    <link rel='stylesheet' href='/css/locations.css' />
    <link rel='stylesheet' href='/css/global-profile.css' />

    <link rel="stylesheet" type="text/css" href="/css/slick/slick.css"/>
    // Add the new slick-theme.css if you want the default styling
    <link rel="stylesheet" type="text/css" href="/css/slick/slick-theme.css"/>
    <link rel='stylesheet' href='/css/leaflet/leaflet.css' />
@endsection

@section('map-scripts')
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/css/slick/slick.min.js"></script>
@endsection


@section('content')
    
    <div class="beacon col-xs-12 no-padding">

        <div class="container">

            <div class="beacon-profile-img">
                <img src="/img/beacons/beacon-profile.jpg">
                <div class="beacon-social col-xs-12 no-padding">
                    <div class="beacon-social-link">
                        <i class="far fa-thumbs-up"></i> Like
                    </div>
                    <div class="beacon-social-link">
                        <i class="fas fa-share-alt"></i> Share
                    </div>
                </div>
            </div>

            <div class="beacon-profile-map">
                <img src="/img/beacons/beacon-map.jpg">
                <div id="customLocationMapBeacon">

                </div>
                <div class="beacon-social col-xs-12 no-padding">
                    <div class="beacon-social-link" id='beacon-places'>
                        <i class="fas fa-globe-europe"></i> Places
                    </div>
                    <div class="beacon-social-link" id='beacon-images'>
                        <i class="far fa-images"></i> Images
                    </div>
                    <div class="beacon-social-link" id='beacon-events'>
                        <i class="far fa-calendar-alt"></i> Events
                    </div>
                    <div class="beacon-social-link" id='beacon-community'>
                        <i class="fas fa-user-friends"></i> Community
                    </div>
                </div>
            </div>


            <div class="beacon-main-section beacon-main-places col-xs-12 no-padding">
                
                <div class="beacon-main-section-title col-xs-12">
                    PLACES
                </div>

                <div class="beacon-main-section-places">

                    <div class="beacon-main-li">
                        <img src="/img/beacons/places1.jpg">
                        <div class="beacon-main-section-text">
                            Romer
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/beacons/places2.jpg">
                        <div class="beacon-main-section-text">
                            Palmengarten
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/beacons/places3.jpg">
                        <div class="beacon-main-section-text">
                            Goethe House
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/beacons/places4.jpg">
                        <div class="beacon-main-section-text">
                            Frankfurt Place
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/beacons/places1.jpg">
                        <div class="beacon-main-section-text">
                            Frankfurt Place 2
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/beacons/places1.jpg">
                        <div class="beacon-main-section-text">
                            Frankfurt Place 3
                        </div>
                    </div>

                </div>

            </div>

            <!-- Images -->
            <div class="beacon-main-section beacon-main-images col-xs-12 no-padding">
                
                <div class="beacon-main-section-title col-xs-12">
                    IMAGES
                </div>

                <div class="beacon-main-section-places">

                    <div class="beacon-main-li">
                        <img src="/img/test/test-photo-1.jpeg">
                        <div class="beacon-main-section-text">
                            Diner for two
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/test/test-photo-2.jpeg">
                        <div class="beacon-main-section-text">
                            Grande Restaurant
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/test/test-photo-3.jpeg">
                        <div class="beacon-main-section-text">
                            Goethe House
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/test/test-photo-4.jpeg">
                        <div class="beacon-main-section-text">
                            Vine Taste
                        </div>
                    </div>

                    <div class="beacon-main-li">
                        <img src="/img/test/test-photo-5.jpeg">
                        <div class="beacon-main-section-text">
                            Vine Club
                        </div>
                    </div>


                </div>

            </div>

            <!-- events -->
            <div class="beacon-main-section beacon-main-events col-xs-12 no-padding">
                
                <div class="beacon-main-section-title col-xs-12">
                    EVENTS
                </div>

                <div class="beacon-main-section-places">


                    <div class="beacon-events-li col-xs-12">
                        <div class="beacon-events-li-img">
                            <img src="/img/beacons/events1.png">
                        </div>
                        <div class="beacon-events-li-right">
                            <div class="beacon-events-li-right-title">
                                Frankfurt Main Event
                            </div>
                            <div class="beacon-events-li-right-date">
                                17 Jan, 2019
                            </div>
                            <div class="beacon-events-li-right-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </div>
                        </div>
                    </div>

                    <div class="beacon-events-li col-xs-12">
                        <div class="beacon-events-li-img">
                            <img src="/img/beacons/places1.jpg">
                        </div>
                        <div class="beacon-events-li-right">
                            <div class="beacon-events-li-right-title">
                                Dela Regata
                            </div>
                            <div class="beacon-events-li-right-date">
                                25 March, 2019
                            </div>
                            <div class="beacon-events-li-right-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </div>
                        </div>
                    </div>

                    <div class="beacon-events-li col-xs-12">
                        <div class="beacon-events-li-img">
                            <img src="/img/beacons/events3.jpg">
                        </div>
                        <div class="beacon-events-li-right">
                            <div class="beacon-events-li-right-title">
                                Night at Square
                            </div>
                            <div class="beacon-events-li-right-date">
                                07 Feb, 2019
                            </div>
                            <div class="beacon-events-li-right-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </div>
                        </div>
                    </div>

                    <div class="beacon-events-li col-xs-12">
                        <div class="beacon-events-li-img">
                            <img src="/img/beacons/places2.jpg">
                        </div>
                        <div class="beacon-events-li-right">
                            <div class="beacon-events-li-right-title">
                                Main Park Event
                            </div>
                            <div class="beacon-events-li-right-date">
                                17 March, 2019
                            </div>
                            <div class="beacon-events-li-right-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </div>
                        </div>
                    </div>


                </div>

            </div>


            <!-- Community -->
            <div class="beacon-main-section beacon-main-community col-xs-12 no-padding">
                
                <div class="beacon-main-section-title col-xs-12">
                    COMMUNITY
                </div>

                <div class="beacon-main-section-places">

                    <div class="beacon-events-li col-xs-12">
                        <div class="beacon-events-li-img">
                            <img style="padding-left: 45px;padding-right: 45px;" src="/img/beacons/cicling.png">
                        </div>
                        <div class="beacon-events-li-right" style="margin-top:15px;">
                            <div class="beacon-events-li-right-title">
                                Cycling Community Page
                            </div>
                            <div class="beacon-events-li-right-date">
                                77k people like this
                            </div>
                            <div class="beacon-events-li-right-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </div>
                        </div>
                    </div>

                    <div class="beacon-events-li col-xs-12">
                        <div class="beacon-events-li-img">
                            <img style="padding-left: 45px;padding-right: 45px;" src="/img/beacons/hiking.png">
                        </div>
                        <div class="beacon-events-li-right" style="margin-top:15px;">
                            <div class="beacon-events-li-right-title">
                                Hiking Community Page
                            </div>
                            <div class="beacon-events-li-right-date">
                                177k people like this
                            </div>
                            <div class="beacon-events-li-right-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </div>
                        </div>
                    </div>



                </div>

            </div>



        </div>




    </div>


    <script>

        //filter places
        $('#beacon-places').click(function(){
            console.log('asdsadad')
            $('.beacon-main-images').hide()
            $('.beacon-main-events').hide()
            $('.beacon-main-community').hide()
            $('.beacon-main-places').fadeIn(500)
        })

        //filter images
        $('#beacon-images').click(function(){
            $('.beacon-main-places').hide()
            $('.beacon-main-events').hide()
            $('.beacon-main-community').hide()
            $('.beacon-main-images').fadeIn(500)
        })

        //filter events
        $('#beacon-events').click(function(){
            $('.beacon-main-places').hide()
            $('.beacon-main-images').hide()
            $('.beacon-main-community').hide()
            $('.beacon-main-events').fadeIn(500)
        })

        //filter community
        $('#beacon-community').click(function(){
            $('.beacon-main-places').hide()
            $('.beacon-main-images').hide()
            $('.beacon-main-events').hide()
            $('.beacon-main-community').fadeIn(500)
        })

    </script>

@endsection






