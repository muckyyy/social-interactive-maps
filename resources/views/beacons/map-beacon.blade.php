<div class="map-beacon">

    <div class="beacons-close">
        <i class="fas fa-times"></i>
    </div>
    
    <div class="map-beacon-img">
        <img src="/img/beacons/frankfurt.jpg">
    </div>

    <a href="/b/1">
        <div class="map-beacon-title col-xs-12 no-padding">
            Frankfurt
        </div>
    </a>

    <div class="beacon-places col-xs-12 no-padding">

        <div class="places-title col-xs-12">
            PLACES
        </div>

        <div class="places-images col-xs-12">
            <div class="places-image col-xs-4">
                <img src="/img/beacons/places1.jpg">
            </div>
            <div class="places-image col-xs-4">
                <img src="/img/beacons/places2.jpg">
            </div>
            <div class="places-image col-xs-4">
                <img src="/img/beacons/places3.jpg">
            </div>
        </div>
    </div>
    <br>
    <div class="beacon-places col-xs-12 no-padding" style="margin-top: 30px;">

        <div class="places-title col-xs-12">
            EVENTS
        </div>

        <div class="places-images col-xs-12">
            <div class="places-image col-xs-4">
                <img src="/img/beacons/events1.png">
            </div>
            <div class="places-image col-xs-4">
                <img src="/img/beacons/events2.jpg">
            </div>
            <div class="places-image col-xs-4">
                <img src="/img/beacons/events3.jpg">
            </div>
        </div>

    </div>


    <div class="beacon-places col-xs-12 no-padding" style="margin-top: 30px;">

        <div class="places-title col-xs-12">
            COMMUNITY
        </div>

        <div class="beacons-community col-xs-12 no-padding">

            <div class="beacons-community-li col-xs-12">
                <div class="beacons-community-img">
                    <img src="/img/beacons/cicling.png">
                </div>
                <div class="beacons-community-text">
                    <div class="beacons-community-text-title col-xs-12 no-padding">
                        Cycling
                    </div>
                    <div class="beacons-community-text-text col-xs-12 no-padding">
                        <div class="beacons-community-likes">77k people like this</div>
                        <div class="beacons-community-sub-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                    </div>
                </div>
            </div>

            <div class="beacons-community-li col-xs-12">
                <div class="beacons-community-img">
                    <img src="/img/beacons/hiking.png">
                </div>
                <div class="beacons-community-text">
                    <div class="beacons-community-text-title col-xs-12 no-padding">
                        Hiking
                    </div>
                    <div class="beacons-community-text-text col-xs-12 no-padding">
                        <div class="beacons-community-likes">97k people like this</div>
                        <div class="beacons-community-sub-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>


{{-- <script>
    $('.beacons-close').click(function(){
        $('.map-beacon').fadeOut()
    })
</script> --}}