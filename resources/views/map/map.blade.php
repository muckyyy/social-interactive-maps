<div id='map' class="custom-map"></div>
<form name="locations-form" id="locations-form" action="/map/getmaplocations" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="bounds" id="loc-bounds" value="">
    <input type="hidden" name="zoom-level" id="loc-zoom-level" value="0">
    <input type="hidden" name="search-depth" id="loc-search-depth" value="0">
</form>

