let prevMarker;
let currentLocationId;
let locationMarker;

function showLocationOnMap(id) {
    let location = locations.filter(location => location.id == id);

    let zoom = map.getZoom();

    if (currentLocationId && currentLocationId == id) {
        return;
    }

    map.setView(
        [
            location[0].lat,
            location[0].lng
        ],
        zoom, {
            "animate": true,
            "pan": {
                "duration": 1
            }
        }
    );

    let locationImage = locationMarkers.filter(location => location.options.locationId == id);

    $('img').removeClass("blinkingIcon");

    let locationIcon = locationImage[0].options.icon;
    let width = locationIcon.options.iconSize[0];
    let height = locationIcon.options.iconSize[1];

    if (prevMarker) {
        let prevMarkerIcon = prevMarker[0].options.icon;
        prevMarkerIcon.options.iconSize = [width, height];
        prevMarker[0].setIcon(prevMarkerIcon);
    }

    locationIcon.options.iconSize = [width * 1.4, height * 1.4];

    locationImage[0].setIcon(locationIcon);

    $(locationImage[0]._icon).addClass('blinkingIcon');

    prevMarker = locationImage;
    currentLocationId = id;
}


function printLocations(locations) {
    locations.forEach(function (location) {
        let icon;

        if (location.icon && !location.custom_icon) {
            icon = L.icon({
                iconUrl: location.icon,
                iconSize: [locations_layer.icon_size_x || 90, locations_layer.icon_size_y || 90],

            })
        } else if (location.custom_icon) {
            icon = L.icon({
                iconUrl: location.custom_icon,
                iconSize: [locations_layer.icon_size_x || 90, locations_layer.icon_size_y || 90],

            })
        } else {
            icon = L.AwesomeMarkers.icon({
                icon: location.icon_class,
                prefix: location.icon_prefix || 'fa',
                markerColor: location.icon_color || 'blue',
            })
        }

        locationMarker = L.marker(
            [
                location.lat,
                location.lng
            ], {
                locationId: location.id,
                icon: icon,
                zIndexOffset: location.icon ? 1000 : 0
            }
        ).on('click', function () {
            showLocationInfo(location);
        })

        locationMarkers.addLayer(locationMarker);

        locationMarkers.addTo(map);
    });

    // enable use of font awesome 5
    let icons = $('.awesome-marker i');

    icons.removeClass("fa");
    icons.addClass("fas");
}

function showLocationInfo(location) {
    removeSelectedAvatar();

    $('.map-beacon').fadeOut()
    $('.beacon-info-section').fadeIn(500)
    $('.right-menu-section').removeClass('right-menu-closed')
    $('.right-menu-section').addClass('right-menu-opened')
    $('.right-menu-close-btn').empty()
    $('.right-menu-close-btn').html('<i class="fas fa-chevron-circle-right right-menu-section-btn"></i>')
    $('.right-menu-ul').show()
    $('.right-menu-section-close-modal-btn').show()
    $('.right-menu-ul-layout-title').text(location.name)
    $('#custom-location-link').attr('href', '/l/' + location.id)

    $('#custom-location-id').val(location.id)
    $('#custom-location-form-id').attr('action', '/add-location-images/' + location.id)

    //location rating
    fillLocationRationg(location.rating)
    $('.right-menu-ul-info-address').text(location.address)
    $('.right-menu-ul-info-website').text(location.website)
    $('.right-menu-ul-info-phone').text(location.phone_number)
    // $('.right-menu-ul-info-time').text(location.time)

    let imageHmtl = '';
    $('.right-menu-ul-photos-ul').empty();

    if (location.images) {
        location.images.forEach(function (image) {
            imageHmtl += '<div class="right-menu-ul-photos-li col-xs-6">';
            imageHmtl += '<img src="/img/locations/' + image.image + '">';
            imageHmtl += '</div>'
        })
    }

    $('.right-menu-ul-photos-ul').append(imageHmtl)
}
