require("./map/config");
require("./map/avatars");
require("./map/beacons");
require("./map/commonJquery");
require("./map/homepage-common");
require("./map/initialize");
require("./map/map-search");
require("./map/map-settings");
require("./map/pins");
require("./map/userPins");




