import Vue from 'vue';

// views 
Vue.component('navigation', require('./views/Navigation.vue'));

// core
Vue.component('VueFontawesome', require('vue-fontawesome-icon/VueFontawesome.vue'));
Vue.component('modal', require('./components/core/Modal.vue'));
Vue.component('app-button', require('./components/core/AppButton.vue'));

// components
Vue.component('add-location', require('./components/AddLocation.vue'));
Vue.component('map-post', require('./components/MapPost.vue'));
Vue.component('personal-feed', require('./components/PersonalFeed.vue'));
Vue.component('map-control', require('./components/MapControl.vue'));
Vue.component('user-profile', require('./components/UserProfile.vue'));
Vue.component('online-users', require('./components/OnlineUsers.vue'));
Vue.component('post', require('./components/Post.vue'));
Vue.component('map-search', require('./components/MapSearch.vue'));
Vue.component('map-chat', require('./components/MapChat.vue'));
Vue.component('main-section', require('./components/MainSection.vue'));

