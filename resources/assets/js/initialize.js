var map;
var locationAddMap;
var marker;
let avatarsLoaded = false;
let buffer = 0;
let alowDataRequests = false;
let firstCall = true;
let clickedMapCoordinates;
let addNewLocationMarker;
let addNewLocationCoordinates;

window.onfocus = function () {
    isTabActive = true;
};

window.onblur = function () {
    isTabActive = false;
};

// assigning beacon globals to variables
let y = 54;
let x = 35;
let beaconClicked = false;
let beaconLoading = false;
let beaconRadius
let activeBeacon;
let settings;
let locations_layer;

console.log('OTHER AVATARS SETTINGS', other_avatars_layer);

map_layers.forEach(layer => {
    if (layer.short_name == "settings_layer") {
        settings = layer;
    }

    if (layer.short_name == "friends_avatars_layer") {
        friends_avatars_layer = layer;
        friendsAvatarsEnabled = layer.active;
    }

    if (layer.short_name == "locations_layer") {
        locations_layer = layer;
        locationsEnabled = layer.active;
    }

    if (layer.short_name == "beacons_layer") {
        beacons_layer = layer;
        beaconsEnabled = layer.active;
    }
})

otherAvatarsEnabled = other_avatars_layer.active;

let user_icon_x = settings.icon_size_x;
let user_icon_y = settings.icon_size_y;

let other_avatars_icon_x = other_avatars_layer.icon_size_x;
let other_avatars_icon_y = other_avatars_layer.icon_size_y;

let opacity = 1;

function radiusColor(e) {
    if (e && e == 'fade') {
        let colorInterval = setInterval(() => {
            opacity = opacity -= 0.1;
            if (opacity <= 0) {
                beaconLoading = false;
                return clearInterval(colorInterval);
            }
            beaconRadius.setStyle({
                color: 'rgba(51,136,255,' + opacity + ')'
            });
        }, 150)
    } else {
        opacity = 1;
        beaconRadius.setStyle({
            color: 'rgba(51,136,255,1)'
        });
    }
}


function pulsateRadius(e, maxRadius) {
    let currentRadius = beaconRadius.getRadius();
    if (e == 'up') {
        let radiousInterval = setInterval(() => {
            if (currentRadius >= maxRadius) {
                setTimeout(() => {
                    radiusColor('fade');
                }, 200);
                return clearInterval(radiousInterval);
            };
            beaconRadius.setRadius(currentRadius += 10);
        }, 1)
    }
    if (e == 'down') {
        let radiousInterval = setInterval(() => {
            if (currentRadius <= 0) {
                radiusColor();
                beaconLoading = false;
                return clearInterval(radiousInterval);
            };
            beaconRadius.setRadius(currentRadius -= 10);
        }, 1)
    }
}

function iconInterval(e, beaconActive = false, nextBeacon = null) {
    let iconPath = activeBeacon.options.icon.options;
    let x = iconPath.iconSize[0];
    let y = iconPath.iconSize[1];
    let iconInterval = setInterval(() => {
        let icon = L.Icon.extend({
            options: {
                iconUrl: iconPath.iconUrl,
                iconSize: [x, y],
                iconAnchor: [iconPath.iconAnchor[0], iconPath.iconAnchor[1]]
            }
        });
        activeBeacon.setIcon(new icon);
        if (e == 'up') {
            if (x >= iconPath.iconSize[0] + 30) {
                clearInterval(iconInterval);
                return;
            }
            x += 1;
            y += 1;
        }
        if (e == 'down') {
            if (x <= iconPath.iconSize[0] - 30) {
                clearInterval(iconInterval);
                setTimeout(() => {
                    if (beaconActive == true) {
                        showBeaconDetails(nextBeacon);
                    }
                }, 1500)
                return;
            }
            x -= 1;
            y -= 1;
        }
    }, 30)
}

function initMap() {
    map = L.map('map')

    // location map
    setTimeout(() => {
        locationAddMap = L.map('locationMapId', {
            attributionControl: true
        }).setView([43.34124, 18.32523], 12);
        locationAddMap.on('zoomend drag zoomlevelschange', function () {
            setTimeout(() => {
                centerLocationAddMarker();
            }, 1)
        });

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(locationAddMap);
    }, 2000)

    // test for shared location
    let userSharedLocation = false;
    if (userSharedLocation && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setView([pos.lat, pos.lng], settings.initial_zoom)
        });
    } else {
        map.setView([city.geopoint.coordinates[1], city.geopoint.coordinates[0]], settings.initial_zoom);
    }

    L.tileLayer(host, {
        id: id,
        attribution: attribution
    }).addTo(map);

    personalMarkers.addTo(map);
    load_users();
    setInterval(load_users, 10000);

    let settingMenu = $('#settings-items-menu')

    $('#map-settings').click(function () {
        settingMenu.toggle();
        if (settingMenu.is(":hidden")) {
            settingsMarkers.clearLayers();
        } else {
            //Lets request current data
            axios.get('/map/getsettings/').then((res) => {
                let data = res.data;

                map.setView([data.currentloc[0], data.currentloc[1]]);
                marker_scope = L.marker([data.currentloc[0], data.currentloc[1]], {
                    icon: new L.icon({
                        iconUrl: '/img/scope.png',
                        iconSize: [25, 25]
                    })
                })
                marker_dot = L.circleMarker([data.currentloc[0], data.currentloc[1]], {
                    color: '#F25F5C',
                    fillOpacity: 1,
                    stroke: false,
                    radius: 5
                });
                userReachRadius = L.circle([data.currentloc[0], data.currentloc[1]], {
                    color: '#247BA0',
                    fillOpacity: 0.1,
                    stroke: true,
                    radius: data.reachradius
                });
                userPrivacyRadius = L.circle([data.currentloc[0], data.currentloc[1]], {
                    color: '#F25F5C',
                    fillOpacity: 0.1,
                    stroke: true,
                    radius: data.privacyradius
                });
                settingsMarkers.addLayer(marker_scope);
                settingsMarkers.addLayer(marker_dot);
                settingsMarkers.addLayer(userPrivacyRadius);
                settingsMarkers.addLayer(userReachRadius);

                settingsMarkers.addTo(map);

                $reachslider = $('#reach-slider').slider({
                    min: 1000,
                    max: 10000,
                    value: data.reachradius,
                    slide: function (event, ui) {
                        userReachRadius.setRadius(ui.value);
                        $('#form-reach-radius').val(ui.value);
                        $('#save-radius-settings').prop('disabled', false);
                    }
                });

                $privacyslider = $('#privacy-slider').slider({
                    min: 500,
                    max: 5000,
                    value: data.privacyradius,
                    slide: function (event, ui) {
                        userPrivacyRadius.setRadius(ui.value);
                        $('#form-privacy-radius').val(ui.value);
                        $('#save-radius-settings').prop('disabled', false);
                    }
                });
            })
        }
    });
}

$('#share-location').click(function (e) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (location) {
            axios.post('/map/updatelocation', {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude
            }).then(() => {
                otherAvatarsMarkers.eachLayer(layer => {
                    removeFromMap(layer);
                })

                // map.removeLayer(otherAvatarsMarkers);
                otherAvatarsMarkers.clearLayers();
            })
        });
    } else {
        alert('We cannot locate you');
    }
});
$('#dots-simulation').click(function (e) {
    settingsMarkers.clearLayers();
    axios.post('/map/simulatedots', {
        userid: 1

    }).then((response) => {
        console.log(response);
        user_actual_location = L.marker([response.data.actuallocation[0], response.data.actuallocation[1]], {
            icon: new L.icon({
                iconUrl: '/img/scope.png',
                iconSize: [25, 25]
            })
        });
        user_display_location = L.marker([response.data.displayloc[0], response.data.displayloc[1]], {
            icon: new L.icon({
                iconUrl: '/img/scope.png',
                iconSize: [25, 25]
            })
        });
        user_walkstart_location = L.marker([response.data.walktstart[0], response.data.walktstart[1]], {
            icon: new L.icon({
                iconUrl: '/img/scope.png',
                iconSize: [25, 25]
            })
        });

        settingsMarkers.addLayer(user_actual_location);
        settingsMarkers.addLayer(user_display_location);
        settingsMarkers.addLayer(user_walkstart_location);
    })
});


//Submiting locations form
$('#locations-form').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var url = form.attr('action');
    alllocations.clearLayers();

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
        dataType: 'json',
        success: function (data) {
            $first = true;
            $.each(data.locations, function ($key, $val) {

                if ($first) {
                    $locationicon = L.marker([$val.lat, $val.lng], {
                        icon: new L.icon({
                            iconUrl: $val.icon,
                            iconSize: [25, 25]
                        })
                    });
                    $first = false;
                    $currentq = $val.quadrant;
                } else {
                    if ($currentq != $val.quadrant) {
                        $locationicon = L.marker([$val.lat, $val.lng], {
                            icon: new L.icon({
                                iconUrl: $val.icon,
                                iconSize: [25, 25]
                            })
                        });
                        $currentq = $val.quadrant;
                        console.log($currentq);
                    } else {
                        $locationicon = L.marker([$val.lat, $val.lng], {
                            icon: new L.icon({
                                iconUrl: $val.icon,
                                iconSize: [15, 15]
                            })
                        });
                    }
                }

                alllocations.addLayer($locationicon);
            });
            var $lastcoords = null;
            $.each(data.areas, function ($key, $val) {
                var rectangle = L.rectangle([$val._northEast, $val._southWest], {
                    color: 'red',
                    weight: 1
                }).on('click', function (e) {});
            });
            $first = 0;
            mukilayers.clearLayers();
            $.each(data.quadrants, function ($key, $val) {
                m1 = L.marker($val.top_left, {
                    icon: new L.icon({
                        iconUrl: '/img/scope.png',
                        iconSize: [25, 25]
                    })
                });
                m2 = L.marker($val.top_right, {
                    icon: new L.icon({
                        iconUrl: '/img/scope.png',
                        iconSize: [25, 25]
                    })
                });
                m3 = L.marker($val.bottom_left, {
                    icon: new L.icon({
                        iconUrl: '/img/scope.png',
                        iconSize: [25, 25]
                    })
                });
                m4 = L.marker($val.bottom_right, {
                    icon: new L.icon({
                        iconUrl: '/img/scope.png',
                        iconSize: [25, 25]
                    })
                });

                mukilayers.addLayer(m1);
                mukilayers.addLayer(m2);
                mukilayers.addLayer(m3);
                mukilayers.addLayer(m4);

                var recbonds = [
                    [$val.bottom_left.lat, $val.bottom_left.lng],
                    [$val.top_right.lat, $val.top_right.lng]
                ];
                $middle = L.rectangle(recbonds, {
                    color: "red",
                    weight: 1
                });
                mukilayers.addLayer($middle);
            });
            mukilayers.addTo(map);
            var bounds = [
                [43.64701, -79.39425],
                [43.64533, -79.38235]
            ];
        }
    });

    alllocations.addTo(map);
});

//Updating user location
$('#update-user-real-location').submit(function (e) {

    e.preventDefault();
    var form = $(this);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
        dataType: 'json',
        success: function (data) {
            settingsMarkers.clearLayers();
            map.setView([data.currentloc[0], data.currentloc[1]]);
            marker_scope = L.marker([data.currentloc[0], data.currentloc[1]], {
                icon: new L.icon({
                    iconUrl: '/img/scope.png',
                    iconSize: [25, 25]
                })
            })
            marker_dot = L.circleMarker([data.currentloc[0], data.currentloc[1]], {
                color: '#F25F5C',
                fillOpacity: 1,
                stroke: false,
                radius: 5
            });
            userReachRadius = L.circle([data.currentloc[0], data.currentloc[1]], {
                color: '#247BA0',
                fillOpacity: 0.1,
                stroke: true,
                radius: data.reachradius
            });
            userPrivacyRadius = L.circle([data.currentloc[0], data.currentloc[1]], {
                color: '#F25F5C',
                fillOpacity: 0.1,
                stroke: true,
                radius: data.privacyradius
            });
            settingsMarkers.addLayer(marker_scope);
            settingsMarkers.addLayer(marker_dot);
            settingsMarkers.addLayer(userPrivacyRadius);
            settingsMarkers.addLayer(userReachRadius);
            settingsMarkers.addTo(map);


            $reachslider = $('#reach-slider').slider({
                min: 1000,
                max: 10000,
                value: data.reachradius,
                slide: function (event, ui) {
                    userReachRadius.setRadius(ui.value);
                    $('#form-reach-radius').val(ui.value);
                    $('#save-radius-settings').prop('disabled', false);

                }
            });

            $privacyslider = $('#privacy-slider').slider({
                min: 500,
                max: 5000,
                value: data.privacyradius,
                slide: function (event, ui) {
                    userPrivacyRadius.setRadius(ui.value);
                    $('#form-privacy-radius').val(ui.value);
                    $('#save-radius-settings').prop('disabled', false);
                }
            });
        }
    });
});

//Saving reach and privacy radius
$('#user-radius-settings').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
    });
});


//TODO
function load_users() {
    // TODO
    return; // MEssing up with code, check it

    otherAvatarsMarkers.clearLayers();

    if (map.getZoom() >= 1) {

        //Clear previous icons
        var center = map.getCenter();
        var zoomlvl = map.getZoom();

        //Lets load users
        $.ajax({
            dataType: 'json',
            type: 'GET',
            url: '/map/getavatars/' + center.lat + '/' + center.lng + '/' + zoomlvl,

            success: function (data) {
                console.log('USERS LOADED', data)

                $.each(data, function (k, v) {
                    randomAvatarLoad(k, v)
                });
            }
        });
    }
}

function randomAvatarLoad(avatarData) {
    avatar = L.marker([avatarData.lat, avatarData.long], {
        icon: new L.icon({
            iconUrl: '/img/avatar.png',
            iconSize: [25, 25]
        }),
    });
    otherAvatarsMarkers.addLayer(avatar);
    otherAvatarsMarkers.addTo(map);
}

initMap();

var markerTemp = {};
var marketTempCheck = false;

map.on('contextmenu', function (ev) {
    clickedMapCoordinates = ev.latlng;
});

map.on('click', function (ev) {
    clickedMapCoordinates = ev.latlng;
    removeSelectedAvatar();

    map.eachLayer(function (layer) {
        if (beaconLoading || !beaconClicked) return;

        if (activeBeacon) {
            pulsateRadius('down');
            iconInterval('down');
        }

        beaconClicked = false;
        $('.right-menu-section').fadeIn(500)
        $('.map-beacon').fadeOut();
    });

    $('.user-info-section').fadeOut();

    if (marketTempCheck == true) {
        map.removeLayer(markerTemp);
        var latlngTemp = map.mouseEventToLatLng(ev.originalEvent);
        markerTemp = L.marker([latlngTemp.lat, latlngTemp.lng], {
            icon: new L.icon({
                iconUrl: '/img/icons/pinTemp.png',
                iconSize: [29, 45]
            })
        }).addTo(map);

        var locationNextLink = '/add-location/public/' + latlngTemp.lat + '/' + latlngTemp.lng;
        $('#locationNextLink').attr("href", locationNextLink)
        $('.frendly-cloud-btn').fadeIn(500)
    }
});

function loadLocation(e, location) {
    console.log('location', location)
}

function fillLocationRationg(rating) {
    if (rating == null) {
        rating = 'This place is not rated yet.'
    } else {
        let tempStr = '';
        tempStr += "<span style='margin-right:5px;'>" + rating + "</span>"
        for (let i = 0; i < Math.round(rating); i++) {
            tempStr += '<i class="fas fa-star right-menu-ul-star"></i>'
        }
        for (let i = 0; i < 5 - Math.round(rating); i++) {
            tempStr += '<i class="far fa-star right-menu-ul-star"></i>'
        }

        rating = tempStr
    }
    $('.right-menu-ul-layout-rating').html(rating)
}

$('.leaflet-top').hide();

map.on('zoomend dragend zoomlevelschange load', function () {
    var center = map.getCenter();
    var zoomlvl = map.getZoom();

    var rect = L.rectangle(map.getBounds(), {
        color: 'blue',
        weight: 1
    }).on('click', function (e) {

    });
    var $bounds = map.getBounds();

    marker_dot1 = L.circleMarker([$bounds._southWest.lat, $bounds._southWest.lng], {
        color: 'red',
        fillOpacity: 1,
        stroke: false,
        radius: 10
    });
    marker_dot2 = L.circleMarker([$bounds._southWest.lat, $bounds._northEast.lng], {
        color: 'red',
        fillOpacity: 1,
        stroke: false,
        radius: 10
    });
    marker_dot3 = L.circleMarker([$bounds._northEast.lat, $bounds._northEast.lng], {
        color: 'red',
        fillOpacity: 1,
        stroke: false,
        radius: 10
    });
    marker_dot4 = L.circleMarker([$bounds._northEast.lat, $bounds._southWest.lng], {
        color: 'red',
        fillOpacity: 1,
        stroke: false,
        radius: 10
    });
    $blue_lat = $bounds._northEast.lat;
    $blue_lng = $bounds._southWest.lng;
    marker_dot5 = L.circleMarker([$blue_lat, $blue_lng], {
        color: 'blue',
        fillOpacity: 1,
        stroke: false,
        radius: 20
    });

    var $bounds = JSON.stringify(map.getBounds());

    getData($bounds, zoomlvl);

    if (zoomlvl < other_avatars_layer.min_zoom || zoomlvl > other_avatars_layer.max_zoom) {
        map.removeLayer(otherAvatarsMarkers);
        otherAvatarsMarkers.clearLayers();
        avatarsLoaded = false;
    } else {
        if (!avatarsLoaded) {
            avatarsLoaded = true;
        }
    }

    if (zoomlvl < locations_layer.min_zoom || zoomlvl > locations_layer.max_zoom) {
        map.removeLayer(locationMarkers);
        locationMarkers.clearLayers();
    }
});

// load data on map load
(function () {
    let zoomlvl = map.getZoom();
    let $bounds = map.getBounds();
    getData($bounds, zoomlvl);
}());

function checkAvatarsDuration() {
    otherAvatarsPostPopupMarkers.eachLayer(layer => {
        if ((layer.options.addedAt + (other_avatars_layer.post_popup_duration * 1000)) > Date.now() == false) {
            otherAvatarsPostPopupMarkers.removeLayer(layer);
            map.removeLayer(layer);
        }
    })
}

checkAvatarsDuration();

setInterval(() => {
    checkAvatarsDuration();
}, 5000)

function getLocations(bounds, zoom) {
    if (!locationsEnabled) {
        return;
    }
    axios.get('api/get-locations-data', {
        params: {
            mapBounds: bounds,
            mapZoom: zoom
        }
    }).then(res => {
        clearLocationLayers();
        printLocations(res.data.locations);
        document.getElementById('locationsIndicator').innerText = res.data.locations.length;
    })
}

function getUsers() {
    if (!otherAvatarsEnabled || !friendsAvatarsEnabled) {
        return;
    }

    let max_icons = other_avatars_layer.max_icons;
    let pages = other_avatars_layer.cached_pages;
    let limiter = max_icons * pages;

    axios.get(`api/get-avatars-data/${limiter}`, {
        params: {
            user_id: user.id,
            lat: user.displaylocation.coordinates[1],
            lng: user.displaylocation.coordinates[0],
            radius: user.reach,
            layer: 'other_avatars_layer'
        }
    }).then(res => {
        let otherAvatars = res.data.users;
        let interval = Math.ceil(otherAvatars.length / pages);

        let start = 0;
        let end = other_avatars_layer.max_icons;

        if (initialLoad) {
            printUsers(otherAvatars.slice(start, end), other_avatars_layer);
            document.getElementById('usersIndicator').innerText = otherAvatars.length;
            start += interval;
            end += interval;

            // otherAvatarsMarkers.addTo(map);

            printUsersInterval(otherAvatars, interval, start, end);
        } else {
            printUsersInterval(otherAvatars, interval, start, end);
        }
    })
}

function printUsersInterval(otherAvatars, interval, start, end) {
    let otherAvatarsInterval = setInterval(() => {
        if (!otherAvatarsEnabled || !friendsAvatarsEnabled) {
            clearInterval(otherAvatarsInterval);
            return;
        }

        // clearUserLayers();

        otherAvatarsMarkers.eachLayer(layer => {
            removeFromMap(layer);
        })

        otherAvatarsMarkers.clearLayers();

        printUsers(otherAvatars.slice(start, end), other_avatars_layer);
        document.getElementById('usersIndicator').innerText = otherAvatars.length;
        start += interval;
        end += interval;

        // addToMap(otherAvatarsMarkers);

        if (otherAvatars.length <= end) {
            clearInterval(otherAvatarsInterval);
            getUsers();
        }
    }, (other_avatars_layer.display_duration * 1000))
}

function addToMap(layer) {
    layer.addTo(map);
}

function showPostInPostFeed(user, post) {
    if (rightFeedPosts.includes(post.id)) {
        return;
    }

    rightFeedPosts.push(post.id);

    $('#rightMenuPostFeed').prepend(`
        <div class="notification-section-li col-xs-12">
            <div class="notification-section-top-left col-xs-8 no-padding">
                <a href="p/1">
                    <img class="notification-section-top-img" src="/uploads/${user.profile_img}">
                </a>
            <div class="notification-section-top-left-text">
                    <a href="p/1" style="text-decoration: none; color: grey;">
                        ${user.full_name}
                    </a>
                </div>
            </div>
            <div class="notification-section-top-right col-xs-4 no-padding" style="display: flex; flex-direction: row-reverse; justify-content: flex-end;">
                <span class="mr-1">
                    ${Math.floor((Math.abs(new Date() - new Date(post.created_at)) / 1000) / 60)} 
                </span>
                <span>
                    min ago
                </span>
            </div>
            <div class="notification-section-bottom col-xs-12 no-padding">
                <p class="show-read-more" style="text-align: left; word-break: break-all;">
                    ${post.text}
                </p>
            </div>
        </div>
    `)

    markPostAsSeen(post);
}

function markPostAsSeen(post) {
    console.log('main user::', user, 'post:::', post);
    axios.post(`/api/mark-post-as-seen/${post.id}`).then(res => {
        console.log(res);
    })
}

function getBeacons(bounds, zoom) {
    if (!beaconsEnabled) {
        return;
    }
    axios.get('api/getbeacons', {
        params: {
            mapBounds: bounds,
            mapZoom: zoom
        }
    }).then(res => {
        clearBeaconsLayers();
        printBeacons(res.data);
    })
}

function openCustomAvatarPopups() {
    $('.customAvatar').click();
}

function getData(bounds, zoomlvl) {
    if (firstCall && !alowDataRequests) {
        getLocations(bounds, zoomlvl);

        setTimeout(() => {
            getUsers();
        }, 5000) // 5 seconds to prevent users constantly refreshin page in order to get data faster

        getBeacons(bounds, zoomlvl);
        increaseBuffer();
        firstCall = false;
        openCustomAvatarPopups();
    }

    if (!alowDataRequests) {
        return;
    }

    getLocations(bounds, zoomlvl);
    getBeacons(bounds, zoomlvl);
    increaseBuffer();
}

function clearLocationLayers() {
    map.removeLayer(locationMarkers);
    locationMarkers.clearLayers();
}

function clearUserLayers() {
    map.removeLayer(otherAvatarsMarkers);
    otherAvatarsMarkers.clearLayers();
}

function removeFromMap(layer) {
    map.removeLayer(layer);
}

function increaseBuffer() {
    alowDataRequests = false;
    let bufferInterval = setInterval(() => {
        if (buffer >= 2) {
            buffer = 0;
            alowDataRequests = true;
            return clearInterval(bufferInterval);
        }
        buffer += 1;
    }, 1000)
}
