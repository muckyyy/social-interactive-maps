let users;
let userIcon;
let activeAvatar;
let avatars = [];
let initialLoad = true;
let newUserIcon;
let avatar;
let popup;
let displayedUsers;
let rightFeedPosts = [];
let selectedAvatar;

function printUsers(users, setting = null) {
    users.forEach((u, key, users) => {
        let sizeMultiply;
        let avatarColor;
        let markerIcon;

        if (Object.is(users.length - 1, key)) {
            setTimeout(() => {
                alowDataRequests = true;
                initialLoad = false;
            }, 3000)
        }

        if (u.id == user.id) {
            sizeMultiply = 1.2;
            avatarColor = 'light';
        } else {
            sizeMultiply = 1;
            avatarColor = 'dark';
        }

        if (u.profile_img && u.map_profile_display) {
            newUserIcon = new L.icon({
                iconUrl: '/uploads/' + u.profile_img,
                iconAnchor: [setting.icon_ancor_x, setting.icon_ancor_y],
                iconSize: [setting.icon_size_x * sizeMultiply, setting.icon_size_y * sizeMultiply],
                className: "avatar-map-image",
            });

            markerIcon = newUserIcon;
        } else {
            newUserIcon = new L.icon({
                iconUrl: '/img/icons/avatar_walk_' + avatarColor + '_blue.gif',
                iconSize: [setting.icon_size_x * sizeMultiply, setting.icon_size_y * sizeMultiply],
                className: "avatar_icon",
            });
        }

        let popupAction = `<div class='popupActions'>
                                    <a class='far fa-hand-point-right btn btn-primary'></a>
                                    <a class="far fa-comment btn btn-primary"></a>
                                </div>`;

        let onlineStatus = u.lastSeen;
        let userStatus;

        if (u.post) {
            userStatus = u.post.text;
            newUserIcon.options.className = newUserIcon.options.className + ' customAvatar';
        } else {
            userStatus = '';
        }

        let profileImg;

        if (u.profile_img) {
            profileImg = `./uploads/${u.profile_img}`;
        } else {
            profileImg = '/img/no-image.png';
        }

        let popupContent = `<div class='userPopupContent'>
                                    <div class='d-flex align-items-center mb-2'>
                                        <a onclick='showUserDetails(${u.id})'>
                                            <img class='statusPopupImg' src='${profileImg}'>
                                        </a>
                                        <div class='d-flex flex-direction-column'>
                                            <a onclick='showUserDetails(${u.id})' class='ml-2 truncate' style='color:#000'>
                                                ${u.full_name}
                                            </a>
                                            ${onlineStatus}
                                        </div>    
                                        </div>
                                    ${userStatus}  
                                    ${popupAction} 
                                </div>`;

        popup = L.popup({
            offset: [0, -35],
            autoPan: false
        }).setContent(popupContent);

        avatar = L.marker(
            [
                u.lat, u.lng
            ], {
                icon: newUserIcon,
                userId: u.id,
                addedAt: Date.now(),
                customIcon: u.map_profile_display,
                zIndexOffset: 1000
            }).bindPopup(
            popup, {
                closeOnClick: true,
                autoClose: false
            }).on('click', openUserPopup);

        avatar.on('mouseover', function (ev) {
            hoverAvatar(ev);
        });

        if (u.post) {
            let hasLayer = false;

            otherAvatarsPostPopupMarkers.eachLayer((layer) => {
                if (layer.options.userId == u.id) {
                    hasLayer = true;
                }
            })

            if (!hasLayer) {
                avatar.addTo(map);
                otherAvatarsPostPopupMarkers.addLayer(avatar);
            }
        } else {
            otherAvatarsMarkers.addLayer(avatar);
            avatar.addTo(map);
        }

        if ( /* initialLoad && */ !markerIcon) {
            walkInterval(u, avatar, markerIcon, setting);
        } else {
            if (u.post && !u.post_seen) {
                showPostInPostFeed(u, u.post);
            }
        }
    })
}

function walkInterval(usr, avatar, markerIcon = null, setting) {
    let time = 0;
    let newLatLng;
    let x = 0.0001;
    let status = usr.post;
    let walk_duration = setting.walk_duration * 1000;

    let lat = usr.lat;
    let lng = usr.lng;

    let split = 100;
    let splitTime = 1000 / split;

    let walkLat = ((usr.walkToLat - usr.lat) / setting.walk_duration) / splitTime;
    let walkLng = ((usr.walkToLng - usr.lng) / setting.walk_duration) / splitTime;

    let avatarInterval = setInterval(function () {
        newLatLng = new L.LatLng(lat + walkLat, lng + walkLng);

        time += split;

        lat = lat + walkLat;
        lng = lng + walkLng;

        if (time >= walk_duration) {
            clearInterval(avatarInterval);

            if (status) {
                setUserIcon(usr, avatar, true, markerIcon);
            } else {
                setUserIcon(usr, avatar, false, markerIcon);
            }
        }

        avatar.setLatLng(newLatLng);

    }, split)
}

function setUserIcon(usr, avatarIcon, status = false, markerIcon) {
    let userIcon;
    let color;

    if (avatarIcon.options.userId == user.id) {
        color = 'light';
    } else {
        color = 'dark';
    }

    if (markerIcon) {
        let icon = markerIcon.options;

        userIcon = L.Icon.extend({
            options: {
                iconUrl: icon.iconUrl,
                iconSize: icon.iconSize,
                className: icon.className || null
            }
        });
    } else {
        userIcon = L.Icon.extend({
            options: {
                iconUrl: '/img/icons/avatar_stands_' + color + '_blue.png',
            }
        });
    }

    let icon = avatarIcon.setIcon(new userIcon);
    if (status) {
        icon.openPopup();
        if (!usr.post_seen) {
            showPostInPostFeed(usr, usr.post);
        }
    }
}

function openUserPopup(e) {
    if (e.target.options.userId == selectedAvatar) return;

    removeSelectedAvatar();

    let customIcon = e.target.options.customIcon;

    selectedAvatar = e.target.options.userId;

    let avatarId = e.target.options.userId;

    otherAvatarsMarkers.eachLayer((layer) => {
        if (layer.options.userId == avatarId) {
            otherAvatarsMarkers.removeLayer(layer);
        }
    });

    if (customIcon) {
        return;
    }

    let id = e.target.options.userId;

    let inactiveAvatarIcon, userIcon, size;

    if (id == user.id) {
        inactiveAvatarIcon = L.Icon.extend({
            options: {
                iconUrl: "/img/icons/avatar_stands_dark_blue.png",
                iconSize: [other_avatars_layer.icon_size_x, other_avatars_layer.icon_size_y],
            }
        });

        userIcon = L.Icon.extend({
            options: {
                iconUrl: "/img/icons/avatar_points_light_blue.png",
                iconSize: [other_avatars_layer.icon_size_x * 1.2, other_avatars_layer.icon_size_y * 1.2],
            }
        });
    } else if (activeAvatar && activeAvatar.options.userId == user.id) {
        inactiveAvatarIcon = L.Icon.extend({
            options: {
                iconUrl: "/img/icons/avatar_stands_light_blue.png",
                iconSize: [other_avatars_layer.icon_size_x * 1.2, other_avatars_layer.icon_size_y * 1.2],
            }
        });
        userIcon = L.Icon.extend({
            options: {
                iconUrl: "/img/icons/avatar_points_dark_blue.png",
                iconSize: [other_avatars_layer.icon_size_x, other_avatars_layer.icon_size_y],
            }
        });
    } else {
        inactiveAvatarIcon = L.Icon.extend({
            options: {
                iconUrl: "/img/icons/avatar_stands_dark_blue.png",
                iconSize: [other_avatars_layer.icon_size_x, other_avatars_layer.icon_size_y],
            }
        });
        userIcon = L.Icon.extend({
            options: {
                iconUrl: "/img/icons/avatar_points_dark_blue.png",
                iconSize: [other_avatars_layer.icon_size_x, other_avatars_layer.icon_size_y],
            }
        });
    }

    if (activeAvatar) {
        activeAvatar.setIcon(new inactiveAvatarIcon);
    }

    activeAvatar = e.target;
    e.target.setIcon(new userIcon);
}

function showUserDetails(id) {
    aboutUser(id);
}

function aboutUser(id) {
    axios.get(`api/user-details/${id}`).then(res => {
        let image = res.data.image ? '/uploads/' + res.data.image : '/img/no-image.png';
        let latestPost =
            res.data.lastPost ?
            `
                                <div class="w-100 p-4">
                                    <div class="w-100 border-bottom d-flex align-items-center">
                                        <i class="far fa-comment"></i>
                                        <h4 class="ml-1">Last Post</h4>
                                    </div>
                                    <div class='mt-1'>
                                        ${res.data.lastPost.text}
                                    </div>
                                </div>
                                ` :
            '';

        $('.user-info-section').fadeIn();
        $('#rightMenuUser').html(
            `
                <div class="p-4 d-flex align-items-center">
                    <div>
                        <img class='userImage' src="${image}">
                    </div>
                    <div class="ml-4">
                        <a href="/p/${res.data.user_id}" class="truncate" style='display:inline-block; max-width: 200px' title="${res.data.name}">
                            ${res.data.name}
                        </a>
                        <br>
                            <span class="truncate" style='display:inline-block; max-width: 200px' title="${res.data.city}, ${res.data.country}">
                                <i class="fas fa-map-marker-alt"></i>
                            ${res.data.city}, ${res.data.country}
                        </span>
                            <br>
                            <span class="truncate">
                                <i class="fas fa-history"></i>
                                ${res.data.lastSeen}
                            </span>
                    </div>
                </div>
                <div class="w-100 p-4">
                    <div class="w-100 border-bottom d-flex align-items-center">
                        <i class="far fa-address-card"></i>
                        <h4 class="ml-1">About</h4>
                    </div>
                    <div class='mt-1'>
                        ${res.data.about}
                    </div>
                </div>
                <div class="w-100 p-4">
                    <div class="w-100 border-bottom d-flex align-items-center">
                        <i class=" far fa-handshake"></i>
                        <h4 class="mx-1">Friends</h4> (${res.data.friendsTotal})
                    </div>

                    <div class="col-xs-12">
                        <div style="position: relative;">
                            <div class="popup" id="popup2">
                                <div class="popuptext" id="myPopup">
                                    <span class="close-x">&times;</span>
                                    <div style="position: absolute; top: 0; left: 10px; padding:15px 5px 5px 20px;">
                                        <div style="float: left; padding-top: 3px;">
                                            <a id="popup-profile-href" style="text-decoration:none;">
                                                <img id="popup-img" style="width: 60px; height: 60px;">
                                            </a>
                                        </div>
                                        <div style="float: right; margin-left: 10px;">
                                            <a id="popup-fullname-href" style="text-decoration:none;">
                                                <li id="popup-fullname" style="display: inline-block;">
                                                </li>
                                            </a>
                                            <li id="popup-cityname" style="color: grey; display: inline"><br><i class="fas fa-home" style="padding-right: 8px;"></i>Sarajevo , BIH
                                            </li>
                                            <li style="color: grey; display: inline"><br><i class="fas fa-user" style="margin-left: 24px; padding-right: 10px;"></i>10 mutual friends
                                            </li>
                                        </div>
                                    </div>
                                    <div style="position: absolute; bottom: 20px; left: 30px;">
                                        <button type="button" class="btn btn-primary btn-sm">
                                            <i class="fas fa-user" style="padding-right: 6px;"></i>Follow
                                        </button>
                                        <button type="button" class="btn btn-primary btn-sm" style="margin-left: 10px;">
                                            <i class="fas fa-envelope" style="padding-right: 6px;"></i>Message
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-white main-page-followers-img col-xs-12 scrollbar_overflow d-flex overflow-x-auto" style="padding: 5px 0; transition: transform .2s">
                            ${res.data.friends}
                        </div>
                    </div>


                </div>
                ${latestPost}
                <div class="w-100 p-4" style='margin-top: 40px'>
                    <div class="w-100 d-flex justify-content-center">
                        <button class="btn btn-primary button-primary-color mx-1 py-1 px-2">
                            Friend Request
                </button>
                        <button class="btn btn-primary button-primary-color mx-1 py-1 px-2">
                            <i class="far fa-hand-point-right"></i>
                    Poke
                </button>
                        <button class="btn btn-primary button-primary-color mx-1 py-1 px-2">
                            <i class="far fa-comment"></i>
                    Message
                </button>
                    </div>
                </div>
            `
        );
    })
}

function hoverAvatar(ev) {
    // console.log(ev);
}

// remove selected avatar
function removeSelectedAvatar() {
    if (selectedAvatar) {
        if (selectedAvatar == user.id) {
            selectedAvatar = null;
        } else {
            map.eachLayer(layer => {
                if (layer.options.userId && layer.options.userId == selectedAvatar) {
                    removeFromMap(layer);
                    selectedAvatar = null;
                }
            })
        }
    }
}
