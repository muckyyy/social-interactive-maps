require('./bootstrap');
require('./components')

// jquery
global.jQuery = require('jquery');
var $ = global.jQuery;
window.$ = $;

window.Vue = require('vue');

// swal
import VueSwal from 'vue-swal'
Vue.use(VueSwal)

// moment
Vue.use(require('vue-moment'));

// font awesome
import "font-awesome/css/font-awesome.min.css";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(fas)
Vue.component('font-awesome-icon', FontAwesomeIcon)

export const eventBus = new Vue({});

const app = new Vue({
    el: '#app',
});
