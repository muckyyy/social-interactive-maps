<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

Route::get('/{keyword}', 'Auth\RegisterController@showRegistrationForm');
Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/get_avatars', 'HomeController@getUsers')->name('home.get_avatars');

//JS for map
Route::get('/map/getjs', 'HomeController@getjs');
Route::get('/guest-view', 'HomeController@guestIndex');
Route::get('/map/getcities/{lat}/{long}/{zoomlvl}', 'HomeController@getcities');
Route::get('/getavatars', 'Map\AvatarController@getavatars');
Route::get('/getavatars', 'Map\AvatarController@getavatars');
Route::get('/getUserStatus', 'Map\AvatarController@getUserStatus');

Route::get('/search-city/{countryid}/{query}', 'HomeController@getcitiesregister');
Route::get('/searchCities/{countryid}/{query}', 'AdminController@getCities');
Route::get('/map/getsettings/', 'HomeController@getsettings');
Route::post('/map/updatelocation/', 'HomeController@updatelocation');//Save actual location to database
Route::post('/map/savesettings', 'HomeController@saveSettings'); //Save settings reach/privacy to database
Route::post('/map/getmaplocations', 'HomeController@getMapLocations'); //Get map locations
Route::post('/map/simulatedots/', 'HomeController@simulatedots');
//Profile
Route::get('/profile', 'HomeController@profile');
Route::post('/update-profile', 'HomeController@update_profile');
Route::get('/profile/privacy', 'HomeController@privacy');
Route::get('/global-profile/{id}', 'HomeController@globalProfile');
Route::get('/profile/locations', 'HomeController@userLocations');

//Add images to location
Route::post('/add-location-images/{id}', 'LocationController@addLocationImages');

//Add review to location
Route::get('/add-location-review/{id}', 'LocationController@addLocationReview');
Route::get('/update-location-review/{id}', 'LocationController@updateReview');

//User profile
Route::get('/p/{parameter}', 'UserController@index');

Route::get('/profile/{id}/about', 'UserController@profileAbout');
Route::get('/profile/{id}/friends', 'UserController@profileFriends');
Route::get('/profile/{id}/photos', 'UserController@profilePhotos');
Route::get('/profile/{id}/album/{albumId}', 'UserController@profileAlbum');
Route::get('/profile/{id}/videos', 'UserController@profileVideo');

Route::get('/test-api', 'UserController@testApi');

//Beacons
Route::get('/b/{parameter}', 'LocationController@beacon');


Route::get('/get-beacon-data/{id}', 'HomeController@getBeaconData');

//User posts
Route::post('/create-post', 'UserController@createPost');
Route::get('/toggle-like', 'UserController@toggleLike');
Route::get('/toggle-comment-like', 'UserController@toggleCommentLike');

Route::post('/create-comment/{post_id}/{profile_id}/{parent_id}', 'UserController@createComment');

//Get post likes
Route::get('/get-post-likes/{postId}', 'UserController@getPostLike');

//Post api
Route::get('/get-all-posts/{id}', 'UserController@getAllPosts');
Route::post('/testooo/{postId}/{id}/{commentId}', 'UserController@testooo');

//Pins
Route::post('/create-pin', 'PinController@createPin');
//Create random pin
Route::post('/create-map-post', 'PinController@createMapPost'); //Unused

//Get all pins
Route::get('/get-pins', 'HomeController@getPins');

//Get user friends
Route::get('/get-user-friends', 'HomeController@getUserFriends');
//Get user non-friends
Route::get('/get-user-non-friends', 'HomeController@getUserNonFriends');

//Send friend request
Route::get('/send-friend-request', 'HomeController@sendFriendRequest');

//Get friend request
Route::get('/get-friend-request', 'HomeController@getFriendRequest');

//Friend Request response
Route::get('/friend-request-response', 'HomeController@friendRequestResponse');

//Add location
Route::get('/add-location/{type}/{lat}/{long}', 'LocationController@addLocation');
Route::post('/add-location-post', 'LocationController@addLocationPost');
Route::post('/add-custom-location-fields/{id}', 'LocationController@addCustomLocationFields');

Route::get('/get-child-categories', 'CategoryController@getChildCategories');

//Get icons
Route::get('/get-admin-location-icons', 'LocationController@getAdminLocationIcons');

//Location view
Route::get('/l/{id}', 'LocationController@getLocation');

//Stories
Route::post('/create-story-text', 'UserController@createStoryText');
Route::post('/create-story-img', 'UserController@createStoryImg');

//Admin
Route::post('/add-category', 'CategoryController@addCategory');
Route::post('/delete-category/{id}', 'CategoryController@deleteCategory');
//Adding set of icons
Route::get('/admin-category-icons/{id}', 'CategoryController@adminIconsView');
Route::get('/admin-category-icons-post', 'CategoryController@createAdminIcons');

Route::get('/admin-category/{id}', 'CategoryController@addCategoryCustomFields');
Route::post('/add-custom-field/{id}', 'CategoryController@addCustomField');
// Route::get('/admin-edit-category/{id}', 'CategoryController@editCategory');
Route::get('/admin-active-category/{id}/{status}', 'CategoryController@activeCategory');
Route::post('/delete-custom-field/{id}/{categoryId}', 'CategoryController@deleteCustomField');
Route::get('/admin-change-custom-field/{id}/{categoryId}', 'CategoryController@changeCustomField');
Route::post('/change-custom-field-post/{id}/{categoryId}', 'CategoryController@changeCustomFieldPost');
Route::get('admin-change-categort-basics/{id}', 'CategoryController@changeCategoryBasics');
Route::post('/change-category-basics-post/{id}', 'CategoryController@changeCategoryBasicsPost');

Route::post('/admin-region/save', 'AdminController@regionSave');
Route::get('/admin-region/add', 'AdminController@regionAdd');
Route::get('/admin-region/edit/{id}', 'AdminController@regionEdit');
Route::get('/admin-region/get-regions/{country}', 'AdminController@getRegionsAjax');

Route::get('/admin-country/edit/{id}', 'AdminController@countryEdit');
Route::post('/admin-country/save', 'AdminController@countrySave');
Route::get('/admin-country/add', 'AdminController@countryAdd');
Route::post('upload-admin-icon', 'AdminController@uploadAdminIcon');

Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function () {
    Route::get('activity', 'ActivityLoggerController@index');
    Route::get('/', 'AdminController@admin')->name('dashboard');
    Route::get('avatar_settings', 'AdminController@avatar_settings')->name('avatar_settings');
    // Map Settings
    Route::prefix('map_settings')->group(function () {
        // Registered users map layers
        Route::get('/', 'Admin\MapSettingsController@index')->name('map_settings.index');
        Route::post('/toggle_active/{id}', 'Admin\MapSettingsController@toggle_active')->name('map_settings.toggle_active');
        Route::post('/toggle_layer/{id}', 'Admin\MapSettingsController@toggle_layer')->name('map_settings.toggle_active');
        Route::get('/settings_layer', 'Admin\MapSettingsController@settings_layer')->name('map_settings.settings_layer');
        Route::post('/settings_layer', 'Admin\MapSettingsController@store_settings_layer')->name('map_settings.settings_layer.store');
        Route::get('/friends_avatars_layer', 'Admin\MapSettingsController@friends_avatars_layer')->name('friends_avatars_layer');
        Route::post('/friends_avatars_layer', 'Admin\MapSettingsController@store_friends_avatars_layer')->name('map_settings.friends_layer.store');
        Route::get('/other_avatars_layer', 'Admin\MapSettingsController@other_avatars_layer')->name('other_avatars_layer');
        Route::post('/other_avatars_layer', 'Admin\MapSettingsController@store_other_avatars_layer')->name('map_settings.other_avatars_layer.store');
        Route::get('/locations_layer', 'Admin\MapSettingsController@locations_layer')->name('map_settings.locations_layer');
        Route::post('/locations_layer', 'Admin\MapSettingsController@store_locations_layer')->name('map_settings.locations_layer.store');
        Route::get('/beacons_layer', 'Admin\MapSettingsController@beacon_layer')->name('map_settings.beacon_layer');
        Route::post('/beacons_layer', 'Admin\MapSettingsController@store_beacon_layer')->name('map_settings.beacon_layer.store');

        // Guest User Map Layers
        Route::prefix('guest_map_layers')->group(function () {
            Route::get('/', 'Admin\MapSettingsController@guest_map_layers')->name('guest_layer');
            Route::get('/guest_avatars_layer', 'Admin\MapSettingsController@avatars_layer')->name('guest_avatars_layer');
            Route::get('/guest_locations_layer', 'Admin\MapSettingsController@guest_locations_layer')->name('guest_locations_layer');
        });
        // Other Settings
        Route::get('/other_settings', 'Admin\MapSettingsController@other_settings')->name('other_settings');
    });
    // beacon administration
    Route::prefix('beacon_administration')->group(function () {
        Route::get('/', 'Admin\BeaconController@index')->name('beacon_administration.index');
        Route::get('/create', 'Admin\BeaconController@create')->name('beacon_administration.create');
        Route::get('/edit/{id}', 'Admin\BeaconController@edit')->name('beacon_administration.edit');
        Route::post('/update/{id}', 'Admin\BeaconController@update')->name('beacon_administration.update');
        Route::post('/store', 'Admin\BeaconController@store')->name('beacon_administration.store');
        Route::post('/delete/{id}', 'Admin\BeaconController@destroy')->name('beacon_administration.destroy');
        Route::get('/local_beacons', 'Admin\BeaconController@local_beacons')->name('local_beacons');
        Route::get('/default_settings', 'Admin\BeaconController@default_settings')->name('beacon_default_settings');
    });

    Route::prefix('locations')->group(function () {
        Route::get('/', 'Admin\LocationsController@index')->name('locations_management.index');
        Route::get('/create', 'Admin\LocationsController@create')->name('locations_management.create');
        Route::get('/edit/{id}', 'Admin\LocationsController@edit')->name('locations_management.edit');
        Route::post('/update/{id}', 'Admin\LocationsController@update')->name('locations_management.update');
        Route::post('/store', 'Admin\LocationsController@store')->name('locations_management.store');
        Route::post('/delete/{id}', 'Admin\LocationsController@destroy')->name('locations_management.destroy');
    });

    Route::get('lvl-icons', 'AdminController@adminLvlIcons')->name('lvl-icons');

    Route::prefix('tile-server-management')->group(function () {
        Route::get('/', 'AdminController@tile_server_management')->name('tile-server-management');
        Route::get('/tile_servers_add', 'AdminController@tile_servers_add')->name('tile_servers_add');
    });
    Route::prefix('category_management')->group(function () {
        // category list
        Route::get('/', 'Admin\CategoryManagementController@category_management')->name('category_management');
        Route::get('/category_add', 'Admin\CategoryManagementController@category_add')->name('category_add');
        Route::get('/category_add_continue/{id}', 'Admin\CategoryManagementController@category_add_continue')->name('category_add_continue');

        Route::post('/add-location-field-type/{id}', 'Admin\CategoryManagementController@add_location_field_type');
        Route::get('/add_location_field_type/{id}', 'Admin\CategoryManagementController@add_location_field_type')->name('category.location_field.details');

        Route::post('/add-category', 'Admin\CategoryManagementController@addCategory')->name('category.add');
        Route::post('/add-category-groups', 'Admin\CategoryManagementController@addCategoryToGroups')->name('category.add_groups');

        Route::get('/category_icons/{id}', 'CategoryController@adminIconsView')->name('category_icons');
        Route::get('/category_edit/{id}', 'CategoryController@editCategory')->name('category_edit');
        Route::get('/change_category_basics/{id}', 'CategoryController@changeCategoryBasics')->name('category_basics_edit');
        Route::get('/change_custom_field/{id}/{categoryId}', 'CategoryController@changeCustomField')->name('category.change_custom_field');
        Route::get('/admin-edit-category/{id}', 'CategoryController@editCategory')->name('category.admin_edit');
        ;
        Route::post('/add-location-field-type/{id}', 'Admin\CategoryManagementController@add_location_field_type');
        Route::post('/location_field_details/{id}', 'Admin\CategoryManagementController@location_field_details');

        // location fields groups
        Route::prefix('location_fields_groups')->group(function () {
            Route::get('/', 'Admin\LocationFieldGroupsController@index')->name('location_fields_groups.index');
            Route::get('/edit/{id}', 'Admin\LocationFieldGroupsController@edit')->name('location_fields_groups.edit');
            Route::post('/update/{id}', 'Admin\LocationFieldGroupsController@update')->name('location_fields_groups.update');
            Route::get('/create', 'Admin\LocationFieldGroupsController@create_location_fields_groups')->name('location_fields_groups.create');
            Route::post('/create', 'Admin\LocationFieldGroupsController@create_new_location_field_group')->name('location_fields_groups.store');
            Route::post('/reorder/{id}', 'Admin\LocationFieldGroupsController@change_ordering')->name('location_fields_groups.reorder');
            Route::post('/change_visibility/{id}', 'Admin\LocationFieldGroupsController@change_visibility')->name('location_fields_groups.toggle_visible');
            Route::post('/delete/{id}', 'Admin\LocationFieldGroupsController@delete_location_field_group')->name('location_fields_groups.delete');
        });
        // location fields
        Route::prefix('locations_fields')->group(function () {
            Route::get('/', 'Admin\LocationFieldsController@index')->name('location_fields.index');
            Route::get('/create', 'Admin\LocationFieldsController@create')->name('location_fields.create');
            Route::get('/create_continue', 'Admin\LocationFieldsController@create_continue')->name('location_fields.create_continue');
            Route::post('/store', 'Admin\LocationFieldsController@store')->name('location_fields.store');
            Route::post('/change_visibility/{id}', 'Admin\LocationFieldsController@change_visibility')->name('location_fields.toggle_visible');
            Route::post('/delete/{id}', 'Admin\LocationFieldsController@delete')->name('location_fields.delete');
            Route::get('/edit/{id}', 'Admin\LocationFieldsController@edit')->name('location_fields.edit');
            Route::put('/update/{id}', 'Admin\LocationFieldsController@update')->name('location_fields.update');
        });
    });
    Route::prefix('users')->group(function () {
        Route::get('/', 'DashboardUsersController@index')->name('dashboard.users');
        Route::get('/create', 'DashboardUsersController@create')->name('dashboard.users.create');
        Route::post('/create_user', 'DashboardUsersController@create_user')->name('dashboard.users.create');
        Route::get('/registered_invitations', 'DashboardUsersController@registered_invitations')->name('dashboard.users.registered_invitations');

        Route::prefix('roles')->group(function () {
            Route::get('/', 'DashboardUsersController@roles')->name('dashboard.users.roles.index');
            Route::get('/add', 'DashboardUsersController@roles_add')->name('dashboard.users.roles.add');
            Route::get('/edit/{id}', 'DashboardUsersController@roles_edit')->name('dashboard.users.roles.edit');
            Route::put('/edit/{id}', 'DashboardUsersController@roles_update')->name('dashboard.users.roles.update');
            Route::delete('/delete/{id}', 'DashboardUsersController@roles_delete')->name('dashboard.users.roles.delete');
            Route::post('/add', 'DashboardUsersController@roles_create')->name('dashboard.users.roles.store');
            Route::get('/add_users', 'DashboardUsersController@roles_add_users')->name('dashboard.users.roles.add_users');
            Route::post('/add_users', 'DashboardUsersController@roles_post_add_users')->name('dashboard.users.roles.add_users');
            Route::post('/remove_users', 'DashboardUsersController@roles_remove_users')->name('dashboard.users.roles.remove_users');
        });
        Route::prefix('auth_methods')->group(function () {
            Route::get('/', 'DashboardUsersController@auth_methods')->name('dashboard.users.auth_methods');
            Route::get('/add_service', 'DashboardUsersController@add_service')->name('dashboard.users.auth_methods.add_service');
        });
        Route::get('/sanctioned_users', 'DashboardUsersController@sanctioned_users')->name('dashboard.users.sanctioned_users');
        Route::patch('/sanctioned_users/{id}/unsuspend', 'DashboardUsersController@unsuspend_user')->name('dashboard.users.sanctioned_users.unsuspend_user');
    });
});

Route::get('/test', 'PinController@test');
Route::get('/send-email', 'UserController@sendEmail');
Route::post('/contanct-email', 'UserController@contactEmailSend');
Route::get('/elevator-email/{name}/{surname}/{email}/{message}', 'UserController@elevatorEmailSend');

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    return redirect()->back()->withSuccess('<h1>Cache facade value cleared</h1>');
});

// Socialite
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');



Route::post('/profile-photo-update', 'UserController@photoUpdate');

Route::post('/background-image-update/{id}', 'UserController@backgroundImageUpdate');

Route::get('crop-image', 'ImageCropController@index');
Route::post('crop-image', ['as'=>'croppie.upload-image','uses'=>'ImageCropController@imageCrop']);
Route::post('/write-review/{id}', 'LocationController@userReview');

Auth::routes();

// Routes from first page
Route::get('/about-us', 'ShowPagesController@aboutUsPage');
Route::get('/help', 'ShowPagesController@helpPage');
Route::get('/terms', 'ShowPagesController@termsPage');
Route::get('/privacy', 'ShowPagesController@privacyPage');
Route::get('/sitemap', 'ShowPagesController@sitemapPage');
Route::get('/contact-us', 'ShowPagesController@contactUsPage');


// Routes to be moved to api.php
Route::group(['middleware' => 'auth', 'prefix' => 'api'], function () {
    Route::get('users', 'DashboardUsersController@index');

    Route::get('peopleToFollow', 'HomeController@getPeopleToFollow');

    Route::get('get-map-layers', 'MapLayersController@index')->name('map_layers.index');
    
    Route::get('user-details/{id}', 'UserController@getUserDetails')->name('user.details');
    
    Route::get('user-basic-details/{user}', 'UserController@getUserBasicInfo')->name('user.basic-details');
    
    Route::get('cities/{keyword}', 'Auth\RegisterController@showRegistrationForm');
    
    Route::get('getbeacons', 'Map\BeaconController@index');
    
    Route::get('check-location-duplicates', 'Admin\LocationsController@checkDuplicates');
    
    Route::get('search-categories', 'Admin\CategoryManagementController@searchCategories');
    
    Route::get('search-cities', 'Admin\LocationsController@searchCities');
    
    Route::get('search-locations', 'Admin\LocationsController@searchLocations');
    
    Route::get('search-locations-profile', 'Admin\LocationsController@searchLocationsProfile');

    Route::delete('remove-location-icon/{location}', 'Admin\LocationsController@deleteIcon')->name('post.like');

    Route::get('map/get-categories', 'LocationController@getCategories')->name('map.get-categories');

    Route::post('map/add-location', 'LocationController@mapAddLocation')->name('map.add-location');
    
    Route::post('like-post/{id}', 'LikePostController@likePost')->name('post.like');
    
    Route::post('share-post/{post}', 'PostController@sharePost')->name('post.share');

    Route::post('like-comment/{comment}', 'PostController@likeComment')->name('post.likeComment');

    Route::get('search-friends/{keyword}', 'SearchFriendsController')->name('friends.search');
    
    Route::get('get-locations-data', 'LocationsDataController');
    
    Route::get('get-avatars-data/{limiter}', 'AvatarsDataController');

    Route::get('main-avatar-data', 'MainAvatarController');
    
    Route::post('/map_settings/toggle_profile_picture/{user}', 'ToggleMapProfileController')->name('map_settings.toggle_profile_picture');
    
    Route::post('post-comment/{post}', 'PostController@comment');

    Route::post('mark-post-as-seen/{post}', 'MapPostViewController');

    Route::post('send-follow-request', 'FollowersController@create')->name('follow.create');

    Route::delete('send-follow-request/destroy', 'FollowersController@destroy')->name('follow.destroy');

    Route::post('/map_settings/toggle_active/{id}', 'Admin\MapSettingsController@toggle_active');

    Route::get('online-users', 'OnlineUsersController');
});
